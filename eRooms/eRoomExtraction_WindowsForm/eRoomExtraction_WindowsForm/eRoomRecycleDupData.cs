﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eRoomExtraction_WindowsForm
{
    class eRoomRecycleDupData
    {
        public eRoomRecycleDupData(string eRoomID, int recycleNum, int duplicateNum)
        {
            eRoomName = eRoomID;
            recycle = recycleNum;
            duplicate = duplicateNum;
        }

        public string eRoomName { get; internal set; }
        public int recycle { get; internal set; }
        public int duplicate { get; internal set; }
    }
}
