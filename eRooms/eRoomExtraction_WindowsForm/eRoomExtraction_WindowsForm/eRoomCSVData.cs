﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eRoomExtraction_WindowsForm
{
    public class eRoomCSVData
    {
        public eRoomCSVData(string rootProjFolder)
        {
            csvSource = localSourcePath(rootProjFolder);
            csvCol2 = "";
            csvCol3 = "";
            csvDest = "https://ruekertmielke2016.sharepoint.com/sites/8083-GreenfieldWICityof/";
            csvDestProj = "General Records";
        }

        public string csvSource { get; internal set; }
        public string csvCol2 { get; internal set; }
        public string csvCol3 { get; internal set; }
        public string csvDest { get; internal set; }
        public string csvDestProj { get; internal set; }


        string properDestFormat(string proj)
        {
            if(proj.Length > 14)
            {
                proj = proj.Remove(0, 14);
                proj = proj.Trim();
                if (!proj.Contains("-") && proj.Length > 1)
                {
                    proj = proj.Insert(2, "-");
                }
            }
            return proj;
        }

        string localSourcePath(string root)
        {
            string serverPath = "\\" + "\\PDS";
            string path = "\\E$\\s\\";
            path = path.Replace(@"\\", @"\");
            path = serverPath + path;
            root = root.Trim();
            path = path.Trim();
            return path + root;
        }
    }
}
