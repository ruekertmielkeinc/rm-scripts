﻿
namespace eRoomExtraction_WindowsForm
{
    public class eRoom
    {
        public eRoom(string guid, string eDBName, string eIDName, string dName)
        {
            eRoomGUID = guid;
            eRoomDBName = eDBName;
            eRoomIDName = eIDName.Trim();
            dbName = dName;
        }
        public string eRoomGUID { get; internal set; }
        public string eRoomDBName { get; internal set; }
        public string eRoomIDName { get; internal set; }
        public string dbName { get; internal set; }

        public override string ToString()
        {
            return eRoomIDName;
        }

    }
}