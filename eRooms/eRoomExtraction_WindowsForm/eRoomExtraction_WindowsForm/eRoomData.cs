﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using CsvHelper;
using CsvHelper.Configuration;



namespace eRoomExtraction_WindowsForm
{
    internal class eRoomData
    {
        
        //*********************************************************************//
        //********************** LISTS USED IN FORM FUNCTIONS******************//
        //Master List of all eRooms
        IDictionary<string, eRoom> eRoomMasterList = new Dictionary<string, eRoom>();

        //Search Result list
        IDictionary<string, eRoom> searchResultList = new Dictionary<string, eRoom>();

        //Search ListBox list
        IDictionary<string, eRoom> searchListBoxList = new Dictionary<string, eRoom>();

        //Selected results from search to be sent to queue
        IDictionary<string, eRoom> selectedFromSearchList = new Dictionary<string, eRoom>();

        //Queue List
        public IDictionary<string, eRoom> QueueList = new Dictionary<string, eRoom>();
        //**********************************************************************//

        //****************************************************//
        //******** Populate Master eRoom List Methods*********//
        //get master eRoom list data from CSV file
        public void MasterCSVData()
        {
            List<string> databaseList = new List<string>();
            databaseList = GetListofDatabases();
            WriteListToCSV(databaseList);
        }

        //Query the server for a list of all eRoom databases
        //ACCEPTS no arguments
        //RETURNS a list of strings of all eRoom databases
        public List<string> GetListofDatabases()
        {
            List<string> databasesOnServer = new List<string>();
            string connectionString = "server=PDS\\SQLEXPRESS02; integrated Security=SSPI;";    //Database connection info

            string sql = @"SELECT name
                                FROM sys.databases
                                WHERE name LIKE '%eRoom%'";

            SqlConnection conn = new SqlConnection(connectionString);       //Establish connection to database
            SqlCommand command = new SqlCommand(sql, conn);                 //Perform SQL query on connected database

            using (conn)
            {
                conn.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())            //Returns true if more lines to read, false if no more lines
                    {
                        string databaseName = "";
                        databaseName = Convert.ToString(reader["name"]);
                        databasesOnServer.Add(databaseName);
                    }
                }
                conn.Close();
            }
            return databasesOnServer;
        }

        //Loop through each database in list and pass to getData method
        public void WriteListToCSV(List<string> dbList)
        {
            foreach (string db in dbList)
            {
                GeteRoomDataFromDB(db);
            }
        }

        //Get data from each eRoom database in server
        public void GeteRoomDataFromDB(string db)
        {
            string eRoomGUID = "";
            string eRoomDBName = "";
            string eRoomIDName = "";

            string connectionString = "server=PDS\\SQLEXPRESS02; Initial Catalog=" + db + "; integrated Security=SSPI;";    //Database connection info

            string sql = @"SELECT e.eRoomGUID, o.Name AS dbName, r.Name AS idName
                                FROM dbo.eRooms e
                                INNER JOIN dbo.Objects o ON e.eRoomInternalId = o.InternalId
                                INNER JOIN dbo.Objects r ON e.RootDocInternalId = r.InternalId";

            SqlConnection conn = new SqlConnection(connectionString);       //Establish connection to database
            SqlCommand command = new SqlCommand(sql, conn);                 //Perform SQL query on connected database

            using (conn)
            {
                conn.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())            //Returns true if more lines to read, false if no more lines
                    {
                        eRoomGUID = CleanSQLStrings(Convert.ToString(reader["eRoomGUID"]));
                        eRoomDBName = CleanSQLStrings(Convert.ToString(reader["dbName"]));
                        eRoomIDName = CleanSQLStrings(Convert.ToString(reader["idName"]));

                        eRoom eroom = new eRoom(eRoomGUID, eRoomDBName, eRoomIDName, db);
                        if(eRoomMasterList.ContainsKey(eRoomGUID))
                        {
                            continue;
                        }
                        else
                        {
                            eRoomMasterList.Add(eRoomGUID, eroom);
                        }
                    }
                }
                conn.Close();
            }
        }

        //Remove unnecessary characters from string values
        public string CleanSQLStrings(string value)
        {
            value = value.Replace("\0", "");
            return value;
        }
        //****************************************************//

        //Search eRoom list using user input in search box
        public void eRoomSearchResultsPopulateList(string searchString)
        {
            //Clear search result dictionary
            searchResultList.Clear();

            //loop through eRoom list and search for eRoom that user input
            //search is case insensitive
            foreach (KeyValuePair<string, eRoom> eRoom in eRoomMasterList)
            {
                //if found, add to found list
                if (eRoom.Value.eRoomIDName.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) != -1)
                {
                    searchResultList.Add(eRoom);
                }
            }
        }

        //check if any results in search result list already exist in searchListBoxList
        public void FilterSearchResultsList()
        {
            foreach (KeyValuePair<string, eRoom> kvp in searchListBoxList)
            {
                if (searchResultList.ContainsKey(kvp.Key))
                {
                    searchResultList.Remove(kvp.Key);
                }
            }
        }

        //Handle a search that yields no results
        public void CheckSearchResultsForZeroAndFilter()
        {
            if (!searchResultList.Any())
            {
                //display if search yields no results
                string noResultsFound = "No Results Found";
                string caption = "Zero search results";

                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;

                //Display message box
                result = MessageBox.Show(noResultsFound, caption, buttons);
            }
            else
            {
                FilterSearchResultsList();
            }
        }

        //Populate listbox with eRooms returned from search results method
        public void PopulateSearchGUI(ListBox listBox)
        {
            //list box settings
            listBox.Sorted = true;
            listBox.SelectionMode = SelectionMode.MultiSimple;

            //Add all found items to the list box
            //Add filtered items to searchListBoxList for future filtering
            foreach (KeyValuePair<string, eRoom> kvp in searchResultList)
            {
                listBox.Items.Add(kvp);
                searchListBoxList.Add(kvp);
            }
        }

        //Clear values from searchListBox Dictionary list
        public void ClearSearchGUI()
        {
            searchListBoxList.Clear();
        }

        //populate queue listbox with items selected from search list box
        public void PopulateQueueListWithSelected(ListBox searchBox)
        {
            foreach (KeyValuePair<string, eRoom> kvp in searchBox.SelectedItems)
            {
                if (!selectedFromSearchList.ContainsKey(kvp.Key))
                {
                    selectedFromSearchList.Add(kvp);
                }
            }

        }

        //Automatically select all eRooms from search list and add to queue list
        public void PopulateQueueListSelectAll(ListBox searchBox)
        {
            //set all items in search results box to selected
            for (int i = 0; i < searchBox.Items.Count; i++)
            {
                searchBox.SetSelected(i, true);
            }

            //Add all selected items to queue list
            foreach (KeyValuePair<string, eRoom> kvp in searchBox.SelectedItems)
            {
                selectedFromSearchList.Add(kvp);
            }
        }

        //Populate queue box with selected items or all items
        public void PopulateQueueGUI(ListBox queueBox)
        {

            //list box settings
            queueBox.Sorted = true;
            queueBox.Enabled = false;

            //Add all found items to the list box
            foreach (KeyValuePair<string, eRoom> kvp in selectedFromSearchList)
            {
                if (!QueueList.ContainsKey(kvp.Key))
                {
                    queueBox.Items.Add(kvp);
                    QueueList.Add(kvp);
                }
            }
            selectedFromSearchList.Clear();
            searchListBoxList.Clear();
        }

        //Clear queue list
        public void ClearQueueList()
        {
            QueueList.Clear();
        }

        //Open file explorer and allow users to select csv file for input
        public void OpenFileDialogForm(TextBox textBox)
        {
            OpenFileDialog searchCSV = new OpenFileDialog()
            {
                InitialDirectory = "c:\\",
                RestoreDirectory = true,
                Title = "Browse CSV files",
                DefaultExt = "csv",
                Filter = "csv files (*.csv)|*.csv",
                ReadOnlyChecked = true
            };

            if (searchCSV.ShowDialog() == DialogResult.OK)
            {
                textBox.Text = searchCSV.FileName;
            }
        }

        //Get Files from Csv input and add to queue list if not already in queue list
        public void FilecsvDataToSearchList(String csvPath)
        {
            //clear search result list
            searchResultList.Clear();

            //Proceed only if csv file exists
            if (File.Exists(csvPath))
            {
                var csvConfig = new CsvConfiguration(CultureInfo.CurrentCulture)
                {
                    HasHeaderRecord = false,
                };

                var streamReader = File.OpenText(csvPath);
                var csvReader = new CsvReader(streamReader, csvConfig);

                while (csvReader.Read())
                {
                    var GUID = csvReader.GetField(0);
                    var eRoomDBName = csvReader.GetField(1);
                    var eRoomIDName = csvReader.GetField(2);
                    var dbName = csvReader.GetField(3);

                    eRoom newRoom = new eRoom(GUID, eRoomDBName, eRoomIDName, dbName);

                    searchResultList.Add(GUID, newRoom);
                }
            }
            else
            {
                DisplayNonExistCSVErrorMessage();
            }
        }

        //Message box for invalid input error
        public void DisplayNonExistCSVErrorMessage()
        {
            string errorMessage = "CSV file does not exist.  Please input a valid file with the proper .csv extension.";
            string caption = "Error detected";

            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result;

            //Display message box
            result = MessageBox.Show(errorMessage, caption, buttons);
        }

        //Message box for invalid format error
        public void DisplayInvalidCSVErrorMessage()
        {
            string errorMessage = "Invalid CSV file format. File must have 3 columns:  GUID, eRoom Name, database name. " +
                "No header row accepted.";
            string caption = "Error detected";

            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result;

            //Display message box
            result = MessageBox.Show(errorMessage, caption, buttons);
        }

        //Select source or destination path from open dialog window
        public void FindPathFileDialogForm(TextBox textBox)
        {
            FolderBrowserDialog searchFolder = new FolderBrowserDialog()
            {
                ShowNewFolderButton = true
            };

            DialogResult result = searchFolder.ShowDialog();
            if (result == DialogResult.OK)
            {
                textBox.Text = searchFolder.SelectedPath;
            }
        }
        
    }
}