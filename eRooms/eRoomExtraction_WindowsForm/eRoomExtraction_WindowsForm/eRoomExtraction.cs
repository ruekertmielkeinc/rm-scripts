﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using CsvHelper;
using CsvHelper.Configuration;



/*This class handles extracting all data out of each individual eRoom.  To start, each eRoom that is selected a user, along with their chosen
 * source path, and chosen destination path is sent to the "extractData" method. This method is sort of the "Main" method for this class.  The data
 * from each eRoom is extracted and used to build the folder structure in File Explorer needed to mirror the same folder structure as it exists in eRooms.
 * 
 * After each source and destination path is built, the source file is copied from source into its given destination path. 
 * 
 * UNMANAGED FILES -> File Explorer has a limit on how long a file path can be.  That limit is around 256 characters.  Any file path from eRooms that is 
 * longer than 256 is added to a CSV file where it will need to handled manually.
 */

namespace eRoomExtraction_WindowsForm
{
    class eRoomExtraction
    {
        //Master list for all files that have a destination path but the destination path.key value did 
        //not have a matching value in the sourceKeyandPath list
        List<UnmanagedFileData> unMatchedNoSourceMasterList = new List<UnmanagedFileData>();

        List<UnmanagedFileData> UnmanagedFilePaths = new List<UnmanagedFileData>();

        int extraFileCountDuplicates = 0;
        int extraFileCountRecycleBin = 0;
        int duplicateFileKeyCount = 0;

        //Primary method for processing eRooms input by user
        public List<UnmanagedFileData> extractData(KeyValuePair<string, eRoom> eData, string rootDestinationFilePath, string sourceFilePath)
        {
            List<FileData> allFileData;
            Dictionary<string, string> allDestinationPaths;
            Dictionary<string, string> allSourcePaths;
            List<UnmanagedFileData> UnmanagedFilePaths;

            string eRoomIDNum = query_eRoomIDNum(eData);
            eRoomCSVData eRoomCSV = new eRoomCSVData(eData.Value.eRoomIDName);
            HandleeRoomCSVData(eRoomCSV);
            allFileData = query_eRoomData(eRoomIDNum, eData);
            allDestinationPaths = BuildAllDestinationFilePaths(allFileData, rootDestinationFilePath, eData);
            allSourcePaths = BuildAllSourceFilePaths(sourceFilePath, eData);
            UnmanagedFilePaths = copySourceToDestination(allDestinationPaths, allSourcePaths, eData);

            //Handle number of files that were in the eRooms recycle bin
            //This number will help us clear up the discrepancy between source file number and post-extracted file number
            eRoomRecycleDupData eRoomRecDup = new eRoomRecycleDupData(eData.Value.eRoomIDName, extraFileCountRecycleBin, extraFileCountDuplicates);
            HandleRecycleBinAndDuplicateNumbers(eRoomRecDup);

            return UnmanagedFilePaths;
        }

        //Get eRoom ID number to use for data extraction
        public string query_eRoomIDNum(KeyValuePair<string, eRoom> eData)
        {
            string eRoomIDNum = "";
            string dbName = eData.Value.dbName;
            string GUID = eData.Key;
            string connectionString = "server=PDS\\SQLEXPRESS02; Initial Catalog=" + dbName + "; integrated Security=SSPI;";    //Database connection info

            string sql = @"SELECT eRoomInternalId
                    FROM dbo.eRooms
                    WHERE (CAST(eRoomGUID AS uniqueidentifier) = CAST('" + GUID + "' AS uniqueidentifier))";

            SqlConnection conn = new SqlConnection(connectionString);       //Establish connection to database
            SqlCommand command = new SqlCommand(sql, conn);                 //Perform SQL query on connected database

            using (conn)
            {
                conn.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())            //Returns true if more lines to read, false if no more lines
                    {
                        eRoomIDNum = Convert.ToString(reader["eRoomInternalId"]);
                    }
                }
                conn.Close();
            }
            return eRoomIDNum;
        }

        //Accepts STRING argument identifying which eRoom to query for data
        //Returns LIST of FILE objects containing file data
        public List<FileData> query_eRoomData(string idNum, KeyValuePair<string, eRoom> eData)
        {
            List<FileData> allFileData = new List<FileData>();      //initialize empty List
            string connectionString = "server=PDS\\SQLEXPRESS02; Initial Catalog=" + eData.Value.dbName + "; integrated Security=SSPI;";     //Database connection info

            string sql = @"SELECT o.InternalId, o.Name, o.ParentInternalId, o.eRoomInternalId, f.Filename 
                    FROM dbo.[Objects] AS o 
                    LEFT JOIN dbo.Files AS f 
                    ON o.InternalId = f.InternalId 
                    WHERE o.eRoomInternalId = " + idNum;                    //SQL query for retrieving necessary data

            SqlConnection conn = new SqlConnection(connectionString);       //Establish connection to database
            SqlCommand command = new SqlCommand(sql, conn);                 //Perform SQL query on connected database

            using (conn)
            {
                conn.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())            //Returns true if more lines to read, false if no more lines
                    {
                        FileData file = new FileData();     //Instantiate new File object and set data
                        file.InternalId = Convert.ToString(reader["InternalId"]);
                        file.Name = Convert.ToString(reader["Name"]);
                        file.ParentInternalId = Convert.ToString(reader["ParentInternalId"]);
                        file.eRoomInternalId = Convert.ToString(reader["eRoomInternalId"]);
                        file.FileName = Convert.ToString(reader["Filename"]);
                        allFileData.Add(file);
                    }
                }
                conn.Close();
            }
            return allFileData;
        }

        //File path data has some odd characters that are not supposed to 
        //be there.  This method removes any unwanted characters.
        //Accepts a STRING argument
        //Returns STRING
        public string cleanKeyFilePath(string name)
        {
            name = name.Replace("\x00", "");
            name = name.Replace("/", "-");
            name = name.Replace("\\", "-");
            name = name.Replace("\0", "");
            name = name.Replace(":", "");
            name = name.Replace("|", "-");
            name = name.Trim();

            return name;
        }

        //Separate clean method to include replacing _ with a -
        //Otherwise, method is same as above
        //We do NOT want to replace _ in Key file path values
        public string cleanFilePath(string name)
        {
            name = name.Replace("\x00", "");
            name = name.Replace("/", "-");
            name = name.Replace("\\", "-");
            name = name.Replace("\0", "");
            name = name.Replace(":", "");
            name = name.Replace("|", "-");
            name = name.Replace("_", "-");
            name = name.Replace("*", "-");
            name = name.Replace("\"", "");
            name = name.Replace("?", "");
            name = name.Replace("\r", "");
            name = name.Replace("<", "");
            name = name.Replace(">", "");
            name = name.Replace("~", "-");
            name = name.Trim();

            return name;
        }

        //ERPage is part of the database structure as the files are stored in
        //SQL but is not a part of the way the folder structure exists in eRooms.
        //This method removes all subdirectories referring to ERPage
        //Accepts STRING argument
        //Returns STRING
        public string cleanERPage(string file)
        {
            file = file.Replace("ERPage/", "");
            file = file.Replace("ERPage2/", "");
            file = file.Replace("\"", "");
            return file;
        }

        //Accepts a list of all file data as File objects.
        //Accepts a STRING with root folder as chosen by user
        //Accepts a STRING eRoom Name
        //Returns a dictionary of key/value pairs FileName/FilePath
        public Dictionary<string, string> BuildAllDestinationFilePaths(List<FileData> allFileData, string rootFolder, KeyValuePair<string, eRoom> eData)
        {
            Dictionary<string, string> destinationKeyAndPath = new Dictionary<string, string>();    //Destination dictionary

            //Recursive function to build file path
            string recursiveFunc(string pathName, string parentID)
            {
                string cleanPathName = "";
                string newParentId = "";
                string cleanName = "";

                if (pathName != eData.Value.eRoomDBName)
                {
                    foreach (FileData nextFile in allFileData)
                    {
                        cleanName = cleanFilePath(nextFile.Name);
                        if (cleanName == pathName && nextFile.ParentInternalId == parentID)
                        {
                            foreach (FileData parentFile in allFileData)
                            {
                                if (parentFile.InternalId == parentID)
                                {
                                    newParentId = parentFile.ParentInternalId;
                                    pathName = parentFile.Name;
                                    cleanPathName = cleanFilePath(pathName);
                                    if (cleanPathName != cleanName)
                                    {
                                        return recursiveFunc(cleanPathName, newParentId) + "/" + cleanName;
                                    }
                                }
                            }
                        }
                    }
                }
                return eData.Value.eRoomIDName;
            }

            //Loop through all file data extracted from SQL 
            foreach (FileData file in allFileData)
            {
                if (file.FileName != "")
                {
                    string FilePath = "";                                               //initialize new empty file path
                    string cleanName = cleanFilePath(file.Name);
                    if (cleanName != Path.GetFileName(FilePath))
                    {
                        FilePath = recursiveFunc(cleanName, file.ParentInternalId);         //Build file path with recursive funciton
                        string cleanFullFilePath = cleanERPage(FilePath);                   //Remove all ERPage subdirectories from file path
                        string ValueFilePath = rootFolder + "\\" + cleanFullFilePath;       //Concatenate rootfolder to cleaned file path
                        string cleanKeyFileName = cleanKeyFilePath(file.FileName);             //Remove any extra characters from file name to be used as key
                        destinationKeyAndPath.Add(cleanKeyFileName, ValueFilePath);         //Add key and value to dictionary
                    }
                }
                //else
                //{

                //    EvaluateFilePath(ValueFilePath);
                //}

            }
            return destinationKeyAndPath;
        }

        //Accepts STRING argument eRoomDataPath as input by user
        //Accepts STRING eRoom Name
        //Returns a dictionary of key/value pairs FileName/Filepath
        public Dictionary<string, string> BuildAllSourceFilePaths(string eRoomDataPath, KeyValuePair<string, eRoom> eData)
        {
            Dictionary<string, string> SourceDict = new Dictionary<string, string>();
            string[] subDirs;
            bool subDirFound = false;

            //find eRoom in subdirectory list if not in eRoomDataPath
            if (!eRoomDataPath.Contains(eData.Value.eRoomDBName))
            {
                subDirs = Directory.GetDirectories(eRoomDataPath, "*.*", SearchOption.AllDirectories);

                foreach (string dir in subDirs)
                {
                    string cleanDir = dir.Trim();
                    if (cleanDir.Contains(eData.Value.eRoomDBName))
                    {
                        eRoomDataPath = cleanDir;
                        subDirFound = true;
                        break;
                    }
                }
            }
            if (!subDirFound)
            {
                HandleSubDirectoryNotFound(eData.Value.eRoomDBName);
                return SourceDict;
            }
            else
            {
                //Build source paths
                var SourcePaths = Directory.EnumerateFiles(eRoomDataPath, "*.*", SearchOption.AllDirectories);
                List<string> allSourceFilePaths = SourcePaths.ToList();

                //source base name becomes the key value.  Source path is value.
                //Add each pair to dictionary.
                foreach (string source in allSourceFilePaths)
                {
                    string sourceBaseName = Path.GetFileName(source);
                    if (SourceDict.ContainsKey(sourceBaseName))
                    {
                        continue;
                    }
                    else
                    {
                        SourceDict.Add(sourceBaseName, source);
                    }

                }

                return SourceDict;
            }

        }

        //Match keys from two different dictionaries and use each value 
        //to copy file source to file destination.
        //Accepts two dictionaries
        //No return value
        public List<UnmanagedFileData> copySourceToDestination(Dictionary<string, string> destinationKeyAndPath, Dictionary<string, string> sourceKeyAndPath, KeyValuePair<string, eRoom> eData)
        {
            //Send each destination path to EvaluateFilePath method
            //This will create all directories if they don't already exist
            foreach (KeyValuePair<string, string> path in destinationKeyAndPath)
            {
                if (CheckPathLength(path))
                {
                    HandlePathLengthTooLong(path, sourceKeyAndPath, eData);
                }
                else
                {
                    EvaluateFilePath(path.Value);
                    //if(File.Exists(path.Value))
                    //{
                        //extraFileCountDuplicates++;
                    //}
                    if (path.Value.Contains("Link"))
                    {
                       extraFileCountDuplicates++;
                    }
                    if (path.Value.Contains("ERRecycleBin"))
                    {
                        extraFileCountRecycleBin++;
                    }
                    
                    if (!File.Exists(path.Value) && !path.Value.Contains("ERRecycleBin"))
                    {
                        if (!sourceKeyAndPath.ContainsKey(path.Key))
                        {
                            duplicateFileKeyCount++;
                            continue;
                        }
                        else
                        {
                            try
                            {
                                File.Copy(sourceKeyAndPath[path.Key], destinationKeyAndPath[path.Key], true);
                            }
                            catch(Exception e)
                            {
                                HandleFileCopyError(e.Message);
                            }
                            
                        }
                    }
                }
            }

            //Get all files that exist in destination data but do not have a match in source
            var unMatchedFiles = destinationKeyAndPath.Keys.Except(sourceKeyAndPath.Keys).ToList();
            foreach (string kvp in unMatchedFiles)
            {
                foreach (KeyValuePair<string, string> path in destinationKeyAndPath)
                {
                    if (kvp == path.Key && !UnmanagedFilePaths.ToString().Contains(path.Key))
                    {
                        HandlePathLengthTooLong(path, sourceKeyAndPath, eData);
                        break;
                    }
                }
            }
            return UnmanagedFilePaths;
        }

        //Takes in a string and evaluates string to see if file path directories
        //have been created.  Created directories if they don't already exist
        //Accepts a STRING
        //No return value
        public void EvaluateFilePath(string path)
        {
            try
            {
                string folder = Path.GetDirectoryName(path);
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                    //DirectoryInfo di = 
                }
            }
            catch (IOException ioex)
            {
                Console.WriteLine(ioex.Message);
            }
        }

        //Check if file path length exceeds Windows path limit of 256
        public bool CheckPathLength(KeyValuePair<string, string> path)
        {
            bool TooLong = false;
            if (path.Value.Length > 255)
            {
                TooLong = true;
            }

            return TooLong;
        }

        public void HandlePathLengthTooLong(KeyValuePair<string, string> path, Dictionary<string, string> sourceKeyAndPath, KeyValuePair<string, eRoom> eData)
        {
            string source;
            if (sourceKeyAndPath.TryGetValue(path.Key, out source))
            {
                UnmanagedFileData newUnMatch = new UnmanagedFileData(eData.Value.eRoomIDName, source, path.Value);
                UnmanagedFilePaths.Add(newUnMatch);
            }
        }

        //Handle any key values that exist in Destination list but NOT in the source list
        //This will happen when there is a misspelled file name in the SQL database compared to what exists in the actual data that is being used 
        //to create a destination path
        public void HandleSourceDoesntContainKey(KeyValuePair<string, string> path, KeyValuePair<string, eRoom> eData)
        {
            string source = "Unknown";
            UnmanagedFileData newSourceNoKey = new UnmanagedFileData(eData.Value.eRoomIDName, source, path.Value);
            unMatchedNoSourceMasterList.Add(newSourceNoKey);


        }

        public void HandleeRoomCSVData(eRoomCSVData eRoom)
        {
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = false,
            };

            List<eRoomCSVData> csvRow = new List<eRoomCSVData>();
            csvRow.Add(eRoom);
            
            using (var stream = File.Open("E:\\ExcelSheets\\eRoomMasterList\\eRoomTracker\\RED_eRoomCSV.csv", FileMode.Append))
            using (var writer = new StreamWriter(stream))
            using (var csv = new CsvWriter(writer, config))
            {
                foreach(eRoomCSVData e in csvRow)
                {
                    csv.WriteRecord<eRoomCSVData>(e);
                    csv.NextRecord();
                }
                
            }

        }

        //Accepts an eRoomRecycleDupData object
        //Prints data to csv file for further observation
        public void HandleRecycleBinAndDuplicateNumbers(eRoomRecycleDupData eRoom)
        {
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = false,
            };

            List<eRoomRecycleDupData> csvRow = new List<eRoomRecycleDupData>();
            csvRow.Add(eRoom);

            using (var stream = File.Open("E:\\ExcelSheets\\eRoomCSVFile\\eRoomRecycleDuplicateData.csv", FileMode.Append))
            using (var writer = new StreamWriter(stream))
            using (var csv = new CsvWriter(writer, config))
            {
                foreach (eRoomRecycleDupData e in csvRow)
                {
                    csv.WriteRecord<eRoomRecycleDupData>(e);
                    csv.NextRecord();
                }

            }
        }
        public void HandleSubDirectoryNotFound(string e)
        {
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = false,
            };

            List<string> csvRows = new List<string>();
            csvRows.Add(e);

            using (var stream = File.Open("E:\\ExcelSheets\\eRoomCSVFile\\eRoomSubDirNotFound.csv", FileMode.Append))
            using (var writer = new StreamWriter(stream))
            using (var csv = new CsvWriter(writer, config))
            {
                foreach(string s in csvRows)
                {
                    csv.WriteRecords(s);
                    csv.NextRecord();
                }
             
            }
        }

        public void HandleFileCopyError(string e)
        {
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = false,
            };

            List<string> csvRows = new List<string>();
            csvRows.Add(e);

            using (var stream = File.Open("E:\\ExcelSheets\\eRoomCSVFile\\CopyFileErrors.csv", FileMode.Append))
            using (var writer = new StreamWriter(stream))
            using (var csv = new CsvWriter(writer, config))
            {
                foreach (string s in csvRows)
                {
                    csv.WriteRecords(s);
                    csv.NextRecord();
                }

            }
        }
    }
}