﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using CsvHelper;
using CsvHelper.Configuration;



namespace eRoomExtraction_WindowsForm
{

    public partial class Form1 : Form
    {
        //GLOBAL variables
        eRoomData data = new eRoomData();

        public Form1()
        {

            data.MasterCSVData();
            InitializeComponent();
        }
        //*****************************************************************//
        //*********************BUTTON 1 METHODS****************************//
        List<UnmanagedFileData> unMatchedMasterList = new List<UnmanagedFileData>();
        

        //SUBMIT button
        private void button1_Click(object sender, EventArgs e)
        {
            string rootFilePath = textBox3.Text;
            string sourceFilePath = textBox4.Text;
            
            textBox3.Clear();
            textBox4.Clear();
            BegineRoomExtraction(rootFilePath, sourceFilePath);

        }

        //Starting point for processing all data input into form
        //Associated with Button1 aka SUBMIT button
        public void BegineRoomExtraction(string rootFilePath, string sourceFilePath)
        {
            //check if both directory inputs exist
            if (Directory.Exists(rootFilePath) && Directory.Exists(sourceFilePath))
            {
                foreach (KeyValuePair<string, eRoom> eData in data.QueueList)
                {

                        List<UnmanagedFileData> UnmanagedFiles = new List<UnmanagedFileData>();
                        eRoomExtraction eRoom = new eRoomExtraction();
                        UnmanagedFiles = eRoom.extractData(eData, rootFilePath, sourceFilePath);
                        AddunMatchedFilesListtoMasterList(UnmanagedFiles);     
                }

                if(unMatchedMasterList.Count > 0)
                {
                   CreateCSVofUnmatchedFiles();
                    DisplayUnMatchedFiles();
                }
                Application.Exit();
            }
            else
            {
                string errorMessage = "Directory does not exist.  Select a new directory or create input directory path.";
                string caption = "Invalid Root Folder";

                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;

                //Display message box
                result = MessageBox.Show(errorMessage, caption, buttons);
            }

        }

        //Any unmatched files get added to master unmatched list
        public void AddunMatchedFilesListtoMasterList(List<UnmanagedFileData> UnmanagedFiles)
        {
            foreach (var kvp in UnmanagedFiles)
            {
                unMatchedMasterList.Add(kvp);
            }
            
        }

        public void CreateCSVofUnmatchedFiles()
        {
            string csvFile = "E:\\ExcelSheets\\UnmatchedFiles\\UnmatchedFiles.csv";
            if (!File.Exists(csvFile))
            {
                using (var writer = new StreamWriter(csvFile))
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    writer.AutoFlush = true;

                    csv.WriteHeader<UnmanagedFileData>();
                    csv.NextRecord();
                    foreach (UnmanagedFileData file in unMatchedMasterList)
                    {
                        csv.WriteRecord(file);
                        csv.NextRecord();
                    }
                }
            }
            //If it does exist, append records to existing csv and do not add headers
            else
            {
                var config = new CsvConfiguration(CultureInfo.InvariantCulture)
                {
                    HasHeaderRecord = false,
                };

                using (var stream = File.Open("E:\\ExcelSheets\\UnmatchedFiles\\UnmatchedFiles.csv", FileMode.Append))
                using (var writer = new StreamWriter(stream))
                using (var csv = new CsvWriter(writer, config))
                {
                    foreach (UnmanagedFileData file in unMatchedMasterList)
                    {
                        csv.WriteRecord<UnmanagedFileData>(file);
                        csv.NextRecord();
                    }
                }
            }
        }

        public void DisplayUnMatchedFiles()
        {
            List <string> tempunMatchedList = new List<string>();
            string files = "";
            //string errorMessage = "Files listed below exist in the source data but do not have a match in eRooms.";
            string caption = "UnMatched Files";

            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result;

            //Write unmatched files to messagebox
            if (unMatchedMasterList.Count() > 0)
            {
                foreach(var kvp in unMatchedMasterList)
                {
                    tempunMatchedList.Add(kvp.destinationPath);
                }
                string csvFile = "csv file showing all relevant information about these files located in the D://";
                files = string.Join(Environment.NewLine, tempunMatchedList);
                //Display message box
                 string show = csvFile + "\n" + "\n" + files;
                result = MessageBox.Show(show, caption, buttons);
            }
        }
        //******************************************************************//

        //SEARCH button
        //This code takes in the text entered by the user in textbox and uses it to search
        //the master list of eRooms.  Search is case insenstive.
        private void button2_Click(object sender, EventArgs e)
        {
            string searchString = "";
            //search string entered by user
            if (!string.IsNullOrEmpty(textBox1.Text))
            {
                searchString = textBox1.Text;
                data.eRoomSearchResultsPopulateList(searchString);
                data.CheckSearchResultsForZeroAndFilter();
                data.PopulateSearchGUI(listBox1);
            }
            
            textBox1.Clear();
        }

        //CLEAR button Search
        //Clear all items from Search/CSV Results listbox
        private void button3_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            data.ClearSearchGUI();
            textBox1.Clear();
        }

        //...Button
        //Browse file explorer
        private void button4_Click(object sender, EventArgs e)
        {
            data.OpenFileDialogForm(textBox2);
        }

        //SUBMIT button queue box
        //CSV File Input 
        private void button5_Click(object sender, EventArgs e)
        {
            string csvPath = textBox2.Text;
            //IDictionary<string, eRoom> tempList;
            textBox2.Clear();
            data.FilecsvDataToSearchList(csvPath);
            data.CheckSearchResultsForZeroAndFilter();
            data.PopulateSearchGUI(listBox1);
        }

        //CLEAR button Queue
        //Clear all items from eRoom Extraction Queue listbox
        private void button6_Click(object sender, EventArgs e)
        {
            data.ClearQueueList();
            listBox2.Items.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
        }

        //ADD SELECTED button
        private void button7_Click(object sender, EventArgs e)
        {
            data.PopulateQueueListWithSelected(listBox1);
            data.PopulateQueueGUI(listBox2);
            listBox1.Items.Clear();
            textBox1.Clear();
        }

        //ADD ALL button
        private void button8_Click(object sender, EventArgs e)
        {
            data.PopulateQueueListSelectAll(listBox1);
            data.PopulateQueueGUI(listBox2);
            listBox1.Items.Clear();
            textBox1.Clear();
        }

        //Destination Path Button
        private void button9_Click(object sender, EventArgs e)
        {
            data.FindPathFileDialogForm(textBox3);
        }

        //Source Path Button
        private void button10_Click(object sender, EventArgs e)
        {
            data.FindPathFileDialogForm(textBox4);
        }

    }
}

