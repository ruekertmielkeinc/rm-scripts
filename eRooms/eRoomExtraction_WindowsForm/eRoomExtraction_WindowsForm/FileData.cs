﻿
namespace eRoomExtraction_WindowsForm
{
    public class FileData
    {
        public string InternalId { get; set; }
        public string Name { get; set; }
        public string ParentInternalId { get; set; }
        public string eRoomInternalId { get; set; }
        public string FileName { get; set; }
    }
}