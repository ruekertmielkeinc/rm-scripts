﻿
namespace eRoomExtraction_WindowsForm
{
    public class UnmanagedFileData
    {
        public UnmanagedFileData(string eroomname, string source, string dest)
        {
            eRoomName = eroomname;
            sourcePath = source;
            destinationPath = dest;
        }

        public string eRoomName { get; set; }
        public string sourcePath { get; set; }
        public string destinationPath { get; set; }
    }
}