﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using CsvHelper;
using CsvHelper.Configuration;

namespace eRoomExtraction_WindowsForm
{
	internal class eRoom
	{
        public eRoom(string guid, string eDBName, string eIDName, string dName)
        {
            eRoomGUID = guid;
            eRoomDBName = eDBName;
            eRoomIDName = eIDName;
            dbName = dName;
        }
        public string eRoomGUID { get; internal set; }
        public string eRoomDBName { get; internal set; }
        public string eRoomIDName { get; internal set; }
        public string dbName { get; internal set; }

        public override string ToString()
        {
            return eRoomIDName;
        }
        
    }
}
