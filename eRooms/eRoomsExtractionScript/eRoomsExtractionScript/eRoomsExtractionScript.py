import subprocess
import sys
import os

def install():
    subprocess.check_call([sys.executable, "-m", "pip", "install", "pyodbc"])
install()

import pyodbc

cnxn_str = ("Driver={SQL Server Native Client 11.0};"
            "Server=PDS\SQLEXPRESS02;"
            "Database=eRoom_Departments;"
            "Trusted_Connection=yes;")

dbConn = pyodbc.connect(cnxn_str)

projectName = input("Enter eRoom name you wish to map: ")

myCursor = dbConn.cursor()
myCursor.execute("SELECT * FROM dbo.[Objects] WHERE Name = '" + str(projectName) + "'")
thisProject = myCursor.fetchall()
InternalId, ClientId, ObjId, Name, ParentInternalId, CreatorMemberId, CreationDate, ModDate, ExcludeFromSearch, ExcludeFromUnread, UIFlags, ObjectFlags, DeletedTag, ModTag, HiddenTag, eRoomInternalId, UnreadId, CanOpenStyleId, CanEditStyleId, NonSearchableProperties, TargetId = thisProject[0]

myCursor.execute('SELECT o.InternalId, o.Name, o.ParentInternalId, o.eRoomInternalId, f.Filename FROM dbo.[Objects] AS o INNER JOIN dbo.Files AS f ON o.InternalId = f.InternalId WHERE o.eRoomInternalId = ' + str(eRoomInternalId))
allFiles = myCursor.fetchall()

allFilePaths = []
for row in allFiles:
    InternalId, Name, ParentInternalId, eRoomInternalID, FileName = row
    myCursor.execute("SELECT * FROM dbo.[Objects] WHERE InternalId = " + str(InternalId))
    thisObject = myCursor.fetchall()
    InternalId, ClientId, ObjId, Name, ParentInternalId, CreatorMemberId, CreationDate, ModDate, ExcludeFromSearch, ExcludeFromUnread, UIFlags, ObjectFlags, DeletedTag, ModTag, HiddenTag, eRoomInternalId, UnreadId, CanOpenStyleId, CanEditStyleId, NonSearchableProperties, TargetId = thisObject[0]

    filePath = FileName
    internalCount = 0
    while InternalId != eRoomInternalId:
        if Name.find("ERPage") == -1 and internalCount != 0:
            filePath = Name + "/" + filePath
        myCursor.execute("SELECT * FROM dbo.[Objects] WHERE InternalId = " + str(ParentInternalId))
        thisObject = myCursor.fetchall()
        InternalId, ClientId, ObjId, Name, ParentInternalId, CreatorMemberId, CreationDate, ModDate, ExcludeFromSearch, ExcludeFromUnread, UIFlags, ObjectFlags, DeletedTag, ModTag, HiddenTag, eIntId, UnreadId, CanOpenStyleId, CanEditStyleId, NonSearchableProperties, TargetId = thisObject[0]
        internalCount = internalCount + 1
        
    allFilePaths.append(filePath)

rootFolder = "C:\\Users\\psatterlee\\Desktop\\SourceCode\\RMScripts\\eRooms\\TEST_FOLDER"
for file in allFilePaths:
    newPath = os.path.join(rootFolder, file)
    dirPathOnly = os.path.dirname(newPath)
    dirPathOnly = dirPathOnly.replace('\x00','')
    
    if not os.path.exists(dirPathOnly):
        os.makedirs(dirPathOnly)


