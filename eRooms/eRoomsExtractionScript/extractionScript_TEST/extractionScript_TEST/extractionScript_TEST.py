import os

rootFolder = "C:\\Users\\psatterlee\\Desktop\\SourceCode\\RMScripts\\eRooms\\TEST_FOLDER"

path1 = "Saukville Village 17-10026 Colonial Drive Reconstruction/0_4cb2[]Ruekert-Mielke List of Services.doc"
path2 = "Saukville Village 17-10026 Colonial Drive Reconstruction/Design/Support/Diggers Tickets/0_6bf3[]Diggers Hotline Ticket 20194909650.msg "
path3 = "Saukville Village 17-10026 Colonial Drive Reconstruction/Design/Assessment/0_152b7[]colonial preliminary assessment roll .docx "
path4 = "Saukville Village 17-10026 Colonial Drive Reconstruction/Design/Support/Measure Downs/0_16c2d[]measure downs.pdf "
path5 = "Saukville Village 17-10026 Colonial Drive Reconstruction/Design/Support/Measure Downs/0_16c2d[]measure downs.pdf "

pathList = [path1, path2, path3, path4, path5]

for path in pathList:
    newPath = os.path.join(rootFolder, path)
    dirPathOnly = os.path.dirname(newPath)

    if not os.path.exists(dirPathOnly):
        os.makedirs(dirPathOnly)
