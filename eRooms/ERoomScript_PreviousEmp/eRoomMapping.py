import subprocess
import sys
def install():
    subprocess.check_call([sys.executable, "-m", "pip", "install", "pyodbc"])
install()

import pyodbc

mydb = pyodbc.connect(
    'DRIVER={ODBC Driver 17 for SQL Server};'
    'SERVER=GIS-VM-1\SQLEXPRESS;'
    'DATABASE=eRoom_Projects30;'
    'UID=sa;'
    'PWD=sa.admin;'
)


projectName = raw_input("Enter Full eRoom Name You Wish to Map: ")


mycursor = mydb.cursor()
mycursor.execute("SELECT * FROM dbo.[Objects] WHERE Name = '" + str(projectName) + "'")
thisProject = mycursor.fetchall()
InternalId, ClientId, ObjId, Name, ParentInternalId, CreatorMemberId, CreationDate, ModDate, ExcludeFromSearch, ExcludeFromUnread, UIFlags, ObjectFlags, DeletedTag, ModTag, HiddenTag, eRoomInternalId, UnreadId, CanOpenStyleId, CanEditStyleId, NonSearchableProperties, TargetId = thisProject[0]
query = ('SELECT Objects.InternalId, Name, Objects.ParentInternalId, Objects.eRoomInternalId, Files.Filename FROM Objects '
         'JOIN Files ON Objects.InternalId = Files.InternalId '
         'WHERE (Objects.eRoomInternalId = ' + str(eRoomInternalId) + ')')
mycursor.execute(query)
allFiles = mycursor.fetchall()

outF = open('eRoomFilePaths.txt', 'w')
for row in allFiles:
    InternalId, Name, ParentInternalId, eRoomInternalID, FileName = row
    mycursor.execute("SELECT * FROM dbo.[Objects] WHERE InternalId = " + str(InternalId))
    thisObject = mycursor.fetchall()
    InternalId, ClientId, ObjId, Name, ParentInternalId, CreatorMemberId, CreationDate, ModDate, ExcludeFromSearch, ExcludeFromUnread, UIFlags, ObjectFlags, DeletedTag, ModTag, HiddenTag, eRoomInternalId, UnreadId, CanOpenStyleId, CanEditStyleId, NonSearchableProperties, TargetId = thisObject[0]
    
    filePath = FileName
    internalCount = 0
    while InternalId != eRoomInternalId:
        if Name.find("ERPage") == -1 and internalCount != 0:
            filePath = Name + "/" + filePath
        mycursor.execute("SELECT * FROM dbo.[Objects] WHERE InternalId = " + str(ParentInternalId))
        thisObject = mycursor.fetchall()
        InternalId, ClientId, ObjId, Name, ParentInternalId, CreatorMemberId, CreationDate, ModDate, ExcludeFromSearch, ExcludeFromUnread, UIFlags, ObjectFlags, DeletedTag, ModTag, HiddenTag, eIntId, UnreadId, CanOpenStyleId, CanEditStyleId, NonSearchableProperties, TargetId = thisObject[0]
        internalCount = internalCount + 1
        
        
    outF.write(filePath + '\n')

outF.close()
print('Complete!')
