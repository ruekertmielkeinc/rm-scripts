import os
import pyodbc
import shutil

#Establish cursor connection to SQL database
#No ARGS
#RETURNS a variable with a db connection
def db_Connection():
    cnxn_str = ("Driver={SQL Server Native Client 11.0};"
                "Server=PDS\SQLEXPRESS02;"
                "Database=eRoom_Departments;"
                "Trusted_Connection=yes;")

    dbConn = pyodbc.connect(cnxn_str)
    return dbConn

#Get eRoom project that user would like to map.  To avoid errors and confusion about what exactly the user needs to enter, 
#I'd like to make this into a drop down list or maybe a check box of some sort where we could maybe do more than 1 eRoom
#at a time.  For now, user will enter an eRoom by typing in the eRoom name.
#
#This method could also include a dictionary which would include key:value pairs of each eRoom name along with their unique ID.
#There is a static number of eRooms at this point, or at least I assume there will be before we officially start this process.  
#This would eliminate the need to query the ID and eRoom name information from SQL.  Once a user selects an eRoom to map, 
#we can just automatically assign the eRoom ID to use.
#
#We know that the eRoomInternalID for the IS Dept eRoom that we are using for testing purposes is 225 so we can hardcode 
#that value in for now. 
#No ARGS
#RETURNS a STRING containing eRoom Id value 
def get_eRoomToMap():
    eRoomProject = input("Enter eRoom name you wish to map: ")
    eRoomProject = '225'
    return eRoomProject

#This method queries the database to get relevant information for each file contained within the eRoom selected
#by the user. 
#ACCEPTS a STRING argument of eRoom Id value
#RETURNS a LIST OF TUPLES. Each tuple is one row of queried data from SQL
def query_eRoomData(idNum):
    dbConn = db_Connection()  
    sqlQuery = 'SELECT o.InternalId, o.Name, o.ParentInternalId, o.eRoomInternalId, f.Filename FROM dbo.[Objects] AS o LEFT JOIN dbo.Files AS f ON o.InternalId = f.InternalId WHERE o.eRoomInternalId = ' + idNum

    sqlCursor = dbConn.cursor()
    sqlCursor.execute(sqlQuery)         
    allFileData = sqlCursor.fetchall()
    sqlCursor.close()

    
    return allFileData #List of Tuples


#This method takes in each filepath as it enters the for loop below and removes \x00 and unwanted \ or / if they exist
#ACCEPTS STRING arg
#RETURNS STRING
def cleanFilePath(file):
    file = file.replace('\x00','')
    file = file.replace('/', '-')
    file = file.replace('\\', '-')
    return file

#After filepath is complete and before filepath is added to allFilePaths list
#Remove all ERPage/ entries from filepath
#ACCEPTS a STRING argument
#RETURNS STRING
def cleanERPage(filePath):
    cleanFilePath = filePath.replace('ERPage/', '')
    return cleanFilePath

#This method builds a file path for each individual file contained in a given eRoom.  Inside this method
#is a recursive function that actually builds the filepath.  That built filepath is then added to a list
#ACCEPTS LIST OF TUPLES argument
#RETURNS LIST of STRINGS
def buildAllDestinationFilePaths(allFileData):
    allDestinationFilePaths = [] 

    #Recursive function
    #ACCEPTS two string arguments
    #RETURNS STRING of a completed file path after recursion completes
    def recursiveFunc(pathName, parentID):
            if pathName == "IS Dept":
                return "IS Dept"
            else:
                for file in allFileData:
                    InternalId, Name, ParentInternalId, eRoomInternalId, FileName = file
                    cleanName = cleanFilePath(Name)
                    if cleanName == pathName and ParentInternalId == parentID: 
                        for parentFile in allFileData:
                            PInternalId, PName, PParentInternalId, PeRoomInternalId, PFileName = parentFile
                            if PInternalId == parentID:
                                pathName = PName
                                cleanPathName = cleanFilePath(pathName)
                                return (recursiveFunc(cleanPathName, PParentInternalId) + "/" + cleanName)

    for row in allFileData:
        InternalId, Name, ParentInternalId, eRoomInternalId, FileName = row     #Unpack each tuple in list
        if FileName != None:
            FilePath = ""                            #Initialize file path. Start path with empty string
            cleanName = cleanFilePath(Name)
            FilePath = recursiveFunc(cleanName, ParentInternalId)
            cleanFullFilePath = cleanERPage(FilePath)
            allDestinationFilePaths.append(cleanFullFilePath)

    return allDestinationFilePaths    

   
#Method that takes in a file to search for and a path to copy the file to if found.  eRoomDataPath is initialized
#with a path to where eRoom data currently exists.  Walk method searches through this folder structure for a given file.
#If file is found, it is copied to the newly created folder structure 
#ACCEPTS STRING, STRING args
#NO RETURN value
#
#UPDATE 4.21.23
#Switched from os.walk to os.scandir
#os.walk searches through every folder within a given root folder.  Scandir only scans through one folder
#Scandir is sp
def findFileInDirectory(file, newFilePath):
    eRoomDataPath = "C:\\Users\\psatterlee\\Documents\\eRooms\\TEST_KDRIVE_DUPLICATE"

    for root, dirs, files in os.walk(eRoomDataPath):
        if file in files:
            #fileNameWithExt = os.path.basename(file)
            shutil.copy(os.path.join(root, file), os.path.join(newFilePath, file.name))

def buildSourceFilePathList():
    allSourceFilePaths = []
    eRoomDataPath = "C:\\Users\\psatterlee\\Documents\\eRooms\\TEST_KDRIVE_DUPLICATE"
    for root, dirs, files in os.walk(eRoomDataPath):
        for file in files:
            sourceFilePath = os.path.join(root, file)
            allSourceFilePaths.append(sourceFilePath)

    return allSourceFilePaths

#ACCEPTS LIST args, Destination File Paths and Source File Paths
#NO RETURN value
def copySourceToDestination(destinationFilePaths, sourceFilePaths):
    rootFolder = "C:\\Users\\psatterlee\\Desktop\\SourceCode\\RMScripts\\eRooms\\TEST_FOLDER"   #Initialize root folder for eRoom data storage
    
    for destFile in destinationFilePaths:
        updatedDestFilePath = os.path.join(rootFolder, destFile)

        if not os.path.exists(updatedDestFilePath):
            os.makedirs(updatedDestFilePath)
        destBaseName = os.path.basename(destFile)

        for sourceFile in sourceFilePaths:
            if sourceFile.find(destBaseName) != -1:
                shutil.copy(sourceFile, updatedDestFilePath)



def main():
    eRoomIDNum = get_eRoomToMap()
    allFileData = query_eRoomData(eRoomIDNum)
    allDestinationFilePathsList = buildAllDestinationFilePaths(allFileData)
    allSourceFilePathsList = buildSourceFilePathList()
    copySourceToDestination(allDestinationFilePathsList, allSourceFilePathsList)

if __name__ == "__main__":
    main()
