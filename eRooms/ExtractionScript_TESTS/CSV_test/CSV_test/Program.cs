﻿using System;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using CsvHelper;
using CsvHelper.Configuration;

namespace CSV_test
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    Excel.Application xl = new Excel.Application();
        //    Excel.Workbook workbook = xl.Workbooks.Open(@"C:\Users\psatterlee\Documents\eRooms\TEST_ExcelSheets\eRoomList.xlsx");
        //    Excel.Worksheet sheet = workbook.Sheets[1];

        //    int numRows = sheet.UsedRange.Rows.Count;

        //    List<KeyValuePair<string, string>> eRoomList = new List<KeyValuePair<string, string>>(); 

        //    for(int i = 0; i < numRows; i++)
        //    {
        //        string eRoom = (string)(sheet.Cells[i+1, 1] as Excel.Range).Value;
        //        string dbName = (string)(sheet.Cells[i+1, 2] as Excel.Range).Value;
         
        //        eRoomList.Add(new KeyValuePair<string, string>(eRoom, dbName));
        //    }
 
        //    foreach(KeyValuePair<string, string> x in eRoomList)
        //    {
        //        Console.WriteLine("{0}, {1}", x.Key, x.Value);
        //    }
        //    Console.WriteLine(eRoomList.Count);

        //    string searchString = "Dept";
        //    string searchStringLower = searchString.ToLowerInvariant();
        //    List<KeyValuePair<string, string>> foundList = new List<KeyValuePair<string, string>>();

        //    foreach (KeyValuePair<string, string> x in eRoomList)
        //    {
        //        if (x.Key.ToLowerInvariant().Contains(searchStringLower))
        //        {
        //            foundList.Add(new KeyValuePair<string, string>(x.Key, x.Value));
        //        }
        //    }

        //    foreach (KeyValuePair<string, string> x in foundList)
        //    {
        //        Console.WriteLine("{0}, {1}", x.Key, x.Value);
        //    }
        //    Console.WriteLine(foundList.Count);
        //    //List<string> foundList = eRoomList.
        //    //List<string> foundList = eRoomList.Where(t => t.Contains(searchString, System.StringComparison.CurrentCultureIgnoreCase)).ToList();

        //    //foundList.ForEach(i => Console.WriteLine("{0}\t", i));
        //    //Console.WriteLine(foundList.Count);


        //}

        static void Main(string[] args)
        {
            string csvPath = @"C:\Users\psatterlee\Documents\eRooms\TEST_ExcelSheets\eRoomList.csv";
            IDictionary<string, eRoom> eRoomList = new Dictionary<string, eRoom>();
            var csvConfig = new CsvConfiguration(CultureInfo.CurrentCulture)
            {
                HasHeaderRecord = false
            };

            using var streamReader = File.OpenText(csvPath);
            using var csvReader = new CsvReader(streamReader, csvConfig);

            while(csvReader.Read())
            {
                var GUID = csvReader.GetField(0);
                var eRoomName = csvReader.GetField(1);
                var dbName = csvReader.GetField(2);

                eRoom newRoom = new eRoom(GUID, eRoomName, dbName);
                eRoomList.Add(GUID, newRoom);
            }

            foreach(KeyValuePair<string, eRoom> kvp in eRoomList)
            {
                Console.WriteLine("Key = {0}, eRoomName = {1}, DB = {2}", kvp.Key, kvp.Value.eRoomName, kvp.Value.dbName);
            }
            Console.WriteLine(eRoomList.Count);

            string searchString = "dept";
            List<KeyValuePair<string, eRoom>> foundList = eRoomList.Where(x => x.Value.eRoomName.Contains(searchString, System.StringComparison.CurrentCultureIgnoreCase)).ToList();

            foreach(KeyValuePair<string, eRoom> kvp in foundList)
            {
                Console.WriteLine("Found = {0}, DB = {1}", kvp.Value.eRoomName, kvp.Value.dbName);
            }
            Console.WriteLine(foundList.Count);
        }
    }

    class eRoom
    {
        public eRoom(string guid, string eName, string dName)
        {
            GUID = guid;
            eRoomName = eName;
            dbName = dName;
        }
        public string GUID { get; set; }
        public string eRoomName { get; set; }
        public string dbName { get; set; }
    }
}
