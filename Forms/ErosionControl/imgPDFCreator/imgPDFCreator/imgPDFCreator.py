from fpdf import FPDF

#Simple Hello World PDF creation

#pdf = FPDF()
#pdf.add_page()
#pdf.set_font('Arial')
#pdf.cell(100,10, 'Hello World!')
#pdf.output('C:\\Users\\psatterlee\\Documents\\PowerApps\\ErosionControlInspReport\\TESTHelloWorld.pdf', 'F')




class PDF(FPDF):
    #Set Header for all pages
    #This method overwrites inherited header method from FPDF class
    def header(self):
        #Set R/M logo in upper left corner of all pages
        self.image('RM LOGO_Full Color.png', 10, 8, 33)

        #Set title in upper center of all pages
        self.set_font('Arial', 'B', 14)
        self.set_xy(50, 6)
        self.cell(110, 8, 'Erosion Control Inspection Report', border=1, align='C')

    #Set Footer for all pages
    #This method overwrites inherited footer method from FPDF class
    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Arial italic 8
        self.set_font('Arial', 'I', 8)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'C')



# Instantiation of inherited class
pdf = PDF()
pdf.alias_nb_pages()
pdf.add_page()
pdf.set_font('Times', '', 10)

#Upper left image and text cell box
pdf.set_xy(10, 25)
pdf.image('C:\\Users\\psatterlee\\Documents\\Personal\\Breakfast w Santa pics\\DSC_3892.jpeg', x=None, y=None, w=85, h=65)
pdf.set_xy(8,95)
pdf.cell(89, 30, txt='This is the upper left photo text', border=1, align='C')

#Upper right image and text box
pdf.set_xy(100, 25)
pdf.image('C:\\Users\\psatterlee\\Documents\\Personal\\Breakfast w Santa pics\\DSC_3893.jpeg', x=None, y=None, w=85, h=65)
pdf.set_xy(98,95)
pdf.cell(89, 30, txt='This is the upper right photo text', border=1, align='C')

#Lower left image and text box
pdf.set_xy(10, 155)
pdf.image('C:\\Users\\psatterlee\\Documents\\Personal\\Breakfast w Santa pics\\DSC_3895.jpeg', x=None, y=None, w=85, h=65)
pdf.set_xy(8,225)
pdf.cell(89, 30, txt='This is the lower left photo text', border=1, align='C')

#Lower right image and text box
pdf.set_xy(100, 155)
pdf.image('C:\\Users\\psatterlee\\Documents\\Personal\\Breakfast w Santa pics\\DSC_3897.jpeg', x=None, y=None, w=85, h=65)
pdf.set_xy(98, 225)
pdf.cell(89, 30, txt='This is the lower right photo text', border=1, align='C')

pdf.output('C:\\Users\\psatterlee\\Documents\\PowerApps\\ErosionControlInspReport\\testreport.pdf', 'F')



