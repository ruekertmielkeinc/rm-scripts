import geopandas, time, random, googlemaps
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# Input Arguments
ParcelsPath = r"I:\Projects\2075_BeaverDamCity\10007_TransportationStudy\Data\testdata.shp"
API_KEY = "AIzaSyDiM3kOtKtnP85zN7fiHFAvENifSGPM-5w"

# Create functions used in processing:
#----------------------------------------------------------------------------------------------------------------------
# Extract values from JSON (thanks internet)
def extract_values(obj, key):
    "Pull all values of specified key from nested JSON."
    arr = []

    def extract(obj, arr, key):
        "Recursively search for values of key in JSON tree."
        if isinstance(obj, dict):
            for k, v in obj.items():
                if isinstance(v, (dict, list)):
                    extract(v, arr, key)
                elif k == key:
                    arr.append(v)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)
        return arr

    results = extract(obj, arr, key)
    return results
#-----------------------------------------------------------------------------------------------------------------------
# Create function that adds centroid xy data as columns to data frame
def centroid_columns(geodataframe):
    geodataframe['centroid'] = geodataframe['geometry'].centroid
    # Create coordinate pairs of parcel centroids
    geodataframe['centroid'] = geodataframe['geometry'].centroid
    # Pull out the lat/long of the centroids
    geodataframe['centroid_lat'] = geodataframe['centroid'].y
    geodataframe['centroid_lon'] = geodataframe['centroid'].x
    return geodataframe
#-----------------------------------------------------------------------------------------------------------------------
# Create function that pulls the row items and creates a dictionary (CHANGE FIELDS IF NEEDED)
def create_address_dict(aRow):
    dictionary = {}
    dictionary['fulladdress'] = str(aRow['SITEADRESS']).title()
    dictionary['city'] = str(aRow['PLACENAME']).replace("CITY OF","").title()
    dictionary['addressnumber'] = str(aRow['ADDNUM'])
    dictionary['street'] = str(aRow['PREFIX']) + str(aRow['STREETNAME']).title() + str(aRow['STREETTYPE']).title()
    dictionary['streetname'] = str(aRow['PREFIX']) + str(aRow['STREETNAME']).title()
    dictionary['ZIP'] = str(aRow['ZIPCODE'])
    dictionary['state'] = str(aRow['STATE'])
    dictionary['completeaddress'] = str(aRow['SITEADRESS']).title() + ", " + str(aRow['PLACENAME']).replace("CITY OF","").title()
    return dictionary
#-----------------------------------------------------------------------------------------------------------------------
# Miles to meters function
def miles_to_meters(miles):
    try:
        return round(miles * 1609.344,0)
    except:
        return 0
#-----------------------------------------------------------------------------------------------------------------------
# Create web scrape function
def web_scrape(input_place_id, driver):
    output_dict = {}
    try:
        scrapeurl = f"https://www.google.com/maps/search/?api=1&query=Google&query_place_id={input_place_id}"
        driver.get(scrapeurl)
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'qBF1Pd')))

        # Find all elements with the class name 'W4Efsd' and filter them if necessary
        building_types = driver.find_elements(By.CLASS_NAME, 'W4Efsd')
        building_type_raw_list = building_types[1].text.split("·")
        output_dict["BuildingType"] = building_type_raw_list[0].strip()

        # Find all elements with the class name 'qBF1Pd' and filter them if necessary
        business_name = driver.find_element(By.CLASS_NAME, 'qBF1Pd')
        output_dict["BusinessName"] = business_name.text

    except Exception as e:
        print(f"Error processing address{e} at {scrapeurl}")
        output_dict["BuildingType"] = "No Google Business At Property"
        output_dict["BusinessName"] = "No Google Business At Property"
    return output_dict
#-----------------------------------------------------------------------------------------------------------------------
# Wait a random amount of time to help avoid being IP banned for web scraping
def wait_random():
    randint = random_integer = random.randint(7, 12)
    time.sleep(randint)
#-----------------------------------------------------------------------------------------------------------------------

# Convert the shapefile to a GeoPandas spatial data frame
parcels = geopandas.read_file(ParcelsPath)

# Set the coordinate system
parcels = parcels.to_crs("EPSG:4326")

# # Add temporary filter to isolate cases for testing purpose; later used to isolate land use types
# parcels = parcels[parcels.STATEID == '02720611140321014']

# Create a web driver
driver = webdriver.Chrome()

# Use the API key to create a client object
map_client = googlemaps.Client(API_KEY)

# Wait two seconds so the driver can open
time.sleep(2)

# Loop through the spatial data frame
for index, aRow in parcels.iterrows():

    # Create an address dictionary using the function
    add_dict = create_address_dict(aRow)

    # Inform the user of the address currently being worked on

    # Call the Google API with the parcel address to return a place code
    response = map_client.find_place(input=add_dict['completeaddress'],input_type="textquery")

    try:
        if extract_values(response,"status") != 'ZERO_RESULTS':
            business_dict = web_scrape(extract_values(response,'place_id')[0], driver)

        else:
            business_dict = {}
            business_dict["BuildingType"] = "No Results Found"
            business_dict["BusinessName"] = "No Results Found"

    except:
        business_dict = {}
        business_dict["BuildingType"] = "Failed to Open Web Page With Address"
        business_dict["BusinessName"] = "Failed to Open Web Page With Address"

    # Update the GeoDataFrame with the building type
    parcels.at[index, 'build_type'] = str(business_dict["BuildingType"])
    parcels.at[index, 'build_name'] = str(business_dict["BusinessName"])

    # Use the random wait function made earlier to wait a random amount of time
    wait_random()


# parcels.to_file(r"I:\Projects\2075_BeaverDamCity\10007_TransportationStudy\Data\output.shp")