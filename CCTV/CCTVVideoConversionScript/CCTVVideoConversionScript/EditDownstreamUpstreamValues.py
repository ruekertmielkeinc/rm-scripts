import pipes
import subprocess
import os
import xlsxwriter
import PyPDF2
from datetime import date, datetime
from pathlib import Path
from tkinter import *
from tkinter import messagebox
from tkinter import ttk


class EditDownstreamUpstreamValues:

    def __init__(self, pdfBaseName, tempDown, tempUp):
        
        self.pdfBaseName = pdfBaseName
        self.tempDown = tempDown
        self.tempUp = tempUp
        self.editDNID = StringVar()
        self.editUPID = StringVar()
        self.editDNReturnVal = ""
        self.editUPReturnVal = ""
        self.window = Toplevel()

        self.Editwindow = Frame(self.window, width=700, height=800)
        self.Editwindow.pack()
        self.Editwindow.grab_set()
        
        #Create Header
        self.header = Label(self.Editwindow, relief="ridge", text="CCTV Video Conversion and Excel Creator", font=15)
        self.header.pack(anchor=CENTER)

        #Add empty row
        self.emptyRow = Label(self.Editwindow, text=" \n ")
        self.emptyRow.pack()

        #Show user the current upstream and downstream ID values
        self.Alert = Label(self.Editwindow, text="***   Edit Upstream ID and Downstream ID  ***", font=('Helvetica 12 bold'))
        self.Alert.pack(anchor=CENTER)
        
        #Add empty row
        self.emptyRow = Label(self.Editwindow, text=" \n ")
        self.emptyRow.pack()
                
        #Frame to hold PDF file
        self.pdfFrame = Frame(self.Editwindow)
        self.pdfFrame.pack(anchor=CENTER, padx=(20,20))

        #Display PDF
        self.pdfLabel = Label(self.pdfFrame, text="PDF:  ")
        self.pdfLabel.pack(side=LEFT)
        self.pdfDisplay = Label(self.pdfFrame, text=self.pdfBaseName)
        self.pdfDisplay.pack(side=RIGHT, pady=3)
        
        #Frame to hold Downstream ID and Upstream ID
        self.EDNFrame = Frame(self.Editwindow)
        self.EDNFrame.pack(anchor=CENTER, padx=(20,20))

        #Display Downstream ID
        self.tempDNLabel = Label(self.EDNFrame, text="Downstream ID: ")
        self.tempDNLabel.pack(side=LEFT)
        self.editDNEntry = Entry(self.EDNFrame, textvariable=self.editDNID, width=40, bd=4)
        self.editDNEntry.insert(0, self.tempDown)
        self.editDNEntry.pack(side=RIGHT, pady=3)
                
        #Frame to hold Downstream ID and Upstream ID
        self.EUPFrame = Frame(self.Editwindow)
        self.EUPFrame.pack(anchor=CENTER, padx=(20,20))

        #Display Upstream ID
        self.tempUPLabel = Label(self.EUPFrame, text="Upstream ID: ")
        self.tempUPLabel.pack(side=LEFT)
        self.editUPEntry = Entry(self.EUPFrame, textvariable=self.editUPID, width=40, bd=4)
        self.editUPEntry.insert(0, self.tempUp)
        self.editUPEntry.pack(side=RIGHT, pady=3)
                
        #Add empty row
        self.emptyRow = Label(self.Editwindow, text=" \n ")
        self.emptyRow.pack()
        
        #Create a frame to hold Buttons
        self.buttonFrame = Frame(self.Editwindow, width="350")
        self.buttonFrame.pack(anchor=CENTER)

        #Create button controls
        self.VisuSewerButton = Button(self.buttonFrame, text="Submit", command=self.Submit)
        self.VisuSewerButton.pack(side=LEFT, padx=(25,5))

        self.ExpediterButton = Button(self.buttonFrame, text="Cancel", command=self.editCancel)
        self.ExpediterButton.pack(side=LEFT, padx=(5,5))
                        
        #Add empty row
        self.emptyRow = Label(self.Editwindow, text=" \n ")
        self.emptyRow.pack()

        self.Editwindow.wait_window()

    
    def Submit(self):
        self.editDNReturnVal = self.editDNID.get()
        self.editUPReturnVal = self.editUPID.get()
        self.window.destroy()

    
    def editCancel(self):
        self.Editwindow.destroy()
