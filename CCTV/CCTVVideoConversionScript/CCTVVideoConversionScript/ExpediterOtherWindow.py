import os
from tkinter import *
from tkinter import messagebox
from tkinter import ttk

#Local imports
from CCTVVideoConversionExpOther import *


class ExpediterOtherWindow:
    def __init__(self):
        self.inputVideoFilePath = StringVar()
        self.outputVideoFilePath = StringVar()
        self.inputPDFFilePath = StringVar()
        self.outputPDFFilePath = StringVar()
        self.excelFilePath = StringVar()
        self.window = Toplevel()
        self.correctFilePaths = False

        self.exp_Window = Frame(self.window, width=750, height=600)
        self.exp_Window.pack()
        self.exp_Window.grab_set()
        
        #Create Header
        self.header = Label(self.exp_Window, relief="ridge", text="CCTV Video Conversion and Excel Creator", font=15)
        self.header.pack(anchor=CENTER)

        #Add empty row between drop down and buttons
        self.emptyRow = Label(self.exp_Window, text=" \n ")
        self.emptyRow.pack()

        #Handle Video input and output file paths
        self.videoLabel = Label(self.exp_Window, text="Video File Paths")
        self.videoLabel.pack()

        #Create a frame to hold label and entries for video input
        self.inputVideoFrame = Frame(self.exp_Window)
        self.inputVideoFrame.pack(anchor=CENTER, padx=(25,25))

        #Label and entry box for video input
        self.videoInputLabel = Label(self.inputVideoFrame, text="Video Input: ")
        self.videoInputLabel.pack(side=LEFT)
        self.inputVideoEntry = Entry(self.inputVideoFrame, textvariable=self.inputVideoFilePath, width=100, bd=4)
        self.inputVideoEntry.pack(side=RIGHT, pady=3)

        #Create a frame to hold label and entries for video output
        self.outputVideoFrame = Frame(self.exp_Window)
        self.outputVideoFrame.pack(anchor=CENTER, padx=(25,25))

        #Label and entry box for video output
        self.videoOutputLabel = Label(self.outputVideoFrame, text="Video Output:")
        self.videoOutputLabel.pack(side=LEFT)
        self.outputVideoEntry = Entry(self.outputVideoFrame, textvariable=self.outputVideoFilePath, width=100, bd=4)
        self.outputVideoEntry.pack(side=RIGHT, pady=3)

        #Add an empty row
        self.emptyRow = Label(self.exp_Window, text=" \n ")
        self.emptyRow.pack()
        
        #Handle PDF input and output file paths
        self.pdfLabel = Label(self.exp_Window, text="PDF File Paths")
        self.pdfLabel.pack()

        #Create a frame to hold label and entries for pdf input
        self.inputPDFFrame = Frame(self.exp_Window)
        self.inputPDFFrame.pack(anchor=CENTER, padx=(25,25))

        #Label and entry box for pdf input
        self.pdfInputLabel = Label(self.inputPDFFrame, text="PDF Input: ")
        self.pdfInputLabel.pack(side=LEFT)
        self.inputPDFEntry = Entry(self.inputPDFFrame, textvariable=self.inputPDFFilePath, width=100, bd=4)
        self.inputPDFEntry.pack(side=RIGHT, pady=3)


        #Create a frame to hold label and entries for pdf output
        self.outputPDFFrame = Frame(self.exp_Window)
        self.outputPDFFrame.pack(anchor=CENTER, padx=(25,25))

        #Label and entry box for pdf output
        self.outputPDFLabel = Label(self.outputPDFFrame, text="PDF Output:")
        self.outputPDFLabel.pack(side=LEFT)
        self.outputPDFEntry = Entry(self.outputPDFFrame, textvariable=self.outputPDFFilePath, width=100, bd=4)
        self.outputPDFEntry.pack(side=RIGHT, pady=3)
        
        #Add an empty row
        self.emptyRow = Label(self.exp_Window, text=" \n ")
        self.emptyRow.pack()
        
        #Handle PDF input and output file paths
        self.excelTitleLabel = Label(self.exp_Window, text="Excel Spreadsheet Location File Path")
        self.excelTitleLabel.pack()

        #Create a frame to hold label and entries for excel spreadsheet
        self.excelFrame = Frame(self.exp_Window)
        self.excelFrame.pack(anchor=CENTER, padx=(25,25))

        #Label and entry box for pdf output
        self.excelLabel = Label(self.excelFrame, text="Excel Output:")
        self.excelLabel.pack(side=LEFT)
        self.excelEntry = Entry(self.excelFrame, textvariable=self.excelFilePath, width=100, bd=4)
        self.excelEntry.pack(side=RIGHT, pady=3)
        
        #Add an empty row
        self.emptyRow = Label(self.exp_Window, text=" \n ")
        self.emptyRow.pack()

        #Create a frame to hold Buttons
        self.buttonFrame = Frame(self.exp_Window, width="350")
        self.buttonFrame.pack(anchor=CENTER)
                
        #Create button controls
        self.submitButton = Button(self.buttonFrame, text="OK", command=self.CheckAllPathsOnSubmit)
        self.submitButton.pack(side=LEFT, padx=(25,5))

        self.clearAllButton = Button(self.buttonFrame, text="Clear All", command=self.ClearAllEntries)
        self.clearAllButton.pack(side=LEFT, padx=(5,5))

        self.cancelButton = Button(self.buttonFrame, text="Cancel", command=self.Cancel)
        self.cancelButton.pack(side=LEFT, padx=(5,25))

        
        #Add an empty row
        self.emptyRow = Label(self.exp_Window, text=" \n ")
        self.emptyRow.pack()

        self.window.wait_window()


    #Getter and Setter methods for UIComponent attributes
    def get_inputVideo(self):
        return self.inputVideo

    def set_inputVideo(self, path):
        self.inputVideo = path

    def get_outputVideo(self):
        return self.outputVideo

    def set_outputVideo(self, path):
        self.outputVideo = path
        
    def get_inputPDF(self):
        return self.inputPDF

    def set_inputPDF(self, path):
        self.inputPDF = path

    def get_outputPDF(self):
        return self.outputPDF

    def set_outputPDF(self, path):
        self.outputPDF = path
    
    def get_outputExcel(self):
        return self.outputExcel

    def set_outputExcel(self, path):
        self.outputExcel = path



#############  INDIVIDUAL FILE PATH CHECK METHODS  ###################
    #Check that video input file path is valid
    #To be valid, file path must exist and must contain files with the correct file extension (.mpg)... or others
    def VideoInputFilePathCheck(self):

        #variable to hold if input file path passes all checks
        #default is set to False
        correct = False

        #get video input file path from user input
        inputPath = self.inputVideoFilePath.get()

        #method that takes in inputFilePath and checks for files with proper file extension
        def checkCorrectInputVideoFilePath(inputPath):
            correctFilePath = False    
            for root, dirs, files in os.walk(inputPath):
                for file in files:
                   if file.endswith('.mpg'):
                        correctFilePath = True
                        break
            return correctFilePath

        #If correct file extensions are found, check if user input \ onto file path
        #If yes, continue.  If no, append \ to file path for proper HandBrake formatting
        if checkCorrectInputVideoFilePath(inputPath) == True:
                correct = True
                return correct
               
        #alert box pop up if file path is incorrect
        while checkCorrectInputVideoFilePath(inputPath) == False:
            answer = messagebox.askretrycancel(
                "Error", 
                "Input video file path must contain files with file path .mpg.  Please enter a valid video input file path."
                )
            if answer == True:
                self.inputVideoFilePath.set('')
                self.inputVideoEntry.delete(0, END)
                
            elif answer == False:
                self.window.destroy()
            break

    #Check that video output file path is valid
    #To be valid, file path simply needs to already exist on machine
    #Program will NOT create directory for user
    def VideoOutputFilePathCheck(self):

        #variable to hold if output file path passes all checks
        #default is set to False
        correct = False

        #get output file path that user input in UI
        outputFilePath = self.outputVideoFilePath.get()

        #If correct file extensions are found, check if user input \ onto file path
        #If yes, continue.  If no, append \ to file path for proper HandBrake formatting
        if os.path.isdir(outputFilePath) == True:
                correct = True
                return correct

        #check if output is valid filepath
        while os.path.isdir(outputFilePath) == False:
            answer = messagebox.askretrycancel(
                "Error", 
                "Output video file path must exist before entering file path.  Please create directory or choose an existing directory,"
               + "then input directory address."
                )
            if answer == True:
                self.outputVideo.set('')
                self.outputVideoEntry.delete(0, END)
                
            elif answer == False:
                self.window.destroy()
            break
    
    #Check that PDF input file path is valid
    #To be valid, file path must exist and must contain files with the correct file extension (.pdf)
    def PDFInputFilePathCheck(self):

        #variable to hold if input file path passes all checks
        #default is set to False
        correct = False

        #get video input file path from user input
        inputPath = self.inputPDFFilePath.get()

        #method that takes in inputFilePath and checks for files with proper file extension
        def checkCorrectInputPDFFilePath(inputPath):
            correctFilePath = False    
            for root, dirs, files in os.walk(inputPath):
                for file in files:
                    if file.endswith('.pdf'):
                        correctFilePath = True
                        break
            return correctFilePath

        #Append \ to file path if not already entered for proper Handbrake formatting
        if checkCorrectInputPDFFilePath(inputPath) == True:
                correct = True
                return correct

        #alert box pop up if file path is incorrect
        while checkCorrectInputPDFFilePath(inputPath) == False:
            answer = messagebox.askretrycancel(
                "Error", 
                "Input PDF file path must contain files with file extension .pdf.  Please enter a valid PDF input file path."
                )

            if answer == True:
                self.inputPDFFilePath.set('')
                self.inputPDFEntry.delete(0, END)
                break
            elif answer == False:
                self.window.destroy()
                break

    #Check that PDF output file path is valid
    #To be valid, file path simply needs to already exist on machine
    #Program will NOT create directory for user
    def PDFOutputFilePathCheck(self):

        #variable to hold if output file path passes all checks
        #default is set to False
        correct = False

        #get output pdf file path as entered by user in UI
        outputFilePath = self.outputPDFFilePath.get()

        #append \ to file path if not already entered for proper Handbrake formatting
        if os.path.isdir(outputFilePath) == True:
                correct = True
                return correct

        #check if output is valid filepath
        while os.path.isdir(outputFilePath) == False:
            answer = messagebox.askretrycancel(
                "Error", 
                "Output PDF file path must exist before entering file path."  
                + "Please create directory or choose an existing directory, then input directory address."
                )

            if answer == True:
                self.outputPDFFilePath.set('')
                self.outputPDFEntry.delete(0, END)
                break
            elif answer == False:
                self.window.destroy()
                break

    #Check that output file path for excel spreadsheet is valid
    #To be valid, file path simply needs to already exist
    #Program will NOT create directory for user
    def ExcelOutputFilePathCheck(self):
        
        #variable to hold if output file path passes all checks
        #default is set to False
        correct = False

        #get output file path that user input in UI
        excelFilePath = self.excelFilePath.get()

        #If correct file extensions are found, check if user input \ onto file path
        #If yes, continue.  If no, append \ to file path for proper HandBrake formatting
        if os.path.isdir(excelFilePath) == True:
                correct = True
                return correct

        #check if output is valid filepath
        while os.path.isdir(excelFilePath) == False:
            answer = messagebox.askretrycancel(
                "Error", 
                "Output Excel file path must exist before entering file path.  Please create directory or choose an existing directory,"
               + "then input directory address."
                )
            if answer == True:
                self.excelFilePath.set('')
                self.excelEntry.delete(0, END)
                
            elif answer == False:
                self.window.destroy()
            break
######################################################################


###########  CHECK RESULTS FROM EACH INDIVIDUAL FILE PATH METHOD  ############# 
    #When user clicks submit, this method runs all checks (methods above)
    #to verify that all file paths are correct before beginning conversion and excel operations
    def CheckAllPathsOnSubmit(self):
        if(self.VideoInputFilePathCheck() == True 
           and self.VideoOutputFilePathCheck() == True 
           and self.PDFInputFilePathCheck() == True 
           and self.PDFOutputFilePathCheck() == True
           and self.ExcelOutputFilePathCheck() == True
           ):
            #if all file paths are correct, change variable to TRUE
            self.correctFilePaths = True

            #when TRUE, place each path in a list that will be sent to getExpOtherData via a getter
            if self.correctFilePaths == True:
                global returnValues
                self.returnValues = [self.inputVideoFilePath, self.outputVideoFilePath, self.inputPDFFilePath, self.outputPDFFilePath, self.excelFilePath]
                self.getExpediterOtherData()
###############################################################################


#############  EXP/OTHER GET AND DISPLAY DATA IN WINDOW AS FINAL CHECK  ##############
    #Get data from Expediter/Other window and populate setters for final data submission
    def getExpediterOtherData(self):
        #assign local variables to their corresponding checked input vial the file path getters
        self.inputVideo, self.outputVideo, self.inputPDF, self.outputPDF, self.outputExcel  = self.getFilePaths()

        #Data is passed as a StringVar
        #Needs to be converted to string before going to CCTV class
        #After string conversion, each path is sent to its given setter method to 
        #finalize its place as the correct file path 
        #This is also where each file path checks to make sure they have the proper backslash
        #at their end.  One is appended if it doesn't already have a backslash

        #INPUT VIDEO FILE PATH SETTER
        self.inputVideoString = self.inputVideo.get()
        if self.inputVideoString[-1] != "\\":
            self.inputVideoString += "\\"
            self.set_inputVideo(self.inputVideoString)
        else:
            self.set_inputVideo(self.inputVideoString)

        #OUTPUT VIDEO FILE SETTER
        self.outputVideoString = self.outputVideo.get()
        if self.outputVideoString[-1] != "\\":
            self.outputVideoString += "\\"
            self.set_outputVideo(self.outputVideoString)
        else:
            self.set_outputVideo(self.outputVideoString)

        #INPUT PDF FILE SETTER
        self.inputPDFString = self.inputPDF.get()
        if self.inputPDFString[-1] != "\\":
            self.inputPDFString += "\\"
            self.set_inputPDF(self.inputPDFString)
        else:
            self.set_inputPDF(self.inputPDFString)

        #OUTPUT PDF FILE SETTER
        self.outputPDFString = self.outputPDF.get()
        if self.outputPDFString[-1] != "\\":
            self.outputPDFString += "\\"
            self.set_outputPDF(self.outputPDFString)
        else:
            self.set_outputPDF(self.outputPDFString)

        #OUTPUT EXCEL FILE SETTER
        self.outputExcelString = self.outputExcel.get()
        if self.outputExcelString[-1] != "\\":
            self.outputExcelString += "\\"
            self.set_outputExcel(self.outputExcelString)
        else:
            self.set_outputExcel(self.outputExcelString)

        #Call clear window method to reset Exp/Other window for display of all user input
        self.ClearWindow()

        #call display method to display all input
        self.displayAllUserInputExpOther()

    #Display all info as input by user for expediter or other
    def displayAllUserInputExpOther(self):

        self.inputLabel = Label(self.exp_Window, relief="ridge", text="**Here are the file paths you have entered.  Double check for accuracy and then submit**", font='Helvetica 14 bold')
        self.inputLabel.pack(anchor=CENTER, padx=20)

        #Create a frame to hold labels for video input data
        self.inVideoFrame = Frame(self.exp_Window)
        self.inVideoFrame.pack(anchor=CENTER, padx=(25,25))

        #Video Input by user displayed
        self.videoInputLabelTag = Label(self.inVideoFrame, text="Video Input: ", font=('Helvetica 12 bold'))
        self.videoInputLabelTag.pack(side=LEFT)
        self.inputVideoLabel = Label(self.inVideoFrame, text=self.get_inputVideo())
        self.inputVideoLabel.pack(side=RIGHT, pady=3)

        #Create a frame to hold labels for video output data
        self.outVideoFrame = Frame(self.exp_Window)
        self.outVideoFrame.pack(anchor=CENTER, padx=(25,25))

        #Video Output by user displayed
        self.videoOutputLabelTag = Label(self.outVideoFrame, text="Video Output: ", font=('Helvetica 12 bold'))
        self.videoOutputLabelTag.pack(side=LEFT)
        self.outputVideoLabel = Label(self.outVideoFrame, text=self.get_outputVideo())
        self.outputVideoLabel.pack(side=RIGHT, pady=3)
        
        #Create a frame to hold labels for pdf input data
        self.inPDFFrame = Frame(self.exp_Window)
        self.inPDFFrame.pack(anchor=CENTER, padx=(25,25))

        #PDF Input by user displayed
        self.pdfInputLabelTag = Label(self.inPDFFrame, text="PDF Input: ", font=('Helvetica 12 bold'))
        self.pdfInputLabelTag.pack(side=LEFT)
        self.pdfInputLabel = Label(self.inPDFFrame, text=self.get_inputPDF())
        self.pdfInputLabel.pack(side=RIGHT, pady=3)

        
        #Create a frame to hold labels for pdf output data
        self.outPDFFrame = Frame(self.exp_Window)
        self.outPDFFrame.pack(anchor=CENTER, padx=(25,25))

        #PDF output by user displayed
        self.pdfOutputLabelTag = Label(self.outPDFFrame, text="PDF Output: ", font=('Helvetica 12 bold'))
        self.pdfOutputLabelTag.pack(side=LEFT)
        self.pdfOutputLabel = Label(self.outPDFFrame, text=self.get_outputPDF())
        self.pdfOutputLabel.pack(side=RIGHT, pady=3)
                
        #Create a frame to hold labels for pdf output data
        self.excelFrame = Frame(self.exp_Window)
        self.excelFrame.pack(anchor=CENTER, padx=(25,25))

        #PDF output by user displayed
        self.excelOutputLabelTag = Label(self.excelFrame, text="Excel Output: ", font=('Helvetica 12 bold'))
        self.excelOutputLabelTag.pack(side=LEFT)
        self.excelOutputLabel = Label(self.excelFrame, text=self.get_outputExcel())
        self.excelOutputLabel.pack(side=RIGHT, pady=3)

        #Add an empty row
        self.emptyRow = Label(self.exp_Window, text=" \n ")
        self.emptyRow.pack()

        #Create a frame to hold Buttons
        self.expButtonFrame = Frame(self.exp_Window, width="350")
        self.expButtonFrame.pack(anchor=CENTER)

        #Create button controls
        self.submitVisuButton = Button(self.expButtonFrame, text="Submit Data", command=self.submitExpediterOther)
        self.submitVisuButton.pack(side=LEFT, padx=(25,5))

        #Add an empty row
        self.emptyRow = Label(self.exp_Window, text=" \n ")
        self.emptyRow.pack()

    #Submit data to CCTV class
    def submitExpediterOther(self):
        #close window
        self.window.destroy()

        #submit input to CCTV class
        self.cctv = CCTVconversionExpOther(self.get_inputVideo(), self.get_outputVideo(), self.get_inputPDF(), self.get_outputPDF(), self.get_outputExcel())
        self.cctv.VideoConversion()
        self.cctv.ExcelCreator()
        self.cctv.DisplayUnmatchedData()
        shutil.copytree(self.get_inputPDF(), self.get_outputPDF(), dirs_exist_ok = True)

    #return List of file paths
    def getFilePaths(self):
        return self.returnValues

        #Clear other buttons from TeleProviderWindow
    def ClearWindow(self):

        #Use pack_forget() method to clear each individual widget from window 
        self.videoLabel.pack_forget()
        self.inputVideoFrame.pack_forget()
        self.videoInputLabel.pack_forget()
        self.inputVideoEntry.pack_forget()
        self.outputVideoFrame.pack_forget()
        self.videoOutputLabel.pack_forget()
        self.outputVideoEntry.pack_forget()
        self.pdfLabel.pack_forget()
        self.inputPDFFrame.pack_forget()
        self.pdfInputLabel.pack_forget()
        self.inputPDFEntry.pack_forget()
        self.outputPDFFrame.pack_forget()
        self.outputPDFLabel.pack_forget()
        self.outputPDFEntry.pack_forget()
        self.excelTitleLabel.pack_forget()
        self.excelFrame.pack_forget()
        self.excelLabel.pack_forget()
        self.excelEntry.pack_forget()
        self.emptyRow.pack_forget()
        self.buttonFrame.pack_forget()
        self.submitButton.pack_forget()
        self.clearAllButton.pack_forget()
        self.cancelButton.pack_forget()
        self.emptyRow.pack_forget()
########################################################################################


################  CLEAR ALL AND CANCEL BUTTON METHODS  ###################
    #method to clear all entry boxes when user clicks Clear All button
    def ClearAllEntries(self):
        #Delete all entries in UI
        self.inputVideoEntry.delete(0, END)
        self.outputVideoEntry.delete(0, END)
        self.inputPDFEntry.delete(0, END)
        self.outputPDFEntry.delete(0, END)
        self.excelEntry.delete(0, END)

    #Cancel application when user clicks Cancel button
    def Cancel(self):
        self.window.destroy()
        del self.window
##########################################################################




