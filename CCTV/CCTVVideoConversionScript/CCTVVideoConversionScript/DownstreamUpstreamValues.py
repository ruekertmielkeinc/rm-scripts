import pipes
import subprocess
import os
import xlsxwriter
import PyPDF2
from datetime import date, datetime
from pathlib import Path
from tkinter import *
from tkinter import messagebox
from tkinter import ttk

from EditDownstreamUpstreamValues import *


class DownstreamUpstreamValues:
    def __init__(self, tempDown, tempUp, pdfName):
        self.tempDown = tempDown
        self.tempUp = tempUp
        self.pdfName = pdfName
        self.DownstreamID = ""
        self.UpstreamID = ""
        self.pdfBaseName = os.path.basename(self.pdfName)
        self.window = Toplevel()

        self.DNUPwindow = Frame(self.window, width=700, height=800)
        self.DNUPwindow.pack()
        self.DNUPwindow.grab_set()
        
        #Create Header
        self.header = Label(self.DNUPwindow, relief="ridge", text="CCTV Video Conversion and Excel Creator", font=15)
        self.header.pack(anchor=CENTER)

        #Add empty row
        self.emptyRow = Label(self.DNUPwindow, text=" \n ")
        self.emptyRow.pack()

        #Show user the current upstream and downstream ID values
        self.Alert = Label(self.DNUPwindow, text="***   Upstream ID and Downstream ID values and the PDF file source ***", font=('Helvetica 12 bold'))
        self.Alert.pack(anchor=CENTER)

        #Alert description
        self.alertDesc = Label(self.DNUPwindow, text="The Upstream or Downstream ID listed below may not be correct.  Click Edit to manually enter new ID numbers or click Submit if these Ids are correct.")
        self.alertDesc.pack(anchor=CENTER)
        
        #Add empty row
        self.emptyRow = Label(self.DNUPwindow, text=" \n ")
        self.emptyRow.pack()
                        
        #Frame to hold PDF file
        self.pdfFrame = Frame(self.DNUPwindow)
        self.pdfFrame.pack(anchor=CENTER, padx=(20,20))
        
        #Display PDF
        self.pdfLabel = Label(self.pdfFrame, text="PDF:  ", font=('Helvetica 8 bold'))
        self.pdfLabel.pack(side=LEFT)
        self.pdfDisplay = Label(self.pdfFrame, text=self.pdfBaseName)
        self.pdfDisplay.pack(side=RIGHT, pady=3)
        
        #Frame to hold Downstream ID
        self.DNFrame = Frame(self.DNUPwindow)
        self.DNFrame.pack(anchor=CENTER, padx=(20,20))

        #Display Downstream ID
        self.tempDNLabel = Label(self.DNFrame, text="Downstream ID: ", font=('Helvetica 8 bold'))
        self.tempDNLabel.pack(side=LEFT)
        self.tempDNoutput = Label(self.DNFrame, text=self.tempDown)
        self.tempDNoutput.pack(side=RIGHT, pady=3)

        #Frame to hold Upstream ID
        self.UPFrame = Frame(self.DNUPwindow)
        self.UPFrame.pack(anchor=CENTER, padx=(20,20))

        #Display Upstream ID
        self.tempUPLabel = Label(self.UPFrame, text="Upstream ID: ", font=('Helvetica 8 bold'))
        self.tempUPLabel.pack(side=LEFT)
        self.tempUPLabel = Label(self.UPFrame, text=self.tempUp)
        self.tempUPLabel.pack(side=RIGHT, pady=3)
                
        #Add empty row
        self.emptyRow = Label(self.DNUPwindow, text=" \n ")
        self.emptyRow.pack()
        
        #Create a frame to hold Buttons
        self.buttonFrame = Frame(self.DNUPwindow, width="350")
        self.buttonFrame.pack(anchor=CENTER)

        #Create button controls
        self.VisuSewerButton = Button(self.buttonFrame, text="Submit", command=self.SubmitUPDNValues)
        self.VisuSewerButton.pack(side=LEFT, padx=(25,5))

        self.ExpediterButton = Button(self.buttonFrame, text="Edit", command=self.editValues)
        self.ExpediterButton.pack(side=LEFT, padx=(5,5))
                        
        #Add empty row
        self.emptyRow = Label(self.DNUPwindow, text=" \n ")
        self.emptyRow.pack()

        self.DNUPwindow.wait_window()
 
    def SubmitUPDNValues(self):
        self.DownstreamID = self.tempDown
        self.UpstreamID = self.tempUp

        self.window.destroy()
    
    def editValues(self):
        editWindow = EditDownstreamUpstreamValues(self.pdfBaseName, self.tempDown, self.tempUp)
        self.DownstreamID = editWindow.editDNReturnVal
        self.UpstreamID = editWindow.editUPReturnVal

        self.window.destroy()
            
    def Cancel(self):
        self.window.destroy()





            
        




