import pipes
import subprocess
import os
import xlsxwriter
import PyPDF2
from datetime import date
from pathlib import Path
from tkinter import *
from tkinter import messagebox
from tkinter import ttk

#Local imports
from ExpediterOtherWindow import *
from VisuSewerWindow import *
from TPInputWindow import *


class TeleProviderChoiceWindow:
    def __init__(self, master):
        self.master = master

        #create Teleprovider window frame
        self.teleproviderFrame = Frame(self.master, width=700, height=500)
        self.teleproviderFrame.pack()

        #Create Header
        self.header = Label(self.teleproviderFrame, relief="ridge", text="CCTV Video Conversion and Excel Creator", font=15)
        self.header.pack(anchor=CENTER, padx=50)

        #Add an empty row
        self.emptyRow = Label(self.teleproviderFrame, text=" \n ")
        self.emptyRow.pack()

        #Create a frame to hold Buttons
        self.buttonFrame = Frame(self.teleproviderFrame, width="350")
        self.buttonFrame.pack(anchor=CENTER)

        #Create button controls
        self.VisuSewerButton = Button(self.buttonFrame, text="Visu-Sewer", command=self.openTPInputWindow)
        self.VisuSewerButton.pack(side=LEFT, padx=(25,5))

        self.ExpediterButton = Button(self.buttonFrame, text="Expediters", command=self.openExpWindow)
        self.ExpediterButton.pack(side=LEFT, padx=(5,5))
        
        self.SpeedyCleanButton = Button(self.buttonFrame, text="Speedy-Clean", command=self.openSCWindow)
        self.SpeedyCleanButton.pack(side=LEFT, padx=(5,5))

        self.OtherButton = Button(self.buttonFrame, text="Other", command=self.openTPInputWindow)
        self.OtherButton.pack(side=LEFT, padx=(5,5))

        self.cancelButton = Button(self.buttonFrame, text="Exit", command=self.Cancel)
        self.cancelButton.pack(side=LEFT, padx=(5,25))

        #Add an empty row
        self.emptyRow = Label(self.master, text=" \n ")
        self.emptyRow.pack()

    #Open TPInputWindow and send VisuSewer variable
    def openTPInputWindow(self):
        TPInputWindow("VisuSewer")

    #Open Visu-Sewer window when user clicks Visu-Sewer window
    def openExpWindow(self):
        TPInputWindow("ExpOther")
    
    #Open Speedy-Clean window when user clicks Speedy-Clean window
    def openSCWindow(self):
        TPInputWindow("SpeedyClean")

    #Open Expediter/Other window when user clicks Exediter or Other button
    def openExpWindow(self):
        TPInputWindow("ExpOther")

    #Cancel application when user clicks Cancel button
    def Cancel(self):
        self.master.destroy()







