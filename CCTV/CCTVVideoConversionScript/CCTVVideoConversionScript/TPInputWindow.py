import os
from tkinter import *
from tkinter import messagebox
from tkinter import ttk

from CCTV_ExcelVideoConversion import *


class TPInputWindow:
    def __init__(self, teleProvider):
        self.inputVideoFilePath = StringVar()
        self.inputPDFFilePath = StringVar()
        self.outputFilePath = StringVar()
        self.window = Toplevel()
        self.teleProvider = teleProvider
        

        self.TP_Window = Frame(self.window, width=750, height=600)
        self.TP_Window.pack()
        self.TP_Window.grab_set()

        #Create Header
        self.header = Label(self.TP_Window, relief="ridge", text="CCTV Video Conversion and Excel Creator", font=15)
        self.header.pack(anchor=CENTER)

        #Add empty row
        self.emptyRow = Label(self.TP_Window, text=" \n ")
        self.emptyRow.pack()

        #Handle input file paths
        self.inputLabel = Label(self.TP_Window, text="Input File Paths", font=('Helvetica 16 bold'))
        self.inputLabel.pack()
        
        #Instruction label for input parameters
        self.instructLabel = Label(self.TP_Window, text="***  Please enter file paths for CCTV videos that need to \nbe converted "
        + "and the file path for their corresponding PDF files  ***", font=('Helvetica 12 bold'))
        self.instructLabel.pack()
        
        #Add an empty row
        self.emptyRow = Label(self.TP_Window, text=" \n ")
        self.emptyRow.pack()

        #Create a frame to hold label and entries for video input
        self.inputVideoFrame = Frame(self.TP_Window)
        self.inputVideoFrame.pack(anchor=CENTER, padx=(25,25))

        #Label and entry box for video input
        self.videoInputLabel = Label(self.inputVideoFrame, text="Video Input: ")
        self.videoInputLabel.pack(side=LEFT)
        self.inputVideoEntry = Entry(self.inputVideoFrame, textvariable=self.inputVideoFilePath, width=100, bd=4)
        self.inputVideoEntry.pack(side=RIGHT, pady=3)
        
        #Create a frame to hold label and entries for pdf input
        self.inputPDFFrame = Frame(self.TP_Window)
        self.inputPDFFrame.pack(anchor=CENTER, padx=(25,25))

        #Label and entry box for pdf input
        self.pdfInputLabel = Label(self.inputPDFFrame, text="PDF Input: ")
        self.pdfInputLabel.pack(side=LEFT)
        self.inputPDFEntry = Entry(self.inputPDFFrame, textvariable=self.inputPDFFilePath, width=100, bd=4)
        self.inputPDFEntry.pack(side=RIGHT, pady=3)
        
        #Add an empty row
        self.emptyRow = Label(self.TP_Window, text=" \n ")
        self.emptyRow.pack()
        
        #Handle output file paths
        self.outputLabel = Label(self.TP_Window, text="Output File Path", font=('Helvetica 16 bold'))
        self.outputLabel.pack()

        #Output instruction label 
        self.outInstructLabel = Label(self.TP_Window, text="***  Please enter file path for CCTV video and PDF file "
        + "output.\nThe path you enter must contain an existing folder which will be the root folder for all output."
        + " \nThis program will create a folder structure within this root folder to hold all the processed output  ***", font=('Helvetica 12 bold'))
        self.outInstructLabel.pack()
                
        #Add an empty row
        self.emptyRow = Label(self.TP_Window, text=" \n ")
        self.emptyRow.pack()

        #Create a frame to hold label and entries for output
        self.outputFrame = Frame(self.TP_Window)
        self.outputFrame.pack(anchor=CENTER, padx=(25,25))

        #Label and entry box for output
        self.OutputLabel = Label(self.outputFrame, text="Output: ")
        self.OutputLabel.pack(side=LEFT)
        self.outputEntry = Entry(self.outputFrame, textvariable=self.outputFilePath, width=100, bd=4)
        self.outputEntry.pack(side=RIGHT, pady=3)

        #Add an empty row
        self.emptyRow = Label(self.TP_Window, text=" \n ")
        self.emptyRow.pack()

        #Create a frame to hold Buttons
        self.buttonFrame = Frame(self.TP_Window, width="350")
        self.buttonFrame.pack(anchor=CENTER)
        
        #Add empty row
        self.emptyRow = Label(self.TP_Window, text=" \n ")
        self.emptyRow.pack()

        #Create button controls
        self.submitButton = Button(self.buttonFrame, text="OK", command=self.CheckAllPathsOnSubmit)
        self.submitButton.pack(side=LEFT, padx=(25,5))

        self.clearAllButton = Button(self.buttonFrame, text="Clear All", command=self.ClearAllEntries)
        self.clearAllButton.pack(side=LEFT, padx=(5,5))

        self.cancelButton = Button(self.buttonFrame, text="Cancel", command=self.Cancel)
        self.cancelButton.pack(side=LEFT, padx=(5,25))

        #Add empty row
        self.emptyRow = Label(self.TP_Window, text=" \n ")
        self.emptyRow.pack()

        self.window.wait_window()

    #Getter and Setter methods for UIComponent attributes
    def get_inputVideo(self):
        return self.inputVideo

    def set_inputVideo(self, path):
        self.inputVideo = path

    def get_inputPDF(self):
        return self.inputPDF

    def set_inputPDF(self, path):
        self.inputPDF = path

    def get_outputPath(self):
        return self.outputPath

    def set_outputPath(self, path):
        self.outputPath = path


##########  INDIVIDUAL FILE PATH CHECK METHODS  #################
    #Check that video input file path is valid
    #To be valid, file path must exist and must contain files with the correct file extension (.mpg)... or others
    def VideoInputFilePathCheck(self):

        #variable to hold if input file path passes all checks
        #default is set to False
        correct = False

        #get video input file path from user input
        inputPath = self.inputVideoFilePath.get()

        #method that takes in inputFilePath and checks for files with proper file extension
        def checkCorrectInputVideoFilePath(inputPath):
            correctFilePath = False    
            for root, dirs, files in os.walk(inputPath):
                for file in files:
                   if file.endswith('.mpg') or file.endswith('.MPG') or file.endswith('.avi') or file.endswith('.AVI'):
                        correctFilePath = True
                        break
            return correctFilePath
               
        #alert box pop up if file path is incorrect
        if checkCorrectInputVideoFilePath(inputPath) == False:
            answer = messagebox.askretrycancel(
                "Error", 
                "Input video file path must contain files with correct file extension.  Please enter a valid video input file path."
                )
            if answer == True:
                self.inputVideoFilePath.set('')
                self.inputVideoEntry.delete(0, END)
                
            elif answer == False:
                self.window.destroy()

        #if checkCorrect !False
        else:
            correct = True
            return correct  
    
    #Check that PDF input file path is valid
    #To be valid, file path must exist and must contain files with the correct file extension (.pdf)
    def PDFInputFilePathCheck(self):

        #variable to hold if input file path passes all checks
        #default is set to False
        correct = False

        #get video input file path from user input
        inputPath = self.inputPDFFilePath.get()

        #method that takes in inputFilePath and checks for files with proper file extension
        def checkCorrectInputPDFFilePath(inputPath):
            correctFilePath = False    
            for root, dirs, files in os.walk(inputPath):
                for file in files:
                    if file.endswith('.pdf') or file.endswith('.PDF'):
                        correctFilePath = True
                        break
            return correctFilePath

        #alert box pop up if file path is incorrect
        if checkCorrectInputPDFFilePath(inputPath) == False:
            answer = messagebox.askretrycancel(
                "Error", 
                "Input PDF file path must contain files with file extension .pdf.  Please enter a valid PDF input file path."
                )

            if answer == True:
                self.inputPDFFilePath.set('')
                self.inputPDFEntry.delete(0, END)
            elif answer == False:
                self.window.destroy()

        #correct file path is true
        else:
            correct = True
            return correct

    #Check that output file path is valid
    #To be valid, file path simply needs to already exist on machine
    #Program will NOT create directory for user at this point in program
    def OutputFilePathCheck(self):

        #variable to hold if output file path passes all checks
        #default is set to False
        correct = False

        #get output file path that user input in UI
        outputFilePath = self.outputFilePath.get()

        #check if output is valid filepath
        if os.path.isdir(outputFilePath) == False:
            answer = messagebox.askretrycancel(
                "Error", 
                "Output video file path must exist before entering file path.  Please create directory or choose an existing directory,"
               + "then input directory address."
                )
            if answer == True:
                self.outputFilePath.set('')
                self.outputEntry.delete(0, END)
                
            elif answer == False:
                self.window.destroy()

        #file path passes check
        else:
            correct = True
            return correct
#################################################################


#######  CHECK RESULTS FROM EACH INDIVIDUAL FILE PATH METHOD  ########
    #When user clicks submit, this method runs all checks (methods above)
    #to verify that all file paths are correct before beginning conversion and excel operations
    def CheckAllPathsOnSubmit(self):
        #initialize variable to false
        self.correctFilePaths = False

        if(self.VideoInputFilePathCheck() == True 
           and self.PDFInputFilePathCheck() == True 
           and self.OutputFilePathCheck() == True 
           ):
            #if all file paths are correct change variable to TRUE
            self.correctFilePaths = True

            #when variable is TRUE, place each path in a list that will then be sent
            #to getVisuSewerData method via a getter method
            if self.correctFilePaths == True:
                self.returnValues = [self.inputVideoFilePath, self.inputPDFFilePath, self.outputFilePath]
                self.getVisuSewerInputData(self.returnValues)
######################################################################



####### GET AND DISPLAY DATA IN WINDOW AS FINAL CHECK ###########
    #Get data from window and populate setters for final data submission
    def getVisuSewerInputData(self, returnValuesList):
        #Assign local variables to their corresponding checked input via the file paths getter method
        self.inputVideo, self.inputPDF, self.outputPath  = returnValuesList

        #Data is passed as a StringVar
        #Needs to be converted to string before going to CCTV class
        #After string conversion, each path is sent to its given setter method to 
        #finalize its place as the correct file path 
        #This is also where each file path checks to make sure they have the proper backslash
        #at their end.  One is appended if it doesn't already have a backslash

        #1/25/23
       #Updated to reduce redundancy.  String conversion and \ addition are now done in separate method

        #INPUT VIDEO FILE PATH SETTER
        self.set_inputVideo(self.addBackSlashConvertToString(self.inputVideo))

        #INPUT PDF FILE SETTER
        self.set_inputPDF(self.addBackSlashConvertToString(self.inputPDF))

        #OUTPUT FILE PATH SETTER
        self.set_outputPath(self.addBackSlashConvertToString(self.outputPath))

        #Call clear window method to reset VS window for display of all user inputs
        self.ClearWindow()

        #call display method to display all input
        self.displayAllUserInput()

    #Display all info as input by user
    def displayAllUserInput(self):

        self.InputWindow = Frame(self.window, width=750, height=600)
        self.InputWindow.pack()
        self.InputWindow.grab_set()
        
        #Final Check window for user to look over paths that they've input
        self.inputLabel = Label(self.InputWindow, relief="ridge", text="**Here are the file paths you have entered.  Double check for accuracy and then submit**", font='Helvetica 14 bold')
        self.inputLabel.pack(anchor=CENTER, padx=20)
        
        #Add an empty row
        self.emptyRow = Label(self.InputWindow, text=" \n ")
        self.emptyRow.pack()

        #Create a frame to hold labels for video input data
        self.inputDisplayFrame = Frame(self.InputWindow)
        self.inputDisplayFrame.pack(anchor=CENTER, padx=(25,25))

        #Video Input by user displayed
        self.videoInputLabelTag = Label(self.inputDisplayFrame, text="Video Input: ", font=('Helvetica 12 bold'))
        self.videoInputLabelTag.pack(side=LEFT)
        self.inputVideoLabel = Label(self.inputDisplayFrame, text=self.get_inputVideo())
        self.inputVideoLabel.pack(side=RIGHT, pady=3)
        
        self.inputDisplayPDFFrame = Frame(self.InputWindow)
        self.inputDisplayPDFFrame.pack(anchor=CENTER, padx=(25,25))
        #PDF Input by user displayed
        self.pdfInputLabelTag = Label(self.inputDisplayPDFFrame, text="PDF Input: ", font=('Helvetica 12 bold'))
        self.pdfInputLabelTag.pack(side=LEFT)
        self.pdfInputLabel = Label(self.inputDisplayPDFFrame, text=self.get_inputPDF())
        self.pdfInputLabel.pack(side=RIGHT, pady=3)
        
        #Add an empty row
        self.emptyRow = Label(self.InputWindow, text=" \n ")
        self.emptyRow.pack()

        #Create a frame to hold labels for output data
        self.outputFrame = Frame(self.InputWindow)
        self.outputFrame.pack(anchor=CENTER, padx=(25,25))

        #Output Path by user displayed
        self.outputLabelTag = Label(self.outputFrame, text="Output Path: ", font=('Helvetica 12 bold'))
        self.outputLabelTag.pack(side=LEFT)
        self.outputLabel = Label(self.outputFrame, text=self.get_outputPath())
        self.outputLabel.pack(side=RIGHT, pady=3)

        #Add an empty row
        self.emptyRow = Label(self.InputWindow, text=" \n ")
        self.emptyRow.pack()
        
        #Create a frame to hold Buttons
        self.ButtonFrame = Frame(self.InputWindow, width="350")
        self.ButtonFrame.pack(anchor=CENTER)

        #Create Submit button
        self.submitButton = Button(self.ButtonFrame, text="Submit Data", command=self.submitData)
        self.submitButton.pack(side=LEFT, padx=(25,5))

        #Create Back button
        self.backButton = Button(self.ButtonFrame, text="Back", command=self.BackButton)
        self.backButton.pack(side=RIGHT, padx=(5,25))

        #Add an empty row
        self.emptyRow = Label(self.InputWindow, text=" \n ")
        self.emptyRow.pack()

    #Submit data to CCTV class and run CCTV methods
    def submitData(self):
        #close window
        self.window.destroy()

        #submit input to CCTV class
        self.cctv = CCTV_ExcelVideoConversion(self.get_inputVideo(), self.get_inputPDF(), self.get_outputPath(), self.teleProvider)
        self.cctv.CreateFolderStructure()
        self.cctv.PopulateVideoList()
        self.cctv.PopulatePDFList()
        self.cctv.VideoToPDF_Matcher()
        self.cctv.VideoConversion()
        self.cctv.ExcelSpreadSheetCreator()
        self.cctv.OrderedListExcelPopulator()
        self.cctv.UnOrderedListExcelPopulator()
        self.cctv.CloseExcelWorkbook()
        self.cctv.DisplayUnmatchedData()
        self.cctv.PassPDFinputToOutputFolders()

    def BackButton(self):
        #destroy display input window
        self.window.destroy()

        if self.teleProvider == "VisuSewer":
            TPInputWindow("VisuSewer")

        elif self.teleProvider == "SpeedyClean":
            TPInputWindow("SpeedyClean")

        else:
            TPInputWindow("ExpOther")


        #delete and reset all input fields
        #self.inputPDFFilePath.set('')
        #self.inputPDFEntry.delete(0, END)

        #self.inputVideoFilePath.set('')
        #self.inputVideoEntry.delete(0, END)

        #self.outputFilePath.set('')
        #self.outputEntry.delete(0, END)
    #method that checks for existing \ on end of file path and appends one if not there
    #also converts StringVar to string
    def addBackSlashConvertToString(self, returnValueStringVar):
        convertedString = returnValueStringVar.get()
        if convertedString[-1] != "\\":
            convertedString += "\\"
        return convertedString

    #Clear all widgets from window
    #1/25/23
    #Updated to loop through all children widgets and destroy all
    def ClearWindow(self):
        for widgets in self.window.winfo_children():
            widgets.destroy()
##############################################################################


#############  CLEAR ALL AND CANCEL BUTTON METHODS  ###############
    #method to clear all entry boxes when user clicks Clear All button
    def ClearAllEntries(self):
        #Delete all entries in UI
        self.inputVideoEntry.delete(0, END)
        self.inputPDFEntry.delete(0, END)
        self.outputEntry.delete(0, END)

    #Cancel application when user clicks Cancel button
    def Cancel(self):
        self.window.destroy()
        del self.window
###################################################################
        


    



