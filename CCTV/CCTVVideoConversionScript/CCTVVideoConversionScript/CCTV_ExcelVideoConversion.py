import pipes
import subprocess
import os
import xlsxwriter
import PyPDF2
from datetime import date, datetime
from pathlib import Path
from tkinter import *
from tkinter import messagebox
from tkinter import ttk
import shutil
import Pipe

from DownstreamUpstreamValues import *



class CCTV_ExcelVideoConversion:
    #Constructor
    def __init__(self, inputVideoPath, inputPDFPath, outputPath, teleProvider):
        self.inputVideoPath = inputVideoPath
        self.inputPDFPath = inputPDFPath
        self.outputPath = outputPath
        self.teleProvider = teleProvider
        self.window = Toplevel()

        #All lists to be used throughout class
        self.allVideoFiles = list()
        self.allPDFFiles = list()  
        self.orderedVideoList = list()
        self.orderedPDFList = list()
        self.unorderedVideoList = list()
        self.unorderedPDFList = list()
        self.pipeList = list()


    #create folder structure to be used for storing all files
    def CreateFolderStructure(self):
        videoOutputFolder = "VideoOutput"
        pdfOutputFolder = "PDFOutput"
        matchedFiles = "MatchedFiles"
        unMatchedFiles = "UnMatchedFiles"
        excelOutputFolder = "ExcelOutput"

        #create video matched and unmatched folders within video output folder
        self.matchedVideosFilePath = os.path.join(self.outputPath, videoOutputFolder, matchedFiles, "")
        self.unmatchedVideosFilePath = os.path.join(self.outputPath, videoOutputFolder, unMatchedFiles, "")

        #create pdf matched and unmatched folders within pdf output folder
        self.matchedPDFsFilePath = os.path.join(self.outputPath, pdfOutputFolder, matchedFiles, "")
        self.unmatchedPDFsFilePath = os.path.join(self.outputPath, pdfOutputFolder, unMatchedFiles, "")

        #create excel output folder path
        self.excelOutputFilePath = os.path.join(self.outputPath, excelOutputFolder, "")

        #make all file paths into directories
        os.makedirs(self.matchedVideosFilePath)
        os.makedirs(self.matchedPDFsFilePath)
        os.makedirs(self.unmatchedVideosFilePath)
        os.makedirs(self.unmatchedPDFsFilePath)
        os.makedirs(self.excelOutputFilePath)
#########  ADD ALL VIDEO AND ALL PDF FILES TO THEIR APPROPRIATE LISTS  ###########
    #Populate allVideoFiles list by walking through input video file path 
    #and finding all files with correct file extension
    def PopulateVideoList(self):
        for root, dirs, files in os.walk(self.inputVideoPath):
            for file in files:
                if file.endswith('.mpg') or file.endswith('.MPG') or file.endswith('.avi') or file.endswith('.AVI'):
                    self.allVideoFiles.append(os.path.join(root, file))
    
    #Populate allPDFFiles list by walking through input PDF file path
    #and finding all files with correct file extension
    def PopulatePDFList(self):
        for root, dirs, files in os.walk(self.inputPDFPath):
            for file in files:
                if file.endswith('.pdf') or file.endswith('.PDF'):
                    self.allPDFFiles.append(os.path.join(root, file))
               
    #Comparison method.  Takes in both video and PDF files 
    #and loops through looking for matches
    #If match found, files are added to appropriate list
    #If file has no match, its added to unordered list
    def VideoToPDF_Matcher(self):
                    
        for pdfFile in self.allPDFFiles:
             for video in self.allVideoFiles:
                tempName = os.path.basename(video) 
                videoBaseName = os.path.splitext(tempName)
                if pdfFile.find(videoBaseName[0]) != -1:
                    self.orderedVideoList.append(video)
                    self.orderedPDFList.append(pdfFile)       

        for video in self.allVideoFiles:
            if video not in self.orderedVideoList:
                self.unorderedVideoList.append(video)

        for file in self.allPDFFiles:
            if file not in self.orderedPDFList:
                self.unorderedPDFList.append(file)

    #copy pdf files into the appropriate folder structure
    def PassPDFinputToOutputFolders(self):
        #orderedPDFPath = os.path.dirname(self.orderedPDFList[0])
        for pdf in self.orderedPDFList:
            shutil.copy(pdf, self.matchedPDFsFilePath)

        #unorderedPDFPath = os.path.dirname(self.unorderedPDFList[0])
        for pdf in self.unorderedPDFList:
            shutil.copy(pdf, self.unmatchedPDFsFilePath)
##################################################################################
    
    #Convert matched and unmatched video lists in HandBrake.  Then output them 
    #to their appropriate folder
    def VideoConversion(self):
        
        #Matched video list HandBrake conversion
        #Send converted videos to matched video folder
        row = 0
        column = 0
        if self.orderedVideoList != []:
            for fileName in self.orderedVideoList:
                tempFileNameMinusExtension = os.path.splitext(fileName)
                fileNameMinusExtension = tempFileNameMinusExtension[0]
                fileNameBaseName = os.path.basename(fileNameMinusExtension)
                newFileName = fileName.strip()
                FilePath = self.matchedVideosFilePath + fileNameBaseName
                newFilePath = FilePath.strip()
                matchedStatement = 'HandBrakeCLI -i "' + newFileName + '" -o "' + newFilePath + '".MP4 --optimize --align-av --encoder "x264"'

                subprocess.run(matchedStatement, shell=True)
        
        #Unmatched video list conversion
        #send converted videos to unmatched video folder
        row = 0
        column = 0
        if self.unorderedVideoList != []:
            for fileName in self.unorderedVideoList:
                tempFileNameMinusExtension = os.path.splitext(fileName)
                fileNameMinusExtension = tempFileNameMinusExtension[0]
                fileNameBaseName = os.path.basename(fileNameMinusExtension)
                newFileName = fileName.strip()
                unMatchedStatement = 'HandBrakeCLI -i "' + newFileName + '" -o "' + self.unmatchedVideosFilePath + fileNameBaseName + '".MP4 --optimize --align-av --encoder "x264"'

                subprocess.run(unMatchedStatement, shell=True)
#############  EXCEL SPREADSHEET CREATION AND POPULATING #################
    def ExcelSpreadSheetCreator(self):
        #Create a new excel spreadsheet and define the file path
        self.excel_File = os.path.join(self.excelOutputFilePath, "spreadsheet.xlsx")
        self.workbook = xlsxwriter.Workbook(self.excel_File)
        self.matchedWorksheet = self.workbook.add_worksheet("Matched")
        self.unMatchedWorksheet = self.workbook.add_worksheet("Unmatched")

        #format each worksheet
        self.ExcelSpreadSheetFormatting(self.matchedWorksheet)
        self.ExcelSpreadSheetFormatting(self.unMatchedWorksheet)

    def ExcelSpreadSheetFormatting(self, worksheet):
        #excel spreadsheet formatting 
        title_format = self.workbook.add_format({"bold": True, "center_across": True, "bg_color": "#BACB37"})
        worksheet.set_column('A:A', 40)
        worksheet.set_column('B:B', 40)
        worksheet.set_column('C:C', 20)
        worksheet.set_column('D:D', 20)
        worksheet.set_column('E:E', 20)
        worksheet.set_column('F:F', 20)
        worksheet.write("A1", "Video Path", title_format)
        worksheet.write("B1", "Report Path", title_format)
        worksheet.write("C1", "Upstream", title_format)
        worksheet.write("D1", "Downstream", title_format)
        worksheet.write("E1", "YearTelevised", title_format)
        worksheet.write("F1", "PipeUID", title_format)

    #extract text from PDF to find Upstream and Downstream data
    def extractPDFText(self, pdfName):
        #convert PDF to simple text
        pdfFileObj = open(pdfName, "rb")
        pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
        numPages = pdfReader.numPages

        count = 0
        text = ""

        while count < numPages:
            pageObj = pdfReader.getPage(count)
            count += 1
            text += pageObj.extractText()
        
        return text

    #Get upstream and downstream values from extracted pdf text based on which TP was used
    def tempUPDOWNvalues(self, pdfText):
        #Find Upstream MH indicator which is where, in the pdf text, that 
        # the Up and Downstream numbers will be located
        if self.teleProvider == "VisuSewer":
            upstreamMH = pdfText.partition("Upstream MH")
            wordsNeeded = upstreamMH[2].split('\n')
            UPDNstreamNumbers = wordsNeeded[1:3] 
            tempDownstreamID = UPDNstreamNumbers[1]                 #DownstreamID after VS processing/pre check for numeric
            UpstreamIDtemp = UPDNstreamNumbers[0].split("D")
            tempUpstreamID = UpstreamIDtemp[0]                      #UpstreamID after VS processing/pre check for numeric

        elif self.teleProvider == "ExpOther":
            mhNumbers = pdfText.partition("StreetSanitary")
            wordsNeeded = mhNumbers[2].split("\n")
            UPDNstreamNumbers = wordsNeeded[1:3]
            tempDownstreamID = UPDNstreamNumbers[1]                 #DownstreamID after Exp processing/pre check for numeric
            UpstreamIDtemp = UPDNstreamNumbers[0].split("t")
            tempUpstreamID = UpstreamIDtemp[1]                      #UpstreamID after Exp processing/pre check for numeric

        return tempDownstreamID, tempUpstreamID
                    
    def tempUPDOWNNumericCheck(self, tempDOWN, tempUP, pdfName):
        if tempDOWN.isnumeric() == False or tempUP.isnumeric() == False:
            getDownUpValues = DownstreamUpstreamValues(tempDOWN, tempUP, pdfName)
            tempDownReturn = getDownUpValues.DownstreamID
            tempUpReturn = getDownUpValues.UpstreamID
        else:
            tempDownReturn = tempDOWN
            tempUpReturn = tempUP

        return tempDownReturn, tempUpReturn
############################################################################

    #extract the year file was created and use it to populate column in XL sheet
    def GetYearCreated(self, obj):
        epochDT = os.path.getctime(obj)
        DT = datetime.fromtimestamp(epochDT)
        DTyear = DT.strftime("%Y")
        return DTyear

    #Populate matched excel worksheet
    def OrderedListExcelPopulator(self):
        row = 1
        column = 0
        num = 0

        for (vid, rep) in zip(self.orderedVideoList, self.orderedPDFList):
            DownstreamID = ""
            UpstreamID = ""
            name = "pipe" + str(num)
            baseVidName = os.path.basename(vid)
            tempVidName = os.path.splitext(baseVidName)[0]
            tempVidName += ".MP4"
            basePDFName = os.path.basename(rep)
            #Find and confirm Upstream and Downstream IDs
            pdfText = self.extractPDFText(rep)
            tempDNID, tempUPID = self.tempUPDOWNvalues(pdfText)
            DownstreamID, UpstreamID = self.tempUPDOWNNumericCheck(tempDNID, tempUPID, rep)
            name = Pipe.Pipe(os.path.join(self.matchedVideosFilePath, tempVidName), os.path.join(self.matchedPDFsFilePath, basePDFName), UpstreamID, DownstreamID)
            self.pipeList.append(name)
            self.matchedWorksheet.write(row, column, tempVidName)
            self.matchedWorksheet.write(row, column + 1, basePDFName)
            if self.teleProvider == "VisuSewer" or self.teleProvider == "ExpOther":
                self.matchedWorksheet.write(row, column + 2, name.get_UpstreamID())
                self.matchedWorksheet.write(row, column + 3, name.get_DownstreamID())
            yearCreated = self.GetYearCreated(vid)
            self.matchedWorksheet.write(row, column + 4, yearCreated)
            row += 1
            num += 1

    #Populate UnMatched Excel worksheet
    def UnOrderedListExcelPopulator(self):
        #Populate Unmatched Excel sheet with Videos that do not have a pdf match
        row = 1
        column = 0
        num = 0

        for vid in self.unorderedVideoList:
            DownstreamID = ""
            UpstreamID = ""
            name = "pipe" + str(num)
            baseVidName = os.path.basename(vid)
            tempVidName = os.path.splitext(baseVidName)[0]
            tempVidName += ".MP4"
            basePDFName = "N/A"
            name = Pipe.Pipe(os.path.join(self.unmatchedPDFsFilePath, tempVidName), os.path.join(self.unmatchedPDFsFilePath, basePDFName), UpstreamID, DownstreamID)
            self.pipeList.append(name)
            self.unMatchedWorksheet.write(row, column, tempVidName)
            self.unMatchedWorksheet.write(row, column + 1, basePDFName) 
            self.unMatchedWorksheet.write(row, column + 2, name.get_UpstreamID())
            self.unMatchedWorksheet.write(row, column + 3, name.get_DownstreamID())
            yearCreated = self.GetYearCreated(vid)
            self.unMatchedWorksheet.write(row, column + 4, yearCreated)
            row += 1
            num += 1

        #Populate Unmatched Excel sheet with PDFs that do not have a video match
        column = 0
        num = 0

        for pdf in self.unorderedPDFList:
            DownstreamID = ""
            UpstreamID = ""
            name = "pipe" + str(num)
            tempVidName = "N/A"
            basePDFName = os.path.basename(pdf)
            pdfText = self.extractPDFText(pdf)
            tempDNID, tempUPID = self.tempUPDOWNvalues(pdfText)
            DownstreamID, UpstreamID = self.tempUPDOWNNumericCheck(tempDNID, tempUPID, pdf)
            name = Pipe.Pipe(os.path.join(self.unmatchedPDFsFilePath, tempVidName), os.path.join(self.unmatchedPDFsFilePath, basePDFName), UpstreamID, DownstreamID)
            self.pipeList.append(name)
            self.unMatchedWorksheet.write(row, column, tempVidName)
            self.unMatchedWorksheet.write(row, column + 1, basePDFName)
            if self.teleProvider == "VisuSewer" or self.teleProvider == "ExpOther":
                self.unMatchedWorksheet.write(row, column + 2, name.get_UpstreamID())
                self.unMatchedWorksheet.write(row, column + 3, name.get_DownstreamID())
            yearCreated = self.GetYearCreated(pdf)
            self.unMatchedWorksheet.write(row, column + 4, yearCreated)
            row += 1
            num += 1

    def CloseExcelWorkbook(self):
        self.workbook.close()

    #Show user all PDF and Video input that has no match
    def DisplayUnmatchedData(self):

        self.displayWindow = Frame(self.window, width=700, height=800)
        self.displayWindow.pack()
        self.displayWindow.grab_set()
        
        #Create Header
        self.header = Label(self.displayWindow, relief="ridge", text="CCTV Video Conversion and Excel Creator", font=15)
        self.header.pack(anchor=CENTER)

        #Add empty row
        self.emptyRow = Label(self.displayWindow, text=" \n ")
        self.emptyRow.pack()

        #Alert user that this info is in created excel spreadsheet
        self.excelAlert = Label(self.displayWindow, text="***  The info listed below is all available in the Excel spreadsheet created in your output folder  ***", font=('Helvetica 12 bold'))
        self.excelAlert.pack(anchor=CENTER)
        
        #Add empty row
        self.emptyRow = Label(self.displayWindow, text=" \n ")
        self.emptyRow.pack()

        self.videoLabel = Label(self.displayWindow, text="Unmatched Video File Paths", font=('Helvetica 12 bold'))
        self.videoLabel.pack()

        #Frame to hold all video file names
        self.videoFrame = Frame(self.displayWindow)
        self.videoFrame.pack(anchor=CENTER, padx=(20,20))

        for video in self.unorderedVideoList:
            Label(self.videoFrame, text=video).pack(pady=(3,3))

        #Add empty row
        self.emptyRow = Label(self.displayWindow, text=" \n ")
        self.emptyRow.pack()

        self.PDFLabel = Label(self.displayWindow, text="Unmatched PDF File Paths", font=('Helvetica 12 bold'))
        self.PDFLabel.pack()

        #Frame to hold unmatched PDF files
        self.PDFFrame = Frame(self.displayWindow)
        self.PDFFrame.pack(anchor=CENTER, padx=(20,20))

        for pdf in self.unorderedPDFList:
            Label(self.PDFFrame, text=pdf).pack(pady=(3,3))

        #Add empty row
        self.emptyRow = Label(self.displayWindow, text=" \n ")
        self.emptyRow.pack()

        #OK button
        self.okButton = Button(self.displayWindow, text="OK", command=self.okButtonPressed)
        self.okButton.pack()
        
        #Add empty row
        self.emptyRow = Label(self.displayWindow, text=" \n ")
        self.emptyRow.pack()

    #Close application when user clicks OK button
    def okButtonPressed(self):
        self.window.destroy()
        del self.window


        #            orderedListMHID = DownstreamUpstreamValues(rep, self.teleProvider)
          #  DownstreamID = orderedListMHID.DownstreamID
          #  UpstreamID = orderedListMHID.UpstreamID

















            




    

