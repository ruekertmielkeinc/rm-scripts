import pipes
import subprocess
import os
import xlsxwriter
import PyPDF2
from datetime import date
from pathlib import Path
from tkinter import *
from tkinter import messagebox
from tkinter import ttk
import shutil
import Pipe



class CCTVconversion:
    #Constructor
    def __init__(self, inputVideoPath, outputVideoPath, inputPDFPath, outputPDFPath, outputExcelPath):
        self.inputVideoPath = inputVideoPath
        self.outputVideoPath = outputVideoPath
        self.inputPDFPath = inputPDFPath
        self.outputPDFPath = outputPDFPath
        self.outputExcelPath = outputExcelPath
        self.window = Toplevel()
        
    global allVideoFiles, allPDFFiles, orderedVideoList, unorderedVideoList, orderedPDFList, unorderedPDFList
    allVideoFiles = list()
    allPDFFiles = list()  
    orderedVideoList = list()
    orderedPDFList = list()
    unorderedVideoList = list()
    unorderedPDFList = list()

    #Method that takes in two file paths (video input and video output) 
    #Then uses HandBrake software to convert all input videos to 
    #standards as set in HandBrake script
    #Finished videos are loaded into folder at output file path
    def VideoConversion(self):
        #Walk through video input file path and find all files with mpg extension
        #append those files to video list   
        #***NEEDS UPDATING DEPENDING ON WHAT FILE EXTENSIONS ARE USED FOR VIDEO INPUT 12/19/22***    
        for root, dirs, files in os.walk(self.inputVideoPath):
            for file in files:
                if file.endswith('.mpg') or file.endswith('.MPG'):
                    allVideoFiles.append(os.path.join(root, file))
        
        #BATCH SCRIPT IN NOTEPAD++
        #12/22/22 NO LONGER NEEDED. SCRIPT CONCATENATED DIRECTLY INTO CODE HERE
        #KEEP THE SCRIPT AROUND FOR POSTERITY.  REALLY TIES THE CODE TOGETHER
        #BatchScript for video conversion
        #batchFileLocation = r"C:\Users\psatterlee\Documents\TEST_CCTVvideoConversion\HandBrakeBatchScript.bat"

        #Loop through dirlist
        #Manage file name for use in batch script
        #splitext method removes the file extension so it wont matter 
        #what format the user enters
        row = 0
        column = 0
        for fileName in allVideoFiles:
            tempFileNameMinusExtension = os.path.splitext(fileName)
            fileNameMinusExtension = tempFileNameMinusExtension[0]
            fileNameBaseName = os.path.basename(fileNameMinusExtension)
            newFileName = fileName.strip()
            statement = 'HandBrakeCLI -i "' + newFileName + '" -o "' + self.outputVideoPath + fileNameBaseName + '".MP4 --optimize --align-av --encoder "x264"'

            #Send data to batch script for processing in HandBrake
            subprocess.run(statement, shell=True)
                
    #method to create an excel spreadsheet containing all relevant info regarding CCTV videos
    def ExcelCreator(self):

        #Walk through PDF input file path and find all files with pdf extension
        #append those files to pdf file list
        for root, dirs, files in os.walk(self.inputPDFPath):
            for file in files:
                if file.endswith('.pdf') or file.endswith('.PDF'):
                    allPDFFiles.append(os.path.join(root, file))

        #REMOVED 1.3.23
        #Video list is already created in the video conversion method
        #That list was changed to a global variable so it can be used in this method
        #get list of video names after they've been run through HandBrake
        #video_list = os.listdir(outputFilePath) 
        #video_list = [f for f in video_list if os.path.isfile(outputFilePath+'/'+f)]

        #method that takes in a pdf file name and extracts the text
        #then some assumptions are made about how the file is formatted
        #a tuple is returned containing Upstream and Downstream IDs

        #THIS PROCESS COULD USE IMPROVEMENT BASED ON HOW THE PDF FILES ARE FORMATTED
        #12/19/22 Improvements achieved... I think. Still needs some testing
        def PDFextracter(pdfName):

            #convert PDF to simple text
            pdfFileObj = open(os.path.join(self.inputPDFPath, pdfName), "rb")
            pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
            numPages = pdfReader.numPages

            count = 0
            text = ""

            while count < numPages:
                pageObj = pdfReader.getPage(count)
                count += 1
                text += pageObj.extractText()
            
            #Find Upstream MH indicator which is where, in the pdf text, that 
            # the Up and Downstream numbers will be located
            upstreamMH = text.partition("Upstream MH")
            wordsNeeded = upstreamMH[2].split('\n')
            return wordsNeeded[1:3] 

        #Create a new excel spreadsheet and define the file path
        excel_File = os.path.join(self.outputExcelPath, "spreadsheet.xlsx")
        workbook = xlsxwriter.Workbook(excel_File)
        worksheet = workbook.add_worksheet()

        #excel spreadsheet formatting 
        title_format = workbook.add_format({"bold": True, "center_across": True, "bg_color": "#BACB37"})
        worksheet.set_column('A:A', 100)
        worksheet.set_column('B:B', 100)
        worksheet.set_column('C:C', 20)
        worksheet.set_column('D:D', 20)
        worksheet.set_column('E:E', 20)
        worksheet.set_column('F:F', 20)
        worksheet.write("A1", "Video Path", title_format)
        worksheet.write("B1", "Report Path", title_format)
        worksheet.write("C1", "Upstream", title_format)
        worksheet.write("D1", "Downstream", title_format)
        worksheet.write("E1", "YearTelevised", title_format)
        worksheet.write("F1", "PipeUID", title_format)

        #Loop through video list and insert video name in proper excel column
        row = 1
        column = 0
        pipe_List = []
        num = 0
        DownstreamID = ""
        UpstreamID = ""
        todays_date = date.today()
           
            
        #Find basename minus extension of each video in list
        #use that basename in find() method and loop through 
        #video list to find substring basename
        #if found, add to new video list which will be in same order as
        #PDF list, which will then be entered correctly into Excel
        for pdfFile in allPDFFiles:
             for video in allVideoFiles:
                tempName = os.path.basename(video) 
                videoBaseName = os.path.splitext(tempName)
                if pdfFile.find(videoBaseName[0]) != -1:
                    orderedVideoList.append(video)
                    orderedPDFList.append(pdfFile)       
        
        #Loop through all video file list and search ordered video list for each video
        #If a video is NOT already in the ordered video list, add it to the unordered list
        #This should give a list of all the video files that had no matching pdf
        for video in allVideoFiles:
            if video not in orderedVideoList:
                unorderedVideoList.append(video)

        #Loop through all pdf file list and search ordered pdf list for each file
        #If a file is NOT already in the ordered PDF list, add it to the unordered list
        #This should give a list of all the PDF files that had no matching video
        for file in allPDFFiles:
            if file not in orderedPDFList:
                unorderedPDFList.append(file)

        #1/25/23
        #Create output spreadsheet for unmatched data
        #Unmatched data folder 
        #separate rows in excel for each video and/or pdf record
        #only basename shown in excel, no directory path
        #One ouput path, then create folder structure for the user
                    
        #loop through both ordered video list and pdf list
        #call PDFextracter method to get final IDs for UP and DOWN stream
        #pass all data to Pipe class and instantiate a new pipe object
        #add new pipe object to pipe_list
        #write Pipe data to excel sheet
        for (vid, rep) in zip(orderedVideoList, orderedPDFList):
            name = "pipe" + str(num)
            UPDNstreamNumbers = PDFextracter(rep)
            DownstreamID = UPDNstreamNumbers[1]
            UpstreamIDtemp = UPDNstreamNumbers[0].split("D")
            UpstreamID = UpstreamIDtemp[0]
            baseVidName = os.path.basename(vid)
            tempVidName = os.path.splitext(baseVidName)[0]
            tempVidName += ".MP4"
            basePDFName = os.path.basename(rep)
            name = Pipe.Pipe(os.path.join(self.outputVideoPath, tempVidName), os.path.join(self.outputPDFPath, basePDFName), UpstreamID, DownstreamID)
            pipe_List.append(name)
            worksheet.write(row, column, name.get_OutputFilePath())
            worksheet.write(row, column + 1, name.get_pdfReportPath())
            worksheet.write(row, column + 2, name.get_UpstreamID())
            worksheet.write(row, column + 3, name.get_DownstreamID())
            worksheet.write(row, column + 4, todays_date.year)
            row += 1
            num += 1
        
        workbook.close() 

    #Display unmatched Videos and PDFs for user to see
    def DisplayUnmatchedData(self):

        self.displayWindow = Frame(self.window, width=700, height=800)
        self.displayWindow.pack()
        self.displayWindow.grab_set()
        
        #Create Header
        self.header = Label(self.displayWindow, relief="ridge", text="CCTV Video Conversion and Excel Creator", font=15)
        self.header.pack(anchor=CENTER)

        #Add empty row
        self.emptyRow = Label(self.displayWindow, text=" \n ")
        self.emptyRow.pack()

        self.videoLabel = Label(self.displayWindow, text="Unmatched Video File Paths", font=('Helvetica 12 bold'))
        self.videoLabel.pack()

        #Frame to hold all video file names
        self.videoFrame = Frame(self.displayWindow)
        self.videoFrame.pack(anchor=CENTER, padx=(20,20))

        for video in unorderedVideoList:
            Label(self.videoFrame, text=video).pack(pady=(3,3))

        #Add empty row
        self.emptyRow = Label(self.displayWindow, text=" \n ")
        self.emptyRow.pack()

        self.PDFLabel = Label(self.displayWindow, text="Unmatched PDF File Paths", font=('Helvetica 12 bold'))
        self.PDFLabel.pack()

        #Frame to hold unmatched PDF files
        self.PDFFrame = Frame(self.displayWindow)
        self.PDFFrame.pack(anchor=CENTER, padx=(20,20))

        for pdf in unorderedPDFList:
            Label(self.PDFFrame, text=pdf).pack(pady=(3,3))

        #Add empty row
        self.emptyRow = Label(self.displayWindow, text=" \n ")
        self.emptyRow.pack()

        #OK button
        self.okButton = Button(self.displayWindow, text="OK", command=self.okButtonPressed)
        self.okButton.pack()
        
        #Add empty row
        self.emptyRow = Label(self.displayWindow, text=" \n ")
        self.emptyRow.pack()

    #Close application when user clicks OK button
    def okButtonPressed(self):
        self.window.destroy()
        del self.window






    #method to get input from user and verify input is valid video file path
    #PATH MUST EXIST BEFORE USER ENTERS FILE PATH
    ########################################################################
    #1/11/23
    #METHOD REMOVED SINCE BUILDING OUT UI
    #LOGIC IS STILL USED IN NEW METHODS 
    ########################################################################
    def getFilePathFromUser():

            #method that takes in inputFilePath and checks for files with proper file extension
            def checkCorrectFilePath(inputPath):
                correctFilePath = False    
                for root, dirs, files in os.walk(inputPath):
                    for file in files:
                       if file.endswith('.mpg'):
                            correctFilePath = True
                            break
                return correctFilePath

            #get file path input from user 
            inputFilePath = input("Enter VIDEO INPUT file path: ")

            while checkCorrectFilePath(inputFilePath) == False:
                inputFilePath = input("Directory does not contain files with a valid extension.  Please enter a valid file path: ")

            #check end of file path for backslash
            #add backslash if none exists, continue if already there
            if inputFilePath[-1] != "\\":
                inputFilePath = inputFilePath + "\\"
                return inputFilePath

            if inputFilePath[-1] == "\\":
                return inputFilePath


    #Get video destination file path from user
    #PATH MUST EXIST BEFORE USER ENTERS FILE PATH 
    ########################################################################
    #1/11/23
    #METHOD REMOVED SINCE BUILDING OUT UI
    #LOGIC IS STILL USED IN NEW METHODS 
    ########################################################################
    def getOutputFilePathFromUser():
            #get output file path input from user 
            outputFilePath = input("Enter VIDEO OUTPUT file path: ")

            #check if output is valid filepath
            while os.path.isdir(outputFilePath) == False:
                outputFilePath = input("Invalid file path.  Please enter a valid output file path: ")
               
            #check end of file path for backslash
            #add backslash if none exists, continue if already there
            if outputFilePath[-1] != "\\":

                outputFilePath = outputFilePath + "\\"
                return outputFilePath

            elif outputFilePath[-1] == "\\":
                return outputFilePath


    #Get pdf file input path from user
    #PATH MUST EXIST BEFORE USER ENTERS FILE PATH
    #FOLDER MUST BE POPULATED WITH PDF FILES
    ########################################################################
    #1/11/23
    #METHOD REMOVED SINCE BUILDING OUT UI
    #LOGIC IS STILL USED IN NEW METHODS 
    ########################################################################
    def getPDFinputfilePathFromUser():

            #get file path input from user 
            pdfInputFilePath = input("Enter PDF INPUT file path: ")

            #method that takes in inputFilePath and checks for files with proper file extension
            def checkCorrectFilePath(inputPath):
                correctFilePath = False    
                for root, dirs, files in os.walk(inputPath):
                    for file in files:
                       if file.endswith('.pdf'):
                            correctFilePath = True
                            break
                return correctFilePath
               

            #check end of file path for backslash
            #add backslash if none exists, continue if already there
            if pdfInputFilePath[-1] != "\\":

                pdfInputFilePath = pdfInputFilePath + "\\"
                return pdfInputFilePath

            elif pdfInputFilePath[-1] == "\\":
                return pdfInputFilePath


    #Get pdf file output path from user
    #PATH MUST EXIST BEFORE USER ENTERS FILE PATH
    #FOLDER MUST BE POPULATED WITH PDF FILES
    ########################################################################
    #1/11/23
    #METHOD REMOVED SINCE BUILDING OUT UI
    #LOGIC IS STILL USED IN NEW METHODS 
    ########################################################################
    def getPDFoutputfilePathFromUser():

            #get file path input from user 
            pdfOutputFilePath = input("Enter PDF OUTPUT file path: ")

            #check if input is valid filepath
            while os.path.isdir(pdfOutputFilePath) == False:
                pdfOutputFilePath = input("Invalid file path.  Please enter a valid PDF output file path: ")
               
            #check end of file path for backslash
            #add backslash if none exists, continue if already there
            if pdfOutputFilePath[-1] != "\\":

                pdfOutputFilePath = pdfOutputFilePath + "\\"
                return pdfOutputFilePath

            elif pdfOutputFilePath[-1] == "\\":
                return pdfOutputFilePath













            




    
