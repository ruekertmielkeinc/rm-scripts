
class Pipe:
    def __init__(self, vidFilePath, reportPath, upstreamID, downstreamID):
            self.vidFilePath = vidFilePath
            self.reportPath = reportPath
            self.upstreamID = upstreamID
            self.downstreamID = downstreamID


    def get_OutputFilePath(self):
        return self.vidFilePath

    def get_pdfReportPath(self):
        return self.reportPath

    def get_UpstreamID(self):
        return self.upstreamID

    def get_DownstreamID(self):
        return self.downstreamID

