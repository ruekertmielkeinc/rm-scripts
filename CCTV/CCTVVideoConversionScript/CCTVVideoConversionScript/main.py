import pipes
import subprocess
import os
from tkinter import *
from tkinter import messagebox
from tkinter import ttk
import shutil

#Local imports
import CCTV_ExcelVideoConversion as cctv
from TPInputWindow import *
from TeleProviderChoiceWindow import *

def main():
    root = Tk()
    newWindow = TeleProviderChoiceWindow(root)
    root.mainloop()
 
if __name__ == "__main__":
    main()

