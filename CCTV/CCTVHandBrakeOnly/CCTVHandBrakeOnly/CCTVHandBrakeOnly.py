import pipes
import subprocess
import os
import xlsxwriter
import PyPDF2
from datetime import date
from pathlib import Path
from tkinter import *
from tkinter import messagebox
from tkinter import ttk
import shutil



gui = Tk()
gui.geometry("450x300")


def VideoConversion():
    global inputFilePath, outputFilePath

    inputFilePath = entryInput.get()
    outputFilePath = entryOutput.get()

    #check end of file path for backslash
    #add backslash if none exists, continue if already there
    if inputFilePath[-1] != "\\":
        inputFilePath = inputFilePath + "\\"

    #check end of file path for backslash
    #add backslash if none exists, continue if already there
    if outputFilePath[-1] != "\\":
        outputFilePath = outputFilePath + "\\"

        #list to hold all videos after theyre found in input file path
    allVideoFiles = list()

        #Walk through video input file path and find all files with mpg extension
        #append those files to video list   
        #***NEEDS UPDATING DEPENDING ON WHAT FILE EXTENSIONS ARE USED FOR VIDEO INPUT 12/19/22***    
    for root, dirs, files in os.walk(inputFilePath):
        for file in files:
            if file.endswith('.mpg'):
                allVideoFiles.append(os.path.join(root, file))

        #Loop through dirlist
        #Manage file name for use in batch script
        #splitext method removes the file extension so it wont matter 
        #what format the user enters
    row = 0
    column = 0
    for fileName in allVideoFiles:
        tempFileNameMinusExtension = os.path.splitext(fileName)
        fileNameMinusExtension = tempFileNameMinusExtension[0]
        fileNameBaseName = os.path.basename(fileNameMinusExtension)
        newFileName = fileName.strip()
        statement = 'HandBrakeCLI -i "' + newFileName + '" -o "' + outputFilePath + fileNameBaseName + '".MP4 --optimize --align-av --encoder "x264"'
            

            #Send data to batch script for processing in HandBrake
        subprocess.run(statement, shell=True)
    
entryInput = StringVar()
entryOutput = StringVar()

Label(gui, text='Input').place(x=40, y=60)
userInput = Entry(gui, textvariable=entryInput, width=30).place(x=110, y=60)

Label(gui, text='Output').place(x=40, y=100)
userOutput = Entry(gui, textvariable=entryOutput, width=30).place(x=110, y=100)

submitButton = Button(gui, text='Submit', command=VideoConversion).place(x=40, y=130)
 
gui.mainloop()


            