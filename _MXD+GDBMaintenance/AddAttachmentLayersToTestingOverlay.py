import arcpy
import os

sr = str(sys.argv[1])
spref = arcpy.SpatialReference(int(sr))

layers_path = "I:\WebDevelopment\Resources\LayerFiles\AssetAlly\OVERLAY"
mxd_paths = file("C:\Users\AStJohn\source\AssetAlly\db_scripts\Utilities\PublishPython\OVERLAYMXDList.txt", "r").readlines()

for path in mxd_paths:
    if path[0] =="#": #Add a pound in front of the line in the text file to skip it.
        print "Skipping " + path
        continue

    mxdFile = path.strip("\n")   
    mxd = arcpy.mapping.MapDocument(mxdFile)
    df = arcpy.mapping.ListDataFrames(mxd, "Layers")[0]
    print "Working on " + mxdFile

    theSDE = "Testing_AssetAlly_SDE"
    workspacePath = "I:\DatabaseConnections\WebUser Connections\Testing_AssetAlly_SDE.AssetAlly@gissql.ruekert-mielke.com.sde"
    
    layers = {"Curb Stop Attachments.lyr": ["SELECT * FROM " + theSDE + ".dbo.svw_CurbStopAsbuilts", "POINT"],
              "Hydrant Attachments.lyr": ["SELECT * FROM " + theSDE + ".dbo.svw_HydrantAsbuilts", "POINT"],
              "San Pipe Attachments.lyr": ["SELECT * FROM " + theSDE + ".dbo.svw_SanPipeAsbuilts", "POLYLINE"],
              "San Structure Attachments.lyr": ["SELECT * FROM " + theSDE + ".dbo.svw_SanStructureAsbuilts", "POINT"],
              "Storm Pipe Attachments.lyr": ["SELECT * FROM " + theSDE + ".dbo.svw_StormPipeAsbuilts", "POLYLINE"],
              "Storm Structure Attachments.lyr": ["SELECT * FROM " + theSDE + ".dbo.svw_StormStructureAsbuilts", "POINT"],
              "Storm Water Device Attachments.lyr": ["SELECT * FROM " + theSDE + ".dbo.svw_StormPondAsbuilts", "POLYGON"],
              "Valve Attachments.lyr": ["SELECT * FROM " + theSDE + ".dbo.svw_ValveAsbuilts", "POINT"],
              "Water Pipe Attachments.lyr": ["SELECT * FROM " + theSDE + ".dbo.svw_WaterPipeAsbuilts", "POLYLINE"]}
    for layer in layers:
        lyrName = layer.split(".")[0]

        if arcpy.mapping.ListLayers(mxd, lyrName, df) == []:
            # Add layers (with data source set by the workspace path and query)
            arcpy.MakeQueryLayer_management(input_database=workspacePath, out_layer_name=lyrName, query=layers[layer][0], oid_fields="UID", shape_type=layers[layer][1], srid=sr, spatial_reference=spref)
            arcpy.mapping.AddLayer(df, arcpy.mapping.Layer(lyrName), "BOTTOM")
            print "Query layer " + lyrName + " added to map."
        else:
            print "Layer " + arcpy.mapping.ListLayers(mxd, lyrName, df)[0].name + " already exists in map."

        # Set symbology
        inLayer = arcpy.mapping.ListLayers(mxd, lyrName, df)[0]
        inSymbologyLayer = os.path.join(layers_path, layer)
        arcpy.ApplySymbologyFromLayer_management(in_layer=inLayer, in_symbology_layer=inSymbologyLayer)
        print "Symbology applied."
    
    print "Saving File " + mxdFile
    mxd.save()
    del mxd