import arcpy, pyodbc
import httplib, urllib, json
import sys
import getpass
from subprocess import call
from arcgisservices import *

#ABOUT: This script processes information stored in the _GISAdmin database.  Instead of having to shut down all services,
#this script shuts down all services in 1 folder, processes database changes, and restarts services.

#REQUIRES: installation of pip and pyodbc on this PC.  See AssetAllyDemo\db_scripts\Utilities\pyodbc\using pyodbc.txt

#USAGE: Before starting, make sure to review _GISAdmin.dbo.AASiteInfo to make sure the ProcessUpdate field is set
#to 1 for those sites you wish to update. Add the Python commands between the run something and done running something comments.

def main(argv=None):
    conn = pyodbc.connect("Driver={ODBC Driver 17 for SQL Server};"
                          "Server=gissql.ruekert-mielke.com;"
                          "Database=_GISAdmin;"
                          "uid=GIS;"
                          "pwd=TacoT!ME4BJJ;")

    cursor = conn.cursor()
    cursor.execute("SELECT * FROM dbo.AA_SiteInfo WHERE (IsAssetAlly = 1 AND ProcessUpdate = 1 AND Water = 1)")

    for row in cursor:
        folder = str(row.RESTFolder)
        theConn = str(row.ConnectionFile)
        theSDE = str(row.SDEDatabase)
        theGIS = str(row.GISDatabase)
        theDomain = str(row.SiteName)
        overlay = str(row.OverlayMXDFilePath)
        print "Folder: " + folder
        print row.SDEDatabase

        layers_path = "I:\WebDevelopment\Resources\LayerFiles\AssetAlly\OVERLAY"
        
        mxdFile = overlay.strip("\n").strip("\r")
        mxd = arcpy.mapping.MapDocument(mxdFile)
        df = arcpy.mapping.ListDataFrames(mxd, "Layers")[0]
        print "Working on " + mxdFile
        
        layers = {
                  "Private Well Needs Inspection.lyr"
                  }
        for layer in layers:
            lyrName = layer.split(".")[0]
            inLayer = arcpy.mapping.Layer(os.path.join(layers_path, layer))
            arcpy.mapping.AddLayer(df, inLayer, "BOTTOM")
            print "Layer added - " + lyrName
        
        print "Saving File " + mxdFile
        mxd.save()
        del mxd
         
    conn.close()
        
# Script start
if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
