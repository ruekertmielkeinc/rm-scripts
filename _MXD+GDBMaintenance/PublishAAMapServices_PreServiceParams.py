import sys
import arcpy
import datetime
import os
import logging
import pymssql
import xml.dom.minidom as dom

now = datetime.datetime.now().strftime('%m-%d-%Y_%H_%M_%S')
logfile = "PublishMapServices_Log_" + now + ".log"
logging.basicConfig(filename=logfile, format="%(asctime)s %(message)s", datefmt="%m/%d/%Y %I:%M:%S %p", level=logging.DEBUG)

def get_services_data(serviceType):
    server = r"gissql.ruekert-mielke.com"
    user = r"sa"
    password = r"H0neyBadger$"
    database = r"_GISAdmin"
    connection = pymssql.connect(server, user, password, database)
    cursor = connection.cursor()

    if serviceType == 'ALL':
        cursor.execute(r"SELECT RESTFolder, FeatureMXDFilePath, SecureMXDFilePath, OverlayMXDFilePath, CustomMXDFilePath FROM AA_SiteInfo WHERE ProcessUpdate=1;")
        row = cursor.fetchone()

        RESTFolders, featureMXDs, secureMXDs, overlayMXDs, customMXDs = [], [], [], [], []
        while row:
            # Convert unicode tuples to strings and then move to next row
            RESTFolders.append(row[0].encode("ascii"))
            featureMXDs.append(row[1])
            secureMXDs.append(row[2])
            overlayMXDs.append(row[3])
            customMXDs.append(row[4])
            row = cursor.fetchone()

        [f.encode("ascii") if f is not None else None for f in featureMXDs]
        [s.encode("ascii") if s is not None else None for s in secureMXDs]
        [o.encode("ascii") if o is not None else None for o in overlayMXDs]
        [c.encode("ascii") if c is not None else None for c in customMXDs]

        connection.close()
        return RESTFolders, featureMXDs, secureMXDs, overlayMXDs, customMXDs
    else:
        cursor.execute(r"SELECT RESTFolder, {}MXDFilePath FROM AA_SiteInfo WHERE IsAssetAlly='True' AND ProcessUpdate=1;".format(serviceType.title()))
        row = cursor.fetchone()

        RESTFolders, MXDs = [], []
        while row:
            # Convert unicode tuples to strings and then move to next row
            RESTFolders.append(row[0].encode("ascii"))
            MXDs.append(row[1])
            row = cursor.fetchone()

        [m.encode("ascii") if m is not None else None for m in MXDs]

        connection.close()
        return RESTFolders, MXDs

def modify_sddraft(sddraft, serviceType):
    doc = dom.parse(sddraft)

    types = doc.getElementsByTagName('TypeName')
    for type in types:
        if type.firstChild.data == 'KmlServer':
            for extChild in type.parentNode.childNodes:
                if extChild.tagName == 'Enabled':
                    extChild.firstChild.data = 'false'
        if type.firstChild.data == 'FeatureServer' and serviceType == 'FEATURE':
            for extChild in type.parentNode.childNodes:
                if extChild.tagName == 'Enabled':
                    extChild.firstChild.data = 'true'

    keys = doc.getElementsByTagName('Key')
    for key in keys:
        if key.hasChildNodes():
            if key.firstChild.data == 'maxRecordCount':
                key.nextSibling.firstChild.data = 10000
            if key.firstChild.data == 'MinInstances':
                key.nextSibling.firstChild.data = 0
            if key.firstChild.data == 'MaxInstances':
                key.nextSibling.firstChild.data = 2

    # Output _NEW.sddraft, and remove original sddraft
    newSddraft = sddraft.split('.')[0] + '_NEW.sddraft'
    f = open(newSddraft, 'w')
    doc.writexml(f)
    f.close()
    os.remove(sddraft)

    return newSddraft

def analyze_sddraft(newSddraft):
    logging.info('Analyzing service definition for errors')  
    analysis = arcpy.mapping.AnalyzeForSD(newSddraft)

    logging.info("The following information was returned during analysis of the MXD:")
    for key in ('messages', 'warnings', 'errors'):
        logging.info('----' + key.upper() + '---')
        for ((message, code), layers) in analysis[key].iteritems():
            for layer in layers:
                logging.info('MESSAGE: {}; CODE {}; LAYER {}'.format(message, code, layer.name))

    return analysis

def publish_service(agsConnection, serviceFolder, serviceType, mxd):    
    def delete_sd():  
        try:
            logging.info('Removing sd file: {}'.format(sd))  
            os.remove(sd)
        except OSError:  
            pass 
      
    mxd = mxd.rstrip("\r\n")
    logging.info('\n Publishing map document to map service: {}'.format(mxd))  

    sdPath = mxd.split('.')[0]
    sd = sdPath + '.sd'
    sddraft = sd + 'draft'  

    # Remove existing service definition, if exists, since it will not overwrite and just fail
    delete_sd()

    mapDocument = arcpy.mapping.MapDocument(mxd)
    serviceName = mxd.split('.')[0].split('\\')[-1]

    logging.info('Creating .sddraft file: {}'.format(sddraft)) 
    arcpy.mapping.CreateMapSDDraft(map_document=mapDocument, out_sddraft=sddraft, service_name=serviceName, 
                                   server_type='ARCGIS_SERVER', connection_file_path=agsConnection, 
                                   folder_name=serviceFolder, summary='', tags='AssetAlly')  

    newSddraft = modify_sddraft(sddraft, serviceType)

    analysis = analyze_sddraft(newSddraft)

    if analysis['errors'] == {}:
        logging.info("Staging and uploading service definition.")
        arcpy.StageService_server(in_service_definition_draft=newSddraft, out_service_definition=sd)  
        arcpy.UploadServiceDefinition_server(in_sd_file=sd, in_server=agsConnection, in_service_name=serviceName, 
                                             in_folder_type='EXISTING', in_folder=serviceFolder)  
        logging.info("Service successfully published: {}".format(mxd))  
    else:  
        logging.warn(str(analysis['errors']))  
        logging.warn("Service {} could not be published because errors were found during analysis.".format(mxd))  

    delete_sd()  
    return

def main():
    serviceType = sys.argv[1]

    #agsConnection = r"I:\\DatabaseConnections\\ArcServer Connections\\arcgis on elb.ags.ruekert-mielke.com (admin).ags"
    agsConnection = r"I:\\DatabaseConnections\\ArcServer Connections\\arcgis on www.crunchwrap.org (admin).ags"
    logging.info('Publishing to ArcGIS Server: {}'.format(agsConnection))

    if serviceType == 'ALL':
        RESTFolders, featureMXDs, secureMXDs, overlayMXDs, customMXDs = get_services_data(serviceType)
        for i in range(len(RESTFolders)):
            logging.info("Processing all services for {}".format(RESTFolders[i]))
            if featureMXDs[i] != None:
                publish_service(agsConnection, RESTFolders[i], 'FEATURE', featureMXDs[i])
            if secureMXDs[i] != None:
                publish_service(agsConnection, RESTFolders[i], 'SECURE', secureMXDs[i])
            if overlayMXDs[i] != None:
                publish_service(agsConnection, RESTFolders[i], 'OVERLAY', overlayMXDs[i])
            if customMXDs[i] != None:
                publish_service(agsConnection, RESTFolders[i], 'CUSTOM', customMXDs[i])
    else:
        RESTFolders, serviceMXDs = get_services_data(serviceType)
        for i in range(len(RESTFolders)):
            logging.info("Processing {0} service for {1}".format(serviceType, RESTFolders[i]))
            if serviceMXDs[i] != None:
                publish_service(agsConnection, RESTFolders[i], serviceType, serviceMXDs[i])

    logging.info('Finished publishing map services! :^B')
    print 'Finished publishing map services to ArcGIS Server.'
    print 'Please see ' + logfile + ' for details.'

    return

if __name__ == '__main__':
    main()