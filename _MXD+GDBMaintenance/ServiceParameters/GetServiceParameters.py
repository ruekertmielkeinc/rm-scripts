import urllib
import httplib
import json
import sys
import getpass

def getToken(username, password, serverName, serverPort):
    tokenURL = r"/arcgis/admin/generateToken"
    params = urllib.urlencode({'username': username, 'password': password, 'client': 'requestip', 'f': 'json'})
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}

    httpConn = httplib.HTTPConnection(serverName, serverPort)
    httpConn.request("POST", tokenURL, params, headers)

    response = httpConn.getresponse()
    if (response.status != 200):
        httpConn.close()
        print "Error while fetch tokens from admin URL. Please check the URL and try again."
        return
    else:
        data = response.read()
        httpConn.close()
        token = json.loads(data)        
        return token['token']

def assertJsonSuccess(data):
    obj = json.loads(data)
    if 'status' in obj and obj['status'] == "error":
        print "Error: JSON object returns an error. " + str(obj)
        return False
    else:
        return True

def main():
    username = r'siteadmin'
    password = r'E=>GwN%t9}DkxKlNPCPlHS'
    #serverName = r'elb.ags.ruekert-mielke.com'
    serverName = r'crunchwrap.org'
    serverPort = 80
    
    token = getToken(username, password, serverName, serverPort)

    serverURL = r"/arcgis/admin/services"
    params = urllib.urlencode({'token': token, 'f': 'json'})
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    
    httpConn = httplib.HTTPConnection(serverName, serverPort)
    httpConn.request("POST", serverURL, params, headers)

    response = httpConn.getresponse()
    if (response.status != 200):
        httpConn.close()
        print "Could not read folder information."
        return
    else:
        data = response.read()

        if not assertJsonSuccess(data):          
            print "Error when reading server information. " + str(data)
            return
        else:
            print "Processed server information successfully. Now processing folders..."

        dataObj = json.loads(data)
        httpConn.close()

        folders = dataObj["folders"]
        folders.remove("System")
        folders.remove("Utilities")
        folders.append("")

        #resultFile = r"output.csv"
        #serviceResultFile = open(resultFile,'w')
        #serviceResultFile.write("ServiceName,Folder,Type,Status,Min Instances,Max Instances,FeatureService,kml,wms,Max Records,Cluster,Cache Directory,Jobs Directory,Output Directory" + "\n")

        for folder in folders:
            if folder != "":
                folder += "/"

            folderURL = "/arcgis/admin/services/" + folder
            params = urllib.urlencode({'token': token, 'f': 'json'})
            headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}

            httpConn = httplib.HTTPConnection(serverName, serverPort)
            httpConn.request("POST", folderURL, params, headers)

            response = httpConn.getresponse()
            if (response.status != 200):
                httpConn.close()
                print "Could not read folder information."
                return
            else:
                data = response.read()

                if not assertJsonSuccess(data):          
                    print "Error when reading folder information. " + str(data)
                else:
                    print "Processed folder information successfully. Now processing services..."

                dataObj = json.loads(data)
                httpConn.close()
 
                for item in dataObj['services']:
                    if item["type"] == "MapServer":
                        if folder:
                            sUrl = "/arcgis/admin/services/%s%s.%s" %(folder,item["serviceName"], item["type"])
                        else:
                            sUrl = "/arcgis/admin/services/%s.%s" %(item["serviceName"], item["type"])

                        httpConn.request("POST", sUrl, params, headers)

                        servResponse = httpConn.getresponse()
                        readData = servResponse.read()
                        jsonOBJ = json.loads(readData)

                        if folder:
                            statusUrl = "/arcgis/admin/services/%s%s.%s/status" %(folder,item["serviceName"], item["type"])
                        else:
                            statusUrl = "/arcgis/admin/services/%s.%s/status" %(item["serviceName"], item["type"])

                        httpConn.request("POST", statusUrl, params, headers)

                        servStatusResponse = httpConn.getresponse()
                        readData = servStatusResponse.read()
                        jsonOBJStatus = json.loads(readData)

        #                isCached = jsonOBJ["properties"]["isCached"]
        #                if isCached == "true":
        #                    cacheDir = str(jsonOBJ["properties"]["cacheDir"])
        #                else:
        #                    cacheDir = jsonOBJ["properties"]["isCached"]

        #                if len(jsonOBJ["extensions"]) == 0:
        #                    ln = str(jsonOBJ["serviceName"]) + "," + folder + "," + str(item["type"]) + "," + jsonOBJStatus['realTimeState'] + "," + str(jsonOBJ["minInstancesPerNode"]) + "," + str(jsonOBJ["maxInstancesPerNode"]) + "," + "FeatServHolder" + "," + "Disabled" + "," + "Disabled" +"," + str(jsonOBJ["properties"]["maxRecordCount"]) + "," + str(jsonOBJ["clusterName"]) + "," + cacheDir + "," + "NA" + "," + str(jsonOBJ["properties"]["outputDir"]) +"\n"
        #                else:
        #                    kmlProps = [mapKML for mapKML in jsonOBJ["extensions"] if mapKML["typeName"] == 'KmlServer']#.items()[0][1] == 'KmlServer']
        #                    wmsProps = [mapWMS for mapWMS in jsonOBJ["extensions"] if mapWMS["typeName"] == 'WMSServer']#.items()[0][1] == 'WMSServer']

        #                    featServProps = [featServ for featServ in jsonOBJ["extensions"] if featServ["typeName"] == 'FeatureServer']#.items()[0][1] == 'FeatureServer']

        #                    if len(featServProps) > 0:
        #                        featureStatus = str(featServProps[0]["enabled"])
        #                    else:
        #                        featureStatus = "NA"

        #                    if len(kmlProps) > 0:
        #                        kmlStatus = str(kmlProps[0]["enabled"])
        #                    else:
        #                        kmlStatus = "NA"

        #                    if len(wmsProps) > 0:
        #                        wmsStatus = str(wmsProps[0]["enabled"])
        #                    else:
        #                        wmsStatus = "NA"

        #                    ln = str(jsonOBJ["serviceName"]) + "," + folder + "," + str(item["type"]) + "," + jsonOBJStatus['realTimeState'] + "," + str(jsonOBJ["minInstancesPerNode"]) + "," + str(jsonOBJ["maxInstancesPerNode"]) + "," + featureStatus + "," + kmlStatus + "," + wmsStatus +"," + str(jsonOBJ["properties"]["maxRecordCount"]) + "," + str(jsonOBJ["clusterName"]) + "," + cacheDir + "," + "NA" + "," + str(jsonOBJ["properties"]["outputDir"]) +"\n"

        #                serviceResultFile.write(ln)
        #            else:
        #                httpConn.close()
    return

if __name__ == '__main__':
    main()