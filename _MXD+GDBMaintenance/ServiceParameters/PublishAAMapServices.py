import sys
import arcpy
import datetime
import os
import logging
import pyodbc
import xml.dom.minidom as dom
import urllib
import httplib
import json
import getpass

now = datetime.datetime.now().strftime('%m-%d-%Y_%H_%M_%S')
logfile = "PublishMapServices_Log_" + now + ".log"
logging.basicConfig(filename=logfile, format="%(asctime)s %(message)s", datefmt="%m/%d/%Y %I:%M:%S %p", level=logging.DEBUG)

def get_services_data(serviceType):
    # user = r"sa"
    # password = r"H0neyBadger$"
    conn = pyodbc.connect("Driver={ODBC Driver 17 for SQL Server};"
                          "Server=gissql.ruekert-mielke.com;"
                          "Database=_GISAdmin;"
                          "uid=GIS;"
                          "pwd=TacoT!ME4BJJ;")
    cursor = conn.cursor()

    if serviceType == 'ALL':
        cursor.execute(r"SELECT RESTFolder, FeatureMXDFilePath, SecureMXDFilePath, OverlayMXDFilePath, CustomMXDFilePath FROM AA_SiteInfo WHERE IsAssetAlly = 1 AND ProcessUpdate = 1 AND SiteName = 'testing';")
        row = cursor.fetchone()

        RESTFolders, featureMXDs, secureMXDs, overlayMXDs, customMXDs = [], [], [], [], []
        while row:
            # Convert unicode tuples to strings and then move to next row
            RESTFolders.append(row[0].encode("ascii"))
            featureMXDs.append(row[1])
            secureMXDs.append(row[2])
            overlayMXDs.append(row[3])
            customMXDs.append(row[4])
            row = cursor.fetchone()

        [f.encode("ascii") if f is not None else None for f in featureMXDs]
        [s.encode("ascii") if s is not None else None for s in secureMXDs]
        [o.encode("ascii") if o is not None else None for o in overlayMXDs]
        [c.encode("ascii") if c is not None else None for c in customMXDs]

        conn.close()
        return RESTFolders, featureMXDs, secureMXDs, overlayMXDs, customMXDs
    else:
        cursor.execute(r"SELECT RESTFolder, {}MXDFilePath FROM AA_SiteInfo WHERE IsAssetAlly = 1 AND ProcessUpdate = 1 AND SiteName = 'testing';".format(serviceType.title()))
        row = cursor.fetchone()

        RESTFolders, MXDs = [], []
        while row:
            # Convert unicode tuples to strings and then move to next row
            RESTFolders.append(row[0].encode("ascii"))
            MXDs.append(row[1])
            row = cursor.fetchone()

        [m.encode("ascii") if m is not None else None for m in MXDs]

        conn.close()
        return RESTFolders, MXDs

def getToken(username, password, serverName, serverPort):
    tokenURL = r"/arcgis/admin/generateToken"
    params = urllib.urlencode({'username': username, 'password': password, 'client': 'requestip', 'f': 'json'})
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}

    httpConn = httplib.HTTPConnection(serverName, serverPort)
    httpConn.request("POST", tokenURL, params, headers)

    response = httpConn.getresponse()
    if (response.status != 200):
        httpConn.close()
        print "Error while fetch tokens from admin URL. Please check the URL and try again."
        return
    else:
        data = response.read()
        httpConn.close()
        token = json.loads(data)
        print token
        return token['token']

def assertJsonSuccess(data):
    obj = json.loads(data)
    if 'status' in obj and obj['status'] == "error":
        print "Error: JSON object returns an error. " + str(obj)
        return False
    else:
        return True

def get_service_parameters(serviceFolder, serviceType):
    username = r"siteadmin"
    password = r"u#A8G}fW:}2&#h*xo-KpDwd&"
    serverName = r"elb.ags.ruekert-mielke.com"
    serverPort = 80
    
    token = getToken(username, password, serverName, serverPort)

    serverURL = r"/arcgis/admin/services"
    params = urllib.urlencode({'token': token, 'f': 'json'})
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    
    httpConn = httplib.HTTPConnection(serverName, serverPort)
    httpConn.request("POST", serverURL, params, headers)

    response = httpConn.getresponse()
    if (response.status != 200):
        httpConn.close()
        print "Could not read folder information."
        return
    else:
        data = response.read()

        if not assertJsonSuccess(data):          
            print "Error when reading server information. " + str(data)
            return
        else:
            print "Processed server information successfully. Now processing folders..."

        dataObj = json.loads(data)
        httpConn.close()
        
        folder = serviceFolder
        if folder != "":
            folder += "/"

        folderURL = "/arcgis/admin/services/" + folder
        params = urllib.urlencode({'token': token, 'f': 'json'})
        headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}

        httpConn = httplib.HTTPConnection(serverName, serverPort)
        httpConn.request("POST", folderURL, params, headers)

        response = httpConn.getresponse()
        if (response.status != 200):
            httpConn.close()
            print "Could not read folder information."
            return
        else:
            data = response.read()

            if not assertJsonSuccess(data):          
                print "Error when reading folder information. " + str(data)
            else:
                print "Processed folder information successfully. Now processing services..."

            dataObj = json.loads(data)
            print "dataObj: " + str(dataObj) + "\n"

            httpConn.close()
            for item in dataObj["services"]:
                print "dataObj['services']: " + str(item) + "\n"
                if serviceFolder.endswith('AA'):
                    serviceFolder = serviceFolder[:-2]
                    
                if item["type"] == "MapServer":
                    if folder:
                        # Note that folder has a slash appened: folder == "DemoAA/"
                        sUrl = "/arcgis/admin/services/%s%s.%s" %(folder, folder + serviceType, item["type"])
                    else:
                        sUrl = "/arcgis/admin/services/%s.%s" %(folder + serviceType, item["type"])

                    print sUrl
                    httpConn.request("POST", sUrl, params, headers)
                    jsonOBJ = json.loads(httpConn.getresponse().read())

                    print "jsonOBJ: " + str(jsonOBJ) + "\n"

                    if folder:
                        statusUrl = "/arcgis/admin/services/%s%s.%s/status" %(folder, item["serviceName"], item["type"])
                    else:
                        statusUrl = "/arcgis/admin/services/%s.%s/status" %(item["serviceName"], item["type"])

                    print statusUrl
                    httpConn.request("POST", statusUrl, params, headers)
                    jsonOBJStatus = json.loads(httpConn.getresponse().read()) 

    return jsonOBJ

def modify_sddraft(sddraft, serviceFolder, serviceType):
    doc = dom.parse(sddraft)

    jsonOBJ = get_service_parameters(serviceFolder, serviceType)
    print jsonOBJ

    types = doc.getElementsByTagName('TypeName')
    for type in types:
        # Turn off KmlServer
        if type.firstChild.data == 'KmlServer':
            for extChild in type.parentNode.childNodes:
                if extChild.tagName == 'Enabled':
                    extChild.firstChild.data = 'false'
        # Turn on FeatureServer for FEATURE services
        if type.firstChild.data == 'FeatureServer':
            for extChild in type.parentNode.childNodes:
                if extChild.tagName == 'Enabled':
                    extChild.firstChild.data = 'true'

    keys = doc.getElementsByTagName('Key')
    for key in keys:
        if key.hasChildNodes():
            if key.firstChild.data == 'antialiasingMode':
                key.nextSibling.firstChild.data = jsonOBJ['antialiasingMode']
            if key.firstChild.data == 'textAntialiasingMode':
                key.nextSibling.firstChild.data = jsonOBJ['textAntialiasingMode']
            if key.firstChild.data == 'schemaLockingEnabled':
                key.nextSibling.firstChild.data = jsonOBJ['schemaLockingEnabled']
            if key.firstChild.data == 'enableDynamicLayers':
                key.nextSibling.firstChild.data = jsonOBJ['enableDynamicLayers']
            if key.firstChild.data == 'maxRecordCount':
                key.nextSibling.firstChild.data = jsonOBJ['maxRecordCount']
            if key.firstChild.data == 'MinInstances':
                key.nextSibling.firstChild.data = jsonOBJ['minInstancesPerNode']
            if key.firstChild.data == 'MaxInstances':
                key.nextSibling.firstChild.data = jsonOBJ['maxInstancesPerNode']
            if key.firstChild.data == "UsageTimeout":
                key.nextSibling.firstChild.data = jsonOBJ['maxUsageTime']
            if key.firstChild.data == "WaitTimeout":
                key.nextSibling.firstChild.data = jsonOBJ['maxWaitTime']
            if key.firstChild.data == "IdleTimeout":
                key.nextSibling.firstChild.data = jsonOBJ['maxIdleTime']
            if key.firstChild.data == "recycleInterval":
                key.nextSibling.firstChild.data = jsonOBJ['recycleInterval']
            if key.firstChild.data == "recycleStartTime":
                key.nextSibling.firstChild.data = jsonOBJ['recycleStartTime']
            if key.firstChild.data == "IsCached":
                key.nextSibling.firstChild.data = jsonOBJ['isCached']
            if key.firstChild.data == "cacheOnDemand":
                key.nextSibling.firstChild.data = jsonOBJ['cacheOnDemand']
            if key.firstChild.data == "clientCachingAllowed":
                key.nextSibling.firstChild.data = jsonOBJ['clientCachingAllowed']

    # Output _NEW.sddraft, and remove original sddraft
    newSddraft = sddraft.split('.')[0] + '_NEW.sddraft'
    f = open(newSddraft, 'w')
    doc.writexml(f)
    f.close()
    os.remove(sddraft)

    return newSddraft

def analyze_sddraft(newSddraft):
    logging.info('Analyzing service definition for errors')  
    analysis = arcpy.mapping.AnalyzeForSD(newSddraft)

    logging.info("The following information was returned during analysis of the MXD:")
    for key in ('messages', 'warnings', 'errors'):
        logging.info('----' + key.upper() + '---')
        for ((message, code), layers) in analysis[key].iteritems():
            for layer in layers:
                logging.info('MESSAGE: {}; CODE {}; LAYER {}'.format(message, code, layer.name))

    return analysis

def publish_service(agsConnection, serviceFolder, serviceType, mxd):    
    def delete_sd():  
        try:
            logging.info('Removing sd file: {}'.format(sd))  
            os.remove(sd)
        except OSError:  
            pass 
      
    mxd = mxd.rstrip("\r\n")
    logging.info('\n Publishing map document to map service: {}'.format(mxd))  

    sdPath = mxd.split('.')[0]
    sd = sdPath + '.sd'
    sddraft = sd + 'draft'  

    # Remove existing service definition, if exists, since it will not overwrite and just fail
    delete_sd()

    mapDocument = arcpy.mapping.MapDocument(mxd)
    serviceName = mxd.split('.')[0].split('\\')[-1]

    logging.info('Creating .sddraft file: {}'.format(sddraft)) 
    arcpy.mapping.CreateMapSDDraft(map_document=mapDocument, out_sddraft=sddraft, service_name=serviceName, 
                                   server_type='ARCGIS_SERVER', connection_file_path=agsConnection, 
                                   folder_name=serviceFolder, summary='{0} service for {1}'.format(serviceType.title(), serviceFolder), tags='AssetAlly')  
    
    newSddraft = modify_sddraft(sddraft, serviceFolder, serviceType)

    analysis = analyze_sddraft(newSddraft)

    if analysis['errors'] == {}:
        logging.info("Staging and uploading service definition.")
        arcpy.StageService_server(in_service_definition_draft=newSddraft, out_service_definition=sd)  
        arcpy.UploadServiceDefinition_server(in_sd_file=sd, in_server=agsConnection, in_service_name=serviceName, 
                                             in_folder_type='EXISTING', in_folder=serviceFolder)  
        logging.info("Service successfully published: {}".format(mxd))
    else:  
        logging.warn(str(analysis['errors']))  
        logging.warn("Service {} could not be published because errors were found during analysis.".format(mxd))  

    delete_sd()  
    return

def main():
    serviceType = sys.argv[1]

    agsConnection = r"I:\\DatabaseConnections\\ArcServer Connections\\arcgis on elb.ags.ruekert-mielke.com (admin).ags"
    #agsConnection = r"I:\\DatabaseConnections\\ArcServer Connections\\arcgis on www.crunchwrap.org (admin).ags"
    logging.info('Publishing to ArcGIS Server: {}'.format(agsConnection))

    if serviceType == 'ALL':
        RESTFolders, featureMXDs, secureMXDs, overlayMXDs, customMXDs = get_services_data(serviceType)
        for i in range(len(RESTFolders)):
            logging.info("Processing all services for {}".format(RESTFolders[i]))
            if featureMXDs[i] != None:
                publish_service(agsConnection, RESTFolders[i], 'FEATURE', featureMXDs[i])
            if secureMXDs[i] != None:
                publish_service(agsConnection, RESTFolders[i], 'SECURE', secureMXDs[i])
            if overlayMXDs[i] != None:
                publish_service(agsConnection, RESTFolders[i], 'OVERLAY', overlayMXDs[i])
            if customMXDs[i] != None:
                publish_service(agsConnection, RESTFolders[i], 'CUSTOM', customMXDs[i])
    else:
        print 'Processing {} service.'.format(serviceType)
        RESTFolders, serviceMXDs = get_services_data(serviceType)
        for i in range(len(RESTFolders)):
            logging.info("Processing {0} service for {1}".format(serviceType.title(), RESTFolders[i].title()))
            if serviceMXDs[i] != None:
                publish_service(agsConnection, RESTFolders[i], serviceType, serviceMXDs[i])

    logging.info('Finished publishing map services! :^B')
    print 'Finished publishing map services to ArcGIS Server.'
    print 'Please see ' + logfile + ' for details.'

    return

if __name__ == '__main__':
    main()