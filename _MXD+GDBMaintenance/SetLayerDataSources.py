import arcpy.mapping as mp
import logging
import sys
import os

now = datetime.datetime.now().strftime('%m-%d-%Y_%H_%M_%S')
logfile = "SetLayerDataSources_Log_{}.log".format(now)
logging.basicConfig(filename=logfile, format="%(asctime)s %(message)s", datefmt="%m/%d/%Y %I:%M:%S %p", level=logging.DEBUG)

clientName = sys.argv[1].capitalize()
serviceType = sys.argv[2].upper()

mxd = mp.MapDocument(r"I:\WebDevelopment\AGS\AWS_MSD\{0}_AssetAlly\{0}{1}.mxd".format(clientName, serviceType))
logging.info("Map Document: \t {}".format(mxd))
df = mp.ListDataFrames(mxd, wildcard="Layers")[0]
logging.info("DataFrame: \t {}".format(df))

if serviceType == 'OVERLAY':
    lyrNames = ['Valve Inspections', 'Fire Flow', 'Sanitary Cleaning Forecast', 'Valves by Year Exercised', 'Valve Repairs Needed', 'Valve Repairs Made', 'Sanitary Structure Inspections', 'Sanitary Structure Repairs Needed',
                'Sanitary Structure Repairs Made', 'Storm Structure Inspections', 'Storm Structure Repairs Needed', 'Storm Structure Repairs Made', 'Curb Stop Repairs Needed', 'Curb Stop Repairs Made', 'Hydrant Inspections', 
                'Hydrant Repairs Needed', 'Hydrant Repairs Made', 'Hydrants since Last Flushed', 'Sanitary Structure - Year Inspected', 'Tree Maintenance', 'Trees by Pruning Year Due', 'Hydrants by Year Flow Tested',
                'Water Hydrants by Year Winterized', 'Water Meters - Years to Change Out', 'Water Meters - Years to CCI', 'Water Meters - Years to Flow Test', 'Valve Attachments', 'Hydrant Attachments', 'Storm Structure Attachments',
                'San Structure Attachments', 'Televised Sanitary Pipes', 'Water Pipe Attachments', 'Storm Pipe Attachments', 'San Pipe Attachments', 'Storm Pond Attachments', 'Sanitary Pipe by Size', 'Water Pipe by Size',
                'Sanitary Pipe by Year Cleaned', 'Sanitary Pipe Cleaning Cycle', 'Sanitary Pipe Televising Cycle']
elif serviceType == 'SECURE':  
    lyrNames = ['Documents', 'Sanitary Structure', 'Sanitary Pipe', 'Abandoned Sanitary Pipe', 'Abandoned Sanitary Manhole', 'Water Hydrant', 'Water Valve', 'Water Structure', 'Water Pipe', 'Abandoned Water Hydrant',
                'Abandoned Water Valve', 'Abandoned Water Structure', 'Abandoned Water Pipe', 'Water Meters', 'Water Basins', 'Sanitary Basins', 'Storm Structure', 'Storm Pipe', 'Abandoned Storm Structure', 'Abandoned Storm Pipe',
                'Street Signs', 'Pipe Breaks', 'Curb Stop', 'Private Wells', 'Storm Pond', 'Trees', 'Spot Repairs', 'Light Fixtures', 'Street Poles', 'Basement Backups', 'Open Service Requests', 'Closed Service Requests',
                'Illicit Discharge Outfall Classes', 'Open Requests', 'Closed Requests', 'Open Requests - Polygon', 'Closed Requests - Polygon', 'Clearwater Code Compliance', 'In Stream Sampling', 'Erosion Control Sites']

for lyrName in lyrNames:
    addLyr = mp.Layer(os.path.join(r"I:\WebDevelopment\Resources\LayerFiles\AssetAlly", serviceType, lyrName + r".lyr"))
    mp.AddLayer(data_frame=df, add_layer=addLyr, add_position='BOTTOM')
    logging.info("Layer added: {}".format(lyrName))

clientGISWorkspacePath = os.path.join(r"I:\DatabaseConnections\WebUser Connections", clientName) + r"_AssetAlly_GIS.NorwayAA@gissql.ruekert-mielke.com.sde"
clientSDEWorkspacePath = os.path.join(r"I:\DatabaseConnections\WebUser Connections", clientName) + r"_AssetAlly_SDE.NorwayAA@gissql.ruekert-mielke.com.sde"

layers = mp.ListLayers(mxd, df)
for lyr in layers:
    if "Requests" in lyr.name: # service request layers
        logging.info("Replacing workspace path for GIS layer {0} \n\t\t\t Old workspace path = {1}".format(lyr.name, lyr.workspacePath))
        lyr.findAndReplaceWorkspacePath(find_workspace_path=lyr.workspacePath, replace_workspace_path=clientGISWorkspacePath, validate=False)
        logging.info("Replaced workspace path for GIS layer {0} \n\t\t\t New workspace path = {1}".format(lyr.name, lyr.workspacePath))
    else: # all other layers
        logging.info("Replacing workspace path for SDE layer {0} \n\t\t\t Old workspace path = {1}".format(lyr.name, lyr.workspacePath))
        lyr.findAndReplaceWorkspacePath(find_workspace_path=lyr.workspacePath, replace_workspace_path=clientSDEWorkspacePath, validate=False)
        logging.info("Replaced workspace path for SDE layer {0} \n\t\t\t New workspace path = {1}".format(lyr.name, lyr.workspacePath))

    if lyr.supports("DATASOURCE") and lyr.isFeatureLayer:
        # Update dataset names within the new workspace
        logging.info("Replacing data source for layer: {0} \n\t\t\t Old data source {1}".format(lyr.name, lyr.dataSource))
        lyr.replaceDataSource(workspace_path=lyr.workspacePath, workspace_type="SDE_WORKSPACE", validate=False)
        logging.info("Replaced data source for layer: {0} \n\t\t\t New data source {1}".format(lyr.name, lyr.dataSource))

    if "Demo" in lyr.dataSource:
        logging.info("Data source not properly set: {}".format(lyr.name))
        continue

    if lyr.isBroken:
        logging.error("Broken data source: {}".format(lyr.dataSource))
        continue

mxd.save()
#mxd.saveACopy()