import argparse
import arcpy.mapping as mp
import datetime
import sys
import os
import logging
import pymssql

def get_services_data(serviceType):
    server = r"gissql.ruekert-mielke.com"
    user = r"sa"
    password = r"H0neyBadger$"
    database = r"_GISAdmin"
    connection = pymssql.connect(server, user, password, database)
    cursor = connection.cursor()

    if serviceType == 'ALL':
        cursor.execute(r"SELECT SiteName, FeatureMXDFilePath, SecureMXDFilePath, OverlayMXDFilePath FROM AA_SiteInfo WHERE IsAssetAlly=1 AND ProcessUpdate=1;")
        row = cursor.fetchone()

        RESTFolders, featureMXDs, secureMXDs, overlayMXDs = [], [], [], [], []
        while row:
            # Convert unicode tuples to strings and then move to next row
            RESTFolders.append(row[0].encode("ascii"))
            featureMXDs.append(row[1])
            secureMXDs.append(row[2])
            overlayMXDs.append(row[3])
            row = cursor.fetchone()

        [f.encode("ascii") if f is not None else None for f in featureMXDs]
        [s.encode("ascii") if s is not None else None for s in secureMXDs]
        [o.encode("ascii") if o is not None else None for o in overlayMXDs]

        connection.close()
        return RESTFolders, featureMXDs, secureMXDs, overlayMXDs
    else:
        cursor.execute(r"SELECT SiteName, {}MXDFilePath FROM AA_SiteInfo WHERE IsAssetAlly=1 AND ProcessUpdate=1;".format(serviceType.title()))
        row = cursor.fetchone()

        RESTFolders, MXDs = [], []
        while row:
            # Convert unicode tuples to strings and then move to next row
            RESTFolders.append(row[0].encode("ascii"))
            MXDs.append(row[1])
            row = cursor.fetchone()

        [m.encode("ascii") if m is not None else None for m in MXDs]

        connection.close()
        return RESTFolders, MXDs

def set_dataframe_properties(clientDF, tempDF):
    logging.info("Setting DataFrame properties...")
    tempDF.credits = "Ruekert & Mielke, Asset Ally, 2020"
    tempDF.description = "Standard Asset Ally map document data frame"
    tempDF.name = clientDF.name
    logging.info("\t Name: {}".format(tempDF.name))
    tempDF.extent = clientDF.extent
    logging.info("\t Extent = {}".format(tempDF.extent.JSON))
    tempDF.spatialReference = clientDF.spatialReference
    logging.info("\t Spatial Reference = {}".format(tempDF.spatialReference.name))
    tempDF.geographicTransformations = clientDF.geographicTransformations
    logging.info("\t Geographic transformations = {}".format(tempDF.geographicTransformations))

    # Set other DataFrame properties (shouldn't be different, but to make sure)
    tempDF.displayUnits = clientDF.displayUnits
    tempDF.elementHeight = clientDF.elementHeight
    tempDF.elementPositionX = clientDF.elementPositionX
    tempDF.elementPositionY = clientDF.elementPositionY
    tempDF.elementWidth = clientDF.elementWidth
    tempDF.referenceScale = clientDF.referenceScale
    tempDF.rotation = clientDF.rotation
    tempDF.scale = clientDF.scale

    return

def set_workspace_paths(clientName, tempLyrList):
    #tempGISWorkspacePath = r"I:\DatabaseConnections\WebUser Connections\Demo_AssetAlly_GIS.AssetAlly@gissql.ruekert-mielke.com.sde"
    #tempSDEWorkspacePath = r"I:\DatabaseConnections\WebUser Connections\Demo_AssetAlly_SDE.AssetAlly@gissql.ruekert-mielke.com.sde"   

    clientGISWorkspacePath = os.path.join(r"I:\DatabaseConnections\WebUser Connections", clientName) + r"_AssetAlly_GIS.AssetAlly@gissql.ruekert-mielke.com.sde"
    clientSDEWorkspacePath = os.path.join(r"I:\DatabaseConnections\WebUser Connections", clientName) + r"_AssetAlly_SDE.AssetAlly@gissql.ruekert-mielke.com.sde"
    
    for lyr in tempLyrList:
        if "Requests" in lyr.name: # service request layers
            logging.info("Replacing workspace path for GIS layer {0} \n\t\t\t Old workspace path = {1}".format(lyr.name, lyr.workspacePath))
            lyr.findAndReplaceWorkspacePath(find_workspace_path=lyr.workspacePath, replace_workspace_path=clientGISWorkspacePath, validate=False)
            logging.info("Replaced workspace path for GIS layer {0} \n\t\t\t New workspace path = {1}".format(lyr.name, lyr.workspacePath))
        else: # all other layers
            logging.info("Replacing workspace path for SDE layer {0} \n\t\t\t Old workspace path = {1}".format(lyr.name, lyr.workspacePath))
            lyr.findAndReplaceWorkspacePath(find_workspace_path=lyr.workspacePath, replace_workspace_path=clientSDEWorkspacePath, validate=False)
            logging.info("Replaced workspace path for SDE layer {0} \n\t\t\t New workspace path = {1}".format(lyr.name, lyr.workspacePath))

    logging.info("Workspace paths set for all layers.")
    return

def set_data_sources(clientLyrList, tempLyrList, tempDF):
    for lyr in tempLyrList:
        # Loop through clientLyrList and filter out layer equal to the current lyr from tempLyrList
        clientLyr = [clientLyrList[i] for i in range(len(clientLyrList)) if clientLyrList[i].name == lyr.name][0]
        
        if lyr.supports("DATASOURCE"):
            # Update dataset names within the new workspace
            logging.info("Replacing data source for layer: {0} \n\t\t\t Old data source {1}".format(lyr.name, lyr.dataSource))
            lyr.replaceDataSource(workspace_path=lyr.workspacePath, workspace_type="SDE_WORKSPACE", dataset_name=clientLyr.datasetName, validate=False)
            logging.info("Replaced data source for layer: {0} \n\t\t\t New data source {1}".format(lyr.name, lyr.dataSource))

        #if lyr.supports("DATASOURCE") and "svw" not in lyr.datasetName:
        #    # Update dataset names within the new workspace
        #    logging.info("Replacing data source for layer: {0} \n\t\t\t Old data source {1}".format(lyr.name, lyr.dataSource))
        #    lyr.replaceDataSource(workspace_path=lyr.workspacePath, workspace_type="SDE_WORKSPACE", dataset_name=clientLyr.datasetName, validate=False)
        #    logging.info("Replaced data source for layer: {0} \n\t\t\t New data source {1}".format(lyr.name, lyr.dataSource))
        #elif lyr.supports("DATASOURCE") and "svw" in lyr.datasetName:
        #    logging.info("Removing temp query layer {0} with data source".format(lyr.name, lyr.datasetName))
        #    mp.RemoveLayer(data_frame=tempDF, remove_layer=lyr)
        #    logging.info("Adding client query layer {0} with data source {1}".format(clientLyr.name, clientLyr.datasetName))
        #    mp.AddLayer(data_frame=tempDF, add_layer=clientLyr)

        if "Demo" in lyr.dataSource:
            logging.info("Data source not properly set: {}".format(lyr.name))
            continue

        if lyr.isBroken:
            logging.error("Broken data source: {}".format(lyr.dataSource))
            continue

    logging.info("Data sources set for all layers.")
    return

def copy_template(clientName, serviceType, clientMxdPath):
    # Load template MXD
    templateMxdPath = r"I:\WebDevelopment\AGS\AWS_MSD\Demo_AssetAlly\AssetAlly{}.mxd".format(serviceType)
    templateMXD = mp.MapDocument(templateMxdPath)

    # Load the old MXD and DataFrame (contains client-specific data to retain)
    clientMXD = mp.MapDocument(clientMxdPath)
    clientDF = mp.ListDataFrames(clientMXD, wildcard='Layers')[0]
    clientLyrList = mp.ListLayers(map_document_or_layer=clientMXD, data_frame=clientDF)
    
    # Save a copy of the template MXD as the new, updated MXD
    # Calling it "temp" for temporary, since it will be deleted from disk after saving a copy at the oringial path
    logging.info("Copying template {} MXD to ".format(serviceType))
    tempMxdPath = clientMxdPath.split('.')[0] + '_Temp.' + clientMxdPath.split('.')[-1]
    logging.info("\t Path: " + tempMxdPath)
    templateMXD.saveACopy(tempMxdPath)
    # Clean up reference
    del templateMXD
    
    # Load the new MXD and DataFrame
    tempMXD = mp.MapDocument(tempMxdPath)
    tempDF = mp.ListDataFrames(tempMXD, wildcard='Layers')[0]
    tempLyrList = mp.ListLayers(map_document_or_layer=tempMXD, data_frame=tempDF)

    # Set the client-specific DataFrame properties, in the new (temp) MXD, from the old (client) MXD
    set_dataframe_properties(clientDF, tempDF)
    
    # Set workspace paths for all layers
    set_workspace_paths(clientName, tempLyrList)

    # Set data sources (= workspace path + dataset name) for layers
    set_data_sources(clientLyrList, tempLyrList, tempDF)

    # Save the layer data source changes    
    tempMXD.save()
    logging.info("MXD saved with new workspace paths.")

    # Clean up and "overwrite" the old, outdated MXD with the new, updated MXD
    logging.info("Data transfer complete. Deleting outdated MXD and replacing with updated MXD.")
    del clientMXD
    os.remove(clientMxdPath)            # Remove the original MXD to make way for the updated
    tempMXD.saveACopy(clientMxdPath)    # Save the updated MXD in the place of the outdated (will overwrite)
    logging.info("Replaced outdated MXD with updated MXD: {}".format(clientMxdPath))
    del tempMXD
    os.remove(tempMxdPath)              # Remove the temporary MXD

    return

def main():
    try:
        serviceType = sys.argv[1].upper()
        if serviceType not in ['SECURE', 'FEATURE', 'OVERLAY', 'ALL']:
            print "Please enter a valid service type: SECURE, FEATURE, OVERLAY, or ALL"
        print "Processing service: {}".format(serviceType)
    except:
        serviceType = 'ALL'
        print "Processing all services: SECURE, FEATURE, and OVERLAY"
 
    now = datetime.datetime.now().strftime('%m-%d-%Y_%H_%M_%S')
    logfile = "CopyTemplateMXD_Log_{}.log".format(now)
    logging.basicConfig(filename=logfile, format="%(asctime)s %(message)s", datefmt="%m/%d/%Y %I:%M:%S %p", level=logging.DEBUG)

    # Load services metadata from SQL AA_SiteInfo table
    if serviceType == 'ALL':
        siteNames, featureMXDs, secureMXDs, overlayMXDs = get_services_data(serviceType)
        for i in range(len(siteNames)):
            clientName = siteNames[i].title()
            arcpy.env.workspace = r"I:\DatabaseConnections\sa User Connections\{}_SDE.sa@gissql.ruekert-mielke.com.sde".format(clientName)
            if featureMXDs[i] != None:
                logging.info("Processing {0} service for {1}".format('FEATURE', clientName))
                copy_template(clientName, 'FEATURE', featureMXDs[i].rstrip('\r\n'))
            if secureMXDs[i] != None:
                logging.info("Processing {0} service for {1}".format('SECURE', clientName))
                copy_template(clientName, 'SECURE', secureMXDs[i].rstrip('\r\n'))
            if overlayMXDs[i] != None:
                logging.info("Processing {0} service for {1}".format('OVERLAY', clientName))
                copy_template(clientName, 'OVERLAY', overlayMXDs[i].rstrip('\r\n'))
    else: # serviceType specified
        siteNames, serviceMXDs = get_services_data(serviceType)
        for i in range(len(siteNames)):
            clientName = siteNames[i].title()
            arcpy.env.workspace = r"I:\DatabaseConnections\sa User Connections\{}_SDE.sa@gissql.ruekert-mielke.com.sde".format(clientName)
            logging.info("Processing {0} service for {1}".format(serviceType, clientName))
            if serviceMXDs[i] != None:
                copy_template(clientName, serviceType, serviceMXDs[i].rstrip('\r\n'))

    return

if __name__ == "__main__":
    main()
