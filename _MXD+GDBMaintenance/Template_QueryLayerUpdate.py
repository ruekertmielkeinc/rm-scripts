import arcpy, pyodbc
import httplib, urllib, json
import sys
import getpass
from subprocess import call
from arcgisservices import *

#ABOUT: This script processes information stored in the _GISAdmin database.  Instead of having to shut down all services,
#this script shuts down all services in 1 folder, processes database changes, and restarts services.

#REQUIRES: installation of pip and pyodbc on this PC.  See AssetAllyDemo\db_scripts\Utilities\pyodbc\using pyodbc.txt

#USAGE: Before starting, make sure to review _GISAdmin.dbo.AASiteInfo to make sure the ProcessUpdate field is set
#to 1 for those sites you wish to update. Add the Python commands between the run something and done running something comments.

def main(argv=None):
    conn = pyodbc.connect("Driver={ODBC Driver 17 for SQL Server};"
                          "Server=gissql.ruekert-mielke.com;"
                          "Database=_GISAdmin;"
                          "uid=GIS;"
                          "pwd=TacoT!ME4BJJ;")

    cursor = conn.cursor()
    cursor.execute("SELECT * FROM dbo.AA_SiteInfo WHERE (IsAssetAlly = 1 AND ProcessUpdate = 1)")

    for row in cursor:
        folder = str(row.RESTFolder)
        theConn = str(row.ConnectionFile)
        theSDE = str(row.SDEDatabase)
        theGIS = str(row.GISDatabase)
        theDomain = str(row.SiteName)
        overlay = str(row.OverlayMXDFilePath)
        print "Folder: " + folder
        print row.SDEDatabase

        layers_path = "I:\WebDevelopment\Resources\LayerFiles\AssetAlly\OVERLAY"
        
        mxdFile = overlay.strip("\n")   
        mxd = arcpy.mapping.MapDocument(mxdFile)
        df = arcpy.mapping.ListDataFrames(mxd, "Layers")[0]
        print "Working on " + mxdFile
        
        layers = {
                  "Storm Pipe Attachments.lyr": ["SELECT * FROM " + theSDE + ".dbo.svw_StormPipeAsbuilts", "POLYLINE"],
                  "Storm Structure Attachments.lyr": ["SELECT * FROM " + theSDE + ".dbo.svw_StormStructureAsbuilts", "POINT"],
                  "Storm Water Device Attachments.lyr": ["SELECT * FROM " + theSDE + ".dbo.svw_StormPondAsbuilts", "POLYGON"],
                  }
        for layer in layers:
            lyrName = layer.split(".")[0]
            layerList = arcpy.mapping.ListLayers(mxd, lyrName, df)
            if layerList != []:
                print "Layer " + lyrName + " found"
            else:
                print "Layer " + lyrName + " missing for " + folder
                workspacePath = "I:\DatabaseConnections\WebUser Connections\\" + theSDE + "." + folder + "AA@gissql.ruekert-mielke.com.sde"
                qry = layers[layer][0]
                geom = layers[layer][1]
                spref = arcpy.SpatialReference(sr)
                arcpy.MakeQueryLayer_management(input_database=workspacePath, out_layer_name=lyrName, query=qry, oid_fields="UID", shape_type=geom, srid=sr, spatial_reference=spref)
                arcpy.mapping.AddLayer(df, arcpy.mapping.Layer(lyrName), "BOTTOM")
                print "Query layer " + lyrName + " added to map."  
                layerList = arcpy.mapping.ListLayers(mxd, lyrName, df)
            
            inLayer = layerList[0]
            inSymbologyLayer = os.path.join(layers_path, layer)
            arcpy.ApplySymbologyFromLayer_management(in_layer=inLayer, in_symbology_layer=inSymbologyLayer)
            print "Symbology applied - " + lyrName
        
        print "Saving File " + mxdFile
        mxd.save()
        del mxd


         
    conn.close()
        
# Script start
if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
