import arcpy
import logging
import time
import datetime
import pymssql

now = datetime.datetime.now().strftime('%m-%d-%Y_%H_%M_%S')
logging.basicConfig(filename="GDBAdministration_Log_" + now + ".log", format="%(asctime)s %(message)s", datefmt="%m/%d/%Y %I:%M:%S %p", level=logging.DEBUG)
logging.info("Beginning geodatabase maintenance")

def get_database_connections():
    # Set SQL connection parameters
    server = r"gissql.ruekert-mielke.com"
    user = r"sa"
    password = r"H0neyBadger$"
    database = r"_GISAdmin"

    # Connect to database as administrator
    connection = pymssql.connect(server, user, password, database)

    # Instantiate cursor and SELECT column of sa (admin) connections
    cursor = connection.cursor()
    cursor.execute(r"SELECT SAConnectionFile FROM AASiteInfo;")
    row = cursor.fetchone()

    # Append sa connection file strings to list
    adminConns = []
    while row:
        # Convert unicode tuple to string and move to next row
        adminConns.append(row[0].encode("ascii"))
        row = cursor.fetchone()

    # Close connection to _GISAdmin
    connection.close()

    return adminConns

def close_database(conn):
    # Block new connections to the database.
    logging.info("The database is no longer accepting connections")
    time.sleep(10)
    arcpy.AcceptConnections(conn, False)

    # Disconnect all users from the database.
    logging.info("Disconnecting all users")
    arcpy.DisconnectUser(conn, "ALL")

    return

def reconcile_versions(conn):
    # Get a list of versions to pass into the ReconcileVersions tool.
    logging.info("Compiling a list of versions to reconcile")
    versionList = arcpy.ListVersions(conn)

    if (len(versionList) == 0):
        logging.warn("Geodatabase is not versioned.")
    elif (len(versionList) == 1):
        logging.warn("Only version is default: " + str(versionList[0]))
    else:
        logging.warn("Multiple versions exist: " + str(versionList))
        logging.warn("Allowing users to connect to the database")
        arcpy.AcceptConnections(conn, True)
        logging.warn("Skipping " + conn)

    return

def open_database(conn):    
    # Run the compress tool. 
    logging.info("Compressing geodatabase")
    arcpy.Compress_management(conn)

    # Allow the database to begin accepting connections again.
    logging.info("Allowing users to connect to the database")
    arcpy.AcceptConnections(conn, True)
    return

def rebuild_indexes(conn):
    # Rebuild indexes on the system tables
    # Note: to use the "SYSTEM" option the user must be an geodatabase or database administrator.
    logging.info("Rebuilding indexes on the system tables")
    arcpy.RebuildIndexes_management(conn, "SYSTEM")
    return

def analyze_dataset(conn):
    # Update statistics on the system tables.
    logging.info("Updating statistics on the system tables")
    arcpy.AnalyzeDatasets_management(conn, "SYSTEM")
    return

def main():
    adminConns = get_database_connections()
    for conn in adminConns:
        logging.info("Processing database " + conn)
        # Set the workspace
        #   e.g., arcpy.env.workspace = r"I:\DatabaseConnections\sa User Connections\Testing_AssetAlly_SDE.sa@gissql.ruekert-mielke.com.sde"  
        arcpy.env.workspace = str(conn)
        adminConn = arcpy.env.workspace

        close_database(conn)
        reconcile_versions(conn)
        open_database(conn)
        rebuild_indexes(conn)
        analyze_datasets(conn)

        logging.info("Finished with database: " + conn)

    logging.info("Finished processing all databases")
    logging.info("Geodatabase maintenance finished.")

main()