import arcpy.mapping as mp
import os
import logging
import sys
import pymssql

now = datetime.datetime.now().strftime('%m-%d-%Y_%H_%M_%S')
logfile = "AddLayersToMapDocument_Log_" + now + ".log"
logging.basicConfig(filename=logfile, format="%(asctime)s %(message)s", datefmt="%m/%d/%Y %I:%M:%S %p", level=logging.DEBUG)

def get_services_data():
    server = r"gissql.ruekert-mielke.com"
    user = r"sa"
    password = r"H0neyBadger$"
    database = r"_GISAdmin"
    connection = pymssql.connect(server, user, password, database)

    cursor = connection.cursor()
    cursor.execute(r"SELECT SAConnectionFile, SDEDatabase FROM AA_SiteInfo WHERE IsAssetAlly='True' AND SiteName='norway';")
    row = cursor.fetchone()

    adminConnections, databases = [], []
    while row:
        adminConnections.append(row[0].encode("ascii"))
        databases.append(row[1].encode("ascii"))
        row = cursor.fetchone()

    connection.close()
    return adminConnections, databases

def add_layers(df, serviceType):
    if serviceType == 'OVERLAY':
        lyrNames = ['Valve Inspections', 'Fire Flow', 'Sanitary Cleaning Forecast', 'Valves by Year Exercised', 'Valve Repairs Needed', 'Valve Repairs Made', 'Sanitary Structure Inspections', 'Sanitary Structure Repairs Needed',
                     'Sanitary Structure Repairs Made', 'Storm Structure Inspections', 'Storm Structure Repairs Needed', 'Storm Structure Repairs Made', 'Curb Stop Repairs Needed', 'Curb Stop Repairs Made', 'Hydrant Inspections', 
                     'Hydrant Repairs Needed', 'Hydrant Repairs Made', 'Hydrants since Last Flushed', 'Sanitary Structure - Year Inspected', 'Tree Maintenance', 'Trees by Pruning Year Due', 'Hydrants by Year Flow Tested',
                     'Water Hydrants by Year Winterized', 'Water Meters - Years to Change Out', 'Water Meters - Years to CCI', 'Water Meters - Years to Flow Test', 'Valve Attachments', 'Hydrant Attachments', 'Storm Structure Attachments',
                     'San Structure Attachments', 'Televised Sanitary Pipes', 'Water Pipe Attachments', 'Storm Pipe Attachments', 'San Pipe Attachments', 'Storm Pond Attachments', 'Sanitary Pipe by Size', 'Water Pipe by Size',
                     'Sanitary Pipe by Year Cleaned', 'Sanitary Pipe Cleaning Cycle', 'Sanitary Pipe Televising Cycle']
    elif serviceType == 'SECURE':  
        lyrNames = ['Documents', 'Sanitary Structure', 'Sanitary Pipe', 'Abandoned Sanitary Pipe', 'Abandoned Sanitary Manhole', 'Water Hydrant', 'Water Valve', 'Water Structure', 'Water Pipe', 'Abandoned Water Hydrant',
                    'Abandoned Water Valve', 'Abandoned Water Structure', 'Abandoned Water Pipe', 'Water Meters', 'Water Basins', 'Sanitary Basins', 'Storm Structure', 'Storm Pipe', 'Abandoned Storm Structure', 'Abandoned Storm Pipe',
                    'Street Signs', 'Pipe Breaks', 'Curb Stop', 'Private Wells', 'Storm Pond', 'Trees', 'Spot Repairs', 'Light Fixtures', 'Street Poles', 'Basement Backups', 'Open Service Requests', 'Closed Service Requests',
                    'Illicit Discharge Outfall Classes', 'Open Requests', 'Closed Requests', 'Open Requests - Polygon', 'Closed Requests - Polygon', 'Clearwater Code Compliance', 'In Stream Sampling', 'Erosion Control Sites']
    else:
        logging.error("No service type provided. Exiting.")
        sys.exit()

    lyrDir =  r'I:\\WebDevelopment\\Resources\\LayerFiles\\AssetAlly'
    logging.info("Adding layers to MXD: \t\t" + lyrDir + serviceType)
    for lyrName in lyrNames:
        lyr = mp.Layer(os.path.join(lyrDir + serviceType, lyrName + r".lyr"))
        mp.AddLayer(data_frame=df, add_layer=lyr, add_position='BOTTOM')
        logging.info("Layer added: \t\t" + lyr.name)
        
        adminConnections, databases = get_services_data()

        dataset = databases[0] + '.GIS.' + lyr.datasetName
        #lyr.replaceDataSource(workspace_path=adminConnections[0], workspace_type='SDE_WORKSPACE', dataset_name=dataset, validate=True)
    
        logging.info("Set SDE workspace: \t\t\t" + adminConnections[0])
        logging.info("Set feature class data source: \t" + dataset)

    logging.info("Finished adding layers and setting data sources in MXD.")
    return

def main():
    serviceType = sys.argv[1]
    #clientName = sys.argv[2]

    mxdPath = r"I:\\WebDevelopment\\AGS\\AWS_MSD\\Norway_AssetAlly\\NorwaySECURE.mxd"
    mxd = arcpy.mapping.MapDocument(mxdPath)

    #mxd = mp.MapDocument(mxdDir + clientName + r"\\" + clientName + serviceType + r".mxd")
    df = mp.ListDataFrames(mxd, wildcard="Layers")[0]

    add_layers(df, serviceType)

    mxd.save()

main()
