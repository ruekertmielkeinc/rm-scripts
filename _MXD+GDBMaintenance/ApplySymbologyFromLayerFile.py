import arcpy
import os

layers_path = "I:\WebDevelopment\Resources\LayerFiles\AssetAlly\SECURE"
mxd_paths = file("C:\Users\AStJohn\source\AssetAlly\db_scripts\Utilities\PublishPython\SECUREMXDList.txt", "r").readlines()

for path in mxd_paths:
    if path[0] =="#": #Add a pound in front of the line in the text file to skip it.
        print "Skipping " + path
        continue

    mxdFile = path.strip("\n")   
    mxd = arcpy.mapping.MapDocument(mxdFile)
    df = arcpy.mapping.ListDataFrames(mxd, "Layers")[0]
    print "Working on " + mxdFile

    #clientName = path.split("\\")[-1].split("SECURE")[0]
    #theSDE = clientName + "_AssetAlly_SDE"   
    #theSDE = "Testing_AssetAlly_SDE"

    #workspacePath = "I:\DatabaseConnections\WebUser Connections\\" + theSDE + "." + clientName + "AA@gissql.ruekert-mielke.com.sde"
    #workspacePath = "I:\DatabaseConnections\WebUser Connections\\" + theSDE + "." + "ElmGrove@gissql.ruekert-mielke.com.sde"
    #workspacePath = "I:\DatabaseConnections\WebUser Connections\Testing_AssetAlly_SDE.AssetAlly@gissql.ruekert-mielke.com.sde"
    
    #layers = ["Electrical Structure.lyr", "Traffic Signal.lyr"]
    # for layer in arcpy.mapping.ListLayers(mxd, "Electrical Structure", df):
        # #lyrName = layer.split(".")[0]
        # #inLayer = arcpy.mapping.ListLayers(mxd, lyrName, df)[0]
        # inSymbologyLayer = os.path.join(layers_path, layer.name + ".lyr")
        # arcpy.ApplySymbologyFromLayer_management(in_layer=layer, in_symbology_layer=inSymbologyLayer)
        # print "Symbology applied to " + layer.name
        
    for layer in arcpy.mapping.ListLayers(mxd, "Traffic Signal", df):
        inSymbologyLayer = os.path.join(layers_path, layer.name + ".lyr")
        arcpy.ApplySymbologyFromLayer_management(in_layer=layer, in_symbology_layer=inSymbologyLayer)
        print "Symbology applied to " + layer.name
    
    print "Saving File " + mxdFile
    mxd.save()
    del mxd