# 12-4-2019, Dr. Alexander St. John
#
# Usage:
#   python ReplaceLayerAcrossMXDs.py <OLD_LAYER_NAME> <FULL_PATH_TO_NEW_LAYER_FILE> <FEATURE_CLASS_NAME>
#
#       *All command line arguments must have quotes around them
#       OLD_LAYER_NAME = Name of layer as it appears in the TOC of ArcMap
#       FULL_PATH_TO_NEW_LAYER_FILE = full path to hand-crafted layer file, including “.lyr”, with any updated properties, symbology or otherwise
#       FEATURE_CLASS_NAME = feature class name (after the last .) of the layer’s data source as it appears in ArcCatalog

import arcpy
import arcpy.mapping as mp
import datetime
import sys
import os
import logging
import pymssql

now = datetime.datetime.now().strftime('%m-%d-%Y_%H_%M_%S')
logfile = "ReplaceLayerAcrossMXDs_Log_" + now + ".log"
logging.basicConfig(filename=logfile, format="%(asctime)s %(message)s", datefmt="%m/%d/%Y %I:%M:%S %p", level=logging.DEBUG)

def get_services_data():
    server = r"gissql.ruekert-mielke.com"
    user = r"sa"
    password = r"H0neyBadger$"
    database = r"_GISAdmin"
    connection = pymssql.connect(server, user, password, database)

    cursor = connection.cursor()
    cursor.execute(r"SELECT SAConnectionFile, SDEDatabase, SecureMXDFilePath FROM AA_SiteInfo WHERE IsAssetAlly='True';")
    row = cursor.fetchone()

    adminConnections, databases, secureMXDs= [], [], []
    while row:
        adminConnections.append(row[0].encode("ascii"))
        databases.append(row[1].encode("ascii"))
        secureMXDs.append(row[2])
        row = cursor.fetchone()

    [secure.encode("ascii") if secure is not None else None for secure in secureMXDs]

    connection.close()
    return adminConnections, databases, secureMXDs

def main():
    adminConnections, databases, secureMXDs = get_services_data()
    
    oldLyrName = sys.argv[1]
    newLyrPath = sys.argv[2]
    datasetName = sys.argv[3]

    for i in range(len(secureMXDs)):
        mxd = mp.MapDocument(secureMXDs[i])
        df = mxd.activeDataFrame
        logging.info("Open map document: \t\t" + secureMXDs[i])
        logging.info("Active data frame: \t\t" + df.name)

        lyrList = mp.ListLayers(map_document_or_layer=mxd, wildcard=oldLyrName, data_frame=df)
        if not lyrList:
            logging.warn("No layer found with specified name: \t" + oldLyrName + "\n")
            continue
        else:
            oldLyr = lyrList[0]
            newLyr = mp.Layer(newLyrPath)
            logging.info("Old layer name: \t\t\t" + oldLyr.name)
            logging.info("New layer name: \t\t\t" + newLyr.name)

        connection = adminConnections[i]
        dataset = databases[i] + '.GIS.' + datasetName
        newLyr.replaceDataSource(workspace_path=connection, workspace_type='SDE_WORKSPACE', dataset_name=dataset, validate=True)
        logging.info("SDE workspace: \t\t\t" + connection)
        logging.info("Feature class data source: \t" + dataset)

        mp.UpdateLayer(data_frame=df, update_layer=oldLyr, source_layer=newLyr, symbology_only=False)
        logging.info("Layer updated.")
        mxd.save()
        logging.info("Map document saved. \n")
     
main()
