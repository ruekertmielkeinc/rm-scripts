import arcpy, os
from subprocess import call
    
mxdFile = r"I:\WebDevelopment\AGS\AWS_MSD\Reedsburg_AssetAlly\ReedsburgOVERLAY.mxd" 
mxd = arcpy.mapping.MapDocument(mxdFile)    
print 'Working on ' + mxdFile

df = arcpy.mapping.ListDataFrames(mxd, 'Layers')[0]
layers = arcpy.mapping.ListLayers(mxd, "", df)

for lyr in layers:
    newWorkspacePath = "I:\DatabaseConnections\WebUser Connections\Reedsburg_AssetAlly_SDE.ReedsburgAA@gissql.ruekert-mielke.com.sde"
    if "%" not in lyr.datasetName: # feature layer
        print("Replacing workspace path for feature layer {0} \n\t\t\t Old workspace path = {1}".format(lyr.name, lyr.workspacePath))
        lyr.replaceDataSource(workspace_path=newWorkspacePath, workspace_type="SDE_WORKSPACE")
        print("Replaced workspace path for feature layer {0} \n\t\t\t New workspace path = {1}".format(lyr.name, lyr.workspacePath))
    else: # query layer
        print("Replacing workspace path for query layer {0} \n\t\t\t Old workspace path = {1}".format(lyr.name, lyr.workspacePath))
        lyr.findAndReplaceWorkspacePath(find_workspace_path=lyr.workspacePath, replace_workspace_path=newWorkspacePath)
        # Set unique identifier field, coordinate system, SRID (WKID)
        print("Replaced workspace path for query layer {0} \n\t\t\t New workspace path = {1}".format(lyr.name, lyr.workspacePath))

print "Saving File " + mxdFile
mxd.save()
del mxd

print "Done updating layers."