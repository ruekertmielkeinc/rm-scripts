# Dr. St. John, 11-19-2019

# Usage:
# C:\Python27\ArcGIS10.6\python.exe RegisterSpatialViews.py <CLIENT_NAME>

# CLIENT_NAME  = {Testing_AssetAlly, Demo_AssetAlly, Hartland_AssetAlly, etc.}

# WARNING: Don't run with ArcCatalog or ArcMap open
#           arcgisscripting.ExecuteError: ERROR 000464: Cannot get exclusive schema lock.  Either being edited or in use by another application.

import sys
import os.path
import datetime
import arcpy
import logging
import pymssql
from subprocess import call

def get_parameters():
    server = r"gissql.ruekert-mielke.com"
    user, password = r"sa", r"H0neyBadger$"
    database = r"_GISAdmin"
    connection = pymssql.connect(server, user, password, database)

    cursor = connection.cursor()
    cursor.execute(r"SELECT SpatialViewName, UniqueID, GeometryType, AssignDomains, BaseFeatureDataset, BaseFeatureClass FROM AA_SpatialViewInfo WHERE RegisterSpatialView=1 AND ProcessUpdate=1;")
    row = cursor.fetchone()

    paramsAllViews = []
    while row:
        paramsPerView = []
        # row = [(SpatialViewName, UniqueID, GeometryType, AssignDomains, BaseFeatureDataset, BaseFeatureClass)]
        for element in row:
            paramsPerView.append(element)
        row = cursor.fetchone()
        paramsAllViews.append(paramsPerView)

    connection.close()
    return paramsAllViews

def delete_registered_view(workspace, svwName):
    viewPath = os.path.join(workspace, svwName)
    arcpy.Delete_management(in_data=viewPath)
    logging.info("View deleted at {}".format(viewPath))
    return

def drop_and_create_view(svwName):
    server = r"gissql.ruekert-mielke.com"
    user, password = r"sa", r"H0neyBadger$"
    database = r"_GISAdmin"
    connection = pymssql.connect(server, user, password, database)

    cursor = connection.cursor()
    cursor.execute('SELECT GISDatabase, SDEDatabase, SiteName FROM dbo.AA_SiteInfo WHERE IsAssetAlly = 1 AND ProcessUpdate = 1')

    for row in cursor:
        gis = row[0]
        sde = row[1]
        domain = row[2]
        sqlFullPath = os.path.abspath(r"View Definitions\{}.sql".format(svwName))
        statement = 'sqlcmd -S {0} -U {1} -P {2} -v GIS_DBNAME = "{3}" SDE_DBNAME = "{4}" DOMAIN = "{5}" -i "{6}"'.format(server, user, password, gis, sde, domain, sqlFullPath)
        
        logging.info("Recreating view with sqlcmd:")
        logging.info("\t\t {}".format(statement))
        call(statement, shell=True)

    logging.info("View created: {} \n".format(svwName))
    return


def register_view(workspace, params):
    # Parse out spatial view parameters
    svwName, uniqueID, geometryType, assignDomains, baseFD, baseFC = [params[i] for i in range(len(params))]

    # Read parameters from base feature class
    fc = arcpy.Describe(os.path.join(workspace, baseFD, baseFC))
    fcName = fc.name
    shapeField = fc.shapeFieldName
    spatialRef = fc.spatialReference
    extent = str(fc.extent).strip(' NaN')

    #Register spatial view
    try:
        logging.info('Registering view: \t\t' + svwName)
        arcpy.RegisterWithGeodatabase_management(in_dataset=svwName, in_object_id_field=uniqueID,
                                                    in_shape_field=shapeField, in_geometry_type=geometryType,
                                                    in_spatial_reference=spatialRef, in_extent=extent)
        logging.info('Registered view: \t\t' + svwName + '\n')
    except arcpy.ExecuteError:
        logging.warn('WARNING: \t\t' + svwName + ' not registered. There may be an error, or the view may already be registered. \n')
        pass

    # Assign domains to registered spatial view if assignDomains=True
    if assignDomains:
        logging.info('Assigning domains to registered view ' + svwName + '\n')
        assign_domains(fcName, svwName)
    else:
        logging.info('No domains need to be assigned for this view. \n')
        pass

    return

def assign_domains(fcName, svwName):
    fcFields = arcpy.ListFields(fcName)
    svwFields = arcpy.ListFields(svwName)
    svwFieldNames = [str(s.name) for s in svwFields]
    for field in fcFields:
        if field.domain != '' and field.name in svwFieldNames:
            logging.info('Field name: \t\t' + field.name)
            logging.info('Domain name: \t\t' + field.domain)
            logging.info('Match: \t\t\t Yes. ' + field.name + ' used in view. Assigning domain. \n')
            arcpy.AssignDomainToField_management(in_table=svwName, field_name=field.name, domain_name=field.domain)
        if field.domain != '' and field.name not in svwFieldNames:
            logging.info('Field name: \t\t' + field.name)
            logging.info('Domain name: \t\t' + field.domain)
            logging.warn('Match: \t\t\t No. ' + field.name + ' not used in view. Passing. \n')
            pass

    return

def main():
    clientName = str(sys.argv[1])

    now = datetime.datetime.now().strftime('%m-%d-%Y_%H_%M_%S')
    logging.basicConfig(filename=str(sys.argv[0]).split(".")[0] + "_" + clientName + "_Log_" + now + ".log", format="%(asctime)s %(message)s", datefmt="%m/%d/%Y %I:%M:%S %p", level=logging.DEBUG)

    # Set arcpy workspace per client geodatabase via command line argument
    logging.info("Setting workspace for client {0}.".format(clientName))
    arcpy.env.workspace = r"I:\DatabaseConnections\sa User Connections\{}_SDE.sa@gissql.ruekert-mielke.com.sde".format(clientName)
    workspace = arcpy.env.workspace
    logging.info("Connected to SDE: {} \n".format(workspace))

    logging.info('Fetching spatial view parameters from ' + clientName + ' SDE geodatabase.')
    logging.info('Workspace: \t' + arcpy.env.workspace + '\n')
    # Return a list of lists: [[view 1], ... ,[view N]]
    paramsAll = get_parameters()

    # Delete, recreate, and register views 
    for p in paramsAll:
        svwNameWithSchema = p[0]
        # Delete uses arcpy.Delete_management to get down to the system tables
        delete_registered_view(workspace, svwNameWithSchema)

        # DROPs view only if it still exists
        #   Note: If there is a DROP in the logs, be sure to review, because this means the view won't properly update in ArcCatalog
        # Path to view definition SQL files is partially hard-coded in the method
        #   Note: This script must run in the same directory where the folder "View Definitions" resides
        svwName = p[0].split('.')[-1]
        drop_and_create_view(svwName)
        
        # Register and assign domains
        register_view(workspace, p)
    
    logging.info('Finished registering spatial views for ' + clientName)

    return

if __name__ == "__main__":
    main()
