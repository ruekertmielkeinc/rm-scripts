import arcpy, pyodbc
import httplib, urllib, json
import sys, os
import getpass
from subprocess import call
from arcgisservices import *

#ABOUT: This script processes information stored in the _GISAdmin database.  Instead of having to shut down all services,
#this script shuts down all services in 1 folder, processes database changes, and restarts services.

#REQUIRES: installation of pip and pyodbc on this PC.  See AssetAllyDemo\db_scripts\Utilities\pyodbc\using pyodbc.txt

#USAGE: Before starting, make sure to review _GISAdmin.dbo.AASiteInfo to make sure the ProcessUpdate field is set
#to 1 for those sites you wish to update. Add the Python commands between the run something and done running something comments.

def main(argv=None):
    conn = pyodbc.connect("Driver={ODBC Driver 17 for SQL Server};"
                          "Server=gissql.ruekert-mielke.com;"
                          "Database=_GISAdmin;"
                          "uid=GIS;"
                          "pwd=TacoT!ME4BJJ;")

    cursor = conn.cursor()
    cursor.execute("SELECT * FROM dbo.AA_SiteInfo WHERE (IsAssetAlly = 1 AND ProcessUpdate = 1 AND Sanitary = 1)")

    for row in cursor:
        folder = str(row.RESTFolder)
        theConn = str(row.ConnectionFile)
        theSDE = str(row.SDEDatabase)
        theGIS = str(row.GISDatabase)
        theDomain = str(row.SiteName)
        print "Folder: " + folder
        print row.SDEDatabase

        layers_path = "I:\WebDevelopment\Resources\LayerFiles\AssetAlly\OVERLAY"
          
        mxdFile = str(row.OverlayMXDFilePath).strip("\n").strip("\r")
        print mxdFile
        mxd = arcpy.mapping.MapDocument(mxdFile)
        df = arcpy.mapping.ListDataFrames(mxd, "Layers")[0]
        print "Working on " + mxdFile
        
        layers = [
                  "Sanitary Pipe by Year Cleaned.lyr"
                  ]
        for layer in layers:
            lyrName = layer.split(".")[0]
            layerList = arcpy.mapping.ListLayers(mxd, lyrName, df)
            if layerList != []:
                #inLayer = layerList[0]
                #inSymbologyLayer = os.path.join(layers_path, layer)
                #arcpy.ApplySymbologyFromLayer_management(in_layer=inLayer, in_symbology_layer=inSymbologyLayer)
                inLayer = layerList[0]
                inSymbologyLayer = arcpy.mapping.Layer(os.path.join(layers_path, layer))
                arcpy.mapping.UpdateLayer(data_frame=df, update_layer=inLayer, source_layer=inSymbologyLayer, symbology_only=True)   
                print "Symbology applied - " + lyrName
            else:
                print "Layer " + lyrName + " missing for " + folder
                continue
        
        print "Saving File " + mxdFile
        mxd.save()
        del mxd
 
    conn.close()
        
# Script start
if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
