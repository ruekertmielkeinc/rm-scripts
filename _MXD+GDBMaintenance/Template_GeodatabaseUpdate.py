import arcpy, pyodbc
import httplib, urllib, json
import sys
import getpass
from subprocess import call
from arcgisservices import *

#ABOUT: This script processes information stored in the _GISAdmin database.  Instead of having to shut down all services,
#this script shuts down all services in 1 folder, processes database changes, and restarts services.

#REQUIRES: installation of pip and pyodbc on this PC.  See AssetAllyDemo\db_scripts\Utilities\pyodbc\using pyodbc.txt

#USAGE: Before starting, make sure to review _GISAdmin.dbo.AASiteInfo to make sure the ProcessUpdate field is set
#to 1 for those sites you wish to update. Add the Python commands between the run something and done running something comments.

def main(argv=None):
    conn = pyodbc.connect("Driver={ODBC Driver 17 for SQL Server};"
                          "Server=gissql.ruekert-mielke.com;"
                          "Database=_GISAdmin;"
                          "uid=GIS;"
                          "pwd=TacoT!ME4BJJ;")

    cursor = conn.cursor()
    cursor.execute("SELECT * FROM dbo.AA_SiteInfo WHERE (IsAssetAlly = 1 AND ProcessUpdate = 1)")

    for row in cursor:
        folder = str(row.RESTFolder)
        theConn = str(row.ConnectionFile)
        theSDE = str(row.SDEDatabase)
        theGIS = str(row.GISDatabase)
        theDomain = str(row.SiteName)
        print "Folder: " + folder
        if folder != "None":
            print row.SDEDatabase
            StopStartService(folder, "STOP")

        fc = theConn + "/" + theSDE+ ".GIS.StreetsHighways/" + theSDE + ".GIS.StreetPoles"
        arcpy.AddField_management(in_table=fc, field_name="NextMaintYear", field_type="SHORT")

        statement = 'sqlcmd -S gissql.ruekert-mielke.com -U sa -P "L@M9?]!o4nQ=wyUa*#:F^oz:" -v SDE_DBNAME = "' + theSDE + '" -v GIS_DBNAME = "' + theGIS + '" -v DOMAIN = "' + theDomain + '" -i "C:\Users\AStJohn\source\AssetAlly\db_scripts\Updates\AA-1143.sql"'
        print statement
        call(statement, shell=True)

        if folder != "None":
            print "Starting services: " + theDomain
            StopStartService(folder, "START")
            
    conn.close()
        
# Script start
if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
