import arcpy.mapping as mp

def get_services_data():
    server = r"gissql.ruekert-mielke.com"
    user = r"sa"
    password = r"H0neyBadger$"
    database = r"_GISAdmin"
    connection = pymssql.connect(server, user, password, database)

    cursor = connection.cursor()
    cursor.execute(r"SELECT RESTFolder, FeatureMXDFilePath, SecureMXDFilePath, OverlayMXDFilePath, CustomMXDFilePath FROM AASiteInfo WHERE IsAssetAlly='True' AND SiteName='Testing';")
    row = cursor.fetchone()

    RESTFolders, featureMXDs, secureMXDs, overlayMXDs, customMXDs = [], [], [], [], []
    while row:
        # Convert unicode tuples to strings and then move to next row
        RESTFolders.append(row[0].encode("ascii"))
        featureMXDs.append(row[1])
        secureMXDs.append(row[2])
        overlayMXDs.append(row[3])
        customMXDs.append(row[4])
        row = cursor.fetchone()

    for feature in featureMXDs:
        if feature == None:
            continue
        else:
            feature = feature.encode("ascii")
    for secure in secureMXDs:
        if secure == None:
            continue
        else:
            secure = secure.encode("ascii")
    for overlay in overlayMXDs:
        if overlay == None:
            continue
        else:
            overlay = overlay.encode("ascii")
    for custom in customMXDs:
        if custom == None:
            continue
        else:
            custom = custom.encode("ascii")

    connection.close()
    return RESTFolders, featureMXDs, secureMXDs, overlayMXDs, customMXDs

def copy_dataframe(mxd):
    mxdSource = mp.MapDocument(mxd.rstrip("\r\n"))
    dfSource = mxdSource.activeDataFrame

    mxdTarget = mp.MapDocument(mxdMaster)
    dfTarget = mxdMaster.activeDataFrame

    dfTarget.credits = dfSource.credits
    dfTarget.description = dfSource.description
    dfTarget.displayUnits = dfSource.displayUnits
    dfTarget.elementHeight = dfSource.elementHeight
    dfTarget.elementPositionX = dfSource.elementPositionX
    dfTarget.elementPositionY = dfSource.elementPositionY
    dfTarget.elementWidth = dfSource.elementWidth
    dfTarget.extent = dfSource.extent
    dfTarget.geographicTransformations = dfSource.geographicTransformations
    dfTarget.mapUnits = dfSource.mapUnits # read only
    dfTarget.name = dfSource.name
    dfTarget.referenceScale = dfSource.referenceScale
    dfTarget.rotation = dfSource.rotation
    dfTarget.scale = dfSource.scale
    dfTarget.spatialReference = dfSource.spatialReference
    dfTarget.time = dfSource.time # read only
    dfTarget.type = dfSource.type # read only

    mxdTarget.save()
    return


def main():
    RESTFolders, featureMXDs, secureMXDs, overlayMXDs, customMXDs = get_services_data()

    #agsConnection = "I:\\DatabaseConnections\\ArcServer Connections\\arcgis on elb.ags.ruekert-mielke.com (admin).ags"
    #logging.info('Publishing to ArcGIS Server: {}'.format(agsConnection))

    for i in range(len(RESTFolders)):
        if featureMXDs[i] != None:
            copy_dataframe(featureMXDs[i])
        if secureMXDs[i] != None:
            copy_dataframe(secureMXDs[i])
        if overlayMXDs[i] != None:
            copy_dataframe(overlayMXDs[i])
        if customMXDs[i] != None:
            copy_dataframe(customMXDs[i])

main()
    