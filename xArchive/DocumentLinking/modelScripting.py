# ---------------------------------------------------------------------------
# IMPORTS
import arcpy
import os
import datetime
import subprocess
import sys
import pandas
def install():
    subprocess.check_call([sys.executable, "-m", "pip", "install", "pyodbc"])
install()
import pyodbc


uploadTime = datetime.datetime.now()
exportName = "_" + uploadTime.strftime("%Y%m%d%H%M%S") + "_SCRIPT_RESULT"

arcpy.env.workspace = "I:\\Projects\\21_Thiensville\\Active Document Linking\\ProjectDatabase.gdb"

# SET VARIABLES
inputTable = "I:\\Projects\\21_Thiensville\\Active Document Linking\\_0_TODO\\_IN_PROGRESS\\20210311InitialGeocode.csv"
Locator = "I:\\Projects\\21_Thiensville\\Active Document Linking\\Geocoding\\OzCoAddressLocator"
v_SCRIPTING_TEST1 = "I:\\Projects\\21_Thiensville\\Active Document Linking\\ProjectDatabase.gdb\\" + exportName

# GEOCODE ADDRESSES
arcpy.GeocodeAddresses_geocoding(inputTable, Locator, "Key Address VISIBLE NONE", v_SCRIPTING_TEST1, "STATIC", "", "")
print("Geocoding complete!")

#Iterates through the geocoded result and seperates the geocoded addresses and the non-geocoded addresses
sc = arcpy.SearchCursor(v_SCRIPTING_TEST1)
fieldName = "Score"
csvText = ""
unableToCode = ""
for row in sc:
        if row.getValue(fieldName) >= 85:
                csvText = csvText + row.getValue("FileName") + "," + row.getValue("OrigFileName")+ "," + str(row.getValue("Year")) + "," + row.getValue("Address") + "," + row.getValue("UserID") + "," + row.getValue("UploadType") + "," + row.getValue("UploadClass") + "\n"
        else:
                unableToCode = unableToCode + row.getValue("FileName") + "," + row.getValue("OrigFileName")+ "," + str(row.getValue("Year")) + "," + row.getValue("Address") + "," + row.getValue("UserID") + "," + row.getValue("UploadType") + "," + row.getValue("UploadClass") + "\n"

#Writes the geocoded addresses to a csv to be uploaded to the Database
csvName = "I:\\Projects\\21_Thiensville\\Active Document Linking\\_0_TODO\\_IN_PROGRESS\\" + uploadTime.strftime("%Y%m%d%H%M%S") + "_GEOCODED_RESULT.csv"
with open(csvName, 'wb') as file:
        for line in csvText:
                file.write(line)
file.close()

#Writes the non-geocoded addresses to a csv to be reviewed by hand
brokenName = "I:\\Projects\\21_Thiensville\\Active Document Linking\\_0_TODO\\_IN_PROGRESS\\" + uploadTime.strftime("%Y%m%d%H%M%S") + "_UNABLE_TO_GEOCODE.csv"
with open(brokenName, 'wb') as file:
          for line in unableToCode:
                file.write(line)
file.close()

print("Writing to files complete!")


#Creating the dataframe to import to Database
data = pandas.read_csv(csvName)
data.columns= ['FileName','OrigFileName','Year','Address','UserID','UploadType', 'UploadClass']


#Connect to the Database
mydb = pyodbc.connect(
    'DRIVER={ODBC Driver 17 for SQL Server};'
    'SERVER=gissql.ruekert-mielke.com;'
    'DATABASE=Thiensville_AssetAlly_GIS;'
    'UID=sa;'
    'PWD=L@M9?]!o4nQ=wyUa*#:F^oz:;'
)
mycursor = mydb.cursor()

#Create Table
tableName = '_' + uploadTime.strftime("%Y%m%d%H%M%S") + '_GEOCODED'
createQuery = ('CREATE TABLE ' + tableName + ' (FileName nvarchar(250), OrigFileName nvarchar(250), Year nchar(4), Address nvarchar(250), UserID nvarchar(250), UploadType nvarchar(250), UploadClass nvarchar(250))')           
mycursor.execute(createQuery)

#Insert dataframe in to newly created table
for row in data.itertuples():
        mycursor.execute(' INSERT INTO Thiensville_AssetAlly_GIS.dbo.' + tableName + ' (FileName, OrigFileName, Year, Address, UserID, UploadType, UploadClass) '
                'VALUES (?,?,?,?,?,?,?)',
                row.FileName,
                row.OrigFileName,
                row.Year,
                row.Address,
                row.UserID,
                row.UploadType,
                row.UploadClass)
mydb.commit()
print("Succesfully Inserted to Database")


#Get the ixAsbuiltFile for the start of the newly inserted rows for later use
mycursor.execute("SELECT TOP (1) ixAsBuiltFile FROM Thiensville_AssetAlly_GIS.dbo.Uploads_Files ORDER BY ixAsBuiltFile DESC")
allFiles = mycursor.fetchall()
previousIX = -1
for row in allFiles:
        previousIX = row[0]

insertQuery = ('''
        INSERT INTO [dbo].[Uploads_Files]
           ([FileName]
           ,[OrigFileName]
           ,[AsbuiltYear]
           ,[Description]
           ,[UserID]
           ,[EditDate]
           ,[UploadType]
           ,[UploadClass])
     SELECT [FileName]
      ,[OrigFileName]
      ,[Year]
      ,[Address]
      ,[UserID]
        ,GETDATE()
      ,[UploadType]
      ,[UploadClass]
  FROM [dbo].[''' + tableName + ']')

mycursor.execute(insertQuery)


#Export the newly inserted files with ixAsBuiltFile and geocode them
sql_query = pandas.read_sql_query('SELECT * FROM Uploads_Files WHERE (ixAsBuiltFile > ' + str(previousIX) + ')', mydb)
joinReady = "I:\\Projects\\21_Thiensville\\Active Document Linking\\_0_TODO\\_IN_PROGRESS\\" + uploadTime.strftime("%Y%m%d%H%M%S") + "_ReadyToJoin.csv"
sql_query.to_csv(joinReady, index= False, columns= ['FileName','OrigFileName','AsbuiltYear','Description', 'EditDate','UserID','UploadType', 'UploadClass'])

# SET VARIABLES
ixAsGeocoded = '_' + uploadTime.strftime("%Y%m%d%H%M%S") + '_ReadyToJoin'
geocodedOutput = "I:\\Projects\\21_Thiensville\\Active Document Linking\\ProjectDatabase.gdb\\" + ixAsGeocoded

# GEOCODE ADDRESSES
arcpy.GeocodeAddresses_geocoding(joinReady, Locator, "Key Description VISIBLE NONE", geocodedOutput, "STATIC", "", "")
print("Second Geocoding complete!")

###Establish connection to the SDE database to access Document Locations
##mySDE = pyodbc.connect(
##    'DRIVER={ODBC Driver 17 for SQL Server};'
##    'SERVER=127.0.0.1;'
##    'DATABASE=BrownDeer_AssetAlly_SDE;'
##    'UID=sa;'
##    'PWD=sa.1234;'
##)
##sdeCursor = mySDE.cursor()
##
##
##BrownDeer_AssetAlly_SDE_GIS_DocumentLocations = "I:\\DatabaseConnections\\WebUser Connections\\BrownDeer_AssetAlly_SDE.BrownDeerAA@gissql.ruekert-mielke.com.sde\\BrownDeer_AssetAlly_SDE.GIS.Misc\\BrownDeer_AssetAlly_SDE.GIS.DocumentLocations"
##columnNames = ['OBJECTID', 'UID', 'UserID', 'EditDate', 'Owner', 'Status', 'YearAbandoned', 'Source', 'SourceDate', 'Notes', 'Contractor', 'ProjectNumber', 'Asset_Condition', 'Asset_Risk', 'Address', 'StreetName', 'CrossStreetName', 'LocationDescription', 'UnitNumber']
##featureLayer = arcpy.MakeFeatureLayer_management(BrownDeer_AssetAlly_SDE_GIS_DocumentLocations,"DocumentLocations_SCRIPT_lyr")
##
##cursor = arcpy.da.SearchCursor(featureLayer, columnNames)
##
##with arcpy.da.SearchCursor(featureLayer, columnNames) as cursor:
##    for row in cursor:
##        print(row)
##
##
##readyToJoin = "I:\\Projects\\8145_Brown Deer\\Active Document Linking\\Geocoding\\WorkingData.gdb\\" + ixAsGeocoded
##JOINEDRESULT = 'I:\\Projects\\8145_Brown Deer\\Active Document Linking\\Geocoding\\WorkingData.gdb\\_' + uploadTime.strftime("%Y%m%d%H%M%S") + '_JOINEDRESULT'
##arcpy.analysis.SpatialJoin(readyToJoin, featureLayer, JOINEDRESULT)









