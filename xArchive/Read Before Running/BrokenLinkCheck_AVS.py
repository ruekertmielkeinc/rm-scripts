#
#   BEFORE RUNNING READ:
#   -Add files you want to check to DocumentLinking Database
#   -Update the table name in query
#   -Update the output file name
#

import subprocess
import sys
def install():
    subprocess.check_call([sys.executable, "-m", "pip", "install", "pyodbc"])
install()

import pyodbc
import requests
import re


mydb = pyodbc.connect(
    "Driver={ODBC Driver 17 for SQL Server};"
    "Server=gissql.ruekert-mielke.com;"
    "Database=_GISAdmin;"
    "uid=GIS;"
    "pwd=TacoT!ME4BJJ;"
)

mycursor = mydb.cursor()
#UPDATE THE TABLE NAME
mycursor.execute("SELECT AsBuiltLink FROM Germantown_AssetAlly_GIS.dbo.Uploads_Files WHERE FileName LIKE '%.pdf'")
myresult = mycursor.fetchall()
count = 0

#UPDATE THE OUTPUT FILE NAME
outF = open('Germantown_BrokenAsBuiltLinks.txt', 'w')

for row in myresult:
    count += 1
    column = row[0]
    splits = re.findall('"([^"]*)"', column)
    link = splits[0]
    response = requests.head(link)
    
    if response.status_code != 200:
        outF.write( str(response.status_code) + '    ' + link + '\n')

    print(str(response.status_code) + ' ' + link)

outF.close()
print('Complete')
