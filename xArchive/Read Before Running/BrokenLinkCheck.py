#
#   BEFORE RUNNING READ:
#   -Add files you want to check to DocumentLinking Database
#   -Update the table name in query
#   -Update the output file name
#

import subprocess
import sys
def install():
    subprocess.check_call([sys.executable, "-m", "pip", "install", "pyodbc"])
install()

import pyodbc
import requests


mydb = pyodbc.connect(
    'DRIVER={ODBC Driver 17 for SQL Server};'
    'SERVER=gissql.ruekert-mielke.com;'
    'DATABASE=Document_Linking;'
    'UID=lcrain;'
    'PWD=DocTest;'
)

mycursor = mydb.cursor()
#UPDATE THE TABLE NAME
mycursor.execute("SELECT AsBuiltLink FROM ThiensvilleFiles")
myresult = mycursor.fetchall()
count = 0

#UPDATE THE OUTPUT FILE NAME
outF = open('ThiensvilleBrokenLinks.txt', 'w')

for row in myresult:
    count = count + 1
    link = row[0]
    data = link.split('"')
    dataList = data[2]
    response = requests.head(dataList)
    if response.status_code != 200:
        outF.write( str(response.status_code) + '    ' + dataList + '\n')

outF.close()
print('Complete')
