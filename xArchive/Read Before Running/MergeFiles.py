#
#
#   BEFORE RUNNING THE SCRIPT:
#   PASTE REPORTS FILES INTO REPORTS FOLDER
#   PASTE SNAPSHOTS FILES INTO SNAPSHOTS FOLDER
#
#

import subprocess
import sys
def install():
    subprocess.check_call([sys.executable, "-m", "pip", "install", "PyPDF2"])
install()
import os
import PyPDF2
from PyPDF2 import PdfFileMerger


for file in os.listdir("/PDF's/REPORTS/"):
    merger = PdfFileMerger()
    os.chdir("/PDF's/REPORTS/")
    merger.merge(0, file)
    os.chdir("/PDF's/SNAPSHOTS/")
    merger.merge(1, file)
        
    os.chdir("/PDF's/")
    merger.write(file)
    merger.close()

print("DONE")
