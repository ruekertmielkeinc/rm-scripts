How to use this repository

The intent is to collect scripts, whether production ready or still in development.
Scripts should be sorted by purpose, then by quality.

----Example purposes:----
Document Linking
MXD Maintenance
Video Linking
Defect Mapping


----Quality----
Quality should be demarked using prefixes
p_file
d_file
j_file


p - production ready. Someone could open this file or series of files and understand what's going on via readme or very clear comments. Known to work.
d - development. Not quite ready for prime time. May require slight code modification or reading of code to understand what to do.
j - junk. Use at your own risk. There's probably useful stuff in it, but may not be fully functional