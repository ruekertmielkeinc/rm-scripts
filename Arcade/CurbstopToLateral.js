//var lateralUID = $feature.UID
var lateralUID = 4529
var fsLateral = FeatureSetByName($datastore, 'Testing_AssetAlly_SDE.GIS.WaterPipe', ['*'], false)
var fsLateralFilter = Filter(fsLateral, 'UID = @lateralUID')
var lateral = First(fsLateralFilter)

var curbstopUID = lateral.CurbStopUID
//var curbstopUID = 132
var fsCurbstop = FeatureSetByName($datastore, 'Testing_AssetAlly_SDE.GIS.CurbStop', ['*'], false)
var fsCurbstopFilter = Filter(fsCurbstop, 'UID = @curbstopUID')
var curbstop = First(fsCurbstopFilter)

return {
  'edit': [{
    'className': 'Testing_AssetAlly_SDE.GIS.CurbStop',
    'updates': [{ 
      'globalID': curbstop.GlobalID,
      'attributes': {
        'ServiceLineMaterial': $feature.Material,
        'ServiceLineDiameter': $feature.Diameter
      }
    }]
  }]
}