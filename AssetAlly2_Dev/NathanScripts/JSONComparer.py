# VertiGIS AssetAlly Difference Checker

# Created by: Nathan Walker
# Created on: July 11th 2023

import json
from arcgis.gis import GIS
from jsoncomparison import Compare, NO_DIFF

# Declare the template group name and the client list
templategrouptitle = "Village of AssetAlly Content"
clientlist = ["Nathanville"]

# Enable the printing of output results by creating a config file
config = {
    "output": {
        "console": True,
        "file": {}
    }
}

def get_paths(d, root="obj"):
    def recur(d):
        if isinstance(d, dict):
            for key, value in d.items():
                yield f'.{key}'
                yield from (f'.{key}{p}' for p in get_paths(value))

        elif isinstance(d, list):
            for i, value in enumerate(d):
                yield f'[{i}]'
                yield from (f'[{i}]{p}' for p in get_paths(value))

    return (root + p for p in recur(d))

list(get_paths(d))
# same result

# Connect to ArcGIS Online (Uses the active portal in ArcGIS Pro, may need to be changed later to use OAuth)
gis = GIS("pro")

# Get the template group
templategroupquery = f"title:'{templategrouptitle}'"
group = gis.groups.search(query=templategroupquery)[0]

# Get all items shared with the group
templateitems = group.content()

# For every client in the declared client list
for aClient in clientlist:

    # Modify the query to select all the items in each client group
    query = f"title:'Village of {aClient} Content'"
    clientgroup = gis.groups.search(query=query)[0]

    # Get all items shared with the group
    clientitems = clientgroup.content()

    # For every item in the template group
    for templateitem in templateitems:

        # Create what the AssetAlly2 mobile application name would be for the client
        item_name = templateitem.title.replace("AssetAlly",f"{aClient}")

        # For every item in the client group
        for clientitem in clientitems:

            # Check to see if the title and the type match
            if clientitem.title == item_name and clientitem.type == templateitem.type:

                # Create a list of keys to ignore when comparing the JSONs (Changes based on object type)
                exceptions = ["url", "itemId", "id"]

                # Get the template item details
                templateobject = gis.content.get(templateitem.id)
                templateitemdata = templateobject.get_data()

                # Get the client item details
                clientobject = gis.content.get(clientitem.id)
                clientitemdata = clientobject.get_data()

                # Convert the files to JSON format for comparison; expected is the template, actual is the client
                expected = json.dumps(templateitemdata)
                actual = json.dumps(clientitemdata)

                # Specify the keys to search for
                search_keys = ["itemID", "url", "id"]

                # Search for the specified keys and record their paths
                path_dict = search_json(expected, exceptions)

                # Build a new JSON object with asterisks (*) as values for the nested dictionaries
                rules = replace_values(expected, path_dict)

                print(rules)

                # Run the check for differences between JSON outputs
                diff = Compare(config,rules=rules).check(expected, actual)

                if diff != NO_DIFF:
                    # Handle the case where differences are found
                    print(f"Differences found for item: {item_name}")
                else:
                    # Handle the case where no differences are found
                    print(f"No differences found for item: {item_name}")
