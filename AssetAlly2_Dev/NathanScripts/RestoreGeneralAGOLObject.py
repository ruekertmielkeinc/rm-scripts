# Restore ArcGIS Online Object from JSON

# Created by: Nathan Walker
# Created on: August 10th 2023

import arcpy, sys, traceback, json
from arcgis.gis import GIS

try:
    # Connect to AGOL using the active portal
    gis = GIS("pro")

    data = r"K:\backup\20230803-042630ArcGIS_Online_Backup\EverythingElse\783975631b034f1f85e7f50715575061\Kewaunee County Open Data Portal_Data.json"
    properties = r"K:\backup\20230803-042630ArcGIS_Online_Backup\EverythingElse\783975631b034f1f85e7f50715575061\Kewaunee County Open Data Portal_Properties.json"

    # Open the description and read as JSON
    with open(data) as json_data:
        JSONDescription = json.load(json_data)
    print(JSONDescription)

    # Open the properties and read as JSON
    with open(properties) as json_data:
        JSONProperties = json.load(json_data)
    print(JSONProperties)

    # Create the  object
    newitem = gis.content.add(item_properties = JSONProperties)

    # Change the item properties to include the actual data
    item_properties = {"text": json.dumps(JSONDescription)}

    # Update the item with the new properties
    newitem.update(item_properties=item_properties)

    print("Processing Complete, your ArcGIS Online Object is restored")
except:

    tb = sys.exc_info()[2]
    tbinfo = traceback.format_tb(tb)[0]
    pymsg = "PYTHON ERRORS:\nTraceback Info:\n" + tbinfo + "\nError Info:\n     " + str(sys.exc_info()[1])

    msgs = "ARCPY ERRORS:\n" + arcpy.GetMessages(2) + "\n"

    arcpy.AddError(msgs)
    arcpy.AddError(pymsg)

    print(msgs)
    print(pymsg)

    arcpy.AddMessage(arcpy.GetMessages(1))
    print(arcpy.GetMessages(1))