from arcgis.gis import GIS

old_source = "https://maps.assetally.com/server/rest/services/GIS_StormStructure/MapServer"
new_source = "https://maps.assetally.com/server/rest/services/Storm_Structure_Overwrite/MapServer"

# Connect to the ArcGIS organization
gis = GIS("pro")

# Search for all web maps in the organization
web_map_items = gis.content.search(query="type:'Web Map'", max_items=1000)

# Iterate through each web map item
for item in web_map_items:
    try:
        # Get the web map item details
        web_map = gis.content.get(item.id)

        # Get the web map data
        web_map_data = web_map.get_data()

        # Iterate through operational layers in the web map
        for operational_layer in web_map_data['operationalLayers']:

            # Check if the operational layer matches the old layer URL
            if operational_layer['url'] == old_source:

                # Replace the old layer URL with the new layer URL
                print("OLD URL FOUND: " + operational_layer['url'])

                operational_layer['url'] = new_source

                # Update the web map item with the modified data
                web_map.update(data=web_map_data)
                print(f"Layer replaced for web map '{web_map.title}'")

    except Exception as e:
        print(f"Skipped web map '{web_map.title}': {str(e)}")