# Create Backup of AGOL

# Created by: Nathan Walker
# Created on: June 12th 2023

import os, datetime, arcpy, sys, traceback, json, numpy as np, pandas as pd
from arcgis.gis import GIS

try:
    # ------------------------------------------------------------------------------------------------------------------
    # CONNECT TO AGOL AND PREPARE THE LOOP
    # ------------------------------------------------------------------------------------------------------------------
    # Connect to ArcGIS Online (Uses the active portal in ArcGIS Pro, may need to be changed later to use OAuth)
    gis = GIS("pro")

    # Set directory
    os.chdir(r"K:\backup")

    # Get all the items from your ArcGIS Online account (Currently set to 30)
    items = gis.content.search(query="", max_items=1000, categories=["/Categories/AssetAlly/Utilities", "/Categories/AssetAlly/Utilities/Street", "/Categories/AssetAlly/Utilities/Stormwater", "/Categories/AssetAlly/Utilities/Sanitary", "/Categories/AssetAlly/Utilities/Water"])

    # Create pandas dataframe with columns  to populate
    df = pd.DataFrame(columns = ["ItemID", "ItemTitle", "ItemType", "JSONDescription", "JSONProperties",
                                 "ForwardDependency", "BackDependency"])

    # ------------------------------------------------------------------------------------------------------------------
    # CREATE BACKUP FOLDER AND SUBFOLDERS
    # ------------------------------------------------------------------------------------------------------------------
    # Create a backup folder using the current time
    backup_folder = os.path.join(datetime.datetime.now().strftime("%Y%m%d-%I%M%S") + "ArcGIS_Online_Backup")
    if not os.path.exists(backup_folder):
        os.makedirs(backup_folder)

    # Create dictionary of subfolders for looping through
    folder_dict = {"web_map_folder": "WebMaps", "ds_folder": "Dashboards", "sm_folder": "StoryMaps",
                   "wma_folder": "WebMapplication", "fs_folder": "FeatureServices",
                   "ms_folder": "MapServices", "sd_folder": "ServiceDefinitions","im_folder": "Images",
                   "sf_folder": "Shapefiles", "gdb_folder": "FileGeodatabases", "sgdb_folder": "SQLiteGeodatabases",
                   "na_folder": "EverythingElse"}

    # Loop through the dictionary
    for foldervar, name in folder_dict.items():
        exec(foldervar + '=name')
        if not os.path.exists(name):
            os.makedirs(os.path.join(backup_folder, name))

    # ------------------------------------------------------------------------------------------------------------------
    # CREATE BACKUP
    # ------------------------------------------------------------------------------------------------------------------
    # Use a loop to pull all the necessary data for recreation
    for item in items:
        try:
            print("Working on exporting: {}".format(item.id))

            # ----------------------------------------------------------------------------------------------------------
            # PROCESS FOR WEB MAPS
            # ----------------------------------------------------------------------------------------------------------
            if item.type == "Web Map":
                # Retrieve the data resource JSON
                item_data = item.get_data(try_json = False)

                # Create the folder using the item as the folder name
                item_folder = os.path.join(backup_folder, web_map_folder, item.id)
                if not os.path.exists(item_folder):
                    os.makedirs(item_folder)

                # Write the data resource as JSON
                item_data_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') + "_Data" +
                                              ".json")
                with open(item_data_file, "w") as file:
                    file.write(str(item_data))

                # Write item's properties as JSON
                dict_item = dict(item)
                item_properties_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') +
                                                    "_Properties" + ".json")
                with open(item_properties_file, "w") as file_handle:
                    file_handle.write(json.dumps(dict_item))

                # Obtain forward and backward dependencies
                forward = item.dependent_to()
                backward = item.dependent_upon()

                # Add row to the dataframe
                df.loc[len(df.index)] = [item.id, item.title, item.type, item_data_file, item_properties_file, forward,
                                        backward]

            # ----------------------------------------------------------------------------------------------------------
            # PROCESS FOR DASHBOARDS
            # ----------------------------------------------------------------------------------------------------------
            elif item.type == "Dashboard":
                # Retrieve the data resource JSON
                item_data = item.get_data(try_json = False)

                # Create the folder using the item as the folder name
                item_folder = os.path.join(backup_folder, ds_folder, item.id)
                if not os.path.exists(item_folder):
                    os.makedirs(item_folder)

                # Write the data resource as JSON
                item_data_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') + "_Data" +
                                              ".json")
                with open(item_data_file, "w") as file:
                    file.write(str(item_data))

                # Write item's properties as JSON
                dict_item = dict(item)
                item_properties_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') +
                                                    "_Properties" + ".json")
                with open(item_properties_file, "w") as file_handle:
                    file_handle.write(json.dumps(dict_item))

                # Obtain forward and backward dependencies
                forward = item.dependent_to()
                backward = item.dependent_upon()

                # Add row to the dataframe
                df.loc[len(df.index)] = [item.id, item.title, item.type, item_data_file, item_properties_file, forward,
                                        backward]

            # ----------------------------------------------------------------------------------------------------------
            # PROCESS FOR STORY MAPS
            # ----------------------------------------------------------------------------------------------------------
            elif item.type == "StoryMap":
                # Retrieve the data resource JSON
                item_data = item.get_data(try_json = False)

                # Create the folder using the item as the folder name
                item_folder = os.path.join(backup_folder, sm_folder, item.id)
                if not os.path.exists(item_folder):
                    os.makedirs(item_folder)

                # Write the data resource as JSON
                item_data_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') + "_Data" +
                                              ".json")
                with open(item_data_file, "w") as file:
                    file.write(str(item_data))

                # Write item's properties as JSON
                dict_item = dict(item)
                item_properties_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') +
                                                    "_Properties" + ".json")
                with open(item_properties_file, "w") as file_handle:
                    file_handle.write(json.dumps(dict_item))

                # Obtain forward and backward dependencies
                forward = item.dependent_to()
                backward = item.dependent_upon()

                # Add row to the dataframe
                df.loc[len(df.index)] = [item.id, item.title, item.type, item_data_file, item_properties_file, forward,
                                        backward]

            # ----------------------------------------------------------------------------------------------------------
            # PROCESS FOR WEB MAP APPLICATIONS
            # ----------------------------------------------------------------------------------------------------------
            elif item.type == "Web Mapping Application":
                # Retrieve the data resource JSON
                item_data = item.get_data(try_json = False)

                # Create the folder using the item as the folder name
                item_folder = os.path.join(backup_folder, wma_folder, item.id)
                if not os.path.exists(item_folder):
                    os.makedirs(item_folder)

                # Write the data resource as JSON
                item_data_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') + "_Data" +
                                              ".json")
                with open(item_data_file, "w") as file:
                    file.write(str(item_data))

                # Write item's properties as JSON
                dict_item = dict(item)
                item_properties_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') +
                                                    "_Properties" + ".json")
                with open(item_properties_file, "w") as file_handle:
                    file_handle.write(json.dumps(dict_item))

                # Obtain forward and backward dependencies
                forward = item.dependent_to()
                backward = item.dependent_upon()

                # Add row to the dataframe
                df.loc[len(df.index)] = [item.id, item.title, item.type, item_data_file, item_properties_file, forward,
                                        backward]

            # ----------------------------------------------------------------------------------------------------------
            # PROCESS FOR FEATURE SERVICES
            # ----------------------------------------------------------------------------------------------------------
            elif item.type == "Feature Service":
                # Retrieve the data resource JSON
                item_data = item.get_data(try_json = False)

                # Create the folder using the item as the folder name
                item_folder = os.path.join(backup_folder, fs_folder, item.id)
                if not os.path.exists(item_folder):
                    os.makedirs(item_folder)

                # Write the data resource as JSON
                item_data_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') + "_Data" +
                                              ".json")
                with open(item_data_file, "w") as file:
                    file.write(str(item_data))

                # Write item's properties as JSON
                dict_item = dict(item)
                item_properties_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') +
                                                    "_Properties" + ".json")
                with open(item_properties_file, "w") as file_handle:
                    file_handle.write(json.dumps(dict_item))

                result = item.export(item.title, "File Geodatabase")
                result.download(item_folder)
                result.delete()

                # Obtain forward and backward dependencies
                forward = item.dependent_to()
                backward = item.dependent_upon()

                # Add row to the dataframe
                df.loc[len(df.index)] = [item.id, item.title, item.type, item_data_file, item_properties_file, forward,
                                         backward]

            # ----------------------------------------------------------------------------------------------------------
            # PROCESSES FOR SERVICE DEFINITIONS
            # ----------------------------------------------------------------------------------------------------------
            elif item.type == "Service Definition":
                # Retrieve the image
                item_content = gis.content.get(item.id)
                item_data = item.get_data(try_json=False)

                # Create the folder using the item as the folder name
                item_folder = os.path.join(backup_folder, sd_folder, item.id)
                if not os.path.exists(item_folder):
                    os.makedirs(item_folder)

                # Write the data resource as JSON
                item_data_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') + "_Data" +
                                              ".json")
                with open(item_data_file, "w") as file:
                    file.write(str(item_data))

                # Write the definition properties as JSON
                dict_item = dict(item)
                item_properties_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') +
                                                    "_Properties" + ".json")
                with open(item_properties_file, "w") as file_handle:
                    file_handle.write(json.dumps(dict_item))

                # Save the definition
                item_content.download(save_path=item_folder)

                # Add row to the dataframe
                df.loc[len(df.index)] = [item.id, item.title, item.type, item_data_file, item_properties_file, np.NaN,
                                         np.NaN]

            # ----------------------------------------------------------------------------------------------------------
            # PROCESSES FOR IMAGES
            # ----------------------------------------------------------------------------------------------------------
            elif item.type == "Image":
                # Retrieve the image
                item_content = gis.content.get(item.id)

                # Create the folder using the item as the folder name
                item_folder = os.path.join(backup_folder, im_folder, item.id)
                if not os.path.exists(item_folder):
                    os.makedirs(item_folder)

                # Write the image properties as JSON
                dict_item = dict(item)
                item_properties_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') +
                                                    "_Properties" + ".json")
                with open(item_properties_file, "w") as file_handle:
                    file_handle.write(json.dumps(dict_item))

                # Save the image
                item_content.download(save_path=item_folder)

                # Add row to the dataframe
                df.loc[len(df.index)] = [item.id, item.title, item.type, np.NaN, item_properties_file, np.NaN, np.NaN]

            # ----------------------------------------------------------------------------------------------------------
            # PROCESSES FOR SHAPEFILES
            # ----------------------------------------------------------------------------------------------------------
            elif item.type == "Shapefile":
                # Retrieve the shapefile
                item_content = gis.content.get(item.id)

                # Create the folder using the item as the folder name
                item_folder = os.path.join(backup_folder, sf_folder, item.id)
                if not os.path.exists(item_folder):
                    os.makedirs(item_folder)

                # Write the shapefile properties as JSON
                dict_item = dict(item)
                item_properties_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') +
                                                    "_Properties" + ".json")
                with open(item_properties_file, "w") as file_handle:
                    file_handle.write(json.dumps(dict_item))

                # Save the shapefile
                item_content.download(save_path=item_folder)

                # Add row to the dataframe
                df.loc[len(df.index)] = [item.id, item.title, item.type, np.NaN, item_properties_file, np.NaN, np.NaN]

            # ----------------------------------------------------------------------------------------------------------
            # PROCESSES FOR GEODATABASES
            # ----------------------------------------------------------------------------------------------------------
            elif item.type == "File Geodatabase":
                # Retrieve the shapefile
                item_content = gis.content.get(item.id)

                # Create the folder using the item as the folder name
                item_folder = os.path.join(backup_folder, gdb_folder, item.id)
                if not os.path.exists(item_folder):
                    os.makedirs(item_folder)

                # Write the geodatabase properties as JSON
                dict_item = dict(item)
                item_properties_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') +
                                                    "_Properties" + ".json")
                with open(item_properties_file, "w") as file_handle:
                    file_handle.write(json.dumps(dict_item))

                # Save the shapefile
                item_content.download(save_path=item_folder)

                # Add row to the dataframe
                df.loc[len(df.index)] = [item.id, item.title, item.type, np.NaN, item_properties_file, np.NaN, np.NaN]

            # ----------------------------------------------------------------------------------------------------------
            # PROCESSES FOR SQLite GEODATABASES
            # ----------------------------------------------------------------------------------------------------------
            elif item.type == "SQLite Geodatabase":
                # Retrieve the shapefile
                item_content = gis.content.get(item.id)

                # Create the folder using the item as the folder name
                item_folder = os.path.join(backup_folder, sgdb_folder, item.id)
                if not os.path.exists(item_folder):
                    os.makedirs(item_folder)

                # Write the shapefile properties as JSON
                dict_item = dict(item)
                item_properties_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') +
                                                    "_Properties" + ".json")
                with open(item_properties_file, "w") as file_handle:
                    file_handle.write(json.dumps(dict_item))

                # Save the shapefile
                item_content.download(save_path=item_folder)

                # Add row to the dataframe
                df.loc[len(df.index)] = [item.id, item.title, item.type, np.NaN, item_properties_file, np.NaN, np.NaN]

            # ----------------------------------------------------------------------------------------------------------
            # PROCESS FOR ALL ELSE
            # ----------------------------------------------------------------------------------------------------------
            else:
                # Retrieve the object
                item_content = gis.content.get(item.id)

                # Create the folder using the item as the folder name
                item_folder = os.path.join(backup_folder, na_folder, item.id)
                if not os.path.exists(item_folder):
                    os.makedirs(item_folder)
                try:
                    # Write the data resource as JSON
                    item_data_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') + "_Data" +
                                                  ".json")
                    with open(item_data_file, "w") as file:
                        file.write(str(item_data))
                except:
                    item_data_file = np.NaN

                # Write item's properties as JSON
                dict_item = dict(item)
                item_properties_file = os.path.join(item_folder, item.title.replace(':', '').replace('/', '') +
                                                    "_Properties" + ".json")
                with open(item_properties_file, "w") as file_handle:
                    file_handle.write(json.dumps(dict_item))

                # Save the item
                item_content.download(save_path=item_folder)

                # Add row to the dataframe
                df.loc[len(df.index)] = [item.id, item.title, item.type, np.NaN, item_properties_file, item_data_file,
                                         np.NaN]
        except Exception as e:
            print(e)

    # Save the dataframe as a CSV
    df.to_csv(backup_folder + ".csv")
except:

    tb = sys.exc_info()[2]
    tbinfo = traceback.format_tb(tb)[0]
    pymsg = "PYTHON ERRORS:\nTraceback Info:\n" + tbinfo + "\nError Info:\n     " + str(sys.exc_info()[1])

    msgs = "ARCPY ERRORS:\n" + arcpy.GetMessages(2) + "\n"

    arcpy.AddError(msgs)
    arcpy.AddError(pymsg)

    print(msgs)
    print(pymsg)

    arcpy.AddMessage(arcpy.GetMessages(1))
    print(arcpy.GetMessages(1))
