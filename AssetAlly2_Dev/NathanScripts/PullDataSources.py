from arcgis.gis import GIS
import pandas as pd
import requests
import numpy as np

# Connect to the ArcGIS organization
gis = GIS("pro")

# Search for all web maps in the organization
web_map_items = gis.content.search( query = '', item_type = 'Web Map', max_items=600)

df = pd.DataFrame(columns=["MapID", "MapTitle", "LayerTitle", "LayerID", "LayerURL"])

# Iterate through each web map item
for item in web_map_items:

    # Get the web map item details
    web_map = gis.content.get(item.id)

    # Get the web map data
    web_map_data = web_map.get_data()
    print(f"Scanning web map: '{web_map.title}'")

    # Iterate through operational layers in the web map
    for operational_layer in web_map_data['operationalLayers']:
        # Append the name and url
        print(operational_layer['title'])
        rowno = len(df.index)
        try:
            df.loc[rowno,"MapID"] = item['id']
        except:
            df.loc[rowno,"MapID"] = np.NaN

        try:
            df.loc[rowno, "MapTitle"] = item['title']
        except:
            df.loc[rowno, "MapTitle"] = np.NaN

        try:
            df.loc[rowno, "LayerTitle"] = operational_layer['title']
        except:
            df.loc[rowno, "LayerTitle"] = np.NaN

        try:
            df.loc[rowno, "LayerID"] = operational_layer['id']
        except:
            df.loc[rowno, "LayerID"] = np.NaN

        try:
            df.loc[rowno, "LayerURL"] = operational_layer['url']
        except:
            df.loc[rowno, "LayerURL"] = np.NaN

# Save the dataframe as a CSV
df.to_csv(r"C:\Users\NWALKER\Documents\test.csv")

# Isolate single sources
df_names = dict()
for k, v in df.groupby('LayerURL'):
    df_names[k] = v
print(df_names)