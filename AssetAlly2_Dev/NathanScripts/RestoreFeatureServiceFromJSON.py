# Restore Feature Service From JSON

# Created by: Nathan Walker
# Created on: June 11th 2023
import os

import arcpy, sys, traceback, json, zipfile, pandas as pd
from arcgis.gis import GIS

try:
    # Declare input parameters (table from the backup, the itemid of the feature service, and if replace in dependencies
    backup_path = r"K:\backup"
    table = "20230616-020727ArcGIS_Online_Backup.csv"
    input_feature_service = "74ecddff1f9f4f179aa61e86cc43c854"
    replace_dependencies = True

    # Connect to AGOL using the active portal
    gis = GIS("pro")

    # Create pandas data frame from the table
    df = pd.read_csv(backup_path + "\\" + table)

    # Using the input feature service ID, pull the paths from the files and store as variables
    selected = df[df["ItemID"] == input_feature_service]
    description = str(selected.iloc[0]["JSONDescription"])
    properties = str(selected.iloc[0]["JSONProperties"])
    data = str(selected.iloc[0]["DataPath"])

    # Create the file geodatabase object
    newgeodatabase = gis.content.add({'type': 'Feature Layer'}, data = backup_path + "\\" + data)

    # Open the properties and read as JSON
    with open(backup_path + "\\" + properties) as json_data:
        JSONProperties = json.load(json_data)
    print(JSONProperties)

    # Update the feature service with the properties of the JSON
    newgeodatabase.update(item_properties=JSONProperties)

    # Inform the user
    print("Processing Complete, your feature service is restored")

    # If replacing dependencies
    if replace_dependencies == True:
        # Get all the web map objects
        web_map_items = gis.content.search(query="type:'Web Map'", max_items=10000)

        # Iterate through Web Map items
        for item in web_map_items:
            try:
                # Get the web map item details
                web_map = gis.content.get(item.id)

                # Get the web map data
                web_map_data = web_map.get_data()

                # Iterate through operational layers in the web map
                for operational_layer in web_map_data['operationalLayers']:

                    # Check if the operational layer matches the old layer URL
                    if operational_layer['itemId'] == input_feature_service:
                        # Replace the old layer URL with the new layer URL
                        print("OLD ITEMID FOUND: " + operational_layer['itemId'])

                        operational_layer['itemId'] = input_feature_service

                        # Update the web map item with the modified data
                        web_map.update(data=web_map_data)
                        print(f"Layer replaced for web map '{web_map.title}'")

            except Exception as e:
                print(f"Skipped web map '{web_map.title}': {str(e)}")
except:

    tb = sys.exc_info()[2]
    tbinfo = traceback.format_tb(tb)[0]
    pymsg = "PYTHON ERRORS:\nTraceback Info:\n" + tbinfo + "\nError Info:\n     " + str(sys.exc_info()[1])

    msgs = "ARCPY ERRORS:\n" + arcpy.GetMessages(2) + "\n"

    arcpy.AddError(msgs)
    arcpy.AddError(pymsg)

    print(msgs)
    print(pymsg)

    arcpy.AddMessage(arcpy.GetMessages(1))
    print(arcpy.GetMessages(1))
