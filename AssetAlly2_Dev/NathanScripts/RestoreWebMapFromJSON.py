# Restore Web Map From JSON

# Created by: Nathan Walker
# Created on: June 11th 2023

import arcpy, sys, traceback, json, pandas as pd
from arcgis.gis import GIS

try:
    # Declare input parameters (table from the backup and the web map to be restored
    backup_path = r"K:\backup"
    table = "20230616-020727ArcGIS_Online_Backup.csv"
    input_web_map = "ea060ea5870e44bd8912ec9f78761e9b"

    # Connect to AGOL using the active portal
    gis = GIS("pro")

    # Create pandas data frame from the table
    df = pd.read_csv(backup_path + "\\" + table)

    # Using the input web map, pull the paths from the files and store as variables
    selected = df[df["ItemID"] == input_web_map]
    description = str(selected.iloc[0]["JSONDescription"])
    properties = str(selected.iloc[0]["JSONProperties"])

    # Open the description and read as JSON
    with open(backup_path + "\\" + description) as json_data:
        JSONDescription = json.load(json_data)
    print(JSONDescription)

    # Open the properties and read as JSON
    with open(backup_path + "\\" + properties) as json_data:
        JSONProperties = json.load(json_data)
    print(JSONProperties)

    # Create the web map object
    newmap = gis.content.add(item_properties = JSONProperties)

    # Change the item properties to include the actual map data
    item_properties = {"text": json.dumps(JSONDescription)}

    # Update the map with the new properties
    newmap.update(item_properties=item_properties)

    print("Processing Complete, your map is restored")
except:

    tb = sys.exc_info()[2]
    tbinfo = traceback.format_tb(tb)[0]
    pymsg = "PYTHON ERRORS:\nTraceback Info:\n" + tbinfo + "\nError Info:\n     " + str(sys.exc_info()[1])

    msgs = "ARCPY ERRORS:\n" + arcpy.GetMessages(2) + "\n"

    arcpy.AddError(msgs)
    arcpy.AddError(pymsg)

    print(msgs)
    print(pymsg)

    arcpy.AddMessage(arcpy.GetMessages(1))
    print(arcpy.GetMessages(1))
