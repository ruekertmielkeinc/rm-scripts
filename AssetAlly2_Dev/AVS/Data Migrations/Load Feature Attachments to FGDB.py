import os
import pyodbc
import shutil
import arcpy
import random

conn = pyodbc.connect("Driver={ODBC Driver 17 for SQL Server};"
                      "Server=gissql.ruekert-mielke.com;"
                      "Database=Demo_AssetAlly_GIS;"
                      "uid=sa;"
                      "pwd=sKzCf7oJHmR48FT6tZtLfFy8oHYQ4TJd")

root_dir = r"I:\AssetAlly_ArcGISProProjects\AssetAlly\S3 Downloads"
asbuilt_dir = root_dir + r"\Asbuilt"
file_names = tuple(os.listdir(asbuilt_dir))

arcpy.env.workspace = rf"I:\\AssetAlly_ArcGISProProjects\\AssetAlly\\Sanitary\\Sanitary.gdb\\Sanitary"
feature_class = "SanPipe"

fc_index = arcpy.ListFeatureClasses().index(rf"{feature_class}")
fc = arcpy.ListFeatureClasses()[fc_index]

d = {}
with arcpy.da.SearchCursor(fc, ["UID", "GlobalID"]) as cursor:
    for row in cursor:
        key = row[0]
        d[key] = str(row[1])

# Select from SQL database by FeatureClass
query = rf"""SELECT F.FileName, F.OrigFileName, F.ixAsBuiltFile, L.FeatureClass, L.UID
            FROM dbo.Uploads_Files F 
            INNER JOIN dbo.Uploads_Links L 
            ON F.ixAsBuiltFile = L.ixAsBuiltFile 
            WHERE L.FeatureClass = '{feature_class}' AND F.FileName IN {file_names}"""

cursor = conn.cursor()
cursor.execute(query)

# Copy files to feature class folder with GlobalID appended to filename
new_output_dir = root_dir + rf"\FeatureClasses\{feature_class}"
for row in cursor:
    old_file_path = asbuilt_dir + rf"\{str(row.FileName)}"
    
    if os.path.exists(new_output_dir):
        try:
            filename, extension = os.path.splitext(str(row.OrigFileName))
            globalid = d[row.UID]
            new_file_path = new_output_dir + rf"\{filename}___{globalid}{extension}"
            if os.path.exists(new_file_path):
                randint = random.randint(1, 2147483647) # random enough for same feature with same filename attachment
                shutil.copy2(old_file_path, new_output_dir + rf"\{filename}___{randint}___{globalid}{extension}")
            else:
                shutil.copy2(old_file_path, new_file_path)
        except NameError:
            print("Bad or duplicate file name: " + str(row.OrigFileName) + " (UID " + str(row.UID) + ")")
        except KeyError: # Flag orphaned files
            print("UID not found in FGDB: " + str(row.UID))
    else:
        print("\FeatureClasses\{feature_class} does not exist.")

in_dataset = r"I:\\AssetAlly_ArcGISProProjects\\AssetAlly\\Sanitary\\Sanitary.gdb\\Sanitary\\SanPipe"
in_folder = r"I:\\AssetAlly_ArcGISProProjects\\AssetAlly\\S3 Downloads\\FeatureClasses\\SanPipe"
out_match_table = r"I:\\AssetAlly_ArcGISProProjects\\AssetAlly\\Sanitary\\Sanitary.gdb\\SanPipe_MatchTable"
in_key_field = "GlobalID" 
# Optional parameters
in_file_filter = "*"
in_use_relative_paths = "ABSOLUTE"
match_pattern = "SUFFIX"

arcpy.management.GenerateAttachmentMatchTable(in_dataset, in_folder, out_match_table, in_key_field, in_file_filter, in_use_relative_paths, match_pattern)

in_dataset = r"I:\\AssetAlly_ArcGISProProjects\\AssetAlly\\Sanitary\\Sanitary.gdb\\Sanitary\\SanPipe"
in_join_field = "OBJECTID"
in_match_table = r"I:\\AssetAlly_ArcGISProProjects\\AssetAlly\\Sanitary\\Sanitary.gdb\\SanPipe_MatchTable"
in_match_join_field = "MATCHID"
in_match_path_field = "FILENAME"
# Optional parameters
#in_working_folder

arcpy.management.AddAttachments(in_dataset, in_join_field, in_match_table, in_match_join_field, in_match_path_field)


