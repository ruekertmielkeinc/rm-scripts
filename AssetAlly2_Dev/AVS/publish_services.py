import arcpy
import os
import xml.dom.minidom as DOM

# Sign in to portal
arcpy.SignInToPortal("https://r-m.maps.arcgis.com", "astjohn_RM", "Yattering.8301")

# Set output file names
outdir = r"I:\AssetAlly_ArcGISProProjects\AssetAlly\Stormwater\ServiceDefinitions"
service_name = "FeatureSharingDraftExample"
sddraft_filename = service_name + ".sddraft"
sddraft_output_filename = os.path.join(outdir, sddraft_filename)
sd_filename = service_name + ".sd"
sd_output_filename = os.path.join(outdir, sd_filename)

# Reference map to publish
aprx = arcpy.mp.ArcGISProject(r"I:\AssetAlly_ArcGISProProjects\AssetAlly\Stormwater\Stormwater.aprx")
m = aprx.listMaps('OutfallIDDEInspections')[0]

# Create FeatureSharingDraft and set metadata, portal folder, and export data properties
server_type = "HOSTING_SERVER"
sddraft = m.getWebLayerSharingDraft(server_type, "FEATURE", service_name)

# Create Service Definition Draft file
sddraft.exportToSDDraft(sddraft_output_filename)

# Read the .sddraft file
doc = DOM.parse(sddraft_output_filename)
key_list = doc.getElementsByTagName('Key')
value_list = doc.getElementsByTagName('Value')

# Change following to "true" to share
SharetoOrganization = "false"
SharetoEveryone = "false"
SharetoGroup = "true"
# If SharetoGroup is set to "true", uncomment line below and provide group IDs
GroupID = "b187926005e7495fb740716c722a305f"    # GroupID = "f07fab920d71339cb7b1291e3059b7a8, e0fb8fff410b1d7bae1992700567f54a"

# Each key has a corresponding value. In all the cases, value of key_list[i] is value_list[i].
for i in range(key_list.length):
    if key_list[i].firstChild.nodeValue == "PackageUnderMyOrg":
        value_list[i].firstChild.nodeValue = SharetoOrganization
    if key_list[i].firstChild.nodeValue == "PackageIsPublic":
        value_list[i].firstChild.nodeValue = SharetoEveryone
    if key_list[i].firstChild.nodeValue == "PackageShareGroups":
        value_list[i].firstChild.nodeValue = SharetoGroup
    if SharetoGroup == "true" and key_list[i].firstChild.nodeValue == "PackageGroupIDs":
        value_list[i].firstChild.nodeValue = GroupID

# Find all elements named TypeName
# This is where the additional layers and capabilities are defined
typeNames = doc.getElementsByTagName('TypeName')
for typeName in typeNames:
    # Get the TypeName to enable
    if typeName.firstChild.data == "FeatureServer":
        extension = typeName.parentNode
        for extElement in extension.childNodes:
            if extElement.tagName == 'Definition':
                for propArray in extElement.childNodes:
                    if propArray.tagName == 'Info':
                        for propSet in propArray.childNodes:
                            for prop in propSet.childNodes:
                                for prop1 in prop.childNodes:
                                    if prop1.tagName == "Key":
                                        if prop1.firstChild.data == 'webCapabilities':
                                            if prop1.nextSibling.hasChildNodes():
                                                prop1.nextSibling.firstChild.data = "Create,Sync,Query"
                                            else:
                                                txt = doc.createTextNode("Create,Sync,Query")
                                                prop1.nextSibling.appendChild(txt)


# Write to the .sddraft file
f = open(sddraft_output_filename, 'w')
doc.writexml(f)
f.close()

try:
    # Stage Service
    print("Start Staging")
    arcpy.server.StageService(sddraft_output_filename, sd_output_filename)
    # Share to portal
    print("Start Uploading")
    arcpy.server.UploadServiceDefinition(sd_output_filename, server_type)

    print("Finish Publishing")
except Exception as stage_exception:
    if "00374" in str(stage_exception):
        print("The map is not set to allow assignment of unique IDs")
    print("Analyzer errors encountered - {}".format(str(stage_exception)))

except arcpy.ExecuteError:
    print(arcpy.GetMessages(2))

