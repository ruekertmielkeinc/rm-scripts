import arcpy

clientName = "Demo2"
workspace = "I:\DatabaseConnections\GIS User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.GIS@gissql.ruekert-mielke.com.sde"
arcpy.env.workspace = workspace

# Set web user editing credentials
webUserName = "AssetAlly2"

fd = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.Water"
fc = fd + "\\" + clientName + "_AssetAlly_SDE.GIS.Valve"
table = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.WaterValveInspections"

templateTable = "I:\DatabaseConnections\WebUser Connections\AssetAlly\Demo2_AssetAlly_GIS.AssetAlly2@gissql.ruekert-mielke.com.sde\Demo2_AssetAlly_GIS.dbo.WaterValveInspections"
arcpy.management.CreateTable(out_path=workspace, out_name="WaterValveInspections", template=templateTable, out_alias="Water Valve Inspections")

# Field aliases
arcpy.management.AlterField(in_table=table, field="UID", new_field_alias="Valve UID")
arcpy.management.AlterField(in_table=table, field="InspectionDate", new_field_alias="Inspection Date")
arcpy.management.AlterField(in_table=table, field="InspectedBy", new_field_alias="Inspector")
arcpy.management.AlterField(in_table=table, field="ByPass", new_field_alias="Bypass Valve Present?")
arcpy.management.AlterField(in_table=table, field="Turns", new_field_alias="Number of Turns to Open")
arcpy.management.AlterField(in_table=table, field="Torque", new_field_alias="Torque Applied (N m)")
arcpy.management.AlterField(in_table=table, field="Exercised", new_field_alias="Exercised")
arcpy.management.AlterField(in_table=table, field="InitiallyOperable", new_field_alias="Initially Operable")
arcpy.management.AlterField(in_table=table, field="CannotLocate", new_field_alias="Cannot Locate")
arcpy.management.AlterField(in_table=table, field="FoundShut", new_field_alias="Found Shut")
arcpy.management.AlterField(in_table=table, field="Buried", new_field_alias="Buried")
arcpy.management.AlterField(in_table=table, field="ReplaceLid", new_field_alias="Replace Lid")
arcpy.management.AlterField(in_table=table, field="BoxBroken", new_field_alias="Box Broken")
arcpy.management.AlterField(in_table=table, field="BoxMisaligned", new_field_alias="Box Misaligned")
arcpy.management.AlterField(in_table=table, field="NeedToRaise", new_field_alias="Need To Raise")
arcpy.management.AlterField(in_table=table, field="OpNutProblem", new_field_alias="Operating Nut Issue")
arcpy.management.AlterField(in_table=table, field="LidStuck", new_field_alias="Lid Stuck")
arcpy.management.AlterField(in_table=table, field="TurnsHard", new_field_alias="Turns Hard")
arcpy.management.AlterField(in_table=table, field="SpinsFree", new_field_alias="Spins Free")
arcpy.management.AlterField(in_table=table, field="CleanOut", new_field_alias="Clean Out Needed")
arcpy.management.AlterField(in_table=table, field="SnuggedPackingLeak", new_field_alias="Snugged Packing Leak")
arcpy.management.AlterField(in_table=table, field="PackingLeak", new_field_alias="Packing Leak")
arcpy.management.AlterField(in_table=table, field="Other", new_field_alias="Other Issue")
arcpy.management.AlterField(in_table=table, field="MapDiscrepancy", new_field_alias="Map Discrepancy")
arcpy.management.AlterField(in_table=table, field="DiscrepancyComment", new_field_alias="Map Discrepancy Description")
arcpy.management.AlterField(in_table=table, field="BPSize", new_field_alias="Bypass Diameter (in.)")
arcpy.management.AlterField(in_table=table, field="BPTurns", new_field_alias="Bypass Number of Turns to Open/Close")
arcpy.management.AlterField(in_table=table, field="BPTorque", new_field_alias="Bypass Torque Applied (N m)")
arcpy.management.AlterField(in_table=table, field="BPCondition", new_field_alias="Bypass Condition")
arcpy.management.AlterField(in_table=table, field="BPExercised", new_field_alias="Bypass Exercised")
arcpy.management.AlterField(in_table=table, field="BPComments", new_field_alias="Bypass Comments")

# Assign Yes/No domain to "short"/"SmallInteger" fields
fields = arcpy.ListFields(dataset=table, field_type="SmallInteger")
for field in fields:
        arcpy.management.AssignDomainToField(in_table=table, field_name=field.name, domain_name="YesNo")
        arcpy.management.AssignDefaultToField(in_table=table, field_name=field.name, default_value="No")

arcpy.management.ChangePrivileges(in_dataset=table, user=webUserName, View='GRANT', Edit='GRANT')
# Add Global IDs and editor tracking
arcpy.management.AddGlobalIDs(in_datasets=table)
# Enable attachments, AFTER adding global IDs
arcpy.management.EnableAttachments(in_dataset=table)
# The already-added editor tracking fields, from the template, are read-only and must be deleted 
# And then editor tracking can be enabled with the option to (re-)add the fields
#arcpy.management.DeleteField(in_table=table, drop_field=["created_user", "created_date", "last_edited_user", "last_edited_date"])
arcpy.management.EnableEditorTracking(in_dataset=table, creator_field="created_user", creation_date_field="created_date", last_editor_field="last_edited_user", last_edit_date_field="last_edited_date", add_fields="ADD_FIELDS", record_dates_in="UTC")
# Register as versioned, traditional, with the option to move edits to base
arcpy.management.RegisterAsVersioned(in_dataset=table, edit_to_base="EDITS_TO_BASE")

# Define relationship class between the feature class and the new table
relClass = arcpy.management.CreateRelationshipClass(origin_table=fc, destination_table=table, out_relationship_class="GIS.Water\GIS.Valve_WaterValveInspections", relationship_type="SIMPLE", forward_label="GIS.WaterValveInspections", backward_label="GIS.Valve", message_direction="NONE", cardinality="ONE_TO_MANY", attributed="NONE", origin_primary_key="UID", origin_foreign_key="UID")