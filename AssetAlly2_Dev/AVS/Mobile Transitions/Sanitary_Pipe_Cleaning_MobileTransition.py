import arcpy

clientName = "Demo2"
workspace = "I:\DatabaseConnections\GIS User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.GIS@gissql.ruekert-mielke.com.sde"
arcpy.env.workspace = workspace

fd = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.Sanitary"
fc = fd + "\\" + clientName + "_AssetAlly_SDE.GIS.SanPipe"
table = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.SanitaryPipeConditionSegments"

templateTable = "I:\DatabaseConnections\GIS User Connections\AssetAlly\Testing_AssetAlly2_SDE.GIS@gissql.ruekert-mielke.com.sde\Testing_AssetAlly2_SDE.GIS.SanPipeCondSegments"
arcpy.management.CreateTable(out_path=workspace, out_name="SanitaryPipeConditionSegments", template=templateTable, out_alias="Sanitary Pipe Condition Segments")

# Field aliases
arcpy.management.AlterField(in_table=fc, field="", new_field_alias="")
arcpy.management.AlterField(in_table=fc, field="", new_field_alias="")


# Set web user editing credentials
webUserName = "AssetAlly2"

# Add Global IDs and editor tracking
arcpy.management.AddGlobalIDs(in_datasets=table)
# Enable attachments, AFTER adding global IDs
arcpy.management.EnableAttachments(in_dataset=table)
# The already-added editor tracking fields, from the template, are read-only and must be deleted 
# And then editor tracking can be enabled with the option to (re-)add the fields
arcpy.management.DeleteField(in_table=table, drop_field=["created_user", "created_date", "last_edited_user", "last_edited_date"])
arcpy.management.EnableEditorTracking(in_dataset=table, creator_field="created_user", creation_date_field="created_date", last_editor_field="last_edited_user", last_edit_date_field="last_edited_date", add_fields="ADD_FIELDS", record_dates_in="UTC")

# Register as versioned, traditional, with the option to move edits to base
arcpy.management.RegisterAsVersioned(in_dataset=table, edit_to_base="EDITS_TO_BASE")
arcpy.management.ChangePrivileges(in_dataset=table, user=webUserName, View='GRANT', Edit='GRANT')

# Define relationship class between the feature class and the new table
relClass = arcpy.management.CreateRelationshipClass(origin_table=fc, destination_table=table, out_relationship_class="GIS.Sanitary\GIS.SanPipe_SanitaryPipeConditionData", relationship_type="SIMPLE", forward_label="GIS.SanitaryPipeConditionData", backward_label="GIS.SanPipe", message_direction="NONE", cardinality="ONE_TO_MANY", attributed="NONE", origin_primary_key="UID", origin_foreign_key="UID")

