import arcpy

clientName = "Demo2"
workspace = "I:\DatabaseConnections\GIS User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.GIS@gissql.ruekert-mielke.com.sde"
arcpy.env.workspace = workspace

# Set web user editing credentials
webUserName = "AssetAlly2"

fd = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.Water"
fc = fd + "\\" + clientName + "_AssetAlly_SDE.GIS.Hydrant"
table = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.WaterHydrantInspections"

templateTable = "I:\DatabaseConnections\WebUser Connections\AssetAlly\Demo2_AssetAlly_GIS.AssetAlly2@gissql.ruekert-mielke.com.sde\Demo2_AssetAlly_GIS.dbo.WaterHydrantInspections"
arcpy.management.CreateTable(out_path=workspace, out_name="WaterHydrantInspections", template=templateTable, out_alias="Water Hydrant Inspections")

# Field aliases
#arcpy.management.AlterField(in_table=table, field="", new_field_alias="")
#arcpy.management.AlterField(in_table=table, field="", new_field_alias="")

# Assign Yes/No domain to existing "short"/"SmallInteger" fields
fields = arcpy.ListFields(dataset=table, field_type="SmallInteger")
for field in fields:
        arcpy.management.AssignDomainToField(in_table=table, field_name=field.name, domain_name="YesNo")
        arcpy.management.AssignDefaultToField(in_table=table, field_name=field.name, default_value="No")

# Add fields to complete inspection form (in part of refactoring the hydrant inspections table)
shortint_fields_to_add = {"Flagged": "Flagged", 
                          "LeakTested": "Leak Tested", 
                          "OilFilled": "Oil Filled", 
                          "Painted": "Painted", 
                          "OtherPurpose": "Other Purpose",
                          "Obstructed": "Obstructed",
                          "NotPlumb": "Not Plumb",
                          "WrongDirection": "Wrong Direction",
                          "VisibleDamage": "Visible Damage",
                          "Bagged": "Bagged",
                          "Plugged": "Plugged",
                          "VerticalAdjustment": "Vertical Adjustment"}
for name, alias in shortint_fields_to_add.items():
        arcpy.manangement.AddField(in_table=table, field_name=name, field_type="SHORT", field_alias=alias, field_domain="YesNo")
        arcpy.management.AssignDefaultToField(in_table=table, field_name=name, default_value="No")

arcpy.management.ChangePrivileges(in_dataset=table, user=webUserName, View='GRANT', Edit='GRANT')
# Add Global IDs and editor tracking
arcpy.management.AddGlobalIDs(in_datasets=table)
# Enable attachments, AFTER adding global IDs
arcpy.management.EnableAttachments(in_dataset=table)
# The already-added editor tracking fields, from the template, are read-only and must be deleted 
# And then editor tracking can be enabled with the option to (re-)add the fields
#arcpy.management.DeleteField(in_table=table, drop_field=["created_user", "created_date", "last_edited_user", "last_edited_date"])
arcpy.management.EnableEditorTracking(in_dataset=table, creator_field="created_user", creation_date_field="created_date", last_editor_field="last_edited_user", last_edit_date_field="last_edited_date", add_fields="ADD_FIELDS", record_dates_in="UTC")
# Register as versioned, traditional, with the option to move edits to base
arcpy.management.RegisterAsVersioned(in_dataset=table, edit_to_base="EDITS_TO_BASE")

# Define relationship class between the feature class and the new table
relClass = arcpy.management.CreateRelationshipClass(origin_table=fc, destination_table=table, out_relationship_class="GIS.Water\GIS.Hydrant_WaterHydrantInspections", relationship_type="SIMPLE", forward_label="GIS.WaterHydrantInspections", backward_label="GIS.Hydrant", message_direction="NONE", cardinality="ONE_TO_MANY", attributed="NONE", origin_primary_key="UID", origin_foreign_key="UID")