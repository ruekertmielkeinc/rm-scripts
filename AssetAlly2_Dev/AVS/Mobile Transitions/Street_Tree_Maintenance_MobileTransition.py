import arcpy

clientName = "Demo2"

workspace = "I:\DatabaseConnections\sa User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.sa@gissql.ruekert-mielke.com.sde"
arcpy.DisconnectUser(sde_workspace=workspace, users="ALL")

workspace = "I:\DatabaseConnections\GIS User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.GIS@gissql.ruekert-mielke.com.sde"
arcpy.env.workspace = workspace
fd = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.StreetsHighways"
fc = fd + "\\" + clientName + "_AssetAlly_SDE.GIS.Trees"
table = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.TreeMaintenance"

# Add Global IDs and editor tracking to Trees feature class
arcpy.management.AddGlobalIDs(in_datasets=fc)
arcpy.management.EnableEditorTracking(in_dataset=fc, creator_field="created_user", creation_date_field="created_date", last_editor_field="last_edited_user", last_edit_date_field="last_edited_date", add_fields="ADD_FIELDS", record_dates_in="UTC")
arcpy.management.RegisterAsVersioned(in_dataset=fd, edit_to_base="EDITS_TO_BASE")

# CommonName: set to Species and eventually make Genus-Species correct
arcpy.management.AddField(in_table=fc, field_name="CommonName", field_type="TEXT", field_alias="Common Name")
arcpy.management.CalculateField(in_table=fc, field="CommonName", expression="!Species!", expression_type="PYTHON3")
arcpy.management.AssignDomainToField(in_table=fc, field_name="CommonName", domain_name="TreeSpecies")

templateTable = "I:\DatabaseConnections\WebUser Connections\AssetAlly\Demo2_AssetAlly_GIS.AssetAlly2@gissql.ruekert-mielke.com.sde\Demo2_AssetAlly_GIS.dbo.TreesMaintain"
arcpy.management.CreateTable(out_path=workspace, out_name='TreeMaintenance', template=templateTable, out_alias='Tree Maintenance')

# Field aliases
arcpy.management.AlterField(in_table=table, field="MaintDate", new_field_alias="Maintenance Date")
arcpy.management.AlterField(in_table=table, field="MaintBy", new_field_alias="Inspector")
arcpy.management.AlterField(in_table=table, field="ACTIVITY", new_field_alias="Maintenance Type")
arcpy.management.AlterField(in_table=table, field="Planted", new_field_alias="Tree Planted?")
arcpy.management.AlterField(in_table=table, field="Watched", new_field_alias="Watched?")
arcpy.management.AlterField(in_table=table, field="TreatType", new_field_alias="Treatment Type")
arcpy.management.AlterField(in_table=table, field="TreatVolume", new_field_alias="Volume Applied (mL)")
arcpy.management.AlterField(in_table=table, field="PruneType", new_field_alias="Pruning Type")
arcpy.management.AlterField(in_table=table, field="RemovedStakes", new_field_alias="Stakes Removed?")
arcpy.management.AlterField(in_table=table, field="Removed", new_field_alias="Tree Removed?")
arcpy.management.AlterField(in_table=table, field="ChemUsed", new_field_alias="Chemical Used")
arcpy.management.AlterField(in_table=table, field="CablePrune", new_field_alias="Cabled?")
arcpy.management.AlterField(in_table=table, field="GirdleRootRemove", new_field_alias="Girdling Root Removed?")
arcpy.management.AlterField(in_table=table, field="HeaveResolve", new_field_alias="Heaving Resolved?")
arcpy.management.AlterField(in_table=table, field="Staked", new_field_alias="Staked?")
arcpy.management.AlterField(in_table=table, field="MulchRings", new_field_alias="Mulched?")
arcpy.management.AlterField(in_table=table, field="Wrapped", new_field_alias="Tree Wrapped?")

# Treatment chemical domain
arcpy.management.CreateDomain(in_workspace=workspace, domain_name="TreeTreatmentChemical", domain_description="List of chemicals used in tree treatment.", field_type="TEXT", domain_type="CODED", split_policy="DUPLICATE", merge_policy="DEFAULT")
domainValues = {"Alamo": "Alamo", "ArborMectin": "ArborMectin", "Arbotect": "Arbotect", "Banner Maxx": "Banner Maxx", "Baseline": "Baseline", "Cambistat": "Cambistat", 
                "Compass": "Compass", "Dipel": "Dipel", "Floramite": "Floramite", "Forbid": "Forbid", "Heritage": "Heritage", "Imidacloprid": "Imidacloprid",
                "Menace": "Menace", "Meridian": "Meridian", "Onyx": "Onyx", "Shuttle": "Shuttle", "Spectro": "Spectro", "Subdue": "Subdue", "Transtect": "Transtect",
                "Tree-Age": "Tree-Age", "Other": "Other"}
for code in domainValues:
    arcpy.management.AddCodedValueToDomain(in_workspace=workspace, domain_name="TreeTreatmentChemical", code=code, code_description=domainValues[code])

arcpy.management.AssignDomainToField(in_table=table, field_name="TreatType", domain_name="TreeTreatment")
arcpy.management.AssignDomainToField(in_table=table, field_name="ChemUsed", domain_name="TreeTreatmentChemical")
arcpy.management.AssignDomainToField(in_table=table, field_name="PruneType", domain_name="TreePruneNeed")

# Remove apostrophe from HeightClass coded domain values
arcpy.management.DeleteCodedValueFromDomain(in_workspace=workspace, domain_name="TreeHClass", code=["N/A", "0'-15'", "15'-30'", "30'-60'", "60'+"])
domainValues = {"N/A": "N/A", "0-15 ft.": "0-15 ft.", "15-30 ft.": "15-30 ft.", "30-60 ft.": "30-60 ft.", "60+ ft.": "60+ ft."}
for code in domainValues:
    arcpy.management.AddCodedValueToDomain(in_workspace=workspace, domain_name="TreeHClass", code=code, code_description=domainValues[code])
    
# SQL UPDATE for HClass values   
# UPDATE GIS.TREES
# SET HClass = '0-15 ft.'
# WHERE HClass = '0''-15'''
# GO

# UPDATE GIS.TREES
# SET HClass = '15-30 ft.'
# WHERE HClass = '15''-30'''
# GO

# UPDATE GIS.TREES
# SET HClass = '30-60 ft.'
# WHERE HClass = '30''-60'''
# GO

# UPDATE GIS.TREES
# SET HClass = '60+ ft.'
# WHERE HClass = '60''+'
# GO

# Assign Yes/No domain to "short"/"SmallInteger" fields
fields = arcpy.ListFields(dataset=table, field_type="SmallInteger")
for field in fields:
        arcpy.management.AssignDomainToField(in_table=table, field_name=field.name, domain_name="YesNo")

# Set web user editing credentials
webUserName = "AssetAlly2"
arcpy.management.ChangePrivileges(in_dataset=table, user=webUserName, View='GRANT', Edit='GRANT')

# Add Global IDs and editor tracking
arcpy.management.AddGlobalIDs(in_datasets=table)
# Enable attachments, AFTER adding global IDs
arcpy.management.EnableAttachments(in_dataset=table)
# Register as versioned, traditional, with the option to move edits to base
arcpy.management.RegisterAsVersioned(in_dataset=table, edit_to_base="EDITS_TO_BASE")
arcpy.management.EnableEditorTracking(in_dataset=table, creator_field="created_user", creation_date_field="created_date", last_editor_field="last_edited_user", last_edit_date_field="last_edited_date", add_fields="ADD_FIELDS", record_dates_in="UTC")

# Define relationship class between the feature class and the new table
relClass = arcpy.management.CreateRelationshipClass(origin_table=fc, destination_table=table, out_relationship_class="GIS.StreetsHighways\GIS.Trees_TreeMaintenance", relationship_type="SIMPLE", forward_label="GIS.TreeMaintenance", backward_label="GIS.Trees", message_direction="NONE", cardinality="ONE_TO_MANY", attributed="NONE", origin_primary_key="UID", origin_foreign_key="UID")

# If applicable, load data from existing table (in non-spatial "_GIS" database)

workspace = "I:\DatabaseConnections\sa User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.sa@gissql.ruekert-mielke.com.sde"

arcpy.AcceptConnections(sde_workspace=workspace, accept_connections=True)