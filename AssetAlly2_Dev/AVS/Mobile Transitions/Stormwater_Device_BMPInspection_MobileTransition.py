import arcpy

clientName = "Demo2"
workspace = "I:\DatabaseConnections\GIS User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.GIS@gissql.ruekert-mielke.com.sde"
arcpy.env.workspace = workspace

# Set web user editing credentials
webUserName = "AssetAlly2"

fd = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.Storm"
fc = fd + "\\" + clientName + "_AssetAlly_SDE.GIS.StormPond"
table = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.StormwaterDeviceInspections"

templateTable = "I:\DatabaseConnections\WebUser Connections\AssetAlly\Demo_AssetAlly_GIS.AssetAlly@gissql.ruekert-mielke.com.sde\Demo_AssetAlly_GIS.dbo.PondInspection"
arcpy.management.CreateTable(out_path=workspace, out_name="StormwaterDeviceInspections", template=templateTable, out_alias="Stormwater Device Inspections")

# Field aliases
arcpy.management.AlterField(in_table=table, field="InspectionDate", new_field_alias="Inspection Date")
arcpy.management.AlterField(in_table=table, field="StartTime", new_field_alias="Start Time")
arcpy.management.AlterField(in_table=table, field="EndTime", new_field_alias="End Time")
arcpy.management.AlterField(in_table=table, field="InspectorNames", new_field_alias="Inspector")
arcpy.management.AlterField(in_table=table, field="Weather", new_field_alias="Weather Description")
arcpy.management.AlterField(in_table=table, field="LastRainfallDate", new_field_alias="Last Rainfall Date")

# Field name changes
arcpy.management.AlterField(in_table=table, field="PondUID", new_field_name="UID", new_field_alias="Device UID")

# Assign Yes/No domain to "short"/"SmallInteger" fields
fields = arcpy.ListFields(dataset=table, field_type="SmallInteger")
for field in fields:
        arcpy.management.AssignDomainToField(in_table=table, field_name=field.name, domain_name="YesNo")

arcpy.management.ChangePrivileges(in_dataset=table, user=webUserName, View='GRANT', Edit='GRANT')
# Add Global IDs and editor tracking
arcpy.management.AddGlobalIDs(in_datasets=table)
# Enable attachments, AFTER adding global IDs
arcpy.management.EnableAttachments(in_dataset=table)
# The already-added editor tracking fields, from the template, are read-only and must be deleted 
# And then editor tracking can be enabled with the option to (re-)add the fields
#arcpy.management.DeleteField(in_table=table, drop_field=["created_user", "created_date", "last_edited_user", "last_edited_date"])
arcpy.management.EnableEditorTracking(in_dataset=table, creator_field="created_user", creation_date_field="created_date", last_editor_field="last_edited_user", last_edit_date_field="last_edited_date", add_fields="ADD_FIELDS", record_dates_in="UTC")
# Register as versioned, traditional, with the option to move edits to base
arcpy.management.RegisterAsVersioned(in_dataset=table, edit_to_base="EDITS_TO_BASE")

# Define relationship class between the feature class and the new table
relClass = arcpy.management.CreateRelationshipClass(origin_table=fc, destination_table=table, out_relationship_class="GIS.Storm\GIS.StormPond_StormwaterDeviceInspections", relationship_type="SIMPLE", forward_label="GIS.StormwaterDeviceInspections", backward_label="GIS.StormPond", message_direction="NONE", cardinality="ONE_TO_MANY", attributed="NONE", origin_primary_key="UID", origin_foreign_key="UID")