import arcpy

clientName = "Demo2"
workspace = "I:\DatabaseConnections\GIS User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.GIS@gissql.ruekert-mielke.com.sde"
arcpy.env.workspace = workspace

# Set web user editing credentials
webUserName = "AssetAlly2"

fd = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.Water"
fc = fd + "\\" + clientName + "_AssetAlly_SDE.GIS.WaterMeter"
table = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.WaterMeterCrossConnectionInspections"

templateTable = "I:\DatabaseConnections\WebUser Connections\AssetAlly\Demo_AssetAlly_GIS.AssetAlly@gissql.ruekert-mielke.com.sde\Demo_AssetAlly_GIS.dbo.CrossConnectionInspections"
arcpy.management.CreateTable(out_path=workspace, out_name="WaterMeterCrossConnectionInspections", template=templateTable, out_alias="Water Meter Cross Connection Inspections")

# Field aliases
arcpy.management.AlterField(in_table=table, field="PhoneNumber", new_field_alias="Resident Phone Number")
arcpy.management.AlterField(in_table=table, field="Email", new_field_alias="Resident Email")
arcpy.management.AlterField(in_table=table, field="InspectionDate", new_field_alias="Inspection Date")
arcpy.management.AlterField(in_table=table, field="InspectionFrequency", new_field_alias="Inspection Frequency")
arcpy.management.AlterField(in_table=table, field="PrivateWaterSupply", new_field_alias="Private Water Supply?")
arcpy.management.AlterField(in_table=table, field="WellConnPresent", new_field_alias="Public Water/Private Well Connection?")
arcpy.management.AlterField(in_table=table, field="WaterSoftener", new_field_alias="Water Softener(s)?")
arcpy.management.AlterField(in_table=table, field="AirBreakPresent", new_field_alias="Air Break or Air Gap Missing?")
arcpy.management.AlterField(in_table=table, field="WaterSoftenerType", new_field_alias="Water Softener Type")
arcpy.management.AlterField(in_table=table, field="Boilers", new_field_alias="Boiler(s)?")
arcpy.management.AlterField(in_table=table, field="BackflowPresent", new_field_alias="Backflow Preventer Missing?")
arcpy.management.AlterField(in_table=table, field="SinkFaucet", new_field_alias="Sink Faucet(s)?")
arcpy.management.AlterField(in_table=table, field="Faucets2Above", new_field_alias="Are Faucets Less Than 2" Above Sink?")
arcpy.management.AlterField(in_table=table, field="ThreadedConnection", new_field_alias="Faucet(s) Threaded Connection/Hose?")
arcpy.management.AlterField(in_table=table, field="ThreadedHoseVac", new_field_alias="Faucet(s) Threaded Connection/Hose Vacuum Breaker Missing?")
arcpy.management.AlterField(in_table=table, field="TurfSprinklers", new_field_alias="Turf/Irrigation Sprinklers?")
arcpy.management.AlterField(in_table=table, field="VacuumPresent", new_field_alias="Turf/Irrigation Sprinklers Vacuum Breaker Missing?")
arcpy.management.AlterField(in_table=table, field="ThreadedHose", new_field_alias="Threaded Hose Bibs/Spigots/Faucets?")
arcpy.management.AlterField(in_table=table, field="ThreadedBibVac", new_field_alias="Threaded Hose Bibs/Spigots/Faucets Vacuum Breaker Missing?")
arcpy.management.AlterField(in_table=table, field="MeterByPass", new_field_alias="Meter Bypass Present?")
arcpy.management.AlterField(in_table=table, field="MeterByPassOnSealed", new_field_alias="Meter Bypass On/Not Sealed?")
arcpy.management.AlterField(in_table=table, field="CisternNotAbandoned", new_field_alias="Cistern Not Abandoned?")
arcpy.management.AlterField(in_table=table, field="FireProtection", new_field_alias="Fire Protection")
arcpy.management.AlterField(in_table=table, field="FireProtectionSize", new_field_alias="Fire Protection Size")
arcpy.management.AlterField(in_table=table, field="SumpPump", new_field_alias="Sump Pump")
arcpy.management.AlterField(in_table=table, field="HeatType", new_field_alias="Heat Type")
arcpy.management.AlterField(in_table=table, field="NonStandardMaterials", new_field_alias="Non-Standard or Illegal Plumbing Materials Used?")
arcpy.management.AlterField(in_table=table, field="CorrectPlumbing", new_field_alias="Add/Deduct/Sewer Plumbing Installed Incorrectly?")
arcpy.management.AlterField(in_table=table, field="SanSewerDischarge", new_field_alias="Drain Tile/Sump Pump Discharging Into Sewer?")
arcpy.management.AlterField(in_table=table, field="StormWaterDischarge", new_field_alias="Stormwater Discharging Into Sewer?")
arcpy.management.AlterField(in_table=table, field="OtherViolationsPresent", new_field_alias="Other Violations Present?")
arcpy.management.AlterField(in_table=table, field="VacuumBreakersIn", new_field_alias="Inside Vacuum Breakers Fee ($)")
arcpy.management.AlterField(in_table=table, field="VacuumBreakInQty", new_field_alias="Inside Vacuum Breakers Quantity")
arcpy.management.AlterField(in_table=table, field="VacuumBreakersOut", new_field_alias="Outside Vacuum Breakers Fee ($)")
arcpy.management.AlterField(in_table=table, field="VacuumBreakOutQty", new_field_alias="Outside Vacuum Breakers Quantity")
arcpy.management.AlterField(in_table=table, field="AirGaps", new_field_alias="Air Gaps Fee ($)")
arcpy.management.AlterField(in_table=table, field="AirGapQty", new_field_alias="Air Gaps Quantity")
arcpy.management.AlterField(in_table=table, field="ReInspectionFee", new_field_alias="Re-Inspection Fee ($)")
arcpy.management.AlterField(in_table=table, field="ReInspection", new_field_alias="Re-Inspection")
arcpy.management.AlterField(in_table=table, field="TotalCost", new_field_alias="Total Cost ($)")
arcpy.management.AlterField(in_table=table, field="DiscussedWOwner", new_field_alias="Discussed With Owner?")
arcpy.management.AlterField(in_table=table, field="DiscussedWOccupant", new_field_alias="Discussed With Occupant?")
arcpy.management.AlterField(in_table=table, field="OwnerOccupant", new_field_alias="Owner/Occupant Information")
arcpy.management.AlterField(in_table=table, field="GaveInformation", new_field_alias="Provided Education Material?")
arcpy.management.AlterField(in_table=table, field="InstructedOwner", new_field_alias="Instructed Owner/Occupant of Low Hazard Areas?")
arcpy.management.AlterField(in_table=table, field="LeadServiceLine", new_field_alias="Lead Service Line")
arcpy.management.AlterField(in_table=table, field="LeadPlumbing", new_field_alias="Lead Plumbing")
arcpy.management.AlterField(in_table=table, field="ServiceLineMaterial", new_field_alias="Service Line Material")
arcpy.management.AlterField(in_table=table, field="HomePlumbingMaterial", new_field_alias="Home Plumbing Material")

# Assign Yes/No domain to "short"/"SmallInteger" fields
fields = arcpy.ListFields(dataset=table, field_type="SmallInteger")
for field in fields:
        arcpy.management.AssignDomainToField(in_table=table, field_name=field.name, domain_name="YesNo")
        arcpy.management.AssignDefaultToField(in_table=table, field_name=field.name, default_value="No")

arcpy.management.ChangePrivileges(in_dataset=table, user=webUserName, View='GRANT', Edit='GRANT')
# Add Global IDs and editor tracking
arcpy.management.AddGlobalIDs(in_datasets=table)
# Enable attachments, AFTER adding global IDs
arcpy.management.EnableAttachments(in_dataset=table)
# The already-added editor tracking fields, from the template, are read-only and must be deleted 
# And then editor tracking can be enabled with the option to (re-)add the fields
#arcpy.management.DeleteField(in_table=table, drop_field=["created_user", "created_date", "last_edited_user", "last_edited_date"])
arcpy.management.EnableEditorTracking(in_dataset=table, creator_field="created_user", creation_date_field="created_date", last_editor_field="last_edited_user", last_edit_date_field="last_edited_date", add_fields="ADD_FIELDS", record_dates_in="UTC")
# Register as versioned, traditional, with the option to move edits to base
arcpy.management.RegisterAsVersioned(in_dataset=table, edit_to_base="EDITS_TO_BASE")

# Define relationship class between the feature class and the new table
relClass = arcpy.management.CreateRelationshipClass(origin_table=fc, destination_table=table, out_relationship_class="GIS.Water\GIS.WaterMeter_WaterMeterCrossConnectionInspections", relationship_type="SIMPLE", forward_label="GIS.WaterMeterCrossConnectionInspections", backward_label="GIS.WaterMeter", message_direction="NONE", cardinality="ONE_TO_MANY", attributed="NONE", origin_primary_key="UID", origin_foreign_key="PointID")