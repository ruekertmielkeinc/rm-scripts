import arcpy

clientName = "Demo2"
workspace = "I:\DatabaseConnections\GIS User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.GIS@gissql.ruekert-mielke.com.sde"
arcpy.env.workspace = workspace

# Set web user editing credentials
webUserName = "AssetAlly2"

fd = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.Storm"
fc = fd + "\\" + clientName + "_AssetAlly_SDE.GIS.StormStructure"
table = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.StormwaterOutfallIDDEInspections"

templateTable = "I:\DatabaseConnections\WebUser Connections\AssetAlly\Demo_AssetAlly_GIS.AssetAlly@gissql.ruekert-mielke.com.sde\Demo_AssetAlly_GIS.dbo.IllicitDischargeFieldScreening"
arcpy.management.CreateTable(out_path=workspace, out_name="StormwaterOutfallIDDEInspections", template=templateTable, out_alias="Stormwater Outfall IDDE Inspections")

# Field aliases
arcpy.management.AlterField(in_table=table, field="OutfallID", new_field_alias="Outfall ID")
arcpy.management.AlterField(in_table=table, field="InspectionDate", new_field_alias="Inspection Date")
arcpy.management.AlterField(in_table=table, field="InspectionTime", new_field_alias="Inspection Time")
arcpy.management.AlterField(in_table=table, field="Investigators", new_field_alias="Inspector")
arcpy.management.AlterField(in_table=table, field="FormCompletedBy", new_field_alias="Form Completed By")
arcpy.management.AlterField(in_table=table, field="Temperature", new_field_alias="Temperature (F)")
arcpy.management.AlterField(in_table=table, field="RainfallLast24", new_field_alias="Rainfall in Last 24 Hours (in.)")
arcpy.management.AlterField(in_table=table, field="RainfallLast48", new_field_alias="Rainfall in Last 48 Hours (in.)")
arcpy.management.AlterField(in_table=table, field="NearestIntersection", new_field_alias="Nearest Intersection")
arcpy.management.AlterField(in_table=table, field="LandUseInDrainageArea", new_field_alias="Land Use Type In Drainage Area")
arcpy.management.AlterField(in_table=table, field="Location", new_field_alias="Outfall Location")
arcpy.management.AlterField(in_table=table, field="Material", new_field_alias="Outfall Material")
arcpy.management.AlterField(in_table=table, field="OutfallShape", new_field_alias="Outfall Shape")
arcpy.management.AlterField(in_table=table, field="Dimensions", new_field_alias="Dimensions (in. x in.)")
arcpy.management.AlterField(in_table=table, field="Submerged", new_field_alias="Submerged?")
arcpy.management.AlterField(in_table=table, field="FlowPresent", new_field_alias="Flow Present?")
arcpy.management.AlterField(in_table=table, field="FlowDescription", new_field_alias="Flow Description")
arcpy.management.AlterField(in_table=table, field="FlowDepth", new_field_alias="Flow Depth (ft.)")
arcpy.management.AlterField(in_table=table, field="FlowWidth", new_field_alias="Flow Width (ft.)")
arcpy.management.AlterField(in_table=table, field="FlowLength", new_field_alias="Flow Length (ft.)")
arcpy.management.AlterField(in_table=table, field="TimeOfTravel", new_field_alias="Time Of Travel (sec.)")
arcpy.management.AlterField(in_table=table, field="Volume", new_field_alias="Total Flow Volume (cu. ft.)")
arcpy.management.AlterField(in_table=table, field="TimeToFill", new_field_alias="Time to Fill (sec.)")
arcpy.management.AlterField(in_table=table, field="SampleTemperature", new_field_alias="Sample Temperature (F)")
arcpy.management.AlterField(in_table=table, field="pH", new_field_alias="Sample pH")
arcpy.management.AlterField(in_table=table, field="AmmoniaAmount", new_field_alias="Ammonia (mg/l)")
arcpy.management.AlterField(in_table=table, field="Odor", new_field_alias="Odor?")
arcpy.management.AlterField(in_table=table, field="OdorSewage", new_field_alias="Sewage")
arcpy.management.AlterField(in_table=table, field="OdorPetroleum", new_field_alias="Petroleum")
arcpy.management.AlterField(in_table=table, field="OdorRancid", new_field_alias="Rancid")
arcpy.management.AlterField(in_table=table, field="OdorSulfide", new_field_alias="Sulfide")
arcpy.management.AlterField(in_table=table, field="OdorOther", new_field_alias="Other Oder")
arcpy.management.AlterField(in_table=table, field="OdorOtherType", new_field_alias="Other Odor Description")
arcpy.management.AlterField(in_table=table, field="Color", new_field_alias="Color?")
arcpy.management.AlterField(in_table=table, field="ColorClear", new_field_alias="Clear")
arcpy.management.AlterField(in_table=table, field="ColorYellow", new_field_alias="Yellow")
arcpy.management.AlterField(in_table=table, field="ColorRed", new_field_alias="Red")
arcpy.management.AlterField(in_table=table, field="ColorBrown", new_field_alias="Brown")
arcpy.management.AlterField(in_table=table, field="ColorGreen", new_field_alias="Green")
arcpy.management.AlterField(in_table=table, field="ColorGray", new_field_alias="Gray")
arcpy.management.AlterField(in_table=table, field="ColorOrange", new_field_alias="Orange")
arcpy.management.AlterField(in_table=table, field="ColorOther", new_field_alias="Other Color")
arcpy.management.AlterField(in_table=table, field="ColorOtherType", new_field_alias="Other Color Description")
arcpy.management.AlterField(in_table=table, field="Turbidity", new_field_alias="Turbidity?")
arcpy.management.AlterField(in_table=table, field="TurbidityType", new_field_alias="Turbidity Description")
arcpy.management.AlterField(in_table=table, field="Floatables", new_field_alias="Floatables?")
arcpy.management.AlterField(in_table=table, field="FloatablesSewage", new_field_alias="Sewage")
arcpy.management.AlterField(in_table=table, field="FloatablesSuds", new_field_alias="Suds")
arcpy.management.AlterField(in_table=table, field="FloatablesPetroleum", new_field_alias="Petroleum")
arcpy.management.AlterField(in_table=table, field="FloatablesOther", new_field_alias="Other Floatables")
arcpy.management.AlterField(in_table=table, field="FloatablesOtherType", new_field_alias="Other Floatables Description")
arcpy.management.AlterField(in_table=table, field="OutfallDamage", new_field_alias="Outfall Damage?")
arcpy.management.AlterField(in_table=table, field="OutfallDamageSpalling", new_field_alias="Spalling, Cracking, or Chipping")
arcpy.management.AlterField(in_table=table, field="OutfallDamageCorrosion", new_field_alias="Corrosion")
arcpy.management.AlterField(in_table=table, field="OutfallDamagePeelingPaint", new_field_alias="Peeling Paint")
arcpy.management.AlterField(in_table=table, field="Deposits", new_field_alias="Deposits?")
arcpy.management.AlterField(in_table=table, field="DepositsOily", new_field_alias="Oily")
arcpy.management.AlterField(in_table=table, field="DepositsFlowLine", new_field_alias="Flow Line")
arcpy.management.AlterField(in_table=table, field="DepositsPaint", new_field_alias="Paint")
arcpy.management.AlterField(in_table=table, field="DepositsOther", new_field_alias="Other Depoits")
arcpy.management.AlterField(in_table=table, field="DepositsOtherType", new_field_alias="Other Deposits Description")
arcpy.management.AlterField(in_table=table, field="AbnormalVegetation", new_field_alias="Abnormal Vegetation?")
arcpy.management.AlterField(in_table=table, field="AbnormalVegetationType", new_field_alias="Abnormal Vegetation Description")
arcpy.management.AlterField(in_table=table, field="PoorPoolQuality", new_field_alias="Pool Quality?")
arcpy.management.AlterField(in_table=table, field="PoorPoolQualityOdors", new_field_alias="Pool Odors")
arcpy.management.AlterField(in_table=table, field="PoorPoolQualitySuds", new_field_alias="Pool Suds")
arcpy.management.AlterField(in_table=table, field="PoorPoolQualityFloatables", new_field_alias="Pool Floatables")
arcpy.management.AlterField(in_table=table, field="PoorPoolQualityYellow", new_field_alias="Pool Yellow")
arcpy.management.AlterField(in_table=table, field="PoorPoolQualityOilSheen", new_field_alias="Pool Oil Sheen")
arcpy.management.AlterField(in_table=table, field="PoorPoolQualityExcessiveAlgae", new_field_alias="Pool Excessive Algae")
arcpy.management.AlterField(in_table=table, field="PoorPoolQualityOther", new_field_alias="Pool Quality Other")
arcpy.management.AlterField(in_table=table, field="PoorPoolQualityOtherType", new_field_alias="Pool Quality Other Description")
arcpy.management.AlterField(in_table=table, field="LabSample", new_field_alias="Sample Collected?")
arcpy.management.AlterField(in_table=table, field="CollectedFrom", new_field_alias="Sample Collected From")
arcpy.management.AlterField(in_table=table, field="Comments", new_field_alias="Sample Comments")
arcpy.management.AlterField(in_table=table, field="CopperAmount", new_field_alias="Copper (mg/l)")
arcpy.management.AlterField(in_table=table, field="PhenolAmount", new_field_alias="Phenols (mg/l)")
arcpy.management.AlterField(in_table=table, field="SurfactantAmount", new_field_alias="Surfactants (mg/l)")
arcpy.management.AlterField(in_table=table, field="DetergentAmount", new_field_alias="Detergents (mg/l)")
arcpy.management.AlterField(in_table=table, field="ChlorineAmount", new_field_alias="Chlorine (mg/l)")
arcpy.management.AlterField(in_table=table, field="UID", new_field_alias="Outfall UID")
arcpy.management.AlterField(in_table=table, field="SubmergedDepth", new_field_alias="Submerged Depth (ft.)")
arcpy.management.AlterField(in_table=table, field="ColiformResult", new_field_alias="Total Coliform")

# Field name changes
arcpy.management.AlterField(in_table=table, field="Shape", new_field_name="OutfallShape", new_field_alias="Outfall Shape")
arcpy.management.AlterField(in_table=table, field="FeatureUID", new_field_name="UID", new_field_alias="Outfall UID")

# InspectionDate: SQL date -> ArcGIS date
arcpy.management.AddField(in_table=table, field_name="InspectionDate1", field_type="DATE", field_alias="Inspection Date")
arcpy.management.CalculateField(in_table=table, field="InspectionDate1", expression="!InspectionDate!", expression_type="PYTHON3")
arcpy.management.DeleteField(in_table=table, drop_field="InspectionDate")
arcpy.management.AlterField(in_table=table, field="InspectionDate1", new_field_name="InspectionDate")

# Assign Yes/No domain to "short"/"SmallInteger" fields
fields = arcpy.ListFields(dataset=table, field_type="SmallInteger")
for field in fields:
        arcpy.management.AssignDomainToField(in_table=table, field_name=field.name, domain_name="YesNo")

arcpy.management.ChangePrivileges(in_dataset=table, user=webUserName, View='GRANT', Edit='GRANT')
# Add Global IDs and editor tracking
arcpy.management.AddGlobalIDs(in_datasets=table)
# Enable attachments, AFTER adding global IDs
arcpy.management.EnableAttachments(in_dataset=table)
arcpy.management.EnableEditorTracking(in_dataset=table, creator_field="created_user", creation_date_field="created_date", last_editor_field="last_edited_user", last_edit_date_field="last_edited_date", add_fields="ADD_FIELDS", record_dates_in="UTC")
# Register as versioned, traditional, with the option to move edits to base
arcpy.management.RegisterAsVersioned(in_dataset=table, edit_to_base="EDITS_TO_BASE")

# Define relationship class between the feature class and the new table
relClass = arcpy.management.CreateRelationshipClass(origin_table=fc, destination_table=table, out_relationship_class="GIS.Storm\GIS.StormStructure_StormwaterOutfallIDDEInspections", relationship_type="SIMPLE", forward_label="GIS.StormwaterOutfallIDDEInspections", backward_label="GIS.StormStructure", message_direction="NONE", cardinality="ONE_TO_MANY", attributed="NONE", origin_primary_key="UID", origin_foreign_key="UID")