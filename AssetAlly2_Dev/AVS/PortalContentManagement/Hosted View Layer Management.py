import arcgis
from arcgis.gis import GIS
from arcgis.gis import ViewManager, ViewLayerDefParameter
from arcgis.features import FeatureLayerCollection

print(arcgis.__version__)

gis = GIS("Pro")
gis

# Returns all utility views of the specified category.
#  - add ability to the query for wildcards and exclusions (e.g. not containing "TEST")

view_items = gis.content.search(query='title:"Street Trees View", type:"Feature Service"', categories=["/Categories/AssetAlly/Utilities/Street"])
view_items

for view_item in view_items:
    # Only process the official view
    if view_item.title == "Village of AssetAlly Street Trees View":
        # Store view layer definition in variables
        title = view_item.title
        layer = view_item.view_manager.get_definitions(view_item)[0].layer
        query_definition = view_item.view_manager.get_definitions(view_item)[0].query_definition
        fields = view_item.view_manager.get_definitions(view_item)[0].fields
    
    
    # DELETE EXISTING VIEWS
    #view_item.delete()
    
    # PUBLISH


# Returns the utility base for the specified category.

layer_items = gis.content.search(query='title:"Street Utility", type:"Feature Service"', categories=["/Categories/AssetAlly/Utilities/Street"])
layer_items

# Create the view layer from the updated feature layer collection

for layer_item in layer_items:
    # store specified layer item in a feature layer collection
    flc = FeatureLayerCollection.fromitem(layer_item)
    # Create the view from the feature layer collection
    #  - This will have the same title after some testing, I just haven't deleted the view at this point 
    flc.manager.create_view(name=title+' TEST')

# Returns the created view item

new_view_items = gis.content.search(query='title:"TEST", type:"Feature Service"', categories=["/Categories/AssetAlly/Utilities/Street"])
new_view_items

# Update the new view layer via its View Manager
#  Ref: https://developers.arcgis.com/python/api-reference/arcgis.gis.toc.html#arcgis.gis.ViewManager

#x Class test
#class Defs:
#    def __init__(self, l, d, f):
#        self.layer = l
#        self.query_definition = d
#        self.fields = f

# Prepare the ViewLayerDefParameter object
#  Ref: https://developers.arcgis.com/python/api-reference/arcgis.gis.toc.html#arcgis.gis._impl._dataclasses.ViewLayerDefParameter
update_dict = ViewLayerDefParameter(layer=layer, query_definition=query_definition, spatial_filter=None, fields=fields)

for new_view_item in new_view_items:
    # The update(layer_def) method consumes the list of ViewLayerDefParameter
    new_view_item.view_manager.update(layer_def=[update_dict])


