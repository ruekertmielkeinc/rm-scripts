from arcgis.gis import GIS
from arcgis.mapping import WebMap


class KeyNotFoundException(Exception):
    def __init__(self, key):
        super().__init__(f"Key '{key}' not found in the dictionary")

def find_value(dictionary, key_to_find):
    for key, value in dictionary.items():
        if key == key_to_find:
            return value
    raise KeyNotFoundException(key_to_find)


# connect to your GIS
gis = GIS('home')

webmaps = [
    "Village of AssetAlly Tree Maintenance",
    "Village of AssetAlly Water Valve Inspections",
    "Village of AssetAlly Water Hydrant Inspections",
    "Village of AssetAlly Sanitary Manhole Inspections",
    "Village of AssetAlly Stormwater Outfall IDDE Inspections",
    "Village of AssetAlly Stormwater BMP Inspections"
]

wm_list = []
for wm in webmaps:
    webmap_search = gis.content.search(query="title:'{}'".format(wm), item_type="Web Map")
    webmap_search = [wm for wm in webmap_search if "(test)" not in wm.title]
    wm_list.append(WebMap(webmap_search[0]))

for wm in wm_list:
    print(wm.item.title)
    
layer_popup_titles = {
    "Water Structures":"{StructureType}: {StructureNo}",
    "Curb Stop Valves":"{ServiceType} Curb Stop: {ServiceNo}",
    "Water Fittings":"{FittingType}",
    "Water Valves":"{ValveDiameter}\" {ValveType}: {ValveNo}",
    "Water Hydrants":"{HydNo}",
    "Water Pipes":"{Diameter}\" {PipeType}: {PipeID}",
    "Water Distribution Basins":"Basin: {BasinName}",
    "Water Meters":"{ServiceType} Meter: {SerialNumber}",
    "Abandoned Water Structures":"Abandoned {StructureType}: {StructureNo} ({YearAbandoned})",
    "Abandoned Curb Stop Valves":"Abandoned Curb Stop: {ServiceNo} ({YearAbandoned})",
    "Abandoned Water Fittings":"Abandoned {FittingType} ({YearAbandoned})",
    "Abandoned Water Valves":"Abandoned Valve: {ValveNo} ({YearAbandoned})",
    "Abandoned Water Hydrants":"Abandoned Hydrant: {HydNo} ({YearAbandoned})",
    "Abandoned Water Pipes":"Abandoned {PipeType}: {PipeID} ({YearAbandoned})",
    "Trees":"{Diameter}\" {CommonName}",
    "Street Poles":"",
    "Sanitary Structures":"{StructureType}: {StructureNo}",
    "Sanitary Manholes":"{StructureType}: {StructureNo}",
    "Sanitary Pipes":"{Diameter}\" {PipeType}: {PipeID}",
    "Sanitary Basins":"Basin: {Basin}",
    "Abandoned Sanitary Structures":"Abandoned {StructureType}: {StructureNo} ({YearAbandoned})",
    "Abandoned Sanitary Pipes":"Abandoned {PipeType}: {PipeID} ({YearAbandoned})",
    "Stormwater Structures": "{StructureType}: {StructureID}",
    "Stormwater Outfalls":"{OutfallType} {StructureType}: {StructureID}",
    "Stormwater Pipes":"{PipeType}: {PipeID}",
    "Stormwater Devices":"{PondType}: {PondID}",
    "Abandoned Stormwater Structures":"Abandoned {StructureType}: {StructureID} ({YearAbandoned})",
    "Abandoned Stormwater Pipes":"Abandoned {PipeType}: {PipeID} ({YearAbandoned})"
}

table_popup_titles = {
    "Water Hydrant Inspections":"{InspectionDate}: {InspectedBy}",
    "Water Meter Cross Connection Inspections":"{InspectionDate}: {InspectedBy}",
    "Water Valve Inspections":"{InspectionDate}: {InspectedBy}",
    "Tree Maintenance":"{MaintDate}: {MaintBy}",
    "Street Pole Inspections":"",
    "Sanitary Structure Inspections":"{InspDate}: {InspBy}",
    "Sanitary Pipe Cleaning":"",
    "Sanitary Pipe Cleaning Segments":"",
    "Stormwater Device Inspections":"{InspectionDate}: {InspectorNames}",
    "Stormwater Outfall IDDE Inspections":"{InspectionDate}: {Investigators}"
}

# Merge dictionaries
popup_titles = layer_popup_titles | table_popup_titles

fields_to_hide = [
    "OBJECTID",
    "UID",
    "GlobalID",
    "Source",
    "SourceDate",
    "ProjectNumber",
    "Contractor",
    "Asset_Condition",
    "Asset_Risk",
    "Asset_ConsequenceOfFailure",
    "Asset_ProbabilityOfFailure",
    "UserID",
    "EditDate",
    "created_user",
    "created_date",
    "last_edited_user",
    "last_edited_date",
    "Shape"
]

for wm in wm_list:
    print(wm.item.title)

    wm_layer = [l for l in wm.layers if "Other" not in l.title] # Keep only the operational layer, group layer needs separate breakout
    #wm_grouplayer = [l for l in wm.layers if "Other" in l.title]
    wm_table = [t for t in wm.tables]
    wm_toc = wm_layer + wm_table # Merge lists

    for l in wm_toc:
        print("\tLayer/Table: " + l.title)
        try:
            for kk,vv in l.items():
                key_to_search = "popupInfo"
                result = find_value(l, key_to_search)
                if kk == key_to_search:
                    wm_popup_title = vv["title"]
                    for k,v in popup_titles.items():
                        if k == l.title:
                            auth_popup_title = str(v)
                            if wm_popup_title == auth_popup_title:
                                print("\t\t" + " Popup title: PASS\n")
                            else:
                                print("\t\t" + " Popup title: FAIL")
                                print("\t\t\tWeb map: " + wm_popup_title)
                                print("\t\t\tTemplate: " + auth_popup_title + "\n")
        except KeyNotFoundException as e:
            print(e)