import arcpy

aprx = arcpy.mp.ArcGISProject(aprx_path="I:\AssetAlly_ArcGISProProjects\AssetAlly\Water\Water.aprx")
m = aprx.listMaps()[0]

featureSD = m.getWebLayerSharingDraft(server_type="FEDERATED_SERVER", service_type="FEATURE", service_name="Village of AssetAlly Water Valve Inspections")
mapimageSD = m.getWebLayerSharingDraft(server_type="FEDERATED_SERVER", service_type="MAP_IMAGE", service_name="Village of AssetAlly Water Valve Inspections")

# Set properties here for the sharing drafts
sharingDrafts = [featureSD, mapimageSD]
for sd in sharingDrafts:
    if sd == featureSD:
        sd.summary = "Feature service for performing water valve inspections (e.g. exercising) and editing water valve attributes."
    elif sd == mapimageSD:
        sd.summary = "Map image service to accompany the feature service for performing water valve inspections (e.g. exercising) and editing water valve attributes."
    sd.tags = "AssetAlly, Water, Valves, Inspections"
    sd.description = ""
    sd.credits = "Ruekert & Mielke, 2023"

   # Export sharing draft to service definition draft
featureSD.exportToSDDraft(out_sddraft)
mapimageSD.exportToSDDraft(out_sddraft) 

arcpy.server.StageService(in_service_definition_draft, out_service_definition)
arcpy.server.UploadServiceDefinition(in_sd_file, in_server)

# Vector Tile package
arcpy.management.CreateVectorTilePackage(in_map, output_file, service_type)
arcpy.management.SharePackage(in_package, username, password)
arcpy.server.ReplaceWebLayer(target_layer, archive_layer_name, update_layer)