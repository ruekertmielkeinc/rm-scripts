import arcpy

clientName = "Demo2"

workspace = "I:\DatabaseConnections\sa User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.sa@gissql.ruekert-mielke.com.sde"
arcpy.DisconnectUser(sde_workspace=workspace, users="ALL")

workspace = "I:\DatabaseConnections\GIS User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.GIS@gissql.ruekert-mielke.com.sde"
arcpy.env.workspace = workspace
fd = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.Storm"
fc = fd + "\\" + clientName + "_AssetAlly_SDE.GIS.StormPond"

arcpy.management.AlterField(in_table=fc, field="YearAbandoned", new_field_alias="Year Abandoned")
arcpy.management.AlterField(in_table=fc, field="Source", new_field_alias="Source Date")
arcpy.management.AlterField(in_table=fc, field="LocationDescription", new_field_alias="Location Description")
arcpy.management.AlterField(in_table=fc, field="PondID", new_field_alias="Device ID")
arcpy.management.AlterField(in_table=fc, field="PondName", new_field_alias="Device Name")
arcpy.management.AlterField(in_table=fc, field="Subdivision", new_field_alias="Subdivision Name")
arcpy.management.AlterField(in_table=fc, field="PondType", new_field_alias="Device Type")
arcpy.management.AlterField(in_table=fc, field="Capacity", new_field_alias="Capacity (gal.)")
arcpy.management.AlterField(in_table=fc, field="OverflowElevation", new_field_alias="Overflow Elevation (ft.)")
arcpy.management.AlterField(in_table=fc, field="Year100Elevation", new_field_alias="100 Year Elevation (ft.)")
arcpy.management.AlterField(in_table=fc, field="YearConstructed", new_field_alias="Year of Construction")
arcpy.management.AlterField(in_table=fc, field="WaterQuality", new_field_alias="Water Quality Device?")
