import arcpy

clientName = "Demo2"

workspace = "I:\DatabaseConnections\sa User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.sa@gissql.ruekert-mielke.com.sde"
arcpy.DisconnectUser(sde_workspace=workspace, users="ALL")

workspace = "I:\DatabaseConnections\GIS User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.GIS@gissql.ruekert-mielke.com.sde"
arcpy.env.workspace = workspace
fd = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.Water"
fc = fd + "\\" + clientName + "_AssetAlly_SDE.GIS.Hydrant"

arcpy.management.AlterField(in_table=fc, field="YearAbandoned", new_field_alias="Year Abandoned")
arcpy.management.AlterField(in_table=fc, field="SourceDate", new_field_alias="Source Date")
arcpy.management.AlterField(in_table=fc, field="Address", new_field_alias="Address Number")
arcpy.management.AlterField(in_table=fc, field="StreetName", new_field_alias="Street Name")
arcpy.management.AlterField(in_table=fc, field="CrossStreetName", new_field_alias="Cross Street Name")
arcpy.management.AlterField(in_table=fc, field="LocationDescription", new_field_alias="Location Description")
arcpy.management.AlterField(in_table=fc, field="HydNo", new_field_alias="Hydrant ID")
arcpy.management.AlterField(in_table=fc, field="BodyColor", new_field_alias="Body Color")
arcpy.management.AlterField(in_table=fc, field="CapColor", new_field_alias="Cap Color")
arcpy.management.AlterField(in_table=fc, field="Depth", new_field_alias="Depth (ft.)")
arcpy.management.AlterField(in_table=fc, field="NozzleElevation", new_field_alias="Nozzle Elevation (ft.)")
arcpy.management.AlterField(in_table=fc, field="TopNutElevation", new_field_alias="Top Nut Elevation (ft.)")
arcpy.management.AlterField(in_table=fc, field="YearManufactured", new_field_alias="Year Manufactured")
arcpy.management.AlterField(in_table=fc, field="YearInstalled", new_field_alias="Year Installed")
arcpy.management.AlterField(in_table=fc, field="SampleStation", new_field_alias="Sample Station")
arcpy.management.AlterField(in_table=fc, field="Storz", new_field_alias="Storz?")
arcpy.management.AlterField(in_table=fc, field="ValveNo", new_field_alias="Hydrant Valve ID")
arcpy.management.AlterField(in_table=fc, field="GPSDate", new_field_alias="Date of GPS Shot")
arcpy.management.AlterField(in_table=fc, field="DateFlushed", new_field_alias="Latest Date Flushed")
arcpy.management.AlterField(in_table=fc, field="DatePainted", new_field_alias="Latest Date Painted")
arcpy.management.AlterField(in_table=fc, field="DateWinterized", new_field_alias="Latest Date Winterized")
arcpy.management.AlterField(in_table=fc, field="LeadSize", new_field_alias="Hydrant Lead Size")
arcpy.management.AlterField(in_table=fc, field="NozzlesPresent", new_field_alias="Nozzles Present?")
arcpy.management.AlterField(in_table=fc, field="TracerBox", new_field_alias="Tracer Box?")
arcpy.management.AlterField(in_table=fc, field="HydrantPlugged", new_field_alias="Hydrant Plugged?")
arcpy.management.AlterField(in_table=fc, field="ModeledFireFlow", new_field_alias="Modeled Fire Flow (gpm)")
arcpy.management.AlterField(in_table=fc, field="OilReservoir", new_field_alias="Oil Reservoir?")
arcpy.management.AlterField(in_table=fc, field="DateOilFilled", new_field_alias="Latest Date Oil Filled")
arcpy.management.AlterField(in_table=fc, field="GPM", new_field_alias="Flow Rate (gpm)")
arcpy.management.AlterField(in_table=fc, field="PSI", new_field_alias="Residual Pressure (psi)")
arcpy.management.AlterField(in_table=fc, field="PressureZone", new_field_alias="Pressure Zone")
arcpy.management.AlterField(in_table=fc, field="StaticPressure", new_field_alias="Statis Pressure (psi)")
arcpy.management.AlterField(in_table=fc, field="PumperConnection", new_field_alias="Pumper Connection")
arcpy.management.AlterField(in_table=fc, field="DateFlowTested", new_field_alias="Latest Date Flow Tested")
arcpy.management.AlterField(in_table=fc, field="HydrantFlagged", new_field_alias="Hydrant Flagged?")
arcpy.management.AlterField(in_table=fc, field="DateFlagged", new_field_alias="Latest Date Flagged")