import arcpy

clientName = "Demo2"

workspace = "I:\DatabaseConnections\sa User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.sa@gissql.ruekert-mielke.com.sde"
arcpy.DisconnectUser(sde_workspace=workspace, users="ALL")

workspace = "I:\DatabaseConnections\GIS User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.GIS@gissql.ruekert-mielke.com.sde"
arcpy.env.workspace = workspace
fd = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.Storm"
fc = fd + "\\" + clientName + "_AssetAlly_SDE.GIS.StormStructure"

arcpy.management.AlterField(in_table=fc, field="YearAbandoned", new_field_alias="Year Abandoned")
arcpy.management.AlterField(in_table=fc, field="Source", new_field_alias="Source Date")
arcpy.management.AlterField(in_table=fc, field="Address", new_field_alias="Address Number")
arcpy.management.AlterField(in_table=fc, field="StreetName", new_field_alias="Street Name")
arcpy.management.AlterField(in_table=fc, field="CrossStreetName", new_field_alias="Cross Street Name")
arcpy.management.AlterField(in_table=fc, field="LocationDescription", new_field_alias="Location Description")
arcpy.management.AlterField(in_table=fc, field="StructureID", new_field_alias="Structure ID")
arcpy.management.AlterField(in_table=fc, field="StructureType", new_field_alias="Structure Type")
arcpy.management.AlterField(in_table=fc, field="OutfallType", new_field_alias="Outfall Type")
arcpy.management.AlterField(in_table=fc, field="BasinID", new_field_alias="Basin ID")
arcpy.management.AlterField(in_table=fc, field="StructureSize", new_field_alias="Structure Size (in. or in. x in.)")
arcpy.management.AlterField(in_table=fc, field="Cover", new_field_alias="Cover Type")
arcpy.management.AlterField(in_table=fc, field="Frame", new_field_alias="Frame Type")
arcpy.management.AlterField(in_table=fc, field="RimElevation", new_field_alias="Rim Elevation (ft.)")
arcpy.management.AlterField(in_table=fc, field="Depth", new_field_alias="Depth (ft.)")
arcpy.management.AlterField(in_table=fc, field="Priority", new_field_alias="Priority?")
arcpy.management.AlterField(in_table=fc, field="Sump", new_field_alias="Sump?")
arcpy.management.AlterField(in_table=fc, field="YearCleaned", new_field_alias="Last Year Cleaned")
arcpy.management.AlterField(in_table=fc, field="YearInstalled", new_field_alias="Year Installed")
arcpy.management.AlterField(in_table=fc, field="IDDE", new_field_alias="IDDE Program")
