import arcpy

clientName = "Demo2"

workspace = "I:\DatabaseConnections\sa User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.sa@gissql.ruekert-mielke.com.sde"
arcpy.DisconnectUser(sde_workspace=workspace, users="ALL")

workspace = "I:\DatabaseConnections\GIS User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.GIS@gissql.ruekert-mielke.com.sde"
arcpy.env.workspace = workspace
fd = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.Sanitary"
fc = fd + "\\" + clientName + "_AssetAlly_SDE.GIS.SanPipe"

arcpy.management.AlterField(in_table=fc, field="YearAbandoned", new_field_alias="Year Abandoned")
arcpy.management.AlterField(in_table=fc, field="SourceDate", new_field_alias="Source Date")
arcpy.management.AlterField(in_table=fc, field="Address", new_field_alias="Address Number")
arcpy.management.AlterField(in_table=fc, field="StreetName", new_field_alias="Street Name")
arcpy.management.AlterField(in_table=fc, field="CrossStreetName", new_field_alias="Cross Street Name")
arcpy.management.AlterField(in_table=fc, field="LocationDescription", new_field_alias="Location Description")
arcpy.management.AlterField(in_table=fc, field="PipeID", new_field_alias="Pipe ID")
arcpy.management.AlterField(in_table=fc, field="PipeType", new_field_alias="Pipe Type")
arcpy.management.AlterField(in_table=fc, field="Diameter", new_field_alias="Diameter (in.)")
arcpy.management.AlterField(in_table=fc, field="UpMH", new_field_alias="Upstream Structure ID")
arcpy.management.AlterField(in_table=fc, field="UpInv", new_field_alias="Upstream Invert Elevtation (ft.)")
arcpy.management.AlterField(in_table=fc, field="UpDir", new_field_alias="Upstream Direction")
arcpy.management.AlterField(in_table=fc, field="DnMH", new_field_alias="Downstream Structure ID")
arcpy.management.AlterField(in_table=fc, field="DnInv", new_field_alias="Downstream Invert Elevation (ft.)")
arcpy.management.AlterField(in_table=fc, field="DnDir", new_field_alias="Downstream Direction")
arcpy.management.AlterField(in_table=fc, field="DnOD", new_field_alias="Downstream Outside Drop?")
arcpy.management.AlterField(in_table=fc, field="DnODInv", new_field_alias="Outside Drop Invert Elevation (ft.)")
arcpy.management.AlterField(in_table=fc, field="YearInstalled", new_field_alias="Year Installed")
arcpy.management.AlterField(in_table=fc, field="AsBuiltLength", new_field_alias="As-Built Length (ft.)")
arcpy.management.AlterField(in_table=fc, field="YearTelevised", new_field_alias="Latest Year Televised")
arcpy.management.AlterField(in_table=fc, field="TelevisingCycle", new_field_alias="Televising Frequency")
arcpy.management.AlterField(in_table=fc, field="Problem", new_field_alias="Problem?")
arcpy.management.AlterField(in_table=fc, field="YearCleaned", new_field_alias="Latest Year Cleaned")
arcpy.management.AlterField(in_table=fc, field="CleaningCycle", new_field_alias="Cleaning Frequency")
arcpy.management.AlterField(in_table=fc, field="YearLined", new_field_alias="Year Lined")
arcpy.management.AlterField(in_table=fc, field="YearGrouted", new_field_alias="Latest Year Grouted")
arcpy.management.AlterField(in_table=fc, field="YearRootFoamed", new_field_alias="Latest Year Root Foamed")
arcpy.management.AlterField(in_table=fc, field="PipeDepth", new_field_alias="Riser Depth (ft.)")
arcpy.management.AlterField(in_table=fc, field="RiserPipe", new_field_alias="Riser Pipe?")
arcpy.management.AlterField(in_table=fc, field="RiserPipeHeight", new_field_alias="Riser Pipe Height (ft.)")
arcpy.management.AlterField(in_table=fc, field="SharedRiser", new_field_alias="Shared Riser?")
arcpy.management.AlterField(in_table=fc, field="SharedWith", new_field_alias="Riser Shared With")
