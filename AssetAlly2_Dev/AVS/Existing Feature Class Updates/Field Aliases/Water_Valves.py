import arcpy

clientName = "Demo2"

workspace = "I:\DatabaseConnections\sa User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.sa@gissql.ruekert-mielke.com.sde"
arcpy.DisconnectUser(sde_workspace=workspace, users="ALL")

workspace = "I:\DatabaseConnections\GIS User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.GIS@gissql.ruekert-mielke.com.sde"
arcpy.env.workspace = workspace
fd = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.Water"
fc = fd + "\\" + clientName + "_AssetAlly_SDE.GIS.Valve"

arcpy.management.AlterField(in_table=fc, field="YearAbandoned", new_field_alias="Year Abandoned")
arcpy.management.AlterField(in_table=fc, field="SourceDate", new_field_alias="Source Date")
arcpy.management.AlterField(in_table=fc, field="Address", new_field_alias="Address Number")
arcpy.management.AlterField(in_table=fc, field="StreetName", new_field_alias="Street Name")
arcpy.management.AlterField(in_table=fc, field="CrossStreetName", new_field_alias="Cross Street Name")
arcpy.management.AlterField(in_table=fc, field="LocationDescription", new_field_alias="Location Description")
arcpy.management.AlterField(in_table=fc, field="ValveNo", new_field_alias="Valve ID")
arcpy.management.AlterField(in_table=fc, field="ValveDiameter", new_field_alias="Diameter (in.)")
arcpy.management.AlterField(in_table=fc, field="ValveType", new_field_alias="Valve Type")
arcpy.management.AlterField(in_table=fc, field="ValveUsage", new_field_alias="Valve Function")
arcpy.management.AlterField(in_table=fc, field="CloseDirection", new_field_alias="Direction to Close")
arcpy.management.AlterField(in_table=fc, field="Turns", new_field_alias="Number of Turns to Open/Close")
arcpy.management.AlterField(in_table=fc, field="FinalTorque", new_field_alias="Final Torque (N m)")
arcpy.management.AlterField(in_table=fc, field="Structure", new_field_alias="Containing Structure")
arcpy.management.AlterField(in_table=fc, field="Depth", new_field_alias="Depth (ft.)")
arcpy.management.AlterField(in_table=fc, field="YearInstalled", new_field_alias="Year Installed")
arcpy.management.AlterField(in_table=fc, field="TracerWire", new_field_alias="Tracer Wire?")
arcpy.management.AlterField(in_table=fc, field="YearExercised", new_field_alias="Latest Year Exercised")
arcpy.management.AlterField(in_table=fc, field="NormalPosition", new_field_alias="Normal Position")
arcpy.management.AlterField(in_table=fc, field="PressureZone", new_field_alias="Pressure Zone")
