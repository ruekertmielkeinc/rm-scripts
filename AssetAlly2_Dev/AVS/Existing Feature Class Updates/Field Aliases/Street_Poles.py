import arcpy

clientName = "Demo2"

workspace = "I:\DatabaseConnections\sa User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.sa@gissql.ruekert-mielke.com.sde"
arcpy.DisconnectUser(sde_workspace=workspace, users="ALL")

workspace = "I:\DatabaseConnections\GIS User Connections\AssetAlly\\" + clientName + "_AssetAlly_SDE.GIS@gissql.ruekert-mielke.com.sde"
arcpy.env.workspace = workspace
fd = workspace + "\\" + clientName + "_AssetAlly_SDE.GIS.StreetsHighways"
fc = fd + "\\" + clientName + "_AssetAlly_SDE.GIS.StreetPoles"

arcpy.management.AlterField(in_table=fc, field="YearAbandoned", new_field_alias="Year Abandoned")
arcpy.management.AlterField(in_table=fc, field="SourceDate", new_field_alias="Source Date")
arcpy.management.AlterField(in_table=fc, field="Address", new_field_alias="Address Number")
arcpy.management.AlterField(in_table=fc, field="StreetName", new_field_alias="Street Name")
arcpy.management.AlterField(in_table=fc, field="CrossStreetName", new_field_alias="Cross Street Name")
arcpy.management.AlterField(in_table=fc, field="LocationDescription", new_field_alias="Location Description")
arcpy.management.AlterField(in_table=fc, field="PoleID", new_field_alias="")
arcpy.management.AlterField(in_table=fc, field="SideOfStreet", new_field_alias="")
arcpy.management.AlterField(in_table=fc, field="PoleType", new_field_alias="")
arcpy.management.AlterField(in_table=fc, field="PoleHeight", new_field_alias="")
arcpy.management.AlterField(in_table=fc, field="PoleMaterial", new_field_alias="")
arcpy.management.AlterField(in_table=fc, field="PoleCondition", new_field_alias="")
arcpy.management.AlterField(in_table=fc, field="Breakaway", new_field_alias="")
arcpy.management.AlterField(in_table=fc, field="PoleDateInstalled", new_field_alias="")
arcpy.management.AlterField(in_table=fc, field="PoleBent", new_field_alias="")
arcpy.management.AlterField(in_table=fc, field="PoleRusted", new_field_alias="")
arcpy.management.AlterField(in_table=fc, field="PoleSplit", new_field_alias="")
arcpy.management.AlterField(in_table=fc, field="NextMaintYear", new_field_alias="")
arcpy.management.AlterField(in_table=fc, field="Replace", new_field_alias="")
arcpy.management.AlterField(in_table=fc, field="ReplacePoleReason", new_field_alias="")
