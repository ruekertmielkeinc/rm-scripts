SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- SANPIPE
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnUpdate_SanPipe') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnUpdate_SanPipe]
	END
GO

CREATE TRIGGER [GIS].[OnUpdate_SanPipe]
   ON  [GIS].[SANPIPE]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.SANPIPE)

	UPDATE SANPIPE
	SET [UID] = ISNULL([UID], @MaxID + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[RiserPipe] = ISNULL([RiserPipe], 0),
		[DnOD] = ISNULL([DnOD], 0),
		[Problem] = ISNULL([Problem], 0)
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE SANPIPE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[SANPIPE] ENABLE TRIGGER [OnUpdate_SanPipe]
GO

-- SANSTRUCTURE
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnUpdate_SanStructure') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnUpdate_SanStructure]
	END
GO

CREATE TRIGGER [GIS].[OnUpdate_SanStructure]
   ON  [GIS].[SANSTRUCTURE]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.SANSTRUCTURE)

	UPDATE SANSTRUCTURE
	SET [UID] = ISNULL([UID], @MaxID + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[SprayLined] = ISNULL([SprayLined], 0),
		[LidVentHoles] = ISNULL([LidVentHoles], 0),
		[PickHolesConcealed] = ISNULL([PickHolesConcealed], 0),
		[GasketSeal] = ISNULL([GasketSeal], 0),
		[BoltDownLid] = ISNULL([BoltDownLid], 0),
		[SealPresent] = ISNULL([SealPresent], 0),
		[InternalSealOnChimney] = ISNULL([InternalSealOnChimney], 0),
		[InternalSealOnFrame] = ISNULL([InternalSealOnFrame], 0),
		[ExternalSealOnChimney] = ISNULL([ExternalSealOnChimney], 0),
		[ExternalSealOnFrame] = ISNULL([ExternalSealOnFrame], 0),
		[PipeOpenings] = ISNULL([PipeOpenings], 0),
		[BenchPresent] = ISNULL([BenchPresent], 0)
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE SANSTRUCTURE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[SANSTRUCTURE] ENABLE TRIGGER [OnUpdate_SanStructure]
GO

-- STORMPIPE
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnUpdate_StormPipe') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnUpdate_StormPipe]
	END
GO

CREATE TRIGGER [GIS].[OnUpdate_StormPipe]
   ON  [GIS].[STORMPIPE]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.STORMPIPE)
	
	UPDATE STORMPIPE
	SET [UID] = ISNULL([UID], @MaxID + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[Problem] = ISNULL([Problem], 0),
		[Corrugated] = ISNULL([Corrugated], 0),
		[Perforated] = ISNULL([Perforated], 0)
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE STORMPIPE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[STORMPIPE] ENABLE TRIGGER [OnUpdate_StormPipe]
GO

-- STORMPOND
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnUpdate_StormPond') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnUpdate_StormPond]
	END
GO

CREATE TRIGGER [GIS].[OnUpdate_StormPond]
   ON  [GIS].[STORMPOND]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.STORMPOND)

	UPDATE STORMPOND
	SET [UID] = ISNULL([UID], @MaxID + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[WaterQuality] = ISNULL([WaterQuality], 0)
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE STORMPOND.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[STORMPOND] ENABLE TRIGGER [OnUpdate_StormPond]
GO

-- STORMSTRUCTURE
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnUpdate_StormStructure') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnUpdate_StormStructure]
	END
GO

CREATE TRIGGER [GIS].[OnUpdate_StormStructure]
   ON  [GIS].[STORMSTRUCTURE]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.STORMSTRUCTURE)

	UPDATE STORMSTRUCTURE
	SET [UID] = ISNULL([UID], @MaxID + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[Sump] = ISNULL([Sump], 0)
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE STORMSTRUCTURE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[STORMSTRUCTURE] ENABLE TRIGGER [OnUpdate_StormStructure]
GO

-- CURBSTOP
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnUpdate_CurbStop') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnUpdate_CurbStop]
	END
GO

CREATE TRIGGER [GIS].[OnUpdate_CurbStop]
   ON  [GIS].[CURBSTOP]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.CURBSTOP)
	
	UPDATE CURBSTOP
	SET [UID] = ISNULL([UID], @MaxID + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency')
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE CURBSTOP.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[CURBSTOP] ENABLE TRIGGER [OnUpdate_CurbStop]
GO

-- HYDRANT
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnUpdate_Hydrant') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnUpdate_Hydrant]
	END
GO

CREATE TRIGGER [GIS].[OnUpdate_Hydrant]
   ON  [GIS].[HYDRANT]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.HYDRANT)
	
	UPDATE HYDRANT
	SET [UID] = ISNULL([UID], @MaxID + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[Storz] = ISNULL([Storz], 0),
		[TracerBox] = ISNULL([TracerBox], 0),
		[OilReservoir] = ISNULL([OilReservoir], 0),
		[HydrantPlugged] = ISNULL([HydrantPlugged], 0),
		[HydrantFlagged] = ISNULL([HydrantFlagged], 0)
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE HYDRANT.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[HYDRANT] ENABLE TRIGGER [OnUpdate_Hydrant]
GO

-- VALVE
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnUpdate_Valve') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnUpdate_Valve]
	END
GO

CREATE TRIGGER [GIS].[OnUpdate_Valve]
   ON  [GIS].[VALVE]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.VALVE)

	UPDATE VALVE
	SET [UID] = ISNULL([UID], @MaxID + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[TracerWire] = ISNULL([TracerWire], 0),
		[NormalPosition] = ISNULL([NormalPosition], 'Open')
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE VALVE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[VALVE] ENABLE TRIGGER [OnUpdate_Valve]
GO

-- WATERFITTING
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnUpdate_WaterFitting') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnUpdate_WaterFitting]
	END
GO

CREATE TRIGGER [GIS].[OnUpdate_WaterFitting]
   ON  [GIS].[WATERFITTING]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.WATERFITTING)

	UPDATE WATERFITTING
	SET [UID] = ISNULL([UID], @MaxID + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency')
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE WATERFITTING.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[WATERFITTING] ENABLE TRIGGER [OnUpdate_WaterFitting]
GO

-- WATERMETER
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnUpdate_WaterMeter') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnUpdate_WaterMeter]
	END
GO

CREATE TRIGGER [GIS].[OnUpdate_WaterMeter]
   ON  [GIS].[WATERMETER]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxUID Int = (SELECT MAX(UID) FROM GIS.WATERMETER)
	DECLARE @MaxPointID Int = (SELECT MAX(PointID) FROM GIS.WATERMETER)

	UPDATE WATERMETER
	SET [UID] = ISNULL([UID], @MaxUID + i.RowNumber),
		[PointID] = ISNULL([PointID], @MaxPointID + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[PrivateWell] = ISNULL([PrivateWell], 0),
		[SumpPump] = ISNULL([SumpPump], 0),
		[SumpDischarge] = ISNULL([SumpDischarge], 0),
		[LeaksInSystem] = ISNULL([LeaksInSystem], 0),
		[HoseBipBFP] = ISNULL([HoseBipBFP], 0),
		[BPValve] = ISNULL([BPValve], 0),
		[ValveLocked] = ISNULL([ValveLocked], 0),
		[FireServiceMetered] = ISNULL([FireServiceMetered], 0),
		[CrossConnection] = ISNULL([CrossConnection], 0),
		[PropertyType] = ISNULL([PropertyType], 'Residential'),
		[ServiceType] = ISNULL([ServiceType], 'Add')
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE WATERMETER.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[WATERMETER] ENABLE TRIGGER [OnUpdate_WaterMeter]
GO

-- WATERPIPE
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnUpdate_WaterPipe') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnUpdate_WaterPipe]
	END
GO

CREATE TRIGGER [GIS].[OnUpdate_WaterPipe]
   ON  [GIS].[WATERPIPE]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.WATERPIPE)

	UPDATE WATERPIPE
	SET [UID] = ISNULL([UID], @MaxID + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency')
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE WATERPIPE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[WATERPIPE] ENABLE TRIGGER [OnUpdate_WaterPipe]
GO

-- WATERSTRUCTURE
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnUpdate_WaterStructure') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnUpdate_WaterStructure]
	END
GO

CREATE TRIGGER [GIS].[OnUpdate_WaterStructure]
   ON  [GIS].[WATERSTRUCTURE]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.WATERSTRUCTURE)

	UPDATE WATERSTRUCTURE
	SET [UID] = ISNULL([UID], @MaxID + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency')
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE WATERSTRUCTURE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[WATERSTRUCTURE] ENABLE TRIGGER [OnUpdate_WaterStructure]
GO

-- STEETPOLES
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnUpdate_StreetPoles') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnUpdate_StreetPoles]
	END
GO

CREATE TRIGGER [GIS].[OnUpdate_StreetPoles]
   ON  [GIS].[STREETPOLES]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.STREETPOLES)

	UPDATE STREETPOLES
	SET [UID] = ISNULL([UID], @MaxID + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[PoleBent] = ISNULL([PoleBent], 0),
		[PoleRusted] = ISNULL([PoleRusted], 0),
		[PoleSplit] = ISNULL([PoleSplit], 0),
		[Replace] = ISNULL([Replace], 0),
		[Breakaway] = ISNULL([Breakaway], 0)
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE STREETPOLES.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[STREETPOLES] ENABLE TRIGGER [OnUpdate_StreetPoles]
GO

-- TREES
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnUpdate_Trees') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnUpdate_Trees]
	END
GO

CREATE TRIGGER [GIS].[OnUpdate_Trees]
   ON  [GIS].[TREES]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.TREES)

	UPDATE TREES
	SET [UID] = ISNULL([UID], @MaxID + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[Species] = ISNULL([Species], 'Vacant'),
		[Diameter] = ISNULL([Diameter], 0),
		[GSSize] = ISNULL([GSSize], 'Open'),
		[StClear] = ISNULL([StClear], 'OK'),
		[UtilPres] = ISNULL([UtilPres], 'None'),
		[TreeProb] = ISNULL([TreeProb], 0),
		[GypsyMoth] = ISNULL([GypsyMoth], 0),
		[MemorialTree] = ISNULL([MemorialTree], 0),
		[TreeLocation] = ISNULL([TreeLocation], 'Street'),
		[BS] = ISNULL([BS], 0),
		[BW] = ISNULL([BW], 0),
		[CC] = ISNULL([CC], 0),
		[CD] = ISNULL([CD], 0),
		[CR] = ISNULL([CR], 0),
		[CT] = ISNULL([CT], 0),
		[C_D] = ISNULL([C_D], 0),
		[DFP] = ISNULL([DFP], 0),
		[DL] = ISNULL([DL], 0), 
		[DP] = ISNULL([DP], 0),
		[DW] = ISNULL([DW], 0),
		[HG] = ISNULL([HG], 0),
		[IA] = ISNULL([IA], 0),
		[IB] = ISNULL([IB], 0),
		[IM] = ISNULL([IM], 0),
		[IP] = ISNULL([IP], 0),
		[LB] = ISNULL([LB], 0),
		[LC] = ISNULL([LC], 0),
		[LLD] = ISNULL([LLD], 0),
		[LML] = ISNULL([LML], 0),
		[LN] = ISNULL([LN], 0),
		[LS] = ISNULL([LS], 0),
		[MD] = ISNULL([MD], 0),
		[MS] = ISNULL([MS], 0),
		[ND] = ISNULL([ND], 0),
		[OG] = ISNULL([OG], 0),
		[OS] = ISNULL([OS], 0),
		[PL] = ISNULL([PL], 0),
		[PP] = ISNULL([PP], 0),
		[PS] = ISNULL([PS], 0),
		[RD] = ISNULL([RD], 0),
		[RP] = ISNULL([RP], 0),
		[RR] = ISNULL([RR], 0),
		[SP] = ISNULL([SP], 0),
		[TP] = ISNULL([TP], 0),
		[TS] = ISNULL([TS], 0),
		[TW] = ISNULL([TW], 0),
		[UP] = ISNULL([UP], 0),
		[WA] = ISNULL([WA], 0),
		[WG] = ISNULL([WG], 0)
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE TREES.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[TREES] ENABLE TRIGGER [OnUpdate_Trees]
GO