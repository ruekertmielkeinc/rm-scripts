USE WebAppCommonData
GO

SELECT W.Issue
FROM dbo.luWorkOrderItems 
AS W 
INNER JOIN dbo.luUtilityAssets 
AS U 
ON W.AssetID=U.AssetID 
WHERE U.Asset='Hydrant';