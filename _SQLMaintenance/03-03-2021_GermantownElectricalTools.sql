SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- GIS table and views

USE Germantown_AssetAlly_GIS

CREATE TABLE [dbo].[ElectricalCable](
	[Description] [varchar](2000) NULL,
	[EditDate] [datetime] NULL,
	[UserID] [nvarchar](50) NULL,
	[CableUID] [int] IDENTITY(1,1) NOT NULL,
	[ConduitUID] [int] NULL,
	[JacketColor] [nvarchar](50) NULL,
	[JacketMaterial] [nvarchar](50) NULL,
	[AWG] [tinyint] NULL,
	[CableType] [varchar](20) NULL,
 CONSTRAINT [PK_ElectricalCable] PRIMARY KEY CLUSTERED 
(
	[CableUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE VIEW [dbo].[vwElectricalConduitPerStructure]
AS
SELECT	C.ConduitID, C.UID AS ConduitUID, C.UpStructureID, C.UpDirection, C.DownStructureID, C.DownDirection, S.UID AS StructureUID
FROM    Germantown_AssetAlly_SDE.GIS.ELECTRICALCONDUIT C LEFT OUTER JOIN
        Germantown_AssetAlly_SDE.GIS.ELECTRICALSTRUCTURE AS S ON C.UpStructureID = S.StructureID
UNION ALL
SELECT  C.ConduitID, C.UID AS ConduitUID, C.UpStructureID, C.UpDirection, C.DownStructureID, C.DownDirection, S.UID AS StructureUID
FROM    Germantown_AssetAlly_SDE.GIS.ELECTRICALCONDUIT C LEFT OUTER JOIN
        Germantown_AssetAlly_SDE.GIS.ELECTRICALSTRUCTURE AS S ON C.DownStructureID = S.StructureID
GO

CREATE VIEW [dbo].[vwElectricalStructurePerConduit]
AS
SELECT C.ConduitID, C.UID AS UID, C.UpStructureID, U.UID AS UpStructureUID, C.UpDirection, C.DownStructureID, D.UID AS DownStructureUID, C.DownDirection
FROM Germantown_AssetAlly_SDE.GIS.ELECTRICALCONDUIT AS C 
LEFT OUTER JOIN Germantown_AssetAlly_SDE.GIS.ELECTRICALSTRUCTURE AS U ON C.UpStructureID = U.StructureID
LEFT OUTER JOIN Germantown_AssetAlly_SDE.GIS.ELECTRICALSTRUCTURE AS D ON C.DownStructureID = D.StructureID
GO

-- SDE spatial view

USE Germantown_AssetAlly_SDE

CREATE VIEW [dbo].[svw_ElectricalCable]
AS
SELECT		CON.OBJECTID, CON.UID AS ConduitUID, CON.SHAPE, CAB.CableUID, CAB.CableType, CAB.Description
FROM        GIS.ELECTRICALCONDUIT AS CON INNER JOIN
            Germantown_AssetAlly_GIS.dbo.ElectricalCable AS CAB ON CON.UID = CAB.ConduitUID
GO

-- Conduit Triggers

CREATE TRIGGER [GIS].[OnInsert_ElectricalConduit] 
   ON  [GIS].[ELECTRICALCONDUIT]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.ELECTRICALCONDUIT)

	UPDATE ELECTRICALCONDUIT
		SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
			[EditDate] = GETDATE(),
			[Status] = ISNULL([Status], 'Active'),
			[Owner] = ISNULL([Owner], 'Our Agency'),
		FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
		WHERE ELECTRICALCONDUIT.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[ELECTRICALCONDUIT] ENABLE TRIGGER [OnInsert_ElectricalConduit]
GO

CREATE TRIGGER [GIS].[OnUpdate_ElectricalConduit]
   ON  [GIS].[ELECTRICALCONDUIT]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.ELECTRICALCONDUIT)

	UPDATE ELECTRICALCONDUIT
	SET [UID] = ISNULL([UID], @MaxID + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency')
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE ELECTRICALCONDUIT.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[ELECTRICALCONDUIT] ENABLE TRIGGER [OnUpdate_ElectricalConduit]
GO

-- Structure Triggers

CREATE TRIGGER [GIS].[OnInsert_ElectricalStructure] 
   ON  [GIS].[ELECTRICALSTRUCTURE]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.ELECTRICALSTRUCTURE)

	UPDATE ELECTRICALSTRUCTURE
		SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
			[EditDate] = GETDATE(),
			[Status] = ISNULL([Status], 'Active'), 
			[Owner] = ISNULL([Owner], 'Our Agency')
		FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
		WHERE ELECTRICALSTRUCTURE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[ELECTRICALSTRUCTURE] ENABLE TRIGGER [OnInsert_ElectricalStructure]
GO

CREATE TRIGGER [GIS].[OnUpdate_ElectricalStructure]
   ON  [GIS].[ELECTRICALSTRUCTURE]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.ELECTRICALSTRUCTURE)

	UPDATE ELECTRICALSTRUCTURE
	SET [UID] = ISNULL([UID], @MaxID + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency')
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE ELECTRICALSTRUCTURE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[ELECTRICALSTRUCTURE] ENABLE TRIGGER [OnUpdate_ElectricalStructure]
GO