WITH FlowTest (FTSerialNum, LatestFlowTestDate) AS (SELECT SerialNum AS FTSerialNum, MAX(TestDate) AS LatestFlowTestDate 
				FROM dbo.WaterMeterFlowTest GROUP BY SerialNum)
SELECT      I.AutoID, I.SerialNum, I.Status, I.Manufacturer, I.Model, I.PurchaseDate AS ManufactureDate, I.TestDate, I.Comments, 
			I.LowGPM, I.MedGPM, I.HighGPM, I.LowPerc, I.MedPerc, I.HighPerc, ISNULL(D.PropertyAddress, N'N/A') AS PropertyAddress, 
			ISNULL(I.MeterSize, 0) AS MeterSize, ISNULL(WMS.SizeDesc, N'N/A') AS SizeDesc, D.PropertyType, D.InstallDate, D.UID, D.PointID, 
			LatestFlowTestDate
FROM        dbo.WaterMeterInventory AS I LEFT OUTER JOIN
            WebAppCommonData.dbo.WaterMeterSize AS WMS ON CONVERT(nvarchar(5), I.MeterSize) = WMS.MeterSize LEFT OUTER JOIN
            BrownDeer_AssetAlly_SDE.GIS.WATERMETER AS D ON I.SerialNum = D.SerialNumber LEFT OUTER JOIN
			FlowTest AS F ON FTSerialNum = I.SerialNum