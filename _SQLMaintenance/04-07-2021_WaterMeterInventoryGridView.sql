/****** Object:  View [dbo].[vwWaterMeterInventoryGV]    Script Date: 4/7/2021 10:59:17 AM ******/
DROP VIEW [dbo].[vwWaterMeterInventoryGV]
GO

/****** Object:  View [dbo].[vwWaterMeterInventoryGV]    Script Date: 4/7/2021 10:59:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vwWaterMeterInventoryGV]
AS

-- 03-26-2021 AVS Fixes issue where water meters without size don't show up in the inventory as "In Stock"
SELECT	I.AutoID, I.SerialNum, I.Status, I.Manufacturer, I.Model, I.PurchaseDate AS ManufactureDate, I.TestDate, I.Comments,
		I.LowGPM, I.MedGPM, I.HighGPM, I.LowPerc, I.MedPerc, I.HighPerc, ISNULL(D.PropertyAddress, N'N/A') AS PropertyAddress, 
		ISNULL(I.MeterSize, 0) AS MeterSize, ISNULL(WMS.SizeDesc, N'N/A') AS SizeDesc, D.PropertyType, D.InstallDate, D.UID, D.PointID
FROM    dbo.WaterMeterInventory AS I LEFT OUTER JOIN
WebAppCommonData.dbo.WaterMeterSize AS WMS ON CONVERT(nvarchar(5), I.MeterSize) = WMS.MeterSize LEFT OUTER JOIN
[WhitefishBay_AssetAlly_SDE].GIS.WATERMETER AS D ON I.SerialNum = D.SerialNumber

GO


