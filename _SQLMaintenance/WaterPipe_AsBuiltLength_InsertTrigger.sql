/****** Object:  Trigger [OnInsert_WaterPipe]    Script Date: 7/7/2021 8:03:08 AM ******/
DROP TRIGGER [GIS].[OnInsert_WaterPipe]
GO

/****** Object:  Trigger [GIS].[OnInsert_WaterPipe]    Script Date: 7/7/2021 8:03:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [GIS].[OnInsert_WaterPipe] 
   ON  [GIS].[WATERPIPE]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.WATERPIPE)

	UPDATE WATERPIPE
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[AsbuiltLength] = ISNULL([AsbuiltLength], ROUND([Shape].STLength(), 2))
		FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
		WHERE WATERPIPE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[WATERPIPE] ENABLE TRIGGER [OnInsert_WaterPipe]
GO


