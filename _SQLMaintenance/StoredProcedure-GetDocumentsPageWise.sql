USE [$(SDE_DBNAME)]
GO

/****** Object:  StoredProcedure [dbo].[GetDocumentsPageWise]    Script Date: 3/29/2021 3:56:10 PM ******/
DROP PROCEDURE [dbo].[GetDocumentsPageWise]
GO

/****** Object:  StoredProcedure [dbo].[GetDocumentsPageWise]    Script Date: 3/29/2021 3:56:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetDocumentsPageWise]
      @PageIndex INT = 1
      ,@PageSize INT = 10
      ,@PageCount INT OUTPUT
AS
BEGIN
      SET NOCOUNT ON;
      SELECT ROW_NUMBER() OVER
            (
                  ORDER BY [OBJECTID] ASC
            )AS RowNumber
      ,[OBJECTID]
      ,[DocumentType]
      ,[Purpose]
      ,[FileName]
      ,[FilePath]
      ,[DocumentDate]
      ,[DocumentNo]
      ,[DocAddress]
	  ,[Contractor]
	  ,[Project]
	  ,[Notes]
	  ,'https://' + 'columbus' + '.assetally.com/documents/' + FilePath AS FileURL
    INTO #Results
      FROM [GIS].[Documents]
     
      DECLARE @RecordCount INT
      SELECT @RecordCount = COUNT(*) FROM #Results
 
      SET @PageCount = CEILING(CAST(@RecordCount AS DECIMAL(10, 2)) / CAST(@PageSize AS DECIMAL(10, 2)))
      PRINT       @PageCount
           
      SELECT * FROM #Results
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
     
      DROP TABLE #Results
END
GO


