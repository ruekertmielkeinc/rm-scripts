USE [Mukwonago_AssetAlly_GIS]
GO
/****** Object:  UserDefinedFunction [dbo].[StrCatSanInverts]    Script Date: 6/29/2021 10:26:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Sanitary Inverts
ALTER FUNCTION [dbo].[StrCatSanInverts] (@MHNo NVARCHAR(50), @Status VARCHAR(50)) 
RETURNS NVARCHAR(1000)

BEGIN
	-- Declare the return variable here
	DECLARE @ResultVar VARCHAR(1000)

	-- Add the T-SQL statements to compute the return value here
	SELECT @ResultVar = COALESCE(@ResultVar + CHAR(13), '') + LTRIM(ISNULL(CAST(UpInv AS DECIMAL(6,2)), 0)) + ' ' + 
						LTRIM(ISNULL(CAST(Diameter AS DECIMAL(2,0)), 0)) + CHAR(34) + ' ' + ISNULL(UpDir, '')
	FROM [Mukwonago_AssetAlly_SDE].GIS.SANPIPE
	WHERE (UpMH=@MHNo AND [Status]=@Status)
	ORDER BY UpInv DESC
	
	SELECT @ResultVar = COALESCE(@ResultVar + CHAR(13), '') + LTRIM(ISNULL(CAST(DnInv AS DECIMAL(6,2)), 0)) + ' ' + 
						LTRIM(ISNULL(CAST(Diameter AS DECIMAL(2,0)), 0)) + CHAR(34) + ' ' + ISNULL(DnDir, '') + ' ' + 
						CASE WHEN DnOD = 1 THEN 'OD: ' + LTRIM(ISNULL(CAST(DnODInv AS DECIMAL(6,2)), 0)) ELSE '' END
	FROM [Mukwonago_AssetAlly_SDE].GIS.SANPIPE
	WHERE (DnMH=@MHNo AND [Status]=@Status)
	ORDER BY DnInv DESC
	
	-- Return the result of the function
	RETURN @ResultVar

END
