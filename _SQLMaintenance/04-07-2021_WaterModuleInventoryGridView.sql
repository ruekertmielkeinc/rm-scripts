USE [Testing_AssetAlly_GIS]
GO

/****** Object:  View [dbo].[vwWaterMeterModuleInventoryGV]    Script Date: 4/7/2021 12:00:30 PM ******/
DROP VIEW [dbo].[vwWaterMeterModuleInventoryGV]
GO

/****** Object:  View [dbo].[vwWaterMeterModuleInventoryGV]    Script Date: 4/7/2021 12:00:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vwWaterMeterModuleInventoryGV]
AS

SELECT	I.ModuleNo, I.Status, I.ModuleType, I.Model, ISNULL(I.IntendedSize, 0) AS ModuleSize, ISNULL(S.SizeDesc, N'N/A') AS SizeDesc, 
		I.HighLow, I.TestCircleCode, I.Resolution, I.AutoID, D.ARInstallDate, D.PropertyAddress
FROM dbo.WaterModuleInventory AS I LEFT OUTER JOIN
	[Testing_AssetAlly_SDE].GIS.WATERMETER AS D ON I.ModuleNo = D.ARSerial LEFT OUTER JOIN
	WebAppCommonData.dbo.WaterMeterSize AS S ON I.IntendedSize = S.MeterSize

GO


