USE Verona_AssetAlly_SDE
GO

UPDATE GIS.SANPIPE
SET AsBuiltLength = Shape.STLength()
GO

UPDATE GIS.STORMPIPE
SET AsbuiltLength = Shape.STLength()
GO

UPDATE GIS.WATERPIPE
SET AsbuiltLength = Shape.STLength()
GO
