/****** Object:  Trigger [OnInsert_StormPipe]    Script Date: 7/7/2021 8:02:57 AM ******/
DROP TRIGGER [GIS].[OnInsert_StormPipe]
GO

/****** Object:  Trigger [GIS].[OnInsert_StormPipe]    Script Date: 7/7/2021 8:02:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [GIS].[OnInsert_StormPipe] 
   ON  [GIS].[STORMPIPE]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.STORMPIPE)

	UPDATE STORMPIPE
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber), 
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[AsbuiltLength] = ISNULL([AsbuiltLength], ROUND([Shape].STLength(), 2)),
		[Problem] = ISNULL([Problem], 0),
		[Corrugated] = ISNULL([Corrugated], 0),
		[Perforated] = ISNULL([Perforated], 0)
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE STORMPIPE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[STORMPIPE] ENABLE TRIGGER [OnInsert_StormPipe]
GO


