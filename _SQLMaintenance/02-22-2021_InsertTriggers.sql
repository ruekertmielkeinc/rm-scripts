SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- BASEMENTBACKUP
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_BasementBackup') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_BasementBackup]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_BasementBackup] 
   ON  [GIS].[BASEMENTBACKUP]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.BASEMENTBACKUP)

	UPDATE BASEMENTBACKUP
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[PointID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE()
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE BASEMENTBACKUP.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[BASEMENTBACKUP] ENABLE TRIGGER [OnInsert_BasementBackup]
GO

-- CLEARWATERCOMPLIANCE
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_ClearwaterCompliance') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_ClearwaterCompliance]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_ClearwaterCompliance] 
   ON  [GIS].[CLEARWATERCOMPLIANCE]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.CLEARWATERCOMPLIANCE)

	UPDATE CLEARWATERCOMPLIANCE
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE()
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE CLEARWATERCOMPLIANCE.OBJECTID = i.OBJECTID

END
GO

ALTER TABLE [GIS].[CLEARWATERCOMPLIANCE] ENABLE TRIGGER [OnInsert_ClearwaterCompliance]
GO

-- CURBSTOP
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_CurbStop') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_CurbStop]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_CurbStop] 
   ON  [GIS].[CURBSTOP]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.CURBSTOP)

	UPDATE CURBSTOP
	SET [UID] = (ISNULL(@MaxID,0) + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency')
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE CURBSTOP.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[CURBSTOP] ENABLE TRIGGER [OnInsert_CurbStop]
GO

-- DOCUMENTLOCATIONS
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_DocumentLocations') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_DocumentLocations]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_DocumentLocations] 
   ON  [GIS].[DOCUMENTLOCATIONS]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.DOCUMENTLOCATIONS)

	UPDATE DOCUMENTLOCATIONS
	SET [UID] = ISNULL(@MaxID, 0) + i.RowNumber,
		[EditDate] = GETDATE()
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE DOCUMENTLOCATIONS.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[DOCUMENTLOCATIONS] ENABLE TRIGGER [OnInsert_DocumentLocations]
GO

-- EROSIONCONTROLSITES
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_ErosionControlSites') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_ErosionControlSites]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_ErosionControlSites] 
   ON  [GIS].[EROSIONCONTROLSITES]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.EROSIONCONTROLSITES)

	UPDATE EROSIONCONTROLSITES
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE()
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE EROSIONCONTROLSITES.OBJECTID = i.OBJECTID

END
GO

ALTER TABLE [GIS].[EROSIONCONTROLSITES] ENABLE TRIGGER [OnInsert_ErosionControlSites]
GO

-- HYDRANT
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_Hydrant') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_Hydrant]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_Hydrant] 
   ON  [GIS].[HYDRANT]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.HYDRANT)

	UPDATE HYDRANT
		SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
			[EditDate] = GETDATE(),
			[Status] = ISNULL([Status], 'Active'),
			[Owner] = ISNULL([Owner], 'Our Agency'),
			[HydrantFlagged] = ISNULL([HydrantFlagged], 0)
		FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
		WHERE HYDRANT.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[HYDRANT] ENABLE TRIGGER [OnInsert_Hydrant]
GO

-- INSTREAMSAMPLE
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_InStreamSample') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_InStreamSample]
	END
GO
CREATE TRIGGER [GIS].[OnInsert_InStreamSample] 
   ON  [GIS].[INSTREAMSAMPLE]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.INSTREAMSAMPLE)

	UPDATE INSTREAMSAMPLE
		SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
			[EditDate] = GETDATE()
		FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
		WHERE INSTREAMSAMPLE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[INSTREAMSAMPLE] ENABLE TRIGGER [OnInsert_InStreamSample]
GO

-- PLOWROUTES
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_PlowRoutes') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_PlowRoutes]
	END
GO
CREATE TRIGGER [GIS].[OnInsert_PlowRoutes] 
   ON  [GIS].[PLOWROUTES]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.PLOWROUTES)

	UPDATE PLOWROUTES
		SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
			[EditDate] = GETDATE()
		FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
		WHERE PLOWROUTES.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[PLOWROUTES] ENABLE TRIGGER [OnInsert_PlowRoutes]
GO

-- PRIVATEWELL
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_PrivateWell') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_PrivateWell]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_PrivateWell] 
   ON  [GIS].[PRIVATEWELL]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.PRIVATEWELL)

	UPDATE PRIVATEWELL
		SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
			[EditDate] = GETDATE(),
			[Status] = ISNULL([Status], 'Active'),
			[Owner] = ISNULL([Owner], 'Private')
		FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
		WHERE PRIVATEWELL.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[PRIVATEWELL] ENABLE TRIGGER [OnInsert_PrivateWell]
GO

-- SANBASIN
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_SanBasin') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_SanBasin]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_SanBasin] 
   ON  [GIS].[SANBASIN]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.SANBASIN)

	UPDATE GIS.SANBASIN
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE()
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE SANBASIN.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[SANBASIN] ENABLE TRIGGER [OnInsert_SanBasin]
GO

-- SANPIPE
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_SanPipe') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_SanPipe]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_SanPipe] 
   ON  [GIS].[SANPIPE]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.SANPIPE)
	
	UPDATE SANPIPE
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[RiserPipe] = ISNULL([RiserPipe], 0),
		[DnOD] = ISNULL([DnOD], 0),
		[Problem] = ISNULL([Problem], 0)
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE SANPIPE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[SANPIPE] ENABLE TRIGGER [OnInsert_SanPipe]
GO

-- SANSTRUCTURE
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_SanStructure') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_SanStructure]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_SanStructure] 
   ON  [GIS].[SANSTRUCTURE]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.SANSTRUCTURE)

	UPDATE SANSTRUCTURE
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[SprayLined] = ISNULL([SprayLined], 0),
		[LidVentHoles] = ISNULL([LidVentHoles], 0),
		[PickHolesConcealed] = ISNULL([PickHolesConcealed], 0),
		[GasketSeal] = ISNULL([GasketSeal], 0),
		[BoltDownLid] = ISNULL([BoltDownLid], 0),
		[SealPresent] = ISNULL([SealPresent], 0),
		[InternalSealOnChimney] = ISNULL([InternalSealOnChimney], 0),
		[InternalSealOnFrame] = ISNULL([InternalSealOnFrame], 0),
		[ExternalSealOnChimney] = ISNULL([ExternalSealOnChimney], 0),
		[ExternalSealOnFrame] = ISNULL([ExternalSealOnFrame], 0),
		[PipeOpenings] = ISNULL([PipeOpenings], 0),
		[BenchPresent] = ISNULL([BenchPresent], 0)
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE SANSTRUCTURE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[SANSTRUCTURE] ENABLE TRIGGER [OnInsert_SanStructure]
GO

-- SPOTREPAIRS
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_SpotRepairs') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_SpotRepairs]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_SpotRepairs] 
   ON  [GIS].[SPOTREPAIRS]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.SPOTREPAIRS)

	UPDATE SPOTREPAIRS
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE(),
		[EntirePipe] = ISNULL([EntirePipe], 0)
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE SPOTREPAIRS.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[SPOTREPAIRS] ENABLE TRIGGER [OnInsert_SpotRepairs]
GO

-- STORMPIPE
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_StormPipe') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_StormPipe]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_StormPipe] 
   ON  [GIS].[STORMPIPE]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.STORMPIPE)

	UPDATE STORMPIPE
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber), 
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[Problem] = ISNULL([Problem], 0),
		[Corrugated] = ISNULL([Corrugated], 0),
		[Perforated] = ISNULL([Perforated], 0)
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE STORMPIPE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[STORMPIPE] ENABLE TRIGGER [OnInsert_StormPipe]
GO

-- STORMPOND
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_StormPond') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_StormPond]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_StormPond] 
   ON  [GIS].[STORMPOND]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.STORMPOND)

	UPDATE STORMPOND
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[WaterQuality] = ISNULL([WaterQuality], 0)
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE STORMPOND.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[STORMPOND] ENABLE TRIGGER [OnInsert_StormPond]
GO

-- STORMSTRUCTURE
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_StormStructure') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_StormStructure]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_StormStructure] 
   ON  [GIS].[STORMSTRUCTURE]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.STORMSTRUCTURE)

	UPDATE STORMSTRUCTURE
		SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
			[EditDate] = GETDATE(),
			[Status] = ISNULL([Status], 'Active'),
			[Owner] = ISNULL([Owner], 'Our Agency'),
			[Sump] = ISNULL([Sump], 0)
		FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
		WHERE STORMSTRUCTURE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[STORMSTRUCTURE] ENABLE TRIGGER [OnInsert_StormStructure]
GO

-- STREETPOLES
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_StreetPoles') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_StreetPoles]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_StreetPoles] 
   ON  [GIS].[STREETPOLES]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = ISNULL((SELECT MAX(UID) FROM GIS.STREETPOLES), 0)

	UPDATE STREETPOLES
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[PoleBent] = ISNULL([PoleBent], 0),
		[PoleRusted] = ISNULL([PoleRusted], 0),
		[PoleSplit] = ISNULL([PoleSplit], 0),
		[Replace] = ISNULL([Replace], 0),
		[Breakaway] = ISNULL([Breakaway], 0)
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE STREETPOLES.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[STREETPOLES] ENABLE TRIGGER [OnInsert_StreetPoles]
GO

-- TREES
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_Trees') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_Trees]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_Trees] 
   ON  [GIS].[TREES]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = ISNULL((SELECT MAX(UID) FROM GIS.TREES), 0)

	UPDATE TREES
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[Species] = ISNULL([Species], 'Vacant'),
		[Diameter] = ISNULL([Diameter], 0),
		[GSSize] = ISNULL([GSSize], 'Open'),
		[StClear] = ISNULL([StClear], 'OK'),
		[UtilPres] = ISNULL([UtilPres], 'None'),
		[TreeProb] = ISNULL([TreeProb], 0),
		[GypsyMoth] = ISNULL([GypsyMoth], 0),
		[MemorialTree] = ISNULL([MemorialTree], 0),
		[TreeLocation] = ISNULL([TreeLocation], 'Street'),
		[BS] = ISNULL([BS], 0),
		[BW] = ISNULL([BW], 0),
		[CC] = ISNULL([CC], 0),
		[CD] = ISNULL([CD], 0),
		[CR] = ISNULL([CR], 0),
		[CT] = ISNULL([CT], 0),
		[C_D] = ISNULL([C_D], 0),
		[DFP] = ISNULL([DFP], 0),
		[DL] = ISNULL([DL], 0), 
		[DP] = ISNULL([DP], 0),
		[DW] = ISNULL([DW], 0),
		[HG] = ISNULL([HG], 0),
		[IA] = ISNULL([IA], 0),
		[IB] = ISNULL([IB], 0),
		[IM] = ISNULL([IM], 0),
		[IP] = ISNULL([IP], 0),
		[LB] = ISNULL([LB], 0),
		[LC] = ISNULL([LC], 0),
		[LLD] = ISNULL([LLD], 0),
		[LML] = ISNULL([LML], 0),
		[LN] = ISNULL([LN], 0),
		[LS] = ISNULL([LS], 0),
		[MD] = ISNULL([MD], 0),
		[MS] = ISNULL([MS], 0),
		[ND] = ISNULL([ND], 0),
		[OG] = ISNULL([OG], 0),
		[OS] = ISNULL([OS], 0),
		[PL] = ISNULL([PL], 0),
		[PP] = ISNULL([PP], 0),
		[PS] = ISNULL([PS], 0),
		[RD] = ISNULL([RD], 0),
		[RP] = ISNULL([RP], 0),
		[RR] = ISNULL([RR], 0),
		[SP] = ISNULL([SP], 0),
		[TP] = ISNULL([TP], 0),
		[TS] = ISNULL([TS], 0),
		[TW] = ISNULL([TW], 0),
		[UP] = ISNULL([UP], 0),
		[WA] = ISNULL([WA], 0),
		[WG] = ISNULL([WG], 0)
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE TREES.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[TREES] ENABLE TRIGGER [OnInsert_Trees]
GO

-- VALVE
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_Valve') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_Valve]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_Valve] 
   ON  [GIS].[VALVE]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.VALVE)

	UPDATE VALVE
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[TracerWire] = ISNULL([TracerWire], 0),
		[NormalPosition] = ISNULL([NormalPosition], 'Open')
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE VALVE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[VALVE] ENABLE TRIGGER [OnInsert_Valve]
GO

-- WATERBASIN
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_WaterBasin') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_WaterBasin]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_WaterBasin] 
   ON  [GIS].[WATERBASIN]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.WATERBASIN)

	UPDATE GIS.WATERBASIN
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE()
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE WATERBASIN.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[WATERBASIN] ENABLE TRIGGER [OnInsert_WaterBasin]
GO

-- WATERFITTING
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_WaterFitting') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_WaterFitting]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_WaterFitting] 
   ON  [GIS].[WATERFITTING]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.WATERFITTING)

	UPDATE WATERFITTING
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency')
		FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
		WHERE WATERFITTING.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[WATERFITTING] ENABLE TRIGGER [OnInsert_WaterFitting]
GO

-- WATERMETER
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_WaterMeter') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_WaterMeter]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_WaterMeter] 
   ON  [GIS].[WATERMETER] 
   for insert
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxUID Int = (SELECT MAX(UID) FROM GIS.WATERMETER)
	DECLARE @MaxPointID Int = (SELECT MAX(PointID) FROM GIS.WATERMETER)

	UPDATE WATERMETER
	SET [UID] = (ISNULL(@MaxUID, 0) + i.RowNumber),
		[PointID] = (ISNULL(@MaxPointID, 0) + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[PropertyType] = ISNULL([PropertyType], 'Residential'),
		[ServiceType] = ISNULL([ServiceType], 'Add'),
		[PrivateWell] = ISNULL([PrivateWell], 0),
		[SumpPump] = ISNULL([SumpPump], 0),
		[SumpDischarge] = ISNULL([SumpDischarge], 0),
		[LeaksInSystem] = ISNULL([LeaksInSystem], 0),
		[HoseBipBFP] = ISNULL([HoseBipBFP], 0),
		[BPValve] = ISNULL([BPValve], 0),
		[ValveLocked] = ISNULL([ValveLocked], 0),
		[FireServiceMetered] = ISNULL([FireServiceMetered], 0),
		[CrossConnection] = ISNULL([CrossConnection], 0)
		FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
		WHERE WATERMETER.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[WATERMETER] ENABLE TRIGGER [OnInsert_WaterMeter]
GO

-- WATERPIPE
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_WaterPipe') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_WaterPipe]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_WaterPipe] 
   ON  [GIS].[WATERPIPE]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.WATERPIPE)

	UPDATE WATERPIPE
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency')
		FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
		WHERE WATERPIPE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[WATERPIPE] ENABLE TRIGGER [OnInsert_WaterPipe]
GO

-- WATERPIPEBREAK
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_WaterPipeBreak') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_WaterPipeBreak]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_WaterPipeBreak] 
   ON  [GIS].[WATERPIPEBREAK]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.WATERPIPEBREAK)

	UPDATE WATERPIPEBREAK
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE()
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE WATERPIPEBREAK.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[WATERPIPEBREAK] ENABLE TRIGGER [OnInsert_WaterPipeBreak]
GO

-- WATERSTRUCTURE
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_WaterStructure') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_WaterStructure]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_WaterStructure] 
   ON  [GIS].[WATERSTRUCTURE]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.WATERSTRUCTURE)

	UPDATE WATERSTRUCTURE
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency')
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE WATERSTRUCTURE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[WATERSTRUCTURE] ENABLE TRIGGER [OnInsert_WaterStructure]
GO

-- ZONING
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'GIS.OnInsert_Zoning') 
    AND xtype IN (N'TR')
)
	BEGIN
	    DROP TRIGGER [GIS].[OnInsert_Zoning]
	END
GO

CREATE TRIGGER [GIS].[OnInsert_Zoning] 
   ON  [GIS].[ZONING]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.ZONING)

	UPDATE GIS.ZONING
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE()
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE ZONING.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[ZONING] ENABLE TRIGGER [OnInsert_Zoning]
GO