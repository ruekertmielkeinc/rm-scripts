/****** Object:  Trigger [OnInsert_SanPipe]    Script Date: 7/6/2021 12:08:42 PM ******/
DROP TRIGGER [GIS].[OnInsert_SanPipe]
GO

/****** Object:  Trigger [GIS].[OnInsert_SanPipe]    Script Date: 7/6/2021 12:08:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [GIS].[OnInsert_SanPipe] 
   ON  [GIS].[SANPIPE]
   FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MaxID Int = (SELECT MAX(UID) FROM GIS.SANPIPE)
	
	UPDATE SANPIPE
	SET [UID] = (ISNULL(@MaxID, 0) + i.RowNumber),
		[EditDate] = GETDATE(),
		[Status] = ISNULL([Status], 'Active'),
		[Owner] = ISNULL([Owner], 'Our Agency'),
		[AsBuiltLength] = ISNULL([AsBuiltLength], ROUND([Shape].STLength(), 2)),
		[RiserPipe] = ISNULL([RiserPipe], 0),
		[DnOD] = ISNULL([DnOD], 0),
		[Problem] = ISNULL([Problem], 0)
	FROM (SELECT OBJECTID, (ROW_NUMBER() OVER(ORDER BY OBJECTID)) AS RowNumber FROM inserted) i
	WHERE SANPIPE.OBJECTID = i.OBJECTID
END
GO

ALTER TABLE [GIS].[SANPIPE] ENABLE TRIGGER [OnInsert_SanPipe]
GO


