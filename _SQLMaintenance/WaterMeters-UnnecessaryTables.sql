EXEC sp_rename 'dbo.WaterMeter_DocumentLinks', 'xWaterMeter_DocumentLinks'
GO

DROP TABLE [dbo].[WaterMeterData]
GO

DROP TABLE [dbo].[WaterMeterManufacturer]
GO

DROP TABLE [dbo].[WaterMeterModel]
GO

DROP TABLE [dbo].[WaterMeterModuleType]
GO

DROP TABLE [dbo].[WaterMeterNewReplace]
GO

DROP TABLE [dbo].[WaterMeterPropertyType]
GO

DROP TABLE [dbo].[WaterMeterServiceType]
GO

DROP TABLE [dbo].[WaterMeterSize]
GO
