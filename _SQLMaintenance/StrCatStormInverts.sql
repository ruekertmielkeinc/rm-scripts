USE [Kewaunee_AssetAlly_GIS]
GO

/****** Object:  UserDefinedFunction [dbo].[StrCatStormInverts]    Script Date: 6/30/2021 9:18:19 AM ******/
DROP FUNCTION [dbo].[StrCatStormInverts]
GO

/****** Object:  UserDefinedFunction [dbo].[StrCatStormInverts]    Script Date: 6/30/2021 9:18:19 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Storm Inverts
CREATE FUNCTION [dbo].[StrCatStormInverts] (@StructNo NVARCHAR(50), @Status VARCHAR(50)) 
RETURNS NVARCHAR(1000)

BEGIN
	-- Declare the return variable here
	DECLARE @ResultVar VARCHAR(1000)

	-- Add the T-SQL statements to compute the return value here
	SELECT @ResultVar =	COALESCE(@ResultVar + CHAR(13),'') + LTRIM(ISNULL(CAST(UpInv AS DECIMAL(6,2)), 0)) + ' ' +
						CASE WHEN Width IS NULL OR Width = '' THEN '' ELSE CAST(Width AS VARCHAR(5)) + CHAR(34) END +
						CASE WHEN Height IS NULL OR Height = '' THEN '' ELSE ' x ' + CAST(Height AS VARCHAR(5)) + CHAR(34) END +
						' ' + LTRIM(ISNULL(UpDir, '')) 
	FROM [Kewaunee_AssetAlly_SDE].GIS.STORMPIPE
	WHERE (UpMH=@StructNo AND [Status]=@Status)
	ORDER BY UpInv DESC

	SELECT @ResultVar = COALESCE(@ResultVar + CHAR(13),'') + LTRIM(ISNULL(CAST(DnInv AS DECIMAL(6,2)), 0)) + ' ' +
						CASE WHEN Width IS NULL OR Width = '' THEN '' ELSE CAST(Width AS VARCHAR(5)) + CHAR(34) END +
						CASE WHEN Height IS NULL OR Height = '' THEN '' ELSE ' x ' + CAST(Height AS VARCHAR(5)) + CHAR(34) END +
						' ' + LTRIM(ISNULL(DnDir, ''))
	FROM [Kewaunee_AssetAlly_SDE].GIS.STORMPIPE
	WHERE (DnMH=@StructNo AND [Status]=@Status)
	ORDER BY DnInv DESC	
	
	-- Return the result of the function
	RETURN @ResultVar

END
GO


