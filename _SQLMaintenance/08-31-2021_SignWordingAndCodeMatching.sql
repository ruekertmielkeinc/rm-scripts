-- MATCH
-- Sets MUTCDCode and SignSize for signs with a Wording match to WACD.dbo.StreetSignList table

--UPDATE D
--SET D.MUTCDCode = L.MUTCDCode, D.SignSize = ' Select...'
--FROM dbo.StreetSignData D 
--INNER JOIN WebAppCommonData.dbo.StreetSignList L 
--ON D.Wording = L.Wording
--WHERE D.MUTCDCode IS NULL

-- NON-MATCH
-- 1. Sets MUTCDCode, SignSize, and SignNotes=Wording for signs where the Wording does not match an entry in the WACD.dbo.StreetSignList table

--UPDATE D
--SET D.MUTCDCode = ' Select...', D.SignSize = ' Select...', D.SignNotes = D.Wording
--FROM dbo.StreetSignData D 
--LEFT OUTER JOIN WebAppCommonData.dbo.StreetSignList L 
--ON D.Wording = L.Wording
--WHERE D.MUTCDCode IS NULL

-- 2. Sets Wording to default for non-matches

--UPDATE D
--SET  D.Wording = ' Select...'
--FROM dbo.StreetSignData D 
--LEFT OUTER JOIN WebAppCommonData.dbo.StreetSignList L 
--ON D.Wording = L.Wording
--WHERE D.MUTCDCode = ' Select...' AND D.SignSize = ' Select...'