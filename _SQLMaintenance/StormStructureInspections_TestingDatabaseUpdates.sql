USE [Testing_AssetAlly_GIS]
GO

ALTER TABLE [dbo].[StormStructureInspections] DROP CONSTRAINT [DF_StormStructureInspections_RepairMortar]
GO

ALTER TABLE [dbo].[StormStructureInspections] DROP CONSTRAINT [DF_StormStructureInspections_ReplaceBarrel]
GO

ALTER TABLE [dbo].[StormStructureInspections] DROP CONSTRAINT [DF_StormStructureInspections_ReplaceConcentric]
GO

ALTER TABLE [dbo].[StormStructureInspections] DROP CONSTRAINT [DF_StormStructureInspections_ReplaceOffset]
GO

ALTER TABLE [dbo].[StormStructureInspections] DROP CONSTRAINT [DF_StormStructureInspections_Cleaned]
GO

ALTER TABLE [dbo].[StormStructureInspections] DROP CONSTRAINT [DF_StormStructureInspections_BarrelGroutReq]
GO

ALTER TABLE [dbo].[StormStructureInspections] DROP CONSTRAINT [DF_StormStructureInspections_BarrelRepair]
GO

ALTER TABLE [dbo].[StormStructureInspections] DROP CONSTRAINT [DF_StormStructureInspections_TroughRepair]
GO

ALTER TABLE [dbo].[StormStructureInspections] DROP CONSTRAINT [DF_StormStructureInspections_TroughCleanout]
GO

ALTER TABLE [dbo].[StormStructureInspections] DROP CONSTRAINT [DF_StormStructureInspections_RepairBench]
GO

ALTER TABLE [dbo].[StormStructureInspections] DROP CONSTRAINT [DF_StormStructureInspections_ChimGroutReq]
GO

ALTER TABLE [dbo].[StormStructureInspections] DROP CONSTRAINT [DF_StormStructureInspections_ReplaceChimney]
GO

ALTER TABLE [dbo].[StormStructureInspections] DROP CONSTRAINT [DF_StormStructureInspections_ResetFrame]
GO

ALTER TABLE [dbo].[StormStructureInspections] DROP CONSTRAINT [DF_StormStructureInspections_ReplaceFrame]
GO

ALTER TABLE [dbo].[StormStructureInspections] DROP CONSTRAINT [DF_StormStructureInspections_ReplaceLid]
GO

/****** Object:  Table [dbo].[StormStructureInspections]    Script Date: 7/8/2021 12:21:37 PM ******/
DROP TABLE [dbo].[StormStructureInspections]
GO

/****** Object:  Table [dbo].[StormStructureInspections]    Script Date: 7/8/2021 12:21:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StormStructureInspections](
	[InspectionID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [int] NULL,
	[InspectedBy] [nvarchar](50) NULL,
	[MaintDate] [datetime] NULL,
	[ReplaceLid] [bit] NULL,
	[ReplaceFrame] [bit] NULL,
	[ResetFrame] [bit] NULL,
	[ChimneyMat] [nvarchar](20) NULL,
	[ReplaceChimney] [bit] NULL,
	[ChimGroutReq] [bit] NULL,
	[ChimneyHeight] [int] NULL,
	[BarrelMaterial] [nvarchar](20) NULL,
	[RepairBench] [bit] NULL,
	[TroughCleanout] [bit] NULL,
	[TroughRepair] [bit] NULL,
	[BarrelRepair] [bit] NULL,
	[BarrelGroutReq] [bit] NULL,
	[BarrelHeight] [int] NULL,
	[Notes] [nvarchar](250) NULL,
	[EditDate] [datetime] NULL,
	[UserID] [nvarchar](50) NULL,
	[Cleaned] [bit] NULL,
	[CleaningReason] [nvarchar](50) NULL,
	[ConeMaterial] [varchar](20) NULL,
	[ConeHeight] [int] NULL,
	[ReplaceOffset] [bit] NULL,
	[ReplaceConcentric] [bit] NULL,
	[ReplaceBarrel] [bit] NULL,
	[RepairMortar] [bit] NULL,
	[ReplacedChimneyHeight] [int] NULL,
	[ReplacedBarrelHeight] [int] NULL,
	[ReplaceStructure] [bit] NULL,
 CONSTRAINT [PK_StormStructureInspections] PRIMARY KEY CLUSTERED 
(
	[InspectionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[StormStructureInspections] ADD  CONSTRAINT [DF_StormStructureInspections_ReplaceLid]  DEFAULT ((0)) FOR [ReplaceLid]
GO

ALTER TABLE [dbo].[StormStructureInspections] ADD  CONSTRAINT [DF_StormStructureInspections_ReplaceFrame]  DEFAULT ((0)) FOR [ReplaceFrame]
GO

ALTER TABLE [dbo].[StormStructureInspections] ADD  CONSTRAINT [DF_StormStructureInspections_ResetFrame]  DEFAULT ((0)) FOR [ResetFrame]
GO

ALTER TABLE [dbo].[StormStructureInspections] ADD  CONSTRAINT [DF_StormStructureInspections_ReplaceChimney]  DEFAULT ((0)) FOR [ReplaceChimney]
GO

ALTER TABLE [dbo].[StormStructureInspections] ADD  CONSTRAINT [DF_StormStructureInspections_ChimGroutReq]  DEFAULT ((0)) FOR [ChimGroutReq]
GO

ALTER TABLE [dbo].[StormStructureInspections] ADD  CONSTRAINT [DF_StormStructureInspections_RepairBench]  DEFAULT ((0)) FOR [RepairBench]
GO

ALTER TABLE [dbo].[StormStructureInspections] ADD  CONSTRAINT [DF_StormStructureInspections_TroughCleanout]  DEFAULT ((0)) FOR [TroughCleanout]
GO

ALTER TABLE [dbo].[StormStructureInspections] ADD  CONSTRAINT [DF_StormStructureInspections_TroughRepair]  DEFAULT ((0)) FOR [TroughRepair]
GO

ALTER TABLE [dbo].[StormStructureInspections] ADD  CONSTRAINT [DF_StormStructureInspections_BarrelRepair]  DEFAULT ((0)) FOR [BarrelRepair]
GO

ALTER TABLE [dbo].[StormStructureInspections] ADD  CONSTRAINT [DF_StormStructureInspections_BarrelGroutReq]  DEFAULT ((0)) FOR [BarrelGroutReq]
GO

ALTER TABLE [dbo].[StormStructureInspections] ADD  CONSTRAINT [DF_StormStructureInspections_Cleaned]  DEFAULT ((0)) FOR [Cleaned]
GO

ALTER TABLE [dbo].[StormStructureInspections] ADD  CONSTRAINT [DF_StormStructureInspections_ReplaceOffset]  DEFAULT ((0)) FOR [ReplaceOffset]
GO

ALTER TABLE [dbo].[StormStructureInspections] ADD  CONSTRAINT [DF_StormStructureInspections_ReplaceConcentric]  DEFAULT ((0)) FOR [ReplaceConcentric]
GO

ALTER TABLE [dbo].[StormStructureInspections] ADD  CONSTRAINT [DF_StormStructureInspections_ReplaceBarrel]  DEFAULT ((0)) FOR [ReplaceBarrel]
GO

ALTER TABLE [dbo].[StormStructureInspections] ADD  CONSTRAINT [DF_StormStructureInspections_RepairMortar]  DEFAULT ((0)) FOR [RepairMortar]
GO


