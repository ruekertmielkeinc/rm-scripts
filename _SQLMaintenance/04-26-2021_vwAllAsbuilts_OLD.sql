USE [LakePewaukeeSD_AssetAlly_GIS]
GO

/****** Object:  View [dbo].[vw_AllAsbuilts]    Script Date: 4/26/2021 10:18:17 AM ******/
DROP VIEW [dbo].[vw_AllAsbuilts]
GO

/****** Object:  View [dbo].[vw_AllAsbuilts]    Script Date: 4/26/2021 10:18:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_AllAsbuilts]
AS
SELECT        'Hydrant' AS FeatureClass, HydrantAsBuilts.AsbuiltYear, isnull(HydrantAsBuilts.Description, 'View File') AS Description, HydrantAsBuilts.AsbuiltLink, HydrantAsBuilts.FileName, isnull(HydrantFeature.StreetName, ' ') 
                         AS StreetName, isnull(HydrantFeature.HydNo, 'Not Listed') AS FeatureNo, ('/Water/HydrantDashboard.aspx?UID=') AS Link, HydrantFeature.UID, 'True' AS DisplayImage, HydrantAsBuilts.UploadType, HydrantAsBuilts.Fee, 
                         HydrantAsBuilts.Expiration
FROM            dbo.vwAsBuiltRecords AS HydrantAsBuilts INNER JOIN
                         LakePewaukeeSD_AssetAlly_SDE.GIS.HYDRANT AS HydrantFeature ON HydrantAsBuilts.UID = HydrantFeature.UID
WHERE        (HydrantAsBuilts.FeatureClass = N'Hydrant')
UNION ALL
SELECT        'Valve' AS FeatureClass, ValveAsBuilts.AsbuiltYear, isnull(ValveAsBuilts.Description, 'View File') AS Description, ValveAsBuilts.AsbuiltLink, ValveAsBuilts.FileName, isnull(ValveFeature.StreetName, ' ') AS StreetName, 
                         isnull(ValveFeature.ValveNo, ' ') AS FeatureNo, ('/Water/ValveDashboard.aspx?UID=') AS Link, ValveFeature.UID, 'True' AS DisplayImage, ValveAsBuilts.UploadType, ValveAsBuilts.Fee, ValveAsBuilts.Expiration
FROM            dbo.vwAsBuiltRecords AS ValveAsBuilts INNER JOIN
                         LakePewaukeeSD_AssetAlly_SDE.GIS.VALVE AS ValveFeature ON ValveAsBuilts.UID = ValveFeature.UID
WHERE        (ValveAsBuilts.FeatureClass = N'Valve')
UNION ALL
SELECT        'Water Pipe' AS FeatureClass, WaterPipeAsBuilts.AsbuiltYear, isnull(WaterPipeAsBuilts.Description, 'View File') AS Description, WaterPipeAsBuilts.AsbuiltLink, WaterPipeAsBuilts.FileName, 
                         isnull(WaterPipeFeature.StreetName, ' ') AS StreetName, isnull(WaterPipeFeature.PipeID, 'Not Listed') AS FeatureNo, ('/Water/WaterMainDashboard.aspx?UID=') AS Link, WaterPipeFeature.UID, 'True' AS DisplayImage, 
                         WaterPipeAsBuilts.UploadType, WaterPipeAsBuilts.Fee, WaterPipeAsBuilts.Expiration
FROM            dbo.vwAsBuiltRecords AS WaterPipeAsBuilts INNER JOIN
                         LakePewaukeeSD_AssetAlly_SDE.GIS.WaterPipe AS WaterPipeFeature ON WaterPipeAsBuilts.UID = WaterPipeFeature.UID
WHERE        (WaterPipeAsBuilts.FeatureClass = N'WaterPipe')
UNION ALL
SELECT        'Curb Stop' AS FeatureClass, CurbStopAsBuilts.AsbuiltYear, isnull(CurbStopAsBuilts.Description, 'View File') AS Description, CurbStopAsBuilts.AsbuiltLink, CurbStopAsBuilts.FileName, isnull(CurbStopFeature.StreetName, ' ') 
                         AS StreetName, isnull(CurbStopFeature.ServiceNo, 'Not Listed') AS FeatureNo, ('/Water/CurbStopDashboard.aspx?UID=') AS Link, CurbStopFeature.UID, 'True' AS DisplayImage, CurbStopAsBuilts.UploadType, 
                         CurbStopAsBuilts.Fee, CurbStopAsBuilts.Expiration
FROM            dbo.vwAsBuiltRecords AS CurbStopAsBuilts INNER JOIN
                         LakePewaukeeSD_AssetAlly_SDE.GIS.CurbStop AS CurbStopFeature ON CurbStopAsBuilts.UID = CurbStopFeature.UID
WHERE        (CurbStopAsBuilts.FeatureClass = N'CurbStop')
UNION ALL
SELECT        'Sanitary Pipe' AS FeatureClass, SanPipeAsBuilts.AsbuiltYear, ISNULL(SanPipeAsBuilts.Description, '') AS Description, SanPipeAsBuilts.AsbuiltLink, SanPipeAsBuilts.FileName, isnull(SanPipeFeature.StreetName, ' ') AS StreetName, 
                         SanPipeFeature.PipeID AS FeatureNo, ('/Sanitary/SanPipeEditForm.aspx?UID=') AS Link, SanPipeFeature.UID, 'True' AS DisplayImage, SanPipeAsBuilts.UploadType, SanPipeAsBuilts.Fee, SanPipeAsBuilts.Expiration
FROM            dbo.vwAsBuiltRecords AS SanPipeAsBuilts INNER JOIN
                         LakePewaukeeSD_AssetAlly_SDE.GIS.SanPipe AS SanPipeFeature ON SanPipeAsBuilts.UID = SanPipeFeature.UID
WHERE        (SanPipeAsBuilts.FeatureClass = N'SanPipe')
UNION ALL
SELECT        'Sanitary Structure' AS FeatureClass, SanStructureAsBuilts.AsbuiltYear, ISNULL(SanStructureAsBuilts.Description, '') AS Description, SanStructureAsBuilts.AsbuiltLink, SanStructureAsBuilts.FileName, isnull(SanStructureFeature.StreetName, ' ') 
                         AS StreetName, SanStructureFeature.StructureNo AS FeatureNo, ('/Sanitary/SanitaryStructureDashboard.aspx?UID=') AS Link, SanStructureFeature.UID, 'True' AS DisplayImage, SanStructureAsBuilts.UploadType, 
                         SanStructureAsBuilts.Fee, SanStructureAsBuilts.Expiration
FROM            dbo.vwAsBuiltRecords AS SanStructureAsBuilts INNER JOIN
                         LakePewaukeeSD_AssetAlly_SDE.GIS.SanStructure AS SanStructureFeature ON SanStructureAsBuilts.UID = SanStructureFeature.UID
WHERE        (SanStructureAsBuilts.FeatureClass = N'SanStructure')
UNION ALL
SELECT        'Storm Pipe' AS FeatureClass, StormPipeAsBuilts.AsbuiltYear, StormPipeAsBuilts.Description, StormPipeAsBuilts.AsbuiltLink, StormPipeAsBuilts.FileName, isnull(StormPipeFeature.StreetName, ' ') AS StreetName, 
                         StormPipeFeature.PipeID AS FeatureNo, ('/Storm/StormMainEditForm.aspx?UID=') AS Link, StormPipeFeature.UID, 'True' AS DisplayImage, StormPipeAsBuilts.UploadType, StormPipeAsBuilts.Fee, 
                         StormPipeAsBuilts.Expiration
FROM            dbo.vwAsBuiltRecords AS StormPipeAsBuilts INNER JOIN
                         LakePewaukeeSD_AssetAlly_SDE.GIS.StormPipe AS StormPipeFeature ON StormPipeAsBuilts.UID = StormPipeFeature.UID
WHERE        (StormPipeAsBuilts.FeatureClass = N'StormPipe')
UNION ALL
SELECT        'Storm Structure' AS FeatureClass, StormStrucAsBuilts.AsbuiltYear, StormStrucAsBuilts.Description, StormStrucAsBuilts.AsbuiltLink, StormStrucAsBuilts.FileName, isnull(StormStrucFeature.StreetName, ' ') AS StreetName, 
                         StormStrucFeature.StructureID AS FeatureNo, ('/Storm/StormStructureForm.aspx?UID=') AS Link, StormStrucFeature.UID, 'True' AS DisplayImage, StormStrucAsBuilts.UploadType, StormStrucAsBuilts.Fee, 
                         StormStrucAsBuilts.Expiration
FROM            dbo.vwAsBuiltRecords AS StormStrucAsBuilts INNER JOIN
                         LakePewaukeeSD_AssetAlly_SDE.GIS.StormStructure AS StormStrucFeature ON StormStrucAsBuilts.UID = StormStrucFeature.UID
WHERE        (StormStrucAsBuilts.FeatureClass = N'StormStructure')
UNION ALL
SELECT        'Storm Pond' AS FeatureClass, StormPondAttachments.AsbuiltYear, StormPondAttachments.Description, StormPondAttachments.AsbuiltLink, StormPondAttachments.FileName, ' ' AS StreetName, 
                         StormPondFeature.PondID AS FeatureNo, ('/Storm/StormWaterDevice.aspx?UID=') AS Link, StormPondFeature.UID, 'True' AS DisplayImage, StormPondAttachments.UploadType, StormPondAttachments.Fee, 
                         StormPondAttachments.Expiration
FROM            dbo.vwAsBuiltRecords AS StormPondAttachments INNER JOIN
                         LakePewaukeeSD_AssetAlly_SDE.GIS.StormPond AS StormPondFeature ON StormPondAttachments.UID = StormPondFeature.UID
WHERE        (StormPondAttachments.FeatureClass = N'StormPond')
UNION ALL
SELECT        'Water Structure' AS FeatureClass, WaterStructureAttachments.AsbuiltYear, WaterStructureAttachments.Description, WaterStructureAttachments.AsbuiltLink, WaterStructureAttachments.FileName, ' ' AS StreetName, 
                         WaterStructureFeature.StructureNo AS FeatureNo, ('/Water/WaterStructureDashboard.aspx?UID=') AS Link, WaterStructureAttachments.UID, 'True' AS DisplayImage, WaterStructureAttachments.UploadType, 
                         WaterStructureAttachments.Fee, WaterStructureAttachments.Expiration
FROM            dbo.vwAsBuiltRecords AS WaterStructureAttachments INNER JOIN
                         LakePewaukeeSD_AssetAlly_SDE.GIS.WaterStructure AS WaterStructureFeature ON WaterStructureAttachments.UID = WaterStructureFeature.UID
WHERE        (WaterStructureAttachments.FeatureClass = N'WaterStructure')
UNION ALL
SELECT        'Street Pole' AS FeatureClass, StreetPoleAttachments.AsbuiltYear, StreetPoleAttachments.Description, StreetPoleAttachments.AsbuiltLink, StreetPoleAttachments.FileName, isnull(StreetPoleFeature.StreetName, ' ') 
                         AS StreetName, ' ' AS FeatureNo, ('/StreetsHighways/PoleDashboard.aspx?UID=') AS Link, StreetPoleAttachments.UID, 'True' AS DisplayImage, StreetPoleAttachments.UploadType, StreetPoleAttachments.Fee, 
                         StreetPoleAttachments.Expiration
FROM            dbo.vwAsBuiltRecords AS StreetPoleAttachments INNER JOIN
                         LakePewaukeeSD_AssetAlly_SDE.GIS.StreetPoles AS StreetPoleFeature ON StreetPoleAttachments.UID = StreetPoleFeature.UID
WHERE        (StreetPoleAttachments.FeatureClass = N'StreetPole')
GO
