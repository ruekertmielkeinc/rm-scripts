USE [Testing_AssetAlly_GIS]
GO

/****** Object:  View [dbo].[vwAsBuiltRecords]    Script Date: 5/19/2020 10:52:37 AM ******/
DROP VIEW [dbo].[vwAsBuiltRecords]
GO

/****** Object:  View [dbo].[vwAsBuiltRecords]    Script Date: 5/19/2020 10:52:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vwAsBuiltRecords]
AS
SELECT        L.FeatureClass, L.UID, L.AssetGUID, ISNULL(F.AsbuiltYear, N'N/A') AS AsbuiltYear, ISNULL(F.Description, N'') AS Description, F.UploadType, F.UploadClass, F.DocNumber, F.OrigFileName, '/' + F.UploadType + N'/' + F.FileName AS AsbuiltLink,
                          F.FileName, F.ixAsBuiltFile, L.ixAsBuiltLink, F.Expiration, F.Fee
FROM            dbo.Uploads_Files AS F INNER JOIN
                         dbo.Uploads_Links AS L ON F.ixAsBuiltFile = L.ixAsBuiltFile
GO