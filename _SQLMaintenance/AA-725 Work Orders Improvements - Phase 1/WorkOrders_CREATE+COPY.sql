USE [Testing_AssetAlly_GIS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* Create empty dbo.WorkOrders table */
CREATE TABLE [dbo].[WorkOrders](
	[UID] [int] NULL,
	[FeatureClass] [nvarchar](50) NULL,
	[TaxKey] [nvarchar](20) NULL,
	[DateCreated] [date] NULL,
	[CallerName] [nvarchar](50) NULL,
	[PropAddress] [nvarchar](100) NULL,
	[CallerPhone] [nvarchar](20) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Responsibility] [nvarchar](50) NULL,
	[Assignee] [nvarchar](50) NULL,
	[DueDate] [date] NULL,
	[Issue] [nvarchar](1000) NULL,
	[IssueDesc] [nvarchar](1000) NULL,
	[WorkDoneDesc] [nvarchar](1000) NULL,
	[Maintenance] [nvarchar](1000) NULL,
	[EditDate] [datetime] NULL,
	[UserID] [nvarchar](50) NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Utility] [varchar](15) NULL,
	[ClosedBy] [nvarchar](50) NULL,
	[DateClosed] [datetime] NULL,
	[LocationGeo] [geometry] NULL,
	[Fee] [float] NULL,
	[FeeDescription] [nvarchar](200) NULL,
	[AssetGUID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_WorkOrders] PRIMARY KEY CLUSTERED 
(
	[AssetGUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/* Set default value for GUID column */
ALTER TABLE [dbo].[WorkOrders] ADD  DEFAULT (newid()) FOR [AssetGUID]
GO

/* Copy needed data from dbo.ServiceRequest to dbo.WorkOrders */
INSERT INTO dbo.WorkOrders (UID, FeatureClass, TaxKey, DateCreated, CallerName, PropAddress, CallerPhone, CreatedBy, Responsiblity, Assignee, DueDate, Issue, IssueDesc, WorkDoneDesc, Maintenance, EditDate, UserID, ID, Utility, ClosedBy, DateClosed, LocationGeo, Fee)
SELECT UID, FeatureClass, TaxKey, CallDate, CallerName, PropAddress, CallerPhone, ReceivedBy, Responsiblity, Assignee, DueDate, Complaint, ComplaintOther, ISNULL(ActionTaken, ActionTakenOther), Maintenance, EditDate, UserID, ID, Utility, ServicedBy, CompletedDate, LocationGeo, Fee
FROM dbo.ServiceRequest
