USE [Testing_AssetAlly_GIS]
GO

/* Add AssetGUID column (drops then recreates constraints) */
ALTER TABLE [dbo].[Uploads_Links] DROP CONSTRAINT [FK_AsBuiltLinks_AsBuiltFiles]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE dbo.Uploads_Links ADD AssetGUID uniqueidentifier NULL

ALTER TABLE [dbo].[Uploads_Links]  WITH CHECK ADD  CONSTRAINT [FK_AsBuiltLinks_AsBuiltFiles] FOREIGN KEY([ixAsBuiltFile])
REFERENCES [dbo].[Uploads_Files] ([ixAsBuiltFile])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Uploads_Links] CHECK CONSTRAINT [FK_AsBuiltLinks_AsBuiltFiles]
GO

ALTER TABLE [dbo].[Uploads_Links]  WITH CHECK ADD  CONSTRAINT [FK_UploadsLinks_Asset] FOREIGN KEY([AssetGUID])
REFERENCES [dbo].[WorkOrders] ([AssetGUID])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[Uploads_Links] CHECK CONSTRAINT [FK_UploadsLinks_Asset]
GO

/* Back-fill AssetGUID for ServiceRequest */
UPDATE Uploads_Links
SET Uploads_Links.AssetGUID = WorkOrders.AssetGUID
FROM Uploads_Links INNER JOIN WorkOrders ON Uploads_Links.UID = WorkOrders.UID WHERE (Uploads_Links.FeatureClass = "ServiceRequest")

/* Re-value FeatureClass column: ServiceRequest to WorkOrder */
UPDATE Uploads_Links
SET Uploads_Links.FeatureClass = "WorkOrder"
WHERE (Uploads_Links.FeatureClass = "ServiceRequest")